<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework/yii.php';
require_once($yii);

$config=dirname(__FILE__).'/protected/config/main.php';
$config = CMap::mergeArray(
    require_once(dirname(__FILE__).'/protected/config/main.php'),
    include_once(dirname(__FILE__).'/protected/config/main.local.php')
);
date_default_timezone_set('Europe/Moscow');
require_once('constants.php');
Yii::createWebApplication($config)->run();