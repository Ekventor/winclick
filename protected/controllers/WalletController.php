<?php

class WalletController extends Controller
{
    
    /* WMI_MERCHANT_ID	    Идентификатор (номер кошелька) интернет-магазина.
    * WMI_PAYMENT_AMOUNT    Сумма заказа.
    * WMI_CURRENCY_ID	    Идентификатор валюты заказа (ISO 4217).
    * WMI_TO_USER_ID	    Двенадцатизначный номер кошелька плательщика.
    * WMI_PAYMENT_NO	    Идентификатор заказа в системе учета интернет-магазина.
    * WMI_ORDER_ID	    Идентификатор заказа в системе учета Единой кассы.
    * WMI_DESCRIPTION	    Описание заказа.
    * WMI_SUCCESS_URL
    * WMI_FAIL_URL	    Адреса (URL) страниц интернет-магазина, на которые будет отправлен покупатель после успешной или неуспешной оплаты.
    * WMI_EXPIRED_DATE	    Срок истечения оплаты в западно-европейском часовом поясе (UTC+0).
    * WMI_CREATE_DATE
    * WMI_UPDATE_DATE	    Дата создания и изменения заказа в западно-европейском часовом поясе (UTC+0).
    * WMI_ORDER_STATE	    Состояние оплаты заказа: Accepted  — заказ оплачен;
    * WMI_SIGNATURE	    Подпись уведомления об оплате, сформированная с использованием «секретного ключа» интернет-магазина.
    */
    
    public function actionResult() {
        if (empty($_POST) || !is_array($_POST)) {
            Y::end('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . urlencode(Yii::t('app', 'Данные не получены')));
        }
        
        if (isset($_REQUEST['WMI_PAYMENT_NO']) && ($payment = Payments::model()->findByPk((int)$_REQUEST['WMI_PAYMENT_NO']))){
            Yii::app()->wallet->result($_POST, $payment);
        }
        
        Y::end('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . urlencode(Yii::t('app', 'Сервер временно недоступен')));
    }
    
    
    public function actionPay() {
        if (Y::isGuest()) {
	    Y::end('unauthorized access closed.');
	}

        $data = Yii::app()->request->getParam('Number', []);
	    $number = new Number;

        $exchangeRate = Yii::app()->settings->getValue('EXCHANGE_RATE');
        $number->amount = isset($data['amount']) ? $data['amount'] : 0;
        $amount = (int)$number->amount * $exchangeRate;
        $this->performAjaxValidation($number);
        
        $acc_id=Yii::app()->account->getAccIdByCurrency(Y::userId(), 'WK');

        $this->redirect(Yii::app()->wallet->pay($acc_id, $amount));
	//$r = Yii::app()->account->incBalance($acc_id, (int)$number->amount, 'make money test');

        
    }
    
    /**
    * Performs the AJAX validation.
    * @param Menu $model the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='my-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}