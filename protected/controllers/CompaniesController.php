<?php

class CompaniesController extends Controller
{
    
    public function actionIndex() {
        $this->pageTitle = 'Каталог предприятий партнеров агентства';
        $criteria = new CDbCriteria;
        $criteria->order = 'name';
        $this->render('index', ['categories'=>Category::model()->findAll($criteria),
                                'cities' => City::model()->findAll($criteria)]);
    }
    
    
    public function actionSearch() {
        if (empty($_POST)) {
            Y::endJson(['error'=>1, 'message' => Yii::t('app', 'Не указаны данные для поиска.')]);
        }
        $search_all = false;
        if (isset($_POST['string']) && strlen($_POST['string'])>1) {
            $string = implode('&', explode(' ', $_POST['string']));
        } else {
            $search_all = true;
            $criteria = new CDbCriteria;
            $criteria->limit = 100;
            $criteria->order = 'name DESC';
            if (isset($_POST['category']) && ((int)$_POST['category']>0)) {
                $criteria->compare('category_id', (int)$_POST['category']);
            }
            if (isset($_POST['city']) && ((int)$_POST['city']>0)) {
                //select all orgs in this city
                $orgs = Yii::app()->db->createCommand()->
                select('organization_id')->
                from('city_organization_link')->
                where('city_id=:cid', [':cid'=>(int)$_POST['city']])->
                queryColumn();
                //print_r($orgs);
                //Y::end();
                $criteria->addInCondition('id', $orgs);
            }
        }
        
        $query = "
            SELECT
                id,
                category_id,
                name,
                specialization,
                contacts,
                \"desc\",
                img
            FROM
                ".Organization::model()->tableName().", to_tsquery(:string) AS q
            WHERE
                ".((isset($_POST['city']) && ((int)$_POST['city']>0)) ? 'id IN (SELECT organization_id FROM city_organization_link WHERE city_id='.(int)$_POST['city'].') AND ' : '' )."
                ".((isset($_POST['category']) && ((int)$_POST['category']>0)) ? 'category_id='.(int)$_POST['category']. ' AND ' : '')."
                (name @@ q OR
                specialization @@ q OR
                contacts @@ q OR
                \"desc\" @@ q)
            ORDER BY name DESC
            LIMIT 100
        ";
        
        if ($search_all) {
            $list = Organization::model()->findAll($criteria);
        } else {
             $list = Organization::model()->findAllBySql($query, [':string'=>$string]);
        }
        
        if ($list) {
            $in_categories = [];
            if (empty($_POST['category']) || ((int)$_POST['category']<=0)){
                foreach($list as $item) {
                    $in_categories[] = Category::model()->findByPk($item->category_id);
                }
            }
            Y::endJson(['error'=>0, 'html'=>$this->renderPartial('organizations', ['list' => $list, 'in_categories'=>$in_categories], true)]);
        }
        Y::endJson(['error'=>2, 'message'=>Yii::t('app', 'По вашему запросу ничего не найдено')]);
    }
}