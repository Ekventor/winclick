<?php

class ContentController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/columnContent';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('news', 'help', 'info', 'partnerNews', 'partnerHelp'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function missingAction($actionId)
    {
        $item = Menu::model()->findByAttributes(['url' => $actionId]);
        $menuPositions = Y::param('menuPosition');
        foreach ($menuPositions as $key => $position) {
            if ($position['id'] == $item->position) {
                $menuPosition = $key;
                break;
            }
        }

        $pagination = false;
        $isNews = $menuPosition == 'left_news' || $menuPosition == 'left_partner_news';
        $isHelp = $menuPosition == 'left_help' || $menuPosition == 'left_partner_help';
        if ($isNews) {
            $pagination = true;
        }
        $content = Yii::app()->content->getContent('', $actionId, $pagination);

        if ($isNews) {
            $this->setPageTitle('Winklik - Новости');
            $this->render('news', [
                'news' => $content['content'],
                'pages' => $content['paginator'],
                'position' => $menuPosition
            ]);
        } else if ($isHelp) {
            $this->layout = '//layouts/columnHelp';
            $this->setPageTitle('Winklik - Помощь');
            $this->render('help_new', [
                'helps' => $content['content'],
                'position' => $menuPosition
            ]);
        }
    }

    public function actionNews()
    {
        $this->layout = '//layouts/columnNews';
        $this->setPageTitle('Winklik - Новости');
        $position = Y::param('menuPosition')['left_news']['id'];
        $data = Yii::app()->content->getContent($position, null, true, true);

        $this->render('news_new', [
            'news' => $data['content'],
            'pages' => $data['paginator'],
            'position' => 'left_news'
        ]);
    }

    public function actionHelp()
    {
        $this->layout = '//layouts/columnHelp';
        $this->setPageTitle('Winklik - Помощь');
        $position = Y::param('menuPosition')['left_help']['id'];
        $this->render('help_new', [
            'helps' => Yii::app()->content->getContent($position)['content'],
            'position' => 'left_help'
        ]);
    }

    public function actionPartnerHelp()
    {
        $this->setPageTitle('Winklik - Помощь');
        $position = Y::param('menuPosition')['left_partner_help']['id'];
        $this->render('help', [
            'helps' => Yii::app()->content->getContent($position)['content'],
            'position' => 'left_partner_help'
        ]);
    }

    public function actionInfo()
    {
//        $this->layout = '//layouts/column1';
        $this->pageTitle = 'Winklik - Как это работет?';
        $criteria = new CDbCriteria();
        $criteria->compare('alias', 'info');
        $criteria->order = '"order"';

        $slides = Content::model()->findAll($criteria);

        $this->render('info', [
            'slides' => $slides
        ]);
    }

    public function actionPartnerNews()
    {
        $this->setPageTitle('Winklik - Новости');
        $position = Y::param('menuPosition')['left_partner_news']['id'];
        $data = Yii::app()->content->getContent($position, null, true);

        $this->render('news', [
            'news' => $data['content'],
            'pages' => $data['paginator'],
            'position' => 'left_partner_news'
        ]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Content the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Content::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Content $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'content-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
