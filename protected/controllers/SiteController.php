<?php

class SiteController extends Controller
{
	public $layout = '//layouts/main';
    /**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	//TODO: remove
	public function actionMakeMoney() {
		if (!Y::isLogged()) {
			Y::end('unauthorized access closed.');
		}

		$data = Yii::app()->request->getParam('Number', []);
			$number = new Number;
	
		$number->amount = isset($data['amount']) ? $data['amount'] : 0;
	
		$this->performAjaxValidation($number);
		$acc_id= Yii::app()->account->getAccIdByCurrency(Y::userId(), 'RUB');
		$r = Yii::app()->account->incBalance($acc_id, (int)$number->amount, 'make money test');
		
		if (empty($r['error'])) {
			$this->redirect('/');
		} else {
			echo $r['error'];
		}
	}
	
	
	public function actionTestUsers() {
		for ($i =1; $i <= 1000; $i++) {
			$user = new Users;
			$user->login = 'test'. $i;
			$user->email = 'test@test' . $i .'.ru';
			if ($user->save()) {
				echo "user ".$i." created<br />\n";
				$user->password = $user->createPassword('passwd');
				$user->newPassword = 'passwd';
				$user->rePassword = 'passwd';
				$user->activated = true;
				$user->role = Users::ROLE_USER;
				$user->phone = '89231200'.$i;
				Yii::app()->account->create($user->id);
				if ($user->save()) {
				    echo $user->id . " updated<br />\n";
				} else {
				    echo "err: ";
				    print_r($user->getErrors());
				}
				$acc_id = Yii::app()->account->loadList($user->id)->getAccIdByCurrency($user->id, 'WK');
				Yii::app()->account->incBalance($acc_id, 10000, 'test'.$i);
				echo "acc ". $acc_id . " filled<br />\n";
			} else {
			    echo "an error accured: ";
			    print_r($user->getErrors());
			}
		}
	}
	public function actionTestLetters() {
		Yii::app()->settings->handleLetters('ABCD');
	}
	
	public function actionTestMail() {
		Yii::app()->notifier->notify('futualien@gmail.com', 'test message', 'test content', 'email');
	}
	
	public function actionTestSettings(){
		Yii::app()->game->sendSettings();
		
	}

	public function actionTestAUTH() {
		
		print_r(Yii::app()->authService->getGameToken(1));
		
		
	}
	public function actionTestMQ() {
		$falied = $passed = 0;
		$services = ['account', 'game', 'authService'];
		Y::startTimer('total');
		for($i = 0; $i < 100; $i++) {
			Y::startTimer('ping'.$i);
			if (Yii::app()->{$services[rand(0,2)]}->ping()) {
				echo '<p>Ping: <span style="color: #0f0;">pong <<<<<<</span> time: '.Y::stopTimer('ping'.$i).'</p>';
				$passed++;
			} else {
				$falied++;
				echo '<p>Ping: <span style="color: #f00;">falied</span> time: '.Y::stopTimer('ping'.$i).'</p>';
			}
			
			usleep(10);
			echo "<br />\n\n";
		}
		echo "<br />results: passed - ".$passed ." / falied: ".$falied;
		echo "<br /> total time wasted: " . Y::stopTimer('total');
	}
	
	public function actionTestMQNative() {
		
		/* Server hostname */
		$dsn = "tcp://winclick.linode:5002";
		
		/* Create a socket */
		$socket = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_REQ, 'my socket2311');
		
		/* Get list of connected endpoints */
		$endpoints = $socket->getEndpoints();
		var_dump($endpoints);
		/* Check if the socket is connected */
		if (!in_array($dsn, $endpoints['connect'])) {
		    echo "<p>Connecting to $dsn</p>";
		    $socket->connect($dsn);
		} else {
		    echo "<p>Already connected to $dsn</p>";
		}
		$message = $socket->recv();
		var_dump($message);
		/* Send and receive */
		//for ($i=0; $i <= 10; $i++) {
		//	$message = 'хуй';
		//	try {
		//		$socket->send(CJSON::encode(['auth_key'=>'d7db66d8b9af121cfb12512cde636af32c2cf77a', 'ping'=> new stdClass()]));
		//		if ($i == 2) continue;
		//		$message = $socket->recv();
		//	} catch(ZMQSocketException $e) {
		//		$message = $e->getMessage();
		//	}
		//	echo "<p>Server said: {$message}</p>";
		//}
		
		
		
	}
	
	public function actionTestAS() {
		
		print_r(Yii::app()->account->create(Y::userId()));
		
		echo "<p>Ping: ". Yii::app()->account->ping() ? 'pong' : 'falied'."</p>\n";
		$acc_id = 1;
		echo "<p>Get account balance: </p>\n";
		echo "<p>Server said: ".implode(' ', Yii::app()->account->getBalance($acc_id))."</p>";
		
		echo "<p>Get accounts list: </p>\n";
		print_r(Yii::app()->account->getList(Y::userId()));
		
		echo "<p>Get account info: </p>\n";
		//echo "<p>Server said: ".implode(' ', Yii::app()->account->getInfo($acc_id))."</p>";
		print_r(Yii::app()->account->getInfo($acc_id));
		echo "<p>Increase balance: </p>\n";
		echo "<p>Account old balance: ".implode(' ', Yii::app()->account->getBalance($acc_id))."</p>";
		//echo "<p>Server said: ".implode(' ', Yii::app()->account->incBalance($acc_id, $amount = 100.0, $comment = "test inc"))."</p>";
		print_r(Yii::app()->account->incBalance($acc_id, $amount = 100.0, $comment = "test inc"));
		echo "<p>Account new balance: ".implode(' ', Yii::app()->account->getBalance($acc_id))."</p>";
		
		echo "<p>Decrease balance: </p>\n";
		echo "<p>Account old balance: ".implode(' ', Yii::app()->account->getBalance($acc_id))."</p>";
		//echo "<p>Server said: ".implode(' ', Yii::app()->account->decBalance($acc_id, $amount = 50.0, $comment = "test dec"))."</p>";
		print_r(Yii::app()->account->decBalance($acc_id, $amount = 50.5, $comment = "test inc"));
		echo "<p>Account new balance: ".implode(' ', Yii::app()->account->getBalance($acc_id))."</p>";
		
		//echo "<p>Get user accounts: </p>\n";
		////echo "<p>Server said: ".implode(' ', Yii::app()->account->getList(Y::userId()))."</p>";
		
	}	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->layout = '//layouts/main_new';
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$jsonNominals = CJSON::encode(Y::param('nominals'));
        $prizeDigits = Yii::app()->prize->formatPrizeSum();
        if (Y::isGuest()) {
            $slogan = Settings::model()->findByAttributes(['key' => 'slogan']);
            $reviews = Review::model()->findAllByAttributes(['show' => true]);
            $criteria = new CDbCriteria();
            $criteria->compare('alias', 'mainInfo');
            $criteria->order = '"order"';

            $slides = Content::model()->findAll($criteria);

            $this->render('new_index', [
                'slogan' => $slogan,
                'reviews' => $reviews,
				'jsonNominals' => $jsonNominals,
                'slides' => $slides,
                'prizeDigits' => $prizeDigits
            ]);

        } else {
            $this->render('new_game', [
				'jsonNominals' => $jsonNominals,
                'prizeDigits' => $prizeDigits
			]);
        }
	}

	public function actionTypical()
	{
		$this->layout = '//layouts/column1';
		$this->pageTitle = 'Заголовок типовой страницы';
		$this->render('typical');
	}

    public function actionLogin()
    {
        $this->layout = '//layouts/login';

        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $isAdmin = Yii::app()->user->getRole() == 'admin';

            if (isset($isAdmin)) {
                if ($isAdmin) {
                    $this->redirect(array('/backend'));
                } else {
                    $this->redirect(array('/user/winkliks'));
                }
            }
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout = '//layouts/error';

        if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	 
	public function actionGetWins() {
		
		if ($list = Yii::app()->wins->getWins()) {
			$this->renderPartial('wins_table', ['data'=>$list]);
		}
	}
	
	public function actionGetLastWins($last = 0) {
		
		if ($list = Wins::model()->last()->findAll(new CDbCriteria(['params'=>[':lid'=>(int)$last]]))) {
			$ret = [];
			foreach($list as $row) {
				$ret[] = [  	'date' => date('H:i:s', strtotime($row->date)),
						'game_id' => $row->game_id,
						'login' => isset($row->user->login) ? $row->user->login : 'undefined',
						'nominal' => $row->nominal,
					  ];
			}
			Y::endJson(['data'=>$ret, 'error'=>0, 'last'=> $list[0]->id]);
		}
		Y::endJson(['error'=> 1]);
	}


	public function actionCatalog()
	{
		$this->layout = '//layouts/column1';
		$this->setPageTitle('Каталог электронных подарочных сертификатов и карт');
		$sertificates = Giftcard::model()->findAll();
		$this->render('catalog', ['sertificates' => $sertificates]);
	}

    /**
     * Performs the AJAX validation.
     * @param Menu $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='my-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }



}