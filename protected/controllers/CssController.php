<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 15.07.14
 * Time: 15:18
 */
class CssController extends CController
{

    public function actionCss()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'modified DESC';
        $criteria->limit = 1;
        $model = Css::model()->findAll($criteria)[0];
        header('Content-type: text/css');
        $this->renderPartial('css', [
            'css' => $model->css
        ]);
    }

}