<?php

class UserController extends Controller
{
	public function filters()
	{
	    return array(
		'accessControl', // perform access control for CRUD operations
		'postOnly + delete', // we only allow deletion via POST request
	    );
	}

	public function accessRules()
	{
	    return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		    'actions'=>[
                'updateProfile',
                'profile',
                'money',
                'transactions',
                'giftcards',
                'winclicks',
                'mailings',
                'bonus',
                'expressBuy',
                'buyGiftcard'
            ],
		    'users'=>array('@'),
		    'roles'=>array('user', 'admin'),
		),
		array('allow',
		    'actions' => array('updateProfile'),
		    'expression' => 'Yii::app()->user->hasState("changePassword") && Yii::app()->user->getState("changePassword")'
		),
		array('deny',
		    'actions'=>[
                'updateProfile',
                'profile',
                'money',
                'transactions',
                'giftcards',
                'winclicks',
                'mailings',
                'bonus',
                'expressBuy',
                'buyGiftcard'
            ],
		    'users'=>array('*')
		)
	    );
	}


	public function actionActivate($token = null)
	{
		if (Y::isGuest() && $user = Users::model()->findByAttributes(['token'=>$token])) {
			if (!$user->activated) {
				$user->activated = true;
				$user->role = Users::ROLE_USER;
				Yii::app()->account->create($user->id);
				$acc_id=Yii::app()->account->getAccIdByCurrency($user->id, 'WK');
				Yii::app()->account->incBalance($acc_id, 10 , Yii::t('app', 'Подарок'));
				Yii::app()->coupon->addCoupons($user->id, 3);
			}

			$user->token = null;
			$user->setScenario('activate');
			if ($user->save()) {
				Yii::app()->user->setState('changePassword', 1);
				Yii::app()->user->setState('user_id', $user->id);
				$this->redirect('/user/updateprofile');
			}
		}
		$this->render('token_error');
	}

	public function actionUpdateProfile() {
        $this->layout = '//layouts/columnCabinet';
		$this->setPageTitle('РЕДАКТИРОВАНИЕ');

		$check_old = true;
		if (Yii::app()->user->hasState('changePassword') && Yii::app()->user->getState('changePassword')) {
			$check_old = false;
			$user = Users::model()->findByPk(Yii::app()->user->getState('user_id'));
		} else if (Y::isLogged()) {
			$user = Y::user();
		} else {
			$this->redirect('/');
		}

		if (!empty($_POST['Users'])) {
			$user->setScenario('update');
			if (is_null($user->salt)) {
				$user->generateSalt();
			}
            $oldEmail = $user->email;
            $user->attributes = $_POST['Users'];
            $user->validate();

            if ($check_old && ($user->password != $user->createPassword($_POST['oldPassword'], $oldEmail))){
                $user->addError('password', Yii::t('app', 'Старый пароль введен неверно'));
            }


            if ($_POST['Users']['newPassword'] == '' && $_POST['Users']['rePassword'] == '' && $check_old) {
                $user->clearErrors('rePassword');
                $user->clearErrors('newPassword');
            }

            if (!$user->hasErrors()) {
                if (isset($_POST['oldPassword'])) {
                    $user->password = $user->createPassword($_POST['oldPassword']);
                }
                if ($_POST['Users']['newPassword'] != '') {
                    $user->password = $user->createPassword($_POST['Users']['newPassword']);
                }

                $user->save(false);
                if (!$check_old) {
					$loginForm = new LoginForm;
					$loginForm->login = $user->login;
					$loginForm->password = $_POST['Users']['newPassword'];
					Yii::app()->user->setState('changePassword', null);
                    Yii::app()->user->setState('user_id', null);

                    if ($loginForm->validate() && $loginForm->login()) {
						$this->redirect('/user/winkliks');
					} else {
						$this->redirect('/');
					}
                }

                $this->redirect('/user/profile');
            }

		}
		$this->render('update_profile', ['user'=>$user]);
	}
	public function actionProfile()
	{
		$this->layout = '//layouts/columnCabinet';
        $this->setPageTitle('Персональные данные');
        $this->render('profile', ['user'=> Y::user()]);
	}

	public function actionRecover() {
		// if it is ajax validation request
		if(!isset($_POST['ajax']) && $_POST['ajax']!=='recover-form') {
			Y::endJson(['error'=>1, 'errors'=>['common'=>Yii::t('app', 'Ошибка запроса: запрос должен являться AJAX')]]);
		}

		$model=new RecoverForm;
		// collect user input data
		if(isset($_POST['RecoverForm']))
		{
			$model->attributes=$_POST['RecoverForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->recover()) {
				Y::endJson(['error'=>0, 'message'=>Yii::t('app', 'RECOVER_MESSAGE', ['{email}'=>$model->email])]);
			} else {
				Y::endJson(['error'=>1, 'errors'=>$model->getErrors()]);
			}
		}
		Y::endJson(['error'=>1, 'errors'=>['common'=>Yii::t('app', 'Ошибка запроса: нет данных')]]);
	}

	public function actionRegister() {

		// if it is ajax validation request
		if(!isset($_POST['ajax']) && $_POST['ajax']!=='register-form') {
			Y::endJson(['error'=>1, 'errors'=>['common'=>Yii::t('app', 'Ошибка запроса: запрос должен являться AJAX')]]);
		}

		$model = new RegisterForm;
		// collect user input data
		if(isset($_POST['RegisterForm']))
		{
			$model->attributes = $_POST['RegisterForm'];
			if ($model->validate()) {
				//TODO: register user, send email to him
				$user = new Users;
				$user->attributes = ['email'=>$model->email,
						     'login'=>$model->login,
						     ];
				if ($user->save()) {
					Yii::app()->notifier->notify($user->email, Yii::t('app', 'Подтверждение e-mail адреса'), Yii::t('app', 'REG_EMAIL', ['{token}'=>$user->token, '{link}'=>$this->createAbsoluteUrl('/user/activate/token/'.$user->token)]), 'email');
					Y::endJson(['error'=>0, 'message'=>Yii::t('app', 'На ваш емейл {email} отправлено письмо с дальнейшими инструкциями.', ['{email}'=>$model->email])]);
				}
				Y::endJson(['error'=>1, 'errors'=>['common'=>Yii::t('app', 'Ошибка запроса: не удалось сохранить пользователя'), 'errors'=>$user->getErrors()]]);
			} else {
				Y::endJson(['error'=>1, 'errors'=>$model->getErrors()]);
			}
		}
		Y::endJson(['error'=>1, 'errors'=>['common'=>Yii::t('app', 'Ошибка запроса: нет данных')]]);
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{

		// if it is ajax validation request
		if(!isset($_POST['ajax']) || $_POST['ajax']!=='login-form') {
			Y::endJson(['error'=>1, 'errors'=>['common'=>Yii::t('app', 'Ошибка запроса: запрос должен являться AJAX')]]);
		}

		$model=new LoginForm;

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {
				Y::endJson(['error'=>0]);
			} else {
				Y::endJson(['error'=>1, 'errors'=>$model->getErrors()]);
			}
		}
		Y::endJson(['error'=>1, 'errors'=>['common'=>Yii::t('app', 'Ошибка запроса: нет данных')]]);
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		Yii::app()->user->setState('user_id', null);
		Yii::app()->user->setState('password', null);
		Yii::app()->user->setState('gameToken', null);
		Y::logAction('logout', false);
		$this->redirect(Yii::app()->homeUrl);
	}

    public function actionMoney()
    {
        $this->layout = '//layouts/columnCabinet';
        $this->setPageTitle('Деньги');
        $number = new Number;
        $this->render('money', [
            'number' => $number
        ]);
    }

    public function actionTransactions()
    {
        $userId = Yii::app()->user->getId();
        $this->layout = '//layouts/columnCabinet';
        $this->setPageTitle('Транзакции');
		$currencies = Y::param('possible_currencies');
		foreach($currencies as $key => $currency){
			$criteria = new CDbCriteria();
            $inCurrency = is_array($currency) ? $currency : array($currency);
            $criteria->addInCondition('type', $inCurrency);
			$criteria->compare('user_id', Y::userId());
			$criteria->addCondition('amount > 0');
			$criteria->order = 'date DESC';
			$account_history[$key] = new CActiveDataProvider('AccountHistory', array('criteria'=>$criteria));
		}
		$criteria = new CDbCriteria();
		$criteria->compare('user_id', Y::userId());
		$criteria->compare('status', 1);
		$criteria->order = 'date DESC';
		$orderHistory = new CActiveDataProvider('UserGiftcards', ['criteria' => $criteria]);


//        $couponHistory = Yii::app()->coupon->getTransactions($userId);
//        $cardHistory = Yii::app()->card->getCardHistory($userId);
//		$orderHistory = []; //будет метод получения истории заказа
        $this->render('transactions', array(
			'account_history'=>$account_history,
//            'couponHistory' => $couponHistory,
//            'cardHistory' => $cardHistory,
			'orderHistory' => $orderHistory,
			'userId' => $userId
		));
    }

    public function actionGiftcards($buy = 0)
    {
        $this->layout = '//layouts/columnCabinet';
        $this->setPageTitle('Предоплаченные карты');
        $settingCost = Y::app()->settings->getValue('EXCHANGE_RATE');
		$view = 'giftcards';
		if ($buy) {
			$view = 'buyGiftcard';
			$this->setPageTitle('Подарочные карты > Получение электронных подарочных сертификатов');
		}
		$userId = Yii::app()->user->getId();
		$countCard = Yii::app()->request->getParam('countCard', 0);
		$winBalance = round(Yii::app()->account->getBalanceByCurrency(Y::user(), 'RUB'), 2);
		$this->render(
            'universal_card',
            [
                'list' => Yii::app()->myuser->getWins(Y::userId()),
                'winclickCost' => $settingCost,
				'cardNominal' => $buy,
				'countCard' => $countCard,
				'userId' => $userId,
				'winBalance' => $winBalance
            ]
        );
    }

    public function actionWinkliks()
    {
        $this->layout = '//layouts/columnCabinet';
        $this->setPageTitle('Винклики');
        $enoughMoney = true;
        $number = new Number;
        $data = Yii::app()->request->getParam('Number', []);
        $number->amount = isset($data['amount']) ? $data['amount'] : 0;

        $validation = CActiveForm::validate($number);
        if ($validation != '[]' && Yii::app()->request->isAjaxRequest) {
            Y::endJson(['Number_amount' => ['Некорректные данные']]);
        }
//        $this->performAjaxValidation($number);

        if ($number->amount) {
            $r = Yii::app()->account->buy_wk(Y::userId(), $number->amount, 'Покупка WK', 'Покупка WK');
            if ($r['error']) {
                $enoughMoney = false;
                Y::endJson(['Enough_money' => ['']]);
            } else {
                Y::endJson(['error' => 0, 'message' => 'Операция успешно завершена']);
            }
        }
        $user = Users::model()->findByPk(Y::userId());
        //$userParams = Yii::app()->request->getParam('Users', []);
        //if (isset($userParams['auto_convert'])) {
        //    $user->auto_convert = (bool)$userParams['auto_convert'];
        //    $user->update(['auto_convert']);
        //}

        $this->render('winclicks', [
            'number' => $number,
            'enoughMoney' => $enoughMoney,
            'user' => $user
        ]);
    }


    /**
     * Экспресс покупка WK
     */
    public function actionExpressBuy()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $cost = Yii::app()->request->getParam('count', 0);
			$count = $cost ? str_replace('wk', '', $cost) : 0;
			$enoughMoney = true;
            if ($count) {
				$r = Yii::app()->account->buy_wk(Y::userId(), $count, 'Покупка WK', 'Покупка WK');
				if ($r['error']) {
					$enoughMoney = false;
				}
				Y::endJson(['error' => $r['error'] , 'enoughMoney' => $enoughMoney, 'result'=> $r, 'hasResult' => true]);
			}
			Y::endJson(['hasResult' => false]);
        }
    }

    public function actionGetCardBalance()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $userId = Y::userId();
            $noninal = Yii::app()->request->getParam('nominal', 0);
            if ($userId && $noninal) {
                $totalCardBalance = Yii::app()->card->getCardBalance($userId, $noninal);
				$winPercent = Yii::app()->settings->getValue('result_percent;!');
				$cumulatePercent = Yii::app()->settings->getValue('result_percent;B');
				$universalCardCount = floor($totalCardBalance / ($noninal * $winPercent / 100));
				$universalCardBalance = $universalCardCount * $noninal;
				$cumulativeCardCount = floor(($totalCardBalance % ($noninal * $winPercent / 100)) / ($noninal * $cumulatePercent / 100));
				$cumulativeCardBalance = $totalCardBalance - $universalCardBalance;
				Y::endJson(
					[
						'universeCardBalance' => $universalCardBalance,
						'universeCardCount' => $universalCardCount,
						'cumulativeCardCount' => $cumulativeCardCount,
						'cumulativeCardBalance' => $cumulativeCardBalance
					]
				);
            } else {
                Y::endJson(
					[
						'universeCardBalance' => 0,
						'universeCardCount' => 0,
						'cumulativeCardCount' => 0,
						'cumulativeCardBalance' => 0
					]
				);
            }
        }
    }


    public function actionAutoWK() {
		if (isset($_POST['auto_convert'])) {
			$user = Users::model()->findByPk(Y::userId());
			$user->auto_convert = (bool)$_POST['auto_convert'];
			Y::endJson(['result'=> $user->update(['auto_convert'])]);

		}
    }

    public function actionBonus($view = '')
    {
        $this->layout = '//layouts/columnCabinet';
        $this->setPageTitle('Подарочные купоны');
        $userId = Yii::app()->user->getId();
        $couponActiveCount = Yii::app()->coupon->getActiveCouponsCount($userId);
        $couponUsedCount = Yii::app()->coupon->getUsedCouponsCount($userId);
        $render = $view != '' ? 'rule' : 'bonus';
        $this->render('coupons', [
            'activeCouponsCount' => $couponActiveCount,
            'usedCouponsCount' => $couponUsedCount
        ]);
    }

    public function actionBuyCoupons()
    {
        $userId = Yii::app()->user->getId();
        $couponsCount = Yii::app()->request->getParam('coupons', 0);
        if (!$couponsCount) {
            Y::endJson(['error' => 1, 'message' => 'Введите количество купонов']);
        }
        if ($couponsCount > Yii::app()->coupon->getActiveCouponsCount($userId)) {
            Y::endJson(['error' => 1, 'message' => 'Недостаточно купонов']);
        }
        $activeCoupons = Yii::app()->coupon->getActiveCoupons($userId, $couponsCount);
        $used_datetime = date('Y-m-d H:i:s');
        $couponNumbers = [];
        $openBalance = Y::app()->coupon->getActivatedCouponsCount($userId);
        $amount = $couponsCount;
        foreach ($activeCoupons as $coupon) {
            $number = Yii::app()->coupon->generateCouponNumber($coupon->id);
            $coupon->number = $number;
            $coupon->used = 1;
            $coupon->used_datetime = $used_datetime;
            $coupon->save();
            $couponNumbers[] = $number;
        }
        Y::app()->coupon->addChange($openBalance, $amount, $userId, 'Получение купонов');
        if (Y::app()->coupon->sendCoupon($userId, $couponNumbers)) {
            Y::endJson(['error' => 0, 'message' => Y::t('app', 'coupon.success_take')]);
        } else {
            Y::endJson(['error' => 1, 'message' => 'Ошибка отправки почты']);
        }
    }

	public function actionMailings(){
		$this->layout = '//layouts/columnCabinet';
        $this->setPageTitle('Администрация');

		$criteria = new CDbCriteria();
		$criteria->compare('user_id', Y::userId());
		$criteria->compare('readed', 0);

		$dataProvider = new CActiveDataProvider('Mailings', array('criteria'=>$criteria));

		if($dataProvider->getData())
			foreach($dataProvider->getData() as $mailing){
				$mailing->readed = 1;
				$mailing->update(['readed']);
			}

		$this->render('mailings',array(
			'dataProvider'=>$dataProvider,
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
    /**
     * Performs the AJAX validation.
     * @param Menu $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='my-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionPay($status = 'fail') {
	if (in_array($status, ['success', 'fail'])) {
		Yii::app()->user->setFlash('message', $this->renderPartial($status, null, true));
		$this->redirect('/user/winkliks');
	}
	else
		throw new CHttpException(404, 'Страница не найдена');

    }


	public function actionExchange() {
		if (!Y::isAjax() || Y::isGuest()) {
				throw new CHttpException(400, Yii::t('app', 'Страница не найдена'));
		}
		$amount = (int)$_POST['amount'];
		if ($amount <= 0) {
				Y::endJson(['error'=>1, 'message'=>Yii::t('app', 'Некорректно указана сумма для обмена')]);
		}
		$nominal = (int)$_POST['nominal'];
		//try to remove
        $user_id = Yii::app()->user->getId();
//        $criteria = new CDbCriteria;
//        $criteria->compare('user_id', (int)$user_id);
//        $criteria->compare('type', 'CARD' . $nominal);
//        $criteria->order = 'id DESC';
//        $criteria->limit = 1;

//        $list = AccountHistory::model()->find($criteria);
//        $balance = $list !== null ? $list->close_balance : 0;
        $balance = Yii::app()->card->getCardBalance($user_id, $nominal);
        $exRate = Yii::app()->settings->getValue('EXCHANGE_RATE');
        $sum = $amount * $exRate;
        $data = [
            'account_id' => '',
            'open_balance' => $balance,
            'close_balance' => $balance - $sum,
            'amount' => $sum * (-1),
            'date' => date('Y-m-d H:i:s'),
            'user_id' => $user_id,
            'type' => 'CARD' . $nominal,
            'comment' => 'Конвертация'
        ];
        $accountHistory = new AccountHistory();
        $accountHistory->attributes = $data;
        if ($balance < $sum) {
            Y::endJson(['error'=>1, 'message'=>Yii::t('app', 'Недостаточно побед для обмена')]);
        }
        if ($acc_id = Yii::app()->account->getAccIdByCurrency(Y::userId(), 'WK')) {
            $to_inc = $amount;
            $data = Yii::app()->account->incBalance($acc_id, (float)$to_inc, Yii::t('app', 'Обмен'));
            if ($data['error'] == '') {
                $accountHistory->account_id = $acc_id;
                $accountHistory->save();
                Y::endJson(['error' => 0, 'message' => Yii::t('app', 'Операция успешно завершена')]);
            } else {
                Y::endJson(['error' => 2, 'message' => Yii::t('app', 'Ошибка зачисления средств')]);
            }
        }
	}

	/**
	 *
	 */
	public function actionGiftcardTake()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$userId = Yii::app()->user->getId();
//			Y::endJson(['error' => 0, 'message' => 'Реквизиты универсальной карты будут отправлены на указанную вами электронную почту']);
			$winBalance = round(Yii::app()->account->getBalanceByCurrency(Y::user(), 'RUB'), 2);
			if ($winBalance < Y::getSetting('win_amount')) {
				Y::endJson(['error' => 1, 'message' => 'Недостаточный выйгрыш для вывода']);
			}
			$result = Yii::app()->userGiftcard->takingWinAmount($userId);
			if (!$result) {
				Y::endJson(['error' => 1, 'message' => 'Ошибка при приобретении выйгрыша']);
			}

			Y::endJson(['error' => 0, 'message' => 'Реквизиты универсальной карты будут отправлены на указанную вами электронную почту']);

//			if (count($data)) {
//				$result = [];
//				$serts = isset($data['serts']) ? $data['serts'] : [];
//				$nominal = isset($data['nominal']) ? $data['nominal'] : 0;
//				if (!count($serts) || !$nominal) {
//					Y::endJson(['error' => 1, 'message' => 'Ошибка при приобретении карты']);
//				}
//				foreach($serts as $giftcard) {
//					if (!isset($result[$giftcard['id']])) {
//						$result[$giftcard['id']] = 0;
//					}
//					$result[$giftcard['id']]++;
//				}
//				Yii::app()->userGiftcard->takingGiftcard($result, $nominal, $userId);
//			}
//			Y::endJson(['error' => 0, 'result' => []]);
		}
	}

	public function actionBuy() {
		if (!Y::isAjax() || Y::isGuest()) {
				throw new CHttpException(400, Yii::t('app', 'Страница не найдена'));
		}
        $nominal = (int)$_POST['nominal'];
		$count = (int)$_POST['amount'];
		$amount = ($count * 95 * $nominal) / 100;

		if ($amount <= 0) {
				Y::endJson(['error'=>1, 'message'=>Yii::t('app', 'Некорректно указана сумма для обмена')]);
		}

		//try to remove
        $user_id = Yii::app()->user->getId();
        $criteria = new CDbCriteria;
		$criteria->compare('user_id', (int)$user_id);
		$criteria->compare('type', 'CARD' . $nominal);
        $criteria->order = 'id DESC';
        $criteria->limit = 1;
 		//$criteria->addCondition('');
        $list = AccountHistory::model()->find($criteria);
		$balance = 0;
        if ($list !== null) {
            $balance = $list->close_balance;
        } else {
            $wins = Yii::app()->myuser->getWins($user_id);
            $balance = $wins[$nominal]['sum'];
        }
        if ($amount > $balance) {
            Y::endJson(['error' => 1, 'message' => Yii::t('app', 'Недостаточно побед для покупки')]);
        }
        $data = [
            'account_id' => 0,
            'open_balance' => $balance,
            'close_balance' => $balance - $amount,
            'amount' => $amount * (-1),
            'date' => date('Y-m-d H:i:s'),
            'user_id' => $user_id,
            'type' => 'CARD' . $nominal,
            'comment' => 'Покупка'
        ];
        $accountHistory = new AccountHistory();
        $accountHistory->attributes = $data;
        if ($acc_id = Yii::app()->account->getAccIdByCurrency(Y::userId(), 'RUB')) {
            $to_dec = ($nominal * 0.05) * $count;
            $data = Yii::app()->account->decBalance($acc_id, (float)$to_dec, Yii::t('app', 'Оплата карт'));
            if ($data['error'] == '') {
                $accountHistory->account_id = $acc_id;
                $accountHistory->save();
                Y::endJson(['error' => 0, 'message' => Yii::t('app', 'Операция успешно завершена')]);
            } else {
                Y::endJson(['error' => 2, 'message' => Yii::t('app', 'Ошибка списания средств')]);
            }
        }
	}

	public function actionCanPlay()
	{
		if (Yii::app()->request->isAjaxRequest) {
			Y::endJson(['can' => !Yii::app()->user->isShop()]);
		}
	}
}