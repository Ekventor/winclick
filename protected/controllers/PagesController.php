<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 15.07.14
 * Time: 15:18
 */
class PagesController extends Controller
{

    public $layout = '//layouts/columnPages';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'users'=>array('*'),
            ),
        );
    }
    public function actionIndex()
    {
        $url = Yii::app()->request->url;
        $str = str_replace('/', '', $url);

        $model = Pages::model()->findByAttributes(
            array(
                'alias' => $str
            )
        );
        if ($model === null) {
            throw new CHttpException('404', 'Страница не найдена');
        }
        $itemMenu = Menu::model()->findByAttributes(['url' => $model->alias]);

        if ($itemMenu !== null && !$itemMenu->unauthorized && Y::isGuest()) {
            throw new CHttpException('Эта страница видна только для авторизированных пользователей');
        }

        $text = Yii::app()->placeholder->replace($model->content);

        $this->render('index', array(
            'model' => $model,
            'text' => $text
        ));
    }
}