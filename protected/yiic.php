<?php



// fix for fcgi
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));

// change the following paths if necessary
$yiic=dirname(__FILE__).'/../../framework/yii.php';
require_once($yiic);


$config=dirname(__FILE__).'/config/console.php';
$config = CMap::mergeArray(
    require_once(dirname(__FILE__).'/config/console.php'),
    include_once(dirname(__FILE__).'/config/console.local.php')
);
date_default_timezone_set('Europe/Moscow');
require_once(dirname(__FILE__).'/../constants.php');
$app = Yii::createConsoleApplication($config);

$app->commandRunner->addCommands(YII_PATH.'/cli/commands');
$app->run();