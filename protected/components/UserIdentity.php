<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    /**
     * @var
     */
    protected $_id;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		if ($user = Users::model()->findByAttributes(['login'=>$this->username])) {
			if ($user->password == $user->createPassword($this->password)){
				$this->errorCode=self::ERROR_NONE;
				Yii::app()->user->setState('user_id', $user->id);
				Yii::app()->user->setState('password', $user->password);
				$this->_id = $user->id;
			} else {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}
		} else {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		
		return !$this->errorCode;
	}

    /**
     * @return mixed|string
     */
    public function getId()
    {
        return $this->_id;
    }
}