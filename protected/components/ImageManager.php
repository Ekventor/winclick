<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ImageManager
{
    public $file;
        
    public function __construct(CUploadedFile $file) {
	if($file) {
	    $this->file = $file;
	}
    }
    public function getHasError(){
        return $this->file->getHasError();
    }
    public function saveResized($forcedwidth=800, $forcedheight=600, $destfile , $widthOnly = true) {
        if ($this->file->getHasError()) {
            
            return false;    
        };
        $fw = $forcedwidth;
        $fh = $forcedheight;
        $is = getimagesize( $this->file->getTempName() );
        //опускаем часть, которая удерживает изображение в рамках заданных размеров.
        /*  if( $is[0] >= $is[1] ){
              $orientation = 0;
          } else {
              $orientation = 1;
              $fw = $forcedheight;
              $fh = $forcedwidth;
          }*/
        if ( $is[0] > $fw || $is[1] > $fh ){
            if(( ( $is[0] - $fw ) >= ( $is[1] - $fh ) ) || $widthOnly) {
                $iw = $fw;
                $ih = ( $fw / $is[0] ) * $is[1];
            }
            else {
                $ih = $fh;
                $iw = ( $ih / $is[1] ) * $is[0];
            }
        } else{
            $iw = $is[0];
            $ih = $is[1];
        
        }       
            

        $isPng = false;
        switch($this->file->getType()){
            case "image/jpeg":
                $img_src = imagecreatefromjpeg($this->file->getTempName() ); //jpeg file
                $isJpg = true;
            break;
            case "image/gif":
                $img_src = imagecreatefromgif($this->file->getTempName() ); //gif file
          break;
          case "image/png":
              $img_src = imagecreatefrompng($this->file->getTempName() ); //png file
              $isPng = true;
          break;
            default: 
                return false;
            break;
        }
        $img_dst = imagecreatetruecolor( $iw, $ih );
    
        imagecopyresampled( $img_dst, $img_src, 0, 0, 0, 0, $iw, $ih, $is[0], $is[1] ); //do it at all
        
        if ($isPng) {
            if( !imagepng( $img_dst, $destfile, 9 ) )  {
                return false;
            }
        } else {
            if( !imagejpeg( $img_dst, $destfile, 90 ) )  {
                return false;
            }
        }
        return true;
    }

    public function save($destfile)
    {
        $isPng = false;
        switch($this->file->getType()){
            case "image/jpeg":
                $img_src = imagecreatefromjpeg($this->file->getTempName() ); //jpeg file
                break;
            case "image/gif":
                $img_src = imagecreatefromgif($this->file->getTempName() ); //gif file
                break;
            case "image/png":
                $img_src = imagecreatefrompng($this->file->getTempName() ); //png file
                $isPng = true;
                break;
            default:
                return false;
                break;
        }

        if ($isPng) {
            imagesavealpha($img_src, true);
            if( !imagepng( $img_src, $destfile, 9 ) )  {
                return false;
            }
        } else {
            if( !imagejpeg( $img_src, $destfile, 90 ) )  {
                return false;
            }
        }
        return true;
    }
   
}