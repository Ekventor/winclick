<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08.07.14
 * Time: 13:04
 */

/**
 * Class PhpAuthManager
 */
class PhpAuthManager extends CPhpAuthManager
{

    /**
     * @throws CException
     */
    public function init()
    {
        // Иерархию ролей расположим в файле auth.php в директории config приложения
        if ($this->authFile === null) {
            $this->authFile = Yii::getPathOfAlias('application.config.auth') . '.php';
        }

        parent::init();

        // Для гостей у нас и так роль по умолчанию guest.
        if (!Yii::app()->user->isGuest) {
            // Связываем роль, заданную в БД с идентификатором пользователя,
            // возвращаемым UserIdentity.getId().
            $this->assign(Yii::app()->user->role, Yii::app()->user->id);
        }
    }
}