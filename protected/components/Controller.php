<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function beforeAction($action)
    {
        $onoff = Yii::app()->db->createCommand()
            ->select('value')
            ->from('settings')
            ->where("key='site_status'")
            ->queryColumn();
        if (!$onoff[0]) {
            $this->redirect('/service.html');
        }


        Yii::app()->myuser->setOnline();


        $action = $this->getAction();
        if (Yii::app()->user->isShop() && $action->id != 'logout') {
            $this->redirect('/partner/partner/checking/');
        }

        if (Yii::app()->user->isPartner() && $action->id != 'logout') {
            $this->redirect('/statistic/');
        }

        return true;
    }
}