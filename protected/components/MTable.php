<?php

/**
 * Class MTable
 *
 * @author Lu888
 */
Yii::import('zii.widgets.grid.CGridView');

class MTable extends CGridView {

    /**
     * @var array grid header configuration.
     */
    public $headers = array();
    public $headerColumns = array();
    private $headerRows = array();
    public $pdf;

    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns() {
        if ($this->columns === array()) {
            if ($this->dataProvider instanceof CActiveDataProvider) {
                $this->columns = $this->dataProvider->model->attributeNames();
            } else if ($this->dataProvider instanceof IDataProvider) {
                // use the keys of the first row of data as the default columns
                $data = $this->dataProvider->getData();
                if (isset($data[0]) && is_array($data[0]))
                    $this->columns = array_keys($data[0]);
            }
        }

        /* Получаем массив ключей колонок */
        $columns = array();
        foreach ($this->columns as $k => $v) {
            $columns[is_array($v) ? $v['name'] : $v] = $v;
        }

        /* Формируем шапку таблицы */
        $this->renderHeaderRow($this->headers, $columns);

        /* Исключаем колонки, не присутствующие в шапке */
        $this->columns = array();
        foreach ($this->headerColumns as $v) {
            if (is_array($columns[$v])) {
                $this->columns[] = $columns[$v];
            } else {
                $this->columns[] = $columns[$v];
            }
        }

        $this->headerColumns = array();
        $id = $this->getId();
        foreach ($this->columns as $i => $column) {
            if (is_string($column))
                $column = $this->createDataColumn($column);
            else {
                if (!isset($column['class']))
                    $column['class'] = 'CDataColumn';
                $column = Yii::createComponent($column, $this);
            }
            if (!$column->visible) {
                unset($this->columns[$i]);
                continue;
            }
            if ($column->id === null)
                $column->id = $id . '_c' . $i;
            $this->columns[$i] = $column;
            $this->headerColumns[$column->name] = $i;
        }

        foreach ($this->columns as $column) {
            $column->init();
        }
    }

    private function renderHeaderRow($row, $columns, $level = 0) {
        $colspan = 0;
        foreach ($row as $k => $item) {
            if (is_array($item)) {
                if (isset($item['childs'])) {
                    if (count($this->headerRows)) {

                    }
                    $count = $this->renderHeaderRow($item['childs'], $columns, $level + 1);
                    if ($count) {
                        $this->headerRows[$level][] = array(
                            'colspan' => $count,
                            'header' => isset($item['label']) ? $item['label'] : $k
                        );
                        $colspan += $count - 1;
                    } else {
                        unset($row[$k]);
                    }
                }
            } else {
                if (array_key_exists($item, $columns)) {
                    $this->headerRows[$level][] = array(
                        'name' => $item
                    );
                    $this->headerColumns[] = $item;
                } else {
                    unset($row[$k]);
                }
            }
        }
        return count($row) + $colspan;
    }

    /**
     * Renders the table header.
     */
    public function renderTableHeader() {
        if (!$this->hideHeader) {
            echo "<thead>\n";
            if($this->pdf) {
                $this->filterPosition = 'pdf';
            }
            if ($this->filterPosition === self::FILTER_POS_HEADER)
                $this->renderFilter();

            for ($r = 0; $r < count($this->headerRows); $r++) {
                $row = $this->headerRows[$r];
                echo '<tr>';
                foreach ($row as $k => $v) {
                    $htmlOptions = array();
                    $colspan = (isset($v['colspan'])) ? $v['colspan'] : false;
                    if ($colspan > 1) {
                        $htmlOptions['colspan'] = $colspan;
                    }
                    $rowspan = ($r < count($this->headerRows) && !$colspan) ? count($this->headerRows) - $r : false;
                    if ($rowspan) {
                        $htmlOptions['rowspan'] = $rowspan;
                    }

                    if (isset($v['name'])) {
                        $column = $this->columns [$this->headerColumns[$v['name']]];
                        $column->headerHtmlOptions = $htmlOptions;
                        $column->renderHeaderCell();
                    } else {
                        echo $this->renderHeaderCellSimple(isset($v['header']) ? $v['header'] : "&nbsp;", $htmlOptions);
                    }
                }
                echo '</tr>';
            }

            if ($this->filterPosition === self::FILTER_POS_BODY)
                $this->renderFilter();

            echo "</thead>\n";
        }
        else if ($this->filter !== null && ($this->filterPosition === self::FILTER_POS_HEADER || $this->filterPosition === self::FILTER_POS_BODY)) {
            echo "<thead>\n";
            $this->renderFilter();
            echo "</thead>\n";
        }
    }

    private function renderHeaderCellSimple($title, $htmlOptions = array()) {
        echo CHtml::openTag('th', $htmlOptions);
        echo $title;
        echo "</th>";
    }


}