<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08.07.14
 * Time: 13:02
 */

/**
 * Class WebUser
 */
class WebUser extends CWebUser
{
    /**
     * @var null
     */
    private $_model = null;


    public function isAdmin()
    {
        if ($user = $this->getModel()) {
            // в таблице User есть поле role
            return $user->role == 'admin';
        }
        return false;
    }

    public function isContent()
    {
        if ($user = $this->getModel()) {
            // в таблице User есть поле role
            return $user->role == 'content';
        }
        return false;
    }

    public function isShop()
    {
        if ($user = $this->getModel()) {
            // в таблице User есть поле role
            return $user->role == 'shop';
        }
        return false;

    }

    public function isPartner()
    {
        if ($user = $this->getModel()) {
            return $user->role == 'partner';
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        if ($user = $this->getModel()) {
            // в таблице User есть поле role
            return $user->role;
        }
    }

    /**
     * @return null
     */
    private function getModel()
    {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = Users::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }
}