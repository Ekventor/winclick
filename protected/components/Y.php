<?php

class Y {
 
    public static $timers = array();
    private static $user = null;
    private static $reloadUser = false;
    
    /**
    * @return CApplication
    */
    public static function app() {
        return Yii::app();
    }

    /**
    * @return user object
    */
    public static function user() {
        
        if ((is_null(self::$user)||self::$reloadUser) && self::app()->user->hasState('user_id')) {
            if (self::$user = Users::model()->findByPk(Yii::app()->user->getState('user_id'))) {
                self::$reloadUser = false;
                return self::$user;
            }
        } 
        return self::$user;
    }
    /*
     * Устанавливает флаг, для того чтобы следующее обращение к объекту user обновило данные.
     *
     *@return true
     */
    public static function reloadUser() {
        self::$reloadUser = true;
        return true;
    }

    /**
    * @return int ID текущего пользователя
    */
    public static function userId() {
        if (isset(Yii::app()->user) && Yii::app()->user->hasState('user_id')) {
            return Yii::app()->user->getState('user_id');
        } else {
            return -1; //guest
        }
    }
 
    /**
    * @return bool true если пользователь авторизирован
    */
    public static function isLogged() {
        return !Yii::app()->user->isGuest;
    }
    /**
    * @return bool true если пользователь не авторизирован
    */
    public static function isGuest() {
        return Yii::app()->user->isGuest;
    }
    /**
    * @return bool true если пользователь глобальный админ
    */
    public static function isSiteAdmin() {
        if (!Y::isGuest() && (Y::user()->login == Y::param('admin'))) {
            return true;
        }
        return false;
    }
    /**
    * @return bool true если это аякс-запрос
    */
    public static function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
    
    /**
    * @return CDateFormatter
    */
    public static function df() {
        return Yii::app()->dateFormatter;
    }

    /**
    * @return CHttpRequest
    */
    public static function request() {
        return Yii::app()->request;
    }

    /*
     *
     *
     */
    public static function nodejs($request = null) {
        if (!is_null($request)) {
            if (is_array($request) && (count($request)>0)) {
                $line = array();
                foreach ($request as $key => $value) {
                    $line[] = $key.'='.$value;
                }
                $nodejs_url = Y::param('nodejsurl') . '?' . implode('&', $line);
            } elseif (is_string($request)) {
                $nodejs_url = Y::param('nodejsurl') .($request[0]=='?' ? '' : '?').$request;
            } else {
                return false;
            }
            @file_get_contents($nodejs_url);
            return true;
        }
        return false;
    }
    
    /**
    * Проверка наличия роли у текущего пользователя
    *
    * @param string $roleName
    * @return bool
    * @throws AppException
    */
    //public static function checkAccess($roleName = null) {
    //    if (!$roleName) {
    //        throw new CException('Неизвестное название роли');
    //    }
    //    return Yii::app()->user->checkAccess($roleName);
    //}

    /**
    * @return mixed значение параметра из Yii::app()->params
    * @throws CException
    */
    public static function param($path = null) {
        if (is_null($path)) {
            return null;
        }
        $path=str_replace('.',"']['",$path);
        eval("\$value=Yii::app()->params['{$path}'];");
        return $value;
    }
    /**
    * @return bool значение параметра из Yii::app()->user
    */
    public static function hasState($state = null) {
        if (is_null($state)) {
            return false;
        }
        return Yii::app()->user->hasState($state);
    }
    /**
    * Перевод сообщения
    *
    * @see Yii::t
    * @return string
    */
    public static function t($category, $message, $params = array(), $source = null, $language = null) {
        return Yii::t($category, $message, $params, $source, $language);
    }

    /**
    * Сокращение для функции dump класса CVarDumper для отладки приложения
    *
    * @param mixed $var
    * @param bool $toDie завершить приложение после вывода
    */
    public static function dump($var, $toDie = true) {
        echo "<pre>";
        CVarDumper::dump($var, 10, true);
        echo "</pre>";
        if ($toDie) {
            Yii::app()->end();
        }
    }
 
    /**
    * Выводит текст и завершает приложение (применяется в ajax-действиях)
    *
    * @param string $text
    */
    public static function end($text = null) {
        if (isset($text)) echo $text;
        Yii::app()->end();
    }
 
    /**
    * Выводит данные в формате JSON и завершает приложение (применяется в ajax-действиях)
    *
    * @param string $data
    */
    public static function endJson($data) {
        echo CJSON::encode($data);
        Yii::app()->end();
    }
    
    public static function setCache($id,  $value, $expire=0, ICacheDependency $dependency=NULL) {
	return Yii::app()->cache->set($id,  $value, $expire, $dependency);
    }
    public static function getCache($id) {
	return Yii::app()->cache->get($id);
    }
    public static function deleteCache($id) {
	return Yii::app()->cache->delete($id);
    }
    
    public static function startTimer($id = 'default') {
        self::$timers[$id] = microtime(true);
    }
    
    public static function stopTimer($id = 'default') {
        return round((microtime(true) - self::$timers[$id]), 3 ) ;
    }
    
    public static function Cart() {
        return Yii::app()->shoppingCart;
    }
    
    public static function date() {
        return date('Y-m-d H:i:s', time());
    }
    /*
     * Функция логгирования действия пользователя
     * Идентификатор пользователя берется из сессии, либо -1 если пользователь не авторизован
     * Контроллер и экшен берется текущий
     * Если требуется указать конкретного пользователя, контроллер или экшен, то используйте Yii::app()->myuser->logAction(...), где первым параметром идет $user_id
     *
     * @property string $type - действие, которое логируется (в одном экшене может быть несколько логирований)
     * @property boolean $log_params  - флаг, дампить ли состояние глобальных переменных в базу. По умолчанию true
     *
     */
    public static function logAction($type = 'log', $log_params = true) {
        return Yii::app()->myuser->logAction(self::userId(), Yii::app()->getController()->getId(), Yii::app()->getController()->getAction()->getId(), $type, $log_params);
    }
    
    
    public static function getSetting($key) {
        return Yii::app()->settings->getValue($key);
    }
    
    public static function hasSetting($key) {
        return Yii::app()->settings->has($key);
    }
}
?>