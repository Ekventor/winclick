<div class="user-chart">
    <?php if (!empty($data)): ?>
        <?php
        $this->Widget('ext.widgets.highcharts.HighstockWidget', array(
            'options' => array(
                'title' => array('text' => 'Винклики'),

                'chart' => [
                    'events' => [
                        'load' => "js:function(){
                        var series = this.series[0];
                        setInterval(function(){
                            $.post('/statistic/winklik/getData', {}, function(result){
                                series.addPoint([(new Date(result.x)).getTime(), result.y], true, true);
                            }, 'json');
                        }, 600000);
                    }"
                    ]
                ],

                'plotOptions' => [
                    'line' => [
                        'dataGrouping' => [
                            'approximation' => 'sum'
                        ]
                    ]
                ],

                'yAxis' => array(
                    'title' => array('text' => 'Пополнений'),
                ),
                'series' => array(
                    array('name' => 'Винклики', 'data' => $data, 'type' => 'line'),
                ),
                'tooltip' => [
                    'xDateFormat' => '%d.%m.%Y %H:%M'
                ],
                'rangeSelector' => [
                    'selected' => 0,
                    'buttons' => [
                        ['type' => 'day', 'count' => 1, 'text' => 'д'],
                        ['type' => 'day', 'count' => 3, 'text' => '3д'],
                        ['type' => 'week', 'count' => 1, 'text' => 'н'],
                        ['type' => 'month', 'count' => 1, 'text' => '1м'],
                        ['type' => 'month', 'count' => 3, 'text' => '3м'],
                        ['type' => 'month', 'count' => 6, 'text' => '6м']
                    ]
                ]
            ),
            'setupOptions' => [
                'global' => [
                    'useUTC' => false
                ],
                'lang' => [
                    'months' => [
                        'Январь',
                        'Февраль',
                        'Март',
                        'Апрель',
                        'Май',
                        'Июнь',
                        'Июль',
                        'Август',
                        'Сентябрь',
                        'Октябрь',
                        'Ноябрь',
                        'Декабрь',
                    ],
                    'shortMonths' => [
                        'Янв',
                        'Фев',
                        'Мар',
                        'Апр',
                        'Май',
                        'Июн',
                        'Июл',
                        'Авг',
                        'Сен',
                        'Окт',
                        'Ноя',
                        'Дек',
                    ],
                    'weekdays' => [
                        'Воскресенье',
                        'Понедельник',
                        'Вторник',
                        'Среда',
                        'Четверг',
                        'Пятница',
                        'Суббота',
                    ]
                ]
            ]
        ));

        ?>
    <?php else: ?>
        Данные отсутствуют
    <?php endif; ?>


    <!--    <div class="highcharts-container">-->
    <!---->
    <!--    </div>-->

</div>
