<?= CHtml::beginForm('', 'POST', ['id' => 'params-form']); ?>

<?= CHtml::label('Номинал', 'nominal'); ?>
<?= CHtml::dropDownList('nominal', $nominal, Y::param('nominals')); ?>

<?= CHtml::label('Тип операции', 'type'); ?>
<?= CHtml::dropDownList('type', $type, ['win' => 'Выйграные карты', 'exch' => 'Обменянные карты']); ?>

<?= CHtml::endForm(); ?>

<script>
    $(document).ready(function () {
        $('form#params-form select').on('change', function () {
            $('form#params-form').submit();
        });
    });
</script>
<div class="user-chart">
    <?php if (!empty($data)): ?>
        <?php
        $this->Widget('ext.widgets.highcharts.HighstockWidget', array(
            'options' => array(
                'title' => array('text' => 'Карты'),

                'chart' => [
                    'events' => [
                        'load' => "js:function(){
                        var series = this.series[0],
                            \$form = $('#params-form'),
                            nominal = \$form.find('select#nominal').val(),
                            type = \$form.find('select#type').val(),
                            data = {
                                nominal: nominal,
                                type: type
                            };

                        setInterval(function(){
                            $.post('/statistic/giftcard/getData', data, function(result){
                                series.addPoint([(new Date(result.x)).getTime(), result.y], true, true);
                            }, 'json');
                        }, 600000);
                    }"
                    ]
                ],

                'plotOptions' => [
                    'line' => [
                        'dataGrouping' => [
                            'approximation' => 'sum'
                        ]
                    ]
                ],

                'yAxis' => array(
                    'title' => array('text' => 'Поступления'),
                ),
                'series' => array(
                    array('name' => 'Карты', 'data' => $data, 'type' => 'line'),
                ),
                'tooltip' => [
                    'xDateFormat' => '%d.%m.%Y %H:%M'
                ],
                'rangeSelector' => [
                    'selected' => 0,
                    'buttons' => [
                        ['type' => 'day', 'count' => 1, 'text' => 'д'],
                        ['type' => 'day', 'count' => 3, 'text' => '3д'],
                        ['type' => 'week', 'count' => 1, 'text' => 'н'],
                        ['type' => 'month', 'count' => 1, 'text' => '1м'],
                        ['type' => 'month', 'count' => 3, 'text' => '3м'],
                        ['type' => 'month', 'count' => 6, 'text' => '6м']
                    ]
                ]
            ),
            'setupOptions' => [
                'global' => [
                    'useUTC' => false
                ],
                'lang' => [
                    'months' => [
                        'Январь',
                        'Февраль',
                        'Март',
                        'Апрель',
                        'Май',
                        'Июнь',
                        'Июль',
                        'Август',
                        'Сентябрь',
                        'Октябрь',
                        'Ноябрь',
                        'Декабрь',
                    ],
                    'shortMonths' => [
                        'Янв',
                        'Фев',
                        'Мар',
                        'Апр',
                        'Май',
                        'Июн',
                        'Июл',
                        'Авг',
                        'Сен',
                        'Окт',
                        'Ноя',
                        'Дек',
                    ],
                    'weekdays' => [
                        'Воскресенье',
                        'Понедельник',
                        'Вторник',
                        'Среда',
                        'Четверг',
                        'Пятница',
                        'Суббота',
                    ]
                ]
            ]
        ));
        ?>
    <?php else: ?>
        Данные отсутствуют
    <?php endif; ?>


    <!--    <div class="highcharts-container">-->
    <!---->
    <!--    </div>-->

</div>
