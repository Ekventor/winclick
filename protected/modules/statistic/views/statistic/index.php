<table class="tranzaction-table" style="text-align: center;">
    <thead>
        <tr>
            <th rowspan="2">№ <br>п/п</th>
            <th rowspan="2">Показатель</th>
            <th rowspan="2">Еденица <br>измерения</th>
            <th colspan="2">Текущие сутки</th>
            <th rowspan="2">Всего</th>
        </tr>
        <tr>
            <th rowspan="1">C 00.00 мв</th>
            <th rowspan="1">+</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($data as $row): ?>
        <tr>
            <td><?= $row['number'] ?></td>
            <td><?= $row['label'] ?></td>
            <td><?= $row['unit'] ?></td>
            <td><?= $row['todayCount'] ?></td>
            <td id="<?= $row['id']; ?>">0</td>
            <td><?= $row['summaryCount'] ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<script>
    $(document).ready(function() {
        var today = '<?= $currentDate; ?>';
        setInterval(function() {
            $.post('<?= $this->createUrl('getData')?>', {date: today}, function(result) {
                if (result.data) {
                    for (var index in result.data) {
                        $("#" + index).html(result.data[index]);
                    }
                }
            }, 'json');
        }, 600000);
    });
</script>