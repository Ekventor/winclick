<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

    <div class="content-wrap admin">
        <div class="content-head">
            <div class="content-head-right">
                <p>Статистика</p>
            </div>
        </div>
        <div class="content" style="width: 100%;"> <!-- content start -->

            <div class="left-section">
                <div id="sidebar">
                    <?php
                        $this->widget('zii.widgets.CMenu', [
                            'activateParents' => true,
                            'activeCssClass' => 'active',
                            'items'=>[
                                ['label' => 'Пользователи', 'url' => ['/statistic/user']],
//                                ['label' => 'Деньги', 'url' => ['/statistic/money']],
                                ['label' => 'Винклики', 'url' => ['/statistic/winklik']],
//                                ['label' => 'Подарочные карты', 'url' => ['/statistic/giftcard']],
                                ['label' => 'Общая статистика', 'url' => ['/statistic/statistic']]
                            ],
                            'htmlOptions'=>array('class'=>'left-section-ul'),
                        ]);
                    ?>
                </div><!-- sidebar -->
            </div>
            <div class="right-section admin">
                <?php echo $content; ?>
            </div>
            <div style="clear: both;"></div>
        </div> <!-- content end -->
    </div>
<?php $this->endContent(); ?>