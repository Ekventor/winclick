<?php

class DefaultController extends SController
{
	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->render('index');
	}
}