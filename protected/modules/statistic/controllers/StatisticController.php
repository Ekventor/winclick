<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/22/14
 * Time: 12:53 PM
 */
class StatisticController extends SController
{
    public $layout = '/layouts/column2';

    public $currentDate = '';

    public function actionIndex()
    {
        $this->currentDate = date('Y-m-d H:i:s');
        $statisticComponent = $this->module->statistic;
        $data = $statisticComponent->getData();
        $this->render('index', ['data' => $statisticComponent->getData(), 'currentDate' => $this->currentDate]);
    }

    public function actionGetData()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $date = Yii::app()->request->getParam('date', 0);
            if ($date) {
                $currentDate = date('Y-m-d H:i:s');
                $statisticComponent = $this->module->statistic;
                $data = $statisticComponent->getIntervalData($date, $currentDate);
                Y::endJson(['data' => $data]);
            }
        }
    }
}