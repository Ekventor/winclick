<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/18/14
 * Time: 5:24 PM
 */
class GiftcardController extends Controller
{
    public $layout = '/layouts/column2';

    public function actionIndex()
    {
        $nominal = Yii::app()->request->getParam('nominal', 0);
        $type = Yii::app()->request->getParam('type', 'win');
        $nominals = Y::param('nominals');

        if ($type == 'win') {
            $accountHistory = AccountHistory::model()->findAll('amount > :amount and type = :type', [':amount' => 0, ':type' => 'CARD' . $nominals[$nominal]]);
            $sql = "SELECT MIN(date)::text FROM account_history WHERE amount > 0 AND type = 'CARD" . $nominals[$nominal] . "'";
            $minDate = Yii::app()->db->createCommand($sql)->queryScalar();

            $resultDate = [];
            foreach ($accountHistory as $action) {
                $createData = $action->date;
                $createDataFormated = date('Y-m-d H:i', strtotime($createData));
                $datetime = new DateTime($createDataFormated);
                $minutes = (int) $datetime->format('i');
                $needMin = 10 - $minutes % 10;
                $datetime->modify('+' . $needMin . ' min');
                if (!isset($resultDate[$datetime->format('Y-m-d H:i')])) {
                    $resultDate[$datetime->format('Y-m-d H:i')] = 0;
                }
                $resultDate[$datetime->format('Y-m-d H:i')] += ($action->amount / $nominals[$nominal]);
            }


        } else if ($type == 'exch') {

            $userGiftcards = UserGiftcards::model()->findAll('status = :status and nominal = :nominal', [':status' => 1, ':nominal' => $nominals[$nominal]]);
            $sql = "SELECT MIN(date)::text FROM user_giftcards WHERE status = 1 AND nominal = " . $nominals[$nominal];
            $minDate = Yii::app()->db->createCommand($sql)->queryScalar();

            $resultDate = [];
            foreach ($userGiftcards as $giftcard) {
                $createData = $giftcard->date;
                $createDataFormated = date('Y-m-d H:i', strtotime($createData));
                $datetime = new DateTime($createDataFormated);
                $minutes = (int) $datetime->format('i');
                $needMin = 10 - $minutes % 10;
                $datetime->modify('+' . $needMin . ' min');
                if (!isset($resultDate[$datetime->format('Y-m-d H:i')])) {
                    $resultDate[$datetime->format('Y-m-d H:i')] = 0;
                }
                $resultDate[$datetime->format('Y-m-d H:i')] += $giftcard->count;
            }

        }

        $result = [];
        $currentDate = new DateTime(date('Y-m-d H:i'));
        $minutes = (int) $currentDate->format('i');
        $needMin = $minutes % 10;
        $currentDate->modify(' -' . $needMin . ' min');
        $resultData = [];
        if ($minDate) {
            while ($currentDate->getTimestamp() > strtotime($minDate)) {
                $result[$currentDate->format('Y-m-d H:i')] = isset($resultDate[$currentDate->format('Y-m-d H:i')]) ? $resultDate[$currentDate->format('Y-m-d H:i')] : 0;
                $currentDate->modify('-10 min');
            }

            $reverseData = array_reverse($result, true);

            foreach ($reverseData as $key => $reverse) {
                $resultData[] = [strtotime($key)*1000, $reverse];
            }
        }

        $this->render('index', [
            'nominal' => $nominal,
            'type' => $type,
            'data' => $resultData
        ]);
    }

    public function actionGetData()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $nominal = Yii::app()->request->getParam('nominal', 0);
            $type = Yii::app()->request->getParam('type', 'win');
            $nominals = Y::param('nominals');
            $date = new DateTime();
            $minutes = (int) $date->format('i');
            $needMinutes = $minutes % 10;
            $date->modify('-' . $needMinutes . ' min');
            $dateFinish = $date->format('Y-m-d H:i');
            $date->modify('-10 min');
            $dateStart = $date->format('Y-m-d H:i');
            if ($type == 'win') {
                $sql = "SELECT SUM(amount) FROM account_history WHERE date BETWEEN '" . $dateStart . "' AND '" . $dateFinish . "' AND amount > 0 AND type = 'CARD" . $nominals[$nominal] . "'";
            } else if ($type == 'exch') {
                $sql = "SELECT SUM(count) FROM user_giftcards WHERE date BETWEEN '" . $dateStart . "' AND '" . $dateFinish . "' AND status = 1 AND nominal = " . $nominals[$nominal];
            }
            $amount = Yii::app()->db->createCommand($sql)->queryScalar() !== null ? Yii::app()->db->createCommand($sql)->queryScalar() : 0;
            Y::endJson(['x' => date('d F Y H:i', strtotime($dateFinish)), 'y' => $amount]);
        }

    }
}