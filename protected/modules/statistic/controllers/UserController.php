<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/5/14
 * Time: 4:21 PM
*/
class UserController extends SController
{
    public $layout = '/layouts/column2';

    public function actionIndex()
    {

        $users = Users::model()->findAllByAttributes(['activated' => true]);
        $sql = "select min(created)::text from users where activated = true";
        $minDate = Yii::app()->db->createCommand($sql)->queryScalar();

        $resultDate = [];
        foreach ($users as $user) {
            $createData = $user->created;
            $createDataFormated = date('Y-m-d H:i', strtotime($createData));
            $datetime = new DateTime($createDataFormated);
            $minutes = (int) $datetime->format('i');
            $needMin = 10 - $minutes % 10;
            $datetime->modify('+' . $needMin . ' min');
            if (!isset($resultDate[$datetime->format('Y-m-d H:i')])) {
                $resultDate[$datetime->format('Y-m-d H:i')] = 0;
            }
            $resultDate[$datetime->format('Y-m-d H:i')]++;
        }

        $result = [];
        $currentDate = new DateTime(date('Y-m-d H:i'));
        $minutes = (int) $currentDate->format('i');
        $needMin = $minutes % 10;
        $currentDate->modify(' -' . $needMin . ' min');
        $resultData = [];
        if ($minDate) {
            while ($currentDate->getTimestamp() > strtotime($minDate)) {
                $result[$currentDate->format('Y-m-d H:i')] = isset($resultDate[$currentDate->format('Y-m-d H:i')]) ? $resultDate[$currentDate->format('Y-m-d H:i')] : 0;
                $currentDate->modify('-10 min');
            }

            $reverseData = array_reverse($result, true);


            foreach ($reverseData as $key => $reverse) {
                $resultData[] = [strtotime($key)*1000, $reverse];
            }
        }

        $this->render('index', [
            'data' => $resultData
        ]);
    }

    public function actionGetData()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $date = new DateTime();
            $minutes = (int) $date->format('i');
            $needMinutes = $minutes % 10;
            $date->modify('-' . $needMinutes . ' min');
            $dateFinish = $date->format('Y-m-d H:i');
            $date->modify('-10 min');
            $dateStart = $date->format('Y-m-d H:i');
            $sql = "select count(id) from users where created between '" . $dateStart . "' and '" . $dateFinish . "' and activated = true";
            Y::endJson(['x' => date('d F Y H:i', strtotime($dateFinish)), 'y' => Yii::app()->db->createCommand($sql)->queryScalar()]);
        }
    }
}