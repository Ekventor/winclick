<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/5/14
 * Time: 4:22 PM
 */
class WinklikController extends SController
{
    public $layout = '/layouts/column2';


    public function actionIndex()
    {
        $accountHistory = AccountHistory::model()->findAll('amount > :amount and type = :type', [':amount' => 0, ':type' => 'WK']);
        $sql = "SELECT MIN(date)::text FROM account_history WHERE amount > 0 AND type = 'WK'";
        $minDate = Yii::app()->db->createCommand($sql)->queryScalar();

        $resultDates = [];
        foreach ($accountHistory as $action) {
            $createData = $action->date;
            $createDataFormated = date('Y-m-d H:i', strtotime($createData));
            $datetime = new DateTime($createDataFormated);
            $minutes = (int) $datetime->format('i');
            $needMin = 10 - $minutes % 10;
            $datetime->modify('+' . $needMin . ' min');
            if (!isset($resultDates[$datetime->format('Y-m-d H:i')])) {
                $resultDates[$datetime->format('Y-m-d H:i')] = 0;
            }
            $resultDates[$datetime->format('Y-m-d H:i')] += $action->amount;
        }

        $result = [];
        $currentDate = new DateTime(date('Y-m-d H:i'));
        $minutes = (int) $currentDate->format('i');
        $needMin = $minutes % 10;
        $currentDate->modify(' -' . $needMin . ' min');
        $resultData = [];
        if ($minDate) {
            while ($currentDate->getTimestamp() > strtotime($minDate)) {
                $result[$currentDate->format('Y-m-d H:i')] = isset($resultDates[$currentDate->format('Y-m-d H:i')]) ? $resultDates[$currentDate->format('Y-m-d H:i')] : 0;
                $currentDate->modify('-10 min');
            }

            $reverseData = array_reverse($result, true);


            foreach ($reverseData as $key => $reverse) {
                $resultData[] = [strtotime($key)*1000, $reverse];
            }
        }

        $this->render('index', [
            'data' => $resultData
        ]);

    }

    public function actionGetData()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $date = new DateTime();
            $minutes = (int) $date->format('i');
            $needMinutes = $minutes % 10;
            $date->modify('-' . $needMinutes . ' min');
            $dateFinish = $date->format('Y-m-d H:i');
            $date->modify('-10 min');
            $dateStart = $date->format('Y-m-d H:i');
            $sql = "SELECT SUM(amount) FROM account_history WHERE date BETWEEN '" . $dateStart . "' AND '" . $dateFinish . "' AND amount > 0 AND type = 'WK'";
            $amount = Yii::app()->db->createCommand($sql)->queryScalar() !== null ? Yii::app()->db->createCommand($sql)->queryScalar() : 0;
            Y::endJson(['x' => date('d F Y H:i', strtotime($dateFinish)), 'y' => $amount]);
        }

    }
}