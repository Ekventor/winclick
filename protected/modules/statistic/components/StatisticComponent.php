<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/22/14
 * Time: 2:22 PM
 */
class StatisticComponent extends CApplicationComponent
{
    /**
     * @return array
     */
    public function getData()
    {
        $result = array_merge(
            $this->getUserData(),
//            $this->getMoneyData(),
            $this->getWinklikData(),
//            $this->getUniversalCard()
            $this->getGiftcard()
//            $this->getDoubledWinklikData()
        );
        return $result;
    }

    /**
     * @return array
     */
    private function getUserData()
    {
        $result = [];
        $userSummarySql = 'SELECT id FROM users WHERE activated = TRUE';
        $userSummaryIds = Yii::app()->db->createCommand($userSummarySql)->queryColumn();
        $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));
        $userTodaySql = "SELECT id FROM users WHERE activated = TRUE AND created > '" . $date . "'";
        $userTodayIds = Yii::app()->db->createCommand($userTodaySql)->queryColumn();
        $result[] = [
            'id' => 'userCount',
            'number' => '1.',
            'label' => 'Зарегестрированные пользователи',
            'unit' => 'чел.',
            'todayCount' => count($userTodayIds) ? count($userTodayIds) : 0,
            'summaryCount' => count($userSummaryIds) ? count($userSummaryIds) : 0
        ];

        $subStrUserSummary = count($userSummaryIds) ? ' AND user_id IN (' . implode(',', $userSummaryIds) . ')' : ' AND user_id IN (NULL)';
        $subStrUserToday = count($userTodayIds) ? ' AND user_id IN (' .implode(',', $userTodayIds) . ')' : ' AND user_id IN (NULL)';

        $activeUserSummarySql = "SELECT COUNT(DISTINCT(user_id)) from account_history where amount > 0 AND type = 'WK'" . $subStrUserSummary;
        $activeUserSummaryCount = Yii::app()->db->createCommand($activeUserSummarySql)->queryScalar();
        $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));
        $activeUserTodaySql = "SELECT COUNT(DISTINCT(user_id)) from account_history where amount > 0 AND type = 'WK' AND date > '" . $date . "'" . $subStrUserToday;
        $activeUserTodayCount = Yii::app()->db->createCommand($activeUserTodaySql)->queryScalar();
        $result[] = [
            'id' => 'activeUserCount',
            'number' => '1.1.',
            'label' => 'в том числе активные (купившие wk)',
            'unit' => 'чел.',
            'todayCount' => $activeUserTodayCount ? $activeUserTodayCount : 0,
            'summaryCount' => $activeUserSummaryCount ? $activeUserSummaryCount : 0
        ];
        return $result;
    }

    /**
     * @return array
     */
    private function getMoneyData()
    {
        $result = [];
        $moneySummarySql = "SELECT SUM(amount) from account_history where amount > 0 AND type = 'RUB'";
        $moneySummaryAmount = Yii::app()->db->createCommand($moneySummarySql)->queryScalar();
        $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));
        $moneyTodaySql = "SELECT SUM(amount) from account_history where amount > 0 AND type = 'RUB' AND date > '" . $date . "'";
        $moneyTodayAmount = Yii::app()->db->createCommand($moneyTodaySql)->queryScalar();
        $result[] = [
            'id' => 'moneyAmount',
            'number' => '2.',
            'label' => 'Поступило денег',
            'unit' => 'руб.',
            'todayCount' => $moneyTodayAmount ? $moneyTodayAmount : 0,
            'summaryCount' => $moneySummaryAmount ? $moneySummaryAmount : 0
        ];
        return $result;
    }

    /**
     * @return array
     */
    private function getWinklikData()
    {
        $result = [];
        $wkSummarySql = "SELECT SUM(amount) from account_history where amount > 0 AND type = 'WK'";
        $wkSummaryAmount = Yii::app()->db->createCommand($wkSummarySql)->queryScalar();
        $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));
        $wkTodaySql = "SELECT SUM(amount) from account_history where amount > 0 AND type = 'WK' AND date > '" . $date . "'";
        $wkTodayAmount = Yii::app()->db->createCommand($wkTodaySql)->queryScalar();
        $result[] = [
            'id' => 'wkAmount',
            'number' => '2.',
            'label' => 'Приобретено винкликов',
            'unit' => 'wk',
            'todayCount' => $wkTodayAmount ? $wkTodayAmount : 0,
            'summaryCount' => $wkSummaryAmount ? $wkSummaryAmount : 0
        ];
        return $result;
    }

    /**
     * @return array
     */
    private function getUniversalCard()
    {
        $result = [];
        // @todo сумма по выйграным картам
        $result[] = [
            'id' => 'universalCardAmount',
            'number' => '4.',
            'label' => 'Полученные универсальные подарочные карты (на аукционе)',
            'unit' => 'руб.',
            'todayCount' => 0,
            'summaryCount' => 0
        ];

        $nominals = Y::param('nominals');
        $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));
        $sumSummary = 0;
        $sumToday = 0;

        foreach ($nominals as $key => $nominal) {
            $type = 'CARD' . $nominal;
            $cardsSummary = AccountHistory::model()->findAll('type = :type AND amount > 0', [':type' => $type]);
            $cardsSummaryResult = [];
            foreach($cardsSummary as $card) {
                if (!isset($cardsSummaryResult[$card->user_id])) {
                    $cardsSummaryResult[$card->user_id] = 0;
                }
                $cardsSummaryResult[$card->user_id] += $card->amount;
            }
            $cardSummaryCount = 0;
            foreach ($cardsSummaryResult as $amount) {
                $cardSummaryCount += floor($amount / $nominal);
            }

            $cardsToday = AccountHistory::model()->findAll('type = :type AND amount > 0 AND date > :date', [':type' => $type, ':date' => $date]);
            $cardsTodayResult = [];
            foreach($cardsToday as $card) {
                if (!isset($cardsTodayResult[$card->user_id])) {
                    $cardsTodayResult[$card->user_id] = 0;
                }
                $cardsTodayResult[$card->user_id] += $card->amount;
            }
            $cardTodayCount = 0;
            foreach ($cardsTodayResult as $amount) {
                $cardTodayCount += floor($amount / $nominal);
            }
            $sumSummary += $cardSummaryCount * $nominal;
            $sumToday += $cardTodayCount * $nominal;
            $result[] = [
                'id' => 'universalCardCount' . $nominal,
                'number' => '4.' . ($key + 1) . '.',
                'label' => 'Номинал ' . $nominal . ' руб.',
                'unit' => 'шт.',
                'todayCount' => $cardTodayCount,
                'summaryCount' => $cardSummaryCount

            ];
        }
        $result[0]['todayCount'] = $sumToday;
        $result[0]['summaryCount'] = $sumSummary;

        return $result;
    }

    /**
     * @return array
     */
    private function getGiftcard()
    {
        $result = [];
        // @todo сумма по полученным картам
        $nominals = Y::param('nominals');
        $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));

        $sumSummary = 0;
        $sumToday = 0;
//        foreach ($nominals as $key => $nominal) {
            $cardsSummarySql = "SELECT SUM(count) FROM user_giftcards WHERE status = 1";
            $cardsSummaryCount = Yii::app()->db->createCommand($cardsSummarySql)->queryScalar();
            $cardsTodaySql = "SELECT SUM(count) FROM user_giftcards WHERE status = 1 AND date > '" . $date . "'";
            $cardsTodayCount = Yii::app()->db->createCommand($cardsTodaySql)->queryScalar();
//            $result[] = [
//                'id' => 'giftcardCount' . $nominal,
//                'number' => '5.' . ($key + 1) . '.',
//                'label' => 'Номинал ' . $nominal . ' руб.',
//                'unit' => 'шт.',
//                'todayCount' => $cardsTodayCount ? $cardsTodayCount : 0,
//                'summaryCount' => $cardsSummaryCount ? $cardsSummaryCount : 0
//            ];
//            $sumSummary += $cardsSummaryCount * $nominal;
//            $sumToday += $cardsTodayCount * $nominal;
//        }
        $result[] = [
            'id' => 'giftcardAmount',
            'number' => '3.',
            'label' => 'Полученные сертификаты и карты',
            'unit' => 'руб.',
            'todayCount' => $cardsTodayCount,
            'summaryCount' => $cardsSummaryCount
        ];

//        $result['todayCount'] = $cardsTodayCount;
//        $result['summaryCount'] = $cardsSummaryCount;
        return $result;
    }

//    public function getDoubledWinklikData()
//    {
//        $result = [];
//        $moneySummarySql = "SELECT SUM(amount) from account_history where amount > 0 AND type = 'WK'";
//        $moneySummaryAmount = Yii::app()->db->createCommand($moneySummarySql)->queryScalar();
//        $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));
//        $moneyTodaySql = "SELECT SUM(amount) from account_history where amount > 0 AND type = 'WK' AND date > '" . $date . "'";
//        $moneyTodayAmount = Yii::app()->db->createCommand($moneyTodaySql)->queryScalar();
//        $result[] = [
//            'id' => 'doubleAmount',
//            'number' => '2.',
//            'label' => 'Удвоено винкликов',
//            'unit' => 'руб.',
//            'todayCount' => $moneyTodayAmount ? $moneyTodayAmount : 0,
//            'summaryCount' => $moneySummaryAmount ? $moneySummaryAmount : 0
//        ];
//        return $result;
//
//    }

    /**
     * @param $dateStart
     * @param $dateFinish
     * @return array
     */
    public function getIntervalData($dateStart, $dateFinish) {
        $result = [];

        $userSql = "SELECT id FROM users WHERE activated = TRUE AND created BETWEEN '" . $dateStart . "' AND '" . $dateFinish . "'";
        $userIds = Yii::app()->db->createCommand($userSql)->queryColumn();
        $result['userCount'] = count($userIds);

        $subStrActiveUserSql = count($userIds) ? ' AND user_id IN (' . implode(',', $userIds) . ')' : ' AND user_id IN (NULL)';
        $activeUserSql = "SELECT COUNT(DISTINCT(user_id)) from account_history where amount > 0 AND type = 'WK' AND date BETWEEN '"
            . $dateStart . "' AND '" .$dateFinish . "'" . $subStrActiveUserSql;
        $activeUserCount = Yii::app()->db->createCommand($activeUserSql)->queryScalar();
        $result['activeUserCount'] = $activeUserCount;


//        $moneySql = "SELECT SUM(amount) from account_history where amount > 0 AND type = 'RUB' AND date BETWEEN '" . $dateStart . "' AND '" . $dateFinish . "'";
//        $moneyAmount = Yii::app()->db->createCommand($moneySql)->queryScalar();
//        $result['moneyAmount'] = $moneyAmount ? $moneyAmount : 0;

        $wkSql = "SELECT SUM(amount) from account_history where amount > 0 AND type = 'WK' AND date BETWEEN '" . $dateStart . "' AND '" . $dateFinish . "'";
        $wkAmount = Yii::app()->db->createCommand($wkSql)->queryScalar();
        $result['wkAmount'] = $wkAmount ? $wkAmount : 0;

//        $nominals = Y::param('nominals');
//        $sumSummary = 0;
//        foreach ($nominals as $key => $nominal) {
//            $type = 'CARD' . $nominal;
//            $criteria = new CDbCriteria;
//            $criteria->addBetweenCondition('date', $dateStart, $dateFinish);
//            $criteria->compare('type', $type);
//            $criteria->addCondition('amount > 0');
//            $cardsSummary = AccountHistory::model()->findAll($criteria);
//            $cardsSummaryResult = [];
//            foreach($cardsSummary as $card) {
//                if (!isset($cardsSummaryResult[$card->user_id])) {
//                    $cardsSummaryResult[$card->user_id] = 0;
//                }
//                $cardsSummaryResult[$card->user_id] += $card->amount;
//            }
//            $cardSummaryCount = 0;
//            foreach ($cardsSummaryResult as $amount) {
//                $cardSummaryCount += floor($amount / $nominal);
//            }
//            $result['universalCardCount' . $nominal] = $cardSummaryCount;
//
//            $sumSummary += $cardSummaryCount * $nominal;
//        }
//        $result['universalCardAmount'] = $sumSummary;


//        $nominals = Y::param('nominals');
        $sumSummary = 0;
//        foreach ($nominals as $key => $nominal) {
            $cardsSummarySql = "SELECT SUM(count) FROM user_giftcards WHERE status = 1 AND date BETWEEN '" . $dateStart . "' AND '" . $dateFinish . "'";
            $cardsSummaryCount = Yii::app()->db->createCommand($cardsSummarySql)->queryScalar();
//            $sumSummary += $cardsSummaryCount * $nominal;
//            $result['giftcardCount' . $nominal] = $cardsSummaryCount ? $cardsSummaryCount : 0;
//        }
        $result['giftcardAmount'] = $cardsSummaryCount ? $cardsSummaryCount : 0;

        return $result;

    }


}