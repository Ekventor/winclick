<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 09.07.14
 * Time: 12:54
 */
class LoginController extends CController
{
    public function actionIndex()
    {
        $this->layout = '/layouts/login';

        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(array('/backend'));
        }
        // display the login form
        $this->render('index',array('model'=>$model));
    }
}