<?php

class OrganizationController extends AController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/columnAdmin';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'users' => array('@'),
                'roles' => array('admin', 'content')
            ),
            array('deny',  // deny all users
                'users' => array('*')
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $citiesId = Yii::app()->db->createCommand()
            ->select('city_id')
            ->from('city_organization_link')
            ->where('organization_id = '. $id)
            ->queryColumn();

        $stringCity = '';
        if (count($citiesId)) {
            $cities = Yii::app()->db->createCommand()
                ->select('name')
                ->from('city')
                ->where(['in', 'id', $citiesId])
                ->queryColumn();

            $stringCity = implode(', ', $cities);
        }
        $this->render('view', array(
            'cities' => $stringCity,
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Organization;

        $activeCity = null;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $criteria = new CDbCriteria();
        $criteria->order = 'name';

        $cities = City::model()->findAll($criteria);
        $categories = Category::model()->findAll($criteria);

        foreach ($cities as $city) {
            $cityArray[$city->id] = $city->name;
        }

        $categoryArray[] = 'Нет категории';
        foreach ($categories as $category) {
            $categoryArray[$category->id] = $category->name;
        }

        $image = new ImageUpload;

        if (isset($_POST['Organization'])) {
            $model->attributes = $_POST['Organization'];

            if (!empty($_POST['ImageUpload'])) {
                $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('orgImagePath');
                if (!is_dir($path)) {
                    mkdir($path);
                }

                if (!is_null(CUploadedFile::getInstance($image, 'image'))) {
                    $image = new ImageManager(CUploadedFile::getInstance($image, 'image'));
                    $fname = DIRECTORY_SEPARATOR . date('Ymd_His') . '.jpg';
                    $image->saveResized(240, 149, $path . $fname, true);
                    $url = Y::param('orgImagePath') . $fname;
                    $model->img = $url;
                }
            }

            if ($model->save()) {
                $citiesId = Yii::app()->request->getParam('cities_id', []);
                if (count($citiesId)) {
                    foreach ($citiesId as $cityId) {
                        $array = [
                            'city_id' => $cityId,
                            'organization_id' => $model->id
                        ];
                        Yii::app()->db->createCommand()
                            ->insert('city_organization_link', $array);
                    }
                }
                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('create', array(
            'model' => $model,
            'cities' => $cityArray,
            'categories' => $categoryArray,
            'image' => $image,
            'activeCity' => $activeCity
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $activeCity = Yii::app()->db->createCommand()
            ->select('city_id')
            ->from('city_organization_link')
            ->where('organization_id = '. $model->id)
            ->queryColumn();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $criteria = new CDbCriteria();
        $criteria->order = 'name';

        $cities = City::model()->findAll($criteria);
        $categories = Category::model()->findAll($criteria);

        $cityArray[] = 'Нет города';
        foreach ($cities as $city) {
            $cityArray[$city->id] = $city->name;
        }

        $categoryArray[] = 'Нет категории';
        foreach ($categories as $category) {
            $categoryArray[$category->id] = $category->name;
        }

        $image = new ImageUpload;

        if (isset($_POST['Organization'])) {
            $model->attributes = $_POST['Organization'];

            if (!empty($_POST['ImageUpload'])) {
                $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('orgImagePath');
                if (!is_dir($path)) {
                    mkdir($path);
                }

                if (!is_null(CUploadedFile::getInstance($image, 'image'))) {
                    $image = new ImageManager(CUploadedFile::getInstance($image, 'image'));
                    $fname = DIRECTORY_SEPARATOR . date('Ymd_His') . '.jpg';
                    $image->saveResized(240, 149, $path . $fname, true);
                    $url = Y::param('orgImagePath') . $fname;
                    $model->img = $url;
                }
            }

            if ($model->save()) {
                $citiesId = Yii::app()->request->getParam('cities_id', []);
                if (count($citiesId)) {
                    Yii::app()->db->createCommand()
                        ->delete('city_organization_link', 'organization_id = :id', [':id' => $model->id]);
                    foreach ($citiesId as $cityId) {
                        $array = [
                            'city_id' => $cityId,
                            'organization_id' => $model->id
                        ];
                        Yii::app()->db->createCommand()
                            ->insert('city_organization_link', $array);
                    }
                }
                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('update', array(
            'model' => $model,
            'cities' => $cityArray,
            'categories' => $categoryArray,
            'image' => $image,
            'activeCity' => count($activeCity) ? $activeCity : null
        ));
    }

    public function actionImages()
    {
        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);

        if ($_FILES['file']['type'] == 'image/png'
            || $_FILES['file']['type'] == 'image/jpg'
            || $_FILES['file']['type'] == 'image/gif'
            || $_FILES['file']['type'] == 'image/jpeg'
            || $_FILES['file']['type'] == 'image/pjpeg'
        ) {

            $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('orgImagePath');
            if (!is_dir($path)) {
                mkdir($path);
            }

            $fname = DIRECTORY_SEPARATOR . 'org_' . date('Ymd_His') . '.jpg';;

            // copying
            move_uploaded_file($_FILES['file']['tmp_name'], $path . $fname);

            $this->renderPartial('images', [
                'src' => Y::param('orgImagePath') . $fname
            ]);
        }
    }

    public function actionFiles()
    {
        if (isset($_FILES['file'])) {
            if ($_FILES['file']['size'] > 10000000) {
                throw new CException('файл слишком велик');
            }

            $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('orgFilePath');
            if (!is_dir($path)) {
                mkdir($path);
            }

            $fname = '/' . $_FILES['file']['name'];

            move_uploaded_file($_FILES['file']['tmp_name'], $path . $fname);

            $this->renderPartial('files', [
                'fileName' => $_FILES['file']['name'],
                'url' => Y::param('orgFilePath') . $fname
            ]);
        } else {
            throw new CException('Невозможно загрузить файл');
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Organization');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Organization('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Organization']))
            $model->attributes = $_GET['Organization'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Organization the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Organization::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Organization $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'organization-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
