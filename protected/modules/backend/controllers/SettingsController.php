<?php

class SettingsController extends AController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'users'=>array('@'),
                'roles'=>array('admin')
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
	}
	
	public function actionBonus() {
		$exeptions = ['coupon_live'];
		$criteria = new CDbCriteria();
		$criteria->compare('type', 'coupon');
		$criteria->addNotInCondition('key', $exeptions);
		$criteria->order = 'id ASC';
		$this->renderPartial('bonus', ['data'=>Settings::model()->findAll($criteria)]);
	}

	public function actionGiftcard()
	{
		$model = new ImageUpload;
		if (!empty($_POST['ImageUpload'])) {
			//handle images
			$path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..'. Y::param('certPath');
			if (!is_dir($path)) {
				mkdir($path);
			}
			if (!is_null(CUploadedFile::getInstance($model, 'image[present]'))) {
				$image = new ImageManager(CUploadedFile::getInstance($model, 'image[present]'));
				$fname = DIRECTORY_SEPARATOR . 'present.jpg';
				$image->saveResized(243, 143, $path . $fname, true);
			}

			$this->redirect('/backend/settings');
		}

		$criteria = new CDbCriteria();
		$criteria->compare('type', 'giftcard');
		$criteria->order = 'id ASC';
		$this->renderPartial('giftcard', [
			'data'=>Settings::model()->findAll($criteria),
			'model' => $model
		]);
	}

	public function missingAction($actionId)
	{
	    $exeptions = ['transaction_ttl', 'slogan'];
		$criteria = new CDbCriteria();
	    $criteria->compare('type', $actionId);
	    $criteria->order = 'id ASC';
		$criteria->addNotInCondition('key', $exeptions);
	    $settings = new Settings;
	    $dataProvider = new CActiveDataProvider($settings, [
		'criteria' => $criteria,
		'pagination'=> false,
	    ]);
	    $this->renderPartial('type', [
		'dataProvider' => $dataProvider
	    ]);
	}

    /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Settings;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Settings']))
		{
			$model->attributes=$_POST['Settings'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Settings']))
		{
			
			$model->attributes=$_POST['Settings'];
			if ($model->key == 'possible_letters') {
				Yii::app()->settings->handleLetters($_POST['Settings']['value']);
			} else {
				
				if($model->validate() && $model->save()) {
					switch($model->type) {
						case 'demo': //настройки последовательностей
						case 'seq':
							Yii::app()->game->sendSettings();
						break;
						case 'game': // настройки системные
							Yii::app()->account->sendSettings();
						break;
					}
					
					//$this->redirect(array('index'));
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Settings');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

    public function actionSite()
    {
        $positions = ['left', 'center', 'right', 'top', 'reg'];
		$exeptions = ['transaction_ttl', 'slogan'];
		$criteria = new CDbCriteria();
        $criteria->compare('type', 'site');
		$criteria->addNotInCondition('key', $exeptions);
        $criteria->order = 'id ASC';
        $settings = new Settings;
        $dataProvider = new CActiveDataProvider($settings, [
            'criteria' => $criteria,
            'pagination'=> false,
        ]);

        $model = new ImageUpload;
        if (!empty($_POST['ImageUpload'])) {
            //handle images
            $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('infoPath');
            if (!is_dir($path)) {
                mkdir($path);
            }
            foreach ($positions as $position) {
                if (!is_null(CUploadedFile::getInstance($model, 'image[' . $position . ']'))) {
                    $image = new ImageManager(CUploadedFile::getInstance($model, 'image[' . $position . ']'));
                    $fname = DIRECTORY_SEPARATOR . $position . '.png';
                    if ($position != 'top') {
						$image->save($path . $fname);
					} else {
						$image->saveResized(945, 277, $path . $fname, true);
					}
                }
            }

            $this->redirect('/backend/settings');
        }

        $this->renderPartial('site', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

	public function actionImages() {
		$model = new ImageUpload;
		if (!empty($_POST['ImageUpload'])) {
			//handle images
			$path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..'. Y::param('certPath');
			if (!is_dir($path)) {
				mkdir($path);
			}
			foreach(Y::param('nominals') as $nominal) {
				foreach(array_merge(['!', 'default'], str_split(Y::getSetting('possible_letters'))) as $type) {
					if (!is_null(CUploadedFile::getInstance($model, 'image['.$nominal.']['.$type.']'))) {
						$image = new ImageManager(CUploadedFile::getInstance($model, 'image['.$nominal.']['.$type.']'));
						$fname = DIRECTORY_SEPARATOR . $nominal.$type.'.jpg';
						$image->saveResized(Y::param('certImage.width'), Y::param('certImage.height'), $path . $fname, true);
					}
				}
				
			}
			
				if (!is_null(CUploadedFile::getInstance($model, 'image[default]'))) {
						$image = new ImageManager(CUploadedFile::getInstance($model, 'image[default]'));
						$fname = DIRECTORY_SEPARATOR . 'default.jpg';
						$image->saveResized(Y::param('certImage.width'), Y::param('certImage.height'), $path . $fname, true);
				}
					
			$this->redirect('/backend/settings');
		}
		
		$this->renderPartial('_images', [
			'model' => $model,
			'letters' => str_split(Y::getSetting('possible_letters')),
		]);
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Settings('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Settings']))
			$model->attributes=$_GET['Settings'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Settings the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Settings::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Settings $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='settings-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
