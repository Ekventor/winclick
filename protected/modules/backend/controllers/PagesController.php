<?php

class PagesController extends AController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'users' => array('@'),
                'roles' => array('admin', 'content')
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Pages;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $pages = Yii::app()->request->getParam('Pages', []);
        if (count($pages)) {
            $dateTime = date('Y-m-d H:i:s');
            $model->created = $dateTime;
            $model->modified = $dateTime;
            $model->author = Y::user()->login;
            $model->attributes = $_POST['Pages'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $pages = Yii::app()->request->getParam('Pages', []);
        if (count($pages)) {
            $model->attributes = $_POST['Pages'];
            $model->modified = date('Y-m-d H:i:s');
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Pages');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Pages('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Pages']))
            $model->attributes = $_GET['Pages'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Pages the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Pages::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionImages()
    {
        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);

        if ($_FILES['file']['type'] == 'image/png'
            || $_FILES['file']['type'] == 'image/jpg'
            || $_FILES['file']['type'] == 'image/gif'
            || $_FILES['file']['type'] == 'image/jpeg'
            || $_FILES['file']['type'] == 'image/pjpeg') {

            $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('pagesImagePath');
            if (!is_dir($path)) {
                mkdir($path);
            }

            $fname = DIRECTORY_SEPARATOR . 'page_' . date('Ymd_His') . '.jpg';;

            // copying
            move_uploaded_file($_FILES['file']['tmp_name'], $path . $fname);

            $this->renderPartial('images', [
                'src' => Y::param('pagesImagePath') . $fname
            ]);
        }
    }
    public function actionFiles()
    {
        if (isset($_FILES['file'])) {
            if ($_FILES['file']['size'] > 10000000) {
                throw new CException('файл слишком велик');
            }

            $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('pagesFilePath');
            if (!is_dir($path)) {
                mkdir($path);
            }

            $fname = '/' . $_FILES['file']['name'];

            move_uploaded_file($_FILES['file']['tmp_name'], $path . $fname);

            $this->renderPartial('files', [
                'fileName' => $_FILES['file']['name'],
                'url' => Y::param('pagesFilePath') . $fname
            ]);
        } else {
            throw new CException('Невозможно загрузить файл');
        }
    }


    /**
     * Performs the AJAX validation.
     * @param Pages $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pages-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
