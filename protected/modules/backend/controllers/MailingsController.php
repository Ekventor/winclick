<?php

class MailingsController extends AController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	 
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'users'=>array('@'),
                'roles'=>array('admin')
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Mailings;

		if(isset($_POST['Mailings']))
		{
			switch($_POST['type']){
				case 0:
					$users = Users::model()->findAll();
					foreach($users as $user){
						$model=new Mailings;
						$model->attributes=$_POST['Mailings'];
						$model->date = date('Y-m-d H:i:s');
						$model->creator = Y::userId();
						$model->user_id = $user->id;
						$model->save();
					}
				break;
				case 1:
					foreach($_POST['users'] as $user_id){
						$model=new Mailings;
						$model->attributes=$_POST['Mailings'];
						$model->date = date('Y-m-d H:i:s');
						$model->creator = Y::userId();
						$model->user_id = $user_id;
						$model->save();
					}
				break;
			}
			$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = Mailings::model()->with('_creator')->with('user');
		
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Mailings']))
			$model->attributes=$_GET['Mailings'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Mailings the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Mailings::model()->with('_creator')->with('user')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Mailings $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mailings-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
