<?php

class ContentController extends AController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/columnAdmin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',
                'users'=>array('@'),
                'roles'=>array('admin', 'content')
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionImageJson()
    {
        $paths = [
            [
                'path' => '/images/content/',
                'name' => 'Content'
            ],
//            [
//                'path' => '/images/info/',
//                'name' => 'Info'
//            ]
        ];
        $images = [];
        foreach ($paths as $path) {
            $dir = dirname(Yii::app()->basePath) . $path['path'];
            if (file_exists($dir)) {
                $files = scandir($dir);
                unset ($files[0], $files[1]);
                foreach ($files as $file) {
                    $images[] = [
                        'thumb' => $path['path'] . $file,
                        'image' => $path['path'] . $file,
                        'title' => $file,
                        'folder' => $path['name']
                    ];
                }
            }
        }
        Y::endJson($images);
    }

    public function actionInfo()
    {
        $criteria = new CDbCriteria();
        $criteria->compare('alias', 'info');
        $criteria->order = 'id DESC';

        $dataProvider = new CActiveDataProvider('Content', ['criteria' => $criteria]);

        $this->render('info', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionMainInfo()
    {
        $criteria = new CDbCriteria();
        $criteria->compare('alias', 'mainInfo');
        $criteria->order = 'id DESC';

        $dataProvider = new CActiveDataProvider('Content', ['criteria' => $criteria]);

        $this->render('mainInfo', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreateMainInfo()
    {
        $model=new Content;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Content']))
        {
            $model->created = date('Y-m-d H:i:s');
            $model->modified = date('Y-m-d H:i:s');
            $model->short_text = '';
            $model->alias = 'mainInfo';
            $dateFormatier = new CDateFormatter('ru_RU');
            $model->attributes=$_POST['Content'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('createInfo',array(
            'model'=>$model,
        ));

    }

    public function actionUpdateMainInfo($id)
    {
        $model = $this->loadModel($id);

        if(isset($_POST['Content']))
        {
            $model->attributes=$_POST['Content'];
            $model->modified = date('Y-m-d H:i:s');
            if($model->save())
                $this->redirect(array('mainInfo'));
        }

        $this->render('updateInfo',array(
            'model'=>$model,
        ));

    }


    public function actionCreateInfo() {
        $model=new Content;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Content']))
        {
            $model->created = date('Y-m-d H:i:s');
            $model->modified = date('Y-m-d H:i:s');
            $model->short_text = '';
            $model->alias = 'info';
            $dateFormatier = new CDateFormatter('ru_RU');
            $model->attributes=$_POST['Content'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('createInfo',array(
            'model'=>$model,
        ));
    }

    public function actionUpdateInfo($id)
    {
        $model = $this->loadModel($id);

        if(isset($_POST['Content']))
        {
            $model->attributes=$_POST['Content'];
            $model->modified = date('Y-m-d H:i:s');
            if($model->save())
                $this->redirect(array('info'));
        }

        $this->render('updateInfo',array(
            'model'=>$model,
        ));

    }

    public function actionNews()
    {
        $position = Y::param('menuPosition')['left_news'];
        $urls = Yii::app()->db->createCommand()
            ->select('url')
            ->from('menu')
            ->where('position='. $position['id'])
            ->queryColumn();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('alias', $urls);
//        $criteria->order = 'id DESC';

        $dataProvider = new CActiveDataProvider('Content', ['criteria' => $criteria]);

        $this->render('news', [
            'dataProvider' => $dataProvider,
            'position' => 'left_news'
        ]);
    }

    public function actionPartnerNews()
    {
        $position = Y::param('menuPosition')['left_partner_news']['id'];
        $urls = Yii::app()->db->createCommand()
            ->select('url')
            ->from('menu')
            ->where('position='. $position)
            ->queryColumn();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('alias', $urls);
        $criteria->order = 'id DESC';

        $dataProvider = new CActiveDataProvider('Content', ['criteria' => $criteria]);

        $this->render('news', [
            'dataProvider' => $dataProvider,
            'position' => 'left_partner_news'
       ]);
    }


    public function actionHelp()
    {
        $position = Y::param('menuPosition')['left_help'];
        $urls = Yii::app()->db->createCommand()
            ->select('url')
            ->from('menu')
            ->where('position='. $position['id'])
            ->queryColumn();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('alias', $urls);
        $criteria->order = 'id DESC';

        $dataProvider = new CActiveDataProvider('Content', ['criteria' => $criteria]);

        $this->render('help', [
            'dataProvider' => $dataProvider,
            'position' => 'left_help'
        ]);    }


    public function actionPartnerHelp()
    {
        $position = Y::param('menuPosition')['left_partner_help']['id'];
        $urls = Yii::app()->db->createCommand()
            ->select('url')
            ->from('menu')
            ->where('position='. $position)
            ->queryColumn();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('alias', $urls);
        $criteria->order = 'id DESC';

        $dataProvider = new CActiveDataProvider('Content', ['criteria' => $criteria]);

        $this->render('help', [
            'dataProvider' => $dataProvider,
            'position' => 'left_partner_help'
        ]);    }

    /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($position)
	{
		$model=new Content;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        $position = Y::param('menuPosition')[$position];

        $items = Menu::model()->findAllByAttributes(['position' => $position]);
        $urls[0] = 'Нет алиаса';
        foreach ($items as $item) {
            $urls[$item->url] = $item->url . ' - ' . $item->title;
        }

        if(isset($_POST['Content']))
		{
            $model->created = date('Y-m-d H:i:s');
            $dateFormatier = new CDateFormatter('ru_RU');
            $model->attributes=$_POST['Content'];
            $model->modified = date('Y-m-d H:i:s', strtotime($_POST['Content']['modified']));
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
            'urls' => $urls
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$position = Yii::app()->db->createCommand()
            ->select('position')
            ->from('menu')
            ->where("url = '". $model->alias . "'")
            ->queryScalar();

        $items = Menu::model()->findAllByAttributes(['position' => $position]);
        $urls[0] = 'Нет алиаса';
        foreach ($items as $item) {
            $urls[$item->url] = $item->url . ' - ' . $item->title;
        }

        if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
            $model->modified = date('Y-m-d H:i:s', strtotime($_POST['Content']['modified']));
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
            'urls' => $urls
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Content');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Content('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Content']))
			$model->attributes=$_GET['Content'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    public function actionImages()
    {
        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);

        if ($_FILES['file']['type'] == 'image/png'
            || $_FILES['file']['type'] == 'image/jpg'
            || $_FILES['file']['type'] == 'image/gif'
            || $_FILES['file']['type'] == 'image/jpeg'
            || $_FILES['file']['type'] == 'image/pjpeg') {

            $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('contentImagePath');
            if (!is_dir($path)) {
                mkdir($path);
            }

            $fname = DIRECTORY_SEPARATOR . 'content_' . date('Ymd_His') . '.jpg';;

            // copying
            move_uploaded_file($_FILES['file']['tmp_name'], $path . $fname);

            $this->renderPartial('images', [
                'src' => Y::param('contentImagePath') . $fname
            ]);
        }
    }
    public function actionFiles()
    {
        if (isset($_FILES['file'])) {
            if ($_FILES['file']['size'] > 10000000) {
                throw new CException('файл слишком велик');
            }

            $path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('contentFilePath');
            if (!is_dir($path)) {
                mkdir($path);
            }

            $fname = '/' . $_FILES['file']['name'];

            move_uploaded_file($_FILES['file']['tmp_name'], $path . $fname);

            $this->renderPartial('files', [
                'fileName' => $_FILES['file']['name'],
                'url' => Y::param('contentFilePath') . $fname
            ]);
        } else {
            throw new CException('Невозможно загрузить файл');
        }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Content the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Content::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Content $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='content-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
