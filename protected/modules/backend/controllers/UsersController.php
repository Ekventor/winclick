<?php

class UsersController extends AController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'users'=>array('@'),
                'roles'=>array('admin', 'content')
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
	}


    public function actionBalance()
    {
        $doubleWK = Yii::app()->request->getParam('double_wk', 0);
        $wk = Yii::app()->request->getParam('wk', 0);
        if ($doubleWK) {
            if ($acc_id = Yii::app()->account->getAccIdByCurrency(Y::userId(), 'RUB')) {
                $data[] = Yii::app()->account->incBalance($acc_id, (float)$doubleWK, Yii::t('app', ' '));
            }
        }
        if ($wk) {
            if ($acc_id = Yii::app()->account->getAccIdByCurrency(Y::userId(), 'WK')) {
                $data[] = Yii::app()->account->incBalance($acc_id, (float)$wk, Yii::t('app', ' '));
            }
        }
        $this->render('balance');
    }

    /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Users;

        $model->setScenario('adminAdd');

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
            $model->salt = $model->generateSalt();
            $model->password = $model->createPassword($_POST['Users']['password'], $_POST['Users']['email']);
            $model->activated = true;
            $model->created = date('Y-m-d H:i:s');
            $model->modified = date('Y-m-d H:i:s');

			if($model->save())
				Yii::app()->account->create($model->id);
				$this->redirect('admin');
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

    /**
     * Обновление роли пользователя
     */
    public function actionUpdateRole()
    {
        $role = Yii::app()->request->getParam('role', null);
        $userId = Yii::app()->request->getParam('userId', null);
        if ($role !== null && $userId !== null) {
            $user = Users::model()->findByPk($userId);
            $user->role = $role;
            $user->save(false);
        }
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionAutocomplete()
	{
		$data = Yii::app()->myuser->autocomplete($_REQUEST['term']);
		echo json_encode($data);
		Yii::app()->end();
		
	}
}
