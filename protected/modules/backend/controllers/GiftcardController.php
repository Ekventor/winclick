<?php

class GiftcardController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/columnAdmin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'users'=>array('@'),
				'roles' => array('admin', 'content')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Giftcard;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$data = Y::param('nominals');
//		CVarDumper::dump(isset($_POST['Giftcard']) ? $_POST['Giftcard'] : '');
		$giftcard = Yii::app()->request->getParam('Giftcard', []);

		$image = new ImageUpload;

		if(count($giftcard))
		{
			$result = [];
			if (isset($giftcard['nominals'])) {
				$nominals = $giftcard['nominals'];
				foreach ($nominals as $nominal) {
					$result[] = $data[$nominal];
				}
			}
			$model->title = $giftcard['title'];
			$model->desc = $giftcard['desc'];
			$model->short_desc = $giftcard['short_desc'];
			$model->addit_info = $giftcard['addit_info'];
			$model->nominals = CJSON::encode($result);

			$instanceSmallImg = CUploadedFile::getInstanceByName('small_img');
			$instanceImg = CUploadedFile::getInstanceByName('img');
			if (!empty($instanceSmallImg) || !empty($instanceImg)) {
				$path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('giftImagePath');
				if (!is_dir($path)) {
					mkdir($path);
				}

				if (!is_null($instanceSmallImg)) {
					$image = new ImageManager($instanceSmallImg);
					$fname = DIRECTORY_SEPARATOR . date('Ymd_His') . 'small.jpg';
					$image->saveResized(186, 120, $path . $fname, true);
					$url = Y::param('giftImagePath') . $fname;
					$model->small_img = $url;
				}
				if (!is_null($instanceImg)) {
					$image = new ImageManager($instanceImg);
					$fname = DIRECTORY_SEPARATOR . date('Ymd_His') . '.jpg';
					$image->saveResized(800, 600, $path . $fname, true);
					$url = Y::param('giftImagePath') . $fname;
					$model->img = $url;
				}
			}

			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
			'data' => $data,
			'selected' => '',
			'image' => $image
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$data = Y::param('nominals');
		$nominals = CJSON::decode($model->nominals);
		$selected = [];
		foreach ($nominals as $nominal) {
			$selected[] = array_search($nominal, $data);
		}

		$giftcard = Yii::app()->request->getParam('Giftcard', []);

		$image = new ImageUpload;

		if(count($giftcard))
		{
			$result = [];
			if (isset($giftcard['nominals'])) {
				$nominals = $giftcard['nominals'];
				foreach ($nominals as $nominal) {
					$result[] = $data[$nominal];
				}
			}
			$model->title = $giftcard['title'];
			$model->desc = $giftcard['desc'];
			$model->short_desc = $giftcard['short_desc'];
			$model->addit_info = $giftcard['addit_info'];
			$model->nominals = CJSON::encode($result);

			$instanceSmallImg = CUploadedFile::getInstanceByName('small_img');
			$instanceImg = CUploadedFile::getInstanceByName('img');
			if (!empty($instanceSmallImg) || !empty($instanceImg)) {
				$path = Y::app()->basePath . DIRECTORY_SEPARATOR . '..' . Y::param('giftImagePath');
				if (!is_dir($path)) {
					mkdir($path);
				}

				if (!is_null($instanceSmallImg)) {
					$image = new ImageManager($instanceSmallImg);
					$fname = DIRECTORY_SEPARATOR . date('Ymd_His') . 'small.jpg';
					$image->saveResized(186, 120, $path . $fname, true);
					$url = Y::param('giftImagePath') . $fname;
					$model->small_img = $url;
				}
				if (!is_null($instanceImg)) {
					$image = new ImageManager($instanceImg);
					$fname = DIRECTORY_SEPARATOR . date('Ymd_His') . '.jpg';
					$image->saveResized(800, 600, $path . $fname, true);
					$url = Y::param('giftImagePath') . $fname;
					$model->img = $url;
				}
			}

			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
			'data' => $data,
			'selected' => $selected,
			'image' => $image
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Giftcard');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Giftcard('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Giftcard']))
			$model->attributes=$_GET['Giftcard'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Giftcard the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Giftcard::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Giftcard $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='giftcard-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
