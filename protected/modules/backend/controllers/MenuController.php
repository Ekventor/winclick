<?php

class MenuController extends AController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'users' => array('@'),
                'roles' => array('admin', 'content')
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Menu;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Menu'])) {
            $model->attributes = $_POST['Menu'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Menu'])) {
            $model->attributes = $_POST['Menu'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $id = Yii::app()->request->getParam('id', 0);
            $item = Menu::model()->findByPk($id)->delete();
        }
//        $this->loadModel($id)->delete();
//
//		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//		if(!isset($_GET['ajax']))
//			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    /**
     * @throws CException
     */
    public function actionForm()
    {
//        $parents = Yii::app()->db->createCommand()
//            ->select('id, title, url')
//            ->from('menu')
//            ->where('parent = 0')
//            ->queryAll();
        $result['0'] = 'Нет родителей';
//        foreach ($parents as $parent) {
//            $result[$parent['id']] = $parent['url'] . ' - ' . $parent['title'];
//        }

        $id = Yii::app()->request->getParam('id', 0);
        $item = Menu::model()->findByPk($id);
        if ($item === null) {
            $item = new Menu;
        }
        $positionType = [];
        foreach (Y::param('menuPosition') as $key => $position) {
            $positionType[$position['id']] = $key;
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_form', [
                'model' => $item,
                'id' => $id,
                'parents' => $result,
                'position' => $positionType
            ]);
        }
        $menu = Yii::app()->request->getParam('Menu', []);
        if (count($menu)) {
            $item->attributes = $menu;
            if (!$item->order) {
                $order = Yii::app()->db->createCommand()
                    ->select('max("order")')
                    ->from('menu')
                    ->where('position = ' . $item->position)
                    ->queryScalar();
                $item->order = $order++;
            }
            if ($item->save()) {
                $this->redirect(['index']);
            } else {
                $this->redirect(['index']);
            }
        }
    }

    public function actionGetParent()
    {
        $request = Yii::app()->request;
        if ($request->isAjaxRequest) {
            $position = $request->getParam('position', 0);
            $result['0'] = 'Нет родителей';
            if ($position) {
                $parents = Yii::app()->db->createCommand()
                    ->select('id, title, url')
                    ->from('menu')
                    ->where('parent = 0')
                    ->where('position = ' . $position)
                    ->queryAll();
                foreach ($parents as $parent) {
                    $result[$parent['id']] = $parent['url'] . ' - ' . $parent['title'];
                }
            }
            Y::endJson($result);
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new Menu;

        $positions = Y::param('menuPosition');
        $menues = [];

        foreach ($positions as $key => $position) {
            $criteria = new CDbCriteria();
            $criteria->compare('position', $position['id']);
            $criteria->order = '"order"';
            $menu = Menu::model()->findAll($criteria);

            if (count($menu)) {
                $menues[$key]['menu'] = [];
                foreach ($menu as $item) {
                    $menues[$key]['menu'][$item->id] = $item->title;
                }
                $menues[$key]['sort'] = $position['sort'];
            }
        }

        $this->render('index',array(
			'model'=>$model,
            'menues' => $menues
		));
	}

    public function actionUpdateOrder()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $items = Yii::app()->request->getParam('elements', []);
            foreach ($items as $key => $item) {
                $model = Menu::model()->findByPk($item);
                $model->order = $key;
                $model->save();
            }
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Menu('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Menu']))
            $model->attributes = $_GET['Menu'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Menu the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Menu::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Menu $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'menu-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
