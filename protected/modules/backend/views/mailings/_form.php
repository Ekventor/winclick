<?php
/* @var $this MailingsController */
/* @var $model Mailings */
/* @var $form CActiveForm */
?>
<?php Yii::app()->getClientScript()->registerScriptFile('/js/vendor/jquery-ui.min.js');?>
<?php Yii::app()->getClientScript()->registerCssFile('/css/vendor/jquery-ui.css');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mailings-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	
<table class="form-table">
	<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo CHtml::label(Yii::t('app', "Тип рассылки"), "type"); ?>
			</td>
			<td class="input-td">
				<?php echo CHtml::radioButtonList("type", 0, [0=>Y::t('app', "Всем пользователям"), 1=>Y::t('app', "Выбранным пользователям")]); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'title'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'title'); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'title'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td" colspan="4">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'message'); ?><br>
				<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50, 'style'=>'width:100%')); ?>
				<?php echo $form->error($model,'message'); ?>
			</td>
		</tr>
		<tr class="find_users" style="display:none; border-bottom:0px;">
			<td colspan="4">
				<h3 style="maregin-bottom:-20px;"><?php echo Y::t('app', 'Получатели'); ?></h3>
			</td>
		</tr>
		<tr class="find_users" style="display:none; border-bottom:0px; height:auto;">
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Y::t('app', "Поиск пользователей"); ?></label>
			</td>
			<td class="input-td">
				<input type="text" id="users_search_input" autocomplete="off" placeholder="Начните вводить" />
			</td>
		</tr>
	</tbody>
</table>
<div class="find_users"  style="display:none;">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'users-grid',
		'dataProvider'=>new CArrayDataProvider(array()),
		'columns'=>array(
			['id'=>'id', 'name'=>Y::t('app', 'mailing_table.id')],
			['id'=>'login','name'=>Y::t('app', 'mailing_table.login')],
			['id'=>'phone','name'=>Y::t('app', 'mailing_table.phone')],
			array(
				'class'=>'CButtonColumn',
                'deleteButtonLabel' => 'Удалить'
			),
		),
		'emptyText'=>'',
	)); ?>
</div>
	
	<div class="row buttons">
		
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>
	
	
<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
	$('input[name="type"]').change(function(){
		if($('input[name="type"]:checked').val()==0){
			$('.find_users').hide();
		}else{
			$('.find_users').show();
		}
	});
	$('#users_search_input').autocomplete({
		appendTo:'#users_search',
		source: function(request,response){
			$.ajax({
				url:'<?php echo $this->createUrl('users/autocomplete'); ?>',
				data:{
					term:request.term,
				},
				dataType:'json',
				success:function(data){
					response($.map(data, function(item){
					  return{
						label: item.login + ' ( ' + item.phone + ' ) ',
						value: item.login,
						data: item,
					  }
					}));
				}
			});
		
		},
		select:function( event, ui ){
			var data = ui.item.data;
			$('#users-grid table tbody').append(
				'<tr>\
					<td>'+data.id+'</td>\
					<td>'+data.login+'</td>\
					<td>'+data.phone+'</td>\
					<td>\
						<input type="hidden" name="users['+data.id+']" value="'+data.id+'" />\
						<?php echo CHtml::submitButton('Удалить', ['style'=>'margin:0px;', 'onclick'=>'$(this).parent().parent().remove();']); ?>\
					</td>\
				</tr>'
			);
		},
	});
</script>