<?php
/* @var $this MailingsController */
/* @var $model Mailings */

$this->breadcrumbs=array(
	'Mailings'=>array('index'),
	'Create',
);

?>

<h1><?php echo Yii::t('app', 'Создание рассылки'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>