<?php
/* @var $this MailingsController */
/* @var $model Mailings */

$this->breadcrumbs=array(
	'Mailings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Mailings', 'url'=>array('index')),
	array('label'=>'Create Mailings', 'url'=>array('create')),
	array('label'=>'Update Mailings', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Mailings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mailings', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'Просмотр сообщения'); ?> #<?php echo $model->id; ?></h1>

<table class="form-table">
	<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'mailings.id'); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->id); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'mailings.date'); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->date); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'mailings.title'); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->title); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'mailings.creator'); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->_creator->login); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'mailings.user'); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->_user->login); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'mailings.readed'); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo $model->readed ? Yii::t("app", "Yes") : Yii::t("app", "No"); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'mailings.message'); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->message); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
	</tbody>
</table>
