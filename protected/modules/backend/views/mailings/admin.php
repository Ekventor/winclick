<?php
/* @var $this MailingsController */
/* @var $model Mailings */

$this->breadcrumbs=array(
	'Mailings'=>array('index'),
	'Manage',
);

?>

<h1><?php echo Yii::t('app', 'Рассылки'); ?></h1>

<?php
    echo CHtml::link('Создать', $this->createUrl('mailings/create'));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'mailings-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions'=>array(
		'class'=>'admin-table',
	),
	'columns'=>array(
		'date',
		'title',
		array('name'=>Yii::t("app", 'creator'), 'value'=>'$data->_creator->login;'),
		array('name'=>Yii::t("app", 'user'), 'value'=>'$data->user->login;'),
		array('name'=>Yii::t("app", 'readed'), 'value'=>'$data->readed ? Yii::t("app", "Yes") : Yii::t("app", "No");'),
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array('update'=>array('visible'=>'false')),
		),
	),
)); ?>
