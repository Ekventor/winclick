<?php
/* @var $this GiftcardController */
/* @var $model Giftcard */
/* @var $form CActiveForm */
?>

<style>
	#giftcard-form textarea {
		background-color: #98dcf3;
		border: 1px solid #0097c0;
		border-radius: 3px;
		box-shadow: 0 0 3px 1px inset;
		height: 100px;
		padding: 7px;
		width: 125px;
	}

</style>

<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'giftcard-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'htmlOptions' => [
			'enctype' => 'multipart/form-data'
		],
		'enableAjaxValidation'=>false,
	)); ?>

	<table class="form-table">
		<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'title'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'title'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'short_desc'); ?>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<?php $this->widget('Redactor', [
					'model' => $model,
					'attribute' => 'short_desc',
					'lang' => 'ru',
					'editorOptions' => [
						'buttons' => ['html', 'formatting', 'bold', 'italic', 'deleted',
							'unorderedlist', 'orderedlist', 'outdent', 'indent',
							 'link', 'alignment', 'horizontalrule']
//						'imageUpload' => $this->createUrl('images'),
//						'fileUpload' => $this->createUrl('files'),
//						'imageGetJson' => '/backend/content/imageJson/'
					]
				]); ?>
				<?php echo $form->error($model,'short_desc'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'desc'); ?>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<?php $this->widget('Redactor', [
					'model' => $model,
					'attribute' => 'desc',
					'lang' => 'ru',
					'editorOptions' => [
//						'imageUpload' => $this->createUrl('images'),
//						'fileUpload' => $this->createUrl('files'),
//						'imageGetJson' => '/backend/content/imageJson/'
					]
				]); ?>
				<?php echo $form->error($model,'desc'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'nominals'); ?>
			</td>
			<td class="input-td">
				<?= CHtml::checkBoxList('Giftcard[nominals][]', $selected, $data); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'nominals'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'addit_info'); ?>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<?php $this->widget('Redactor', [
					'model' => $model,
					'attribute' => 'addit_info',
					'lang' => 'ru',
					'editorOptions' => [
//						'imageUpload' => $this->createUrl('images'),
//						'fileUpload' => $this->createUrl('files'),
//						'imageGetJson' => '/backend/content/imageJson/'
					]
				]); ?>
				<?php echo $form->error($model,'addit_info'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model, 'small_img'); ?>
			</td>
			<td class="input-td">
				<?= CHtml::image($model->small_img); ?>
				<?php echo CHtml::fileField('small_img', ''); ?>
			</td>
			<td class="example-td">
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model, 'img'); ?>
			</td>
			<td class="input-td">
				<?= CHtml::image($model->img); ?>
				<?php echo CHtml::fileField('img', ''); ?>
			</td>
			<td class="example-td">
			</td>
		</tr>
		</tbody>
	</table>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'wk_button']); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->