<?php
/* @var $this GiftcardController */
/* @var $model Giftcard */

$this->breadcrumbs=array(
	'Giftcards'=>array('index'),
	'Create',
);
?>

<h1>Создание подарочного сертификата</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'data' => $data, 'selected' => $selected, 'image' => $image)); ?>