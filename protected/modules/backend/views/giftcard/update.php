<?php
/* @var $this GiftcardController */
/* @var $model Giftcard */

$this->breadcrumbs=array(
	'Giftcards'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);
?>

<h1>Редактирование подарочного сертификата "<?php echo $model->title; ?>"</h1>

<?php $this->renderPartial(
	'_form',
	['model'=>$model, 'data' => $data, 'selected' => $selected, 'image' => $image]
); ?>