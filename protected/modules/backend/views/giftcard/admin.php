<?php
/* @var $this GiftcardController */
/* @var $model Giftcard */

$this->breadcrumbs=array(
	'Giftcards'=>array('index'),
	'Manage',
);
?>

<h1>Подарочные сертификаты</h1>

<?php echo CHtml::link('Создать',$this->createUrl('create')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'giftcard-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions'=>array(
		'class'=>'admin-table',
	),
	'columns'=>array(
		'id',
		'title',
		'short_desc:raw',
		[
			'type' => 'raw',
			'value' => 'CHtml::image($data->small_img);',
			'header' => Y::t('app', 'giftcard.small_img')
		],
//		'nominals',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
