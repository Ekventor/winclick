<?php
/* @var $this GiftcardController */
/* @var $model Giftcard */

$this->breadcrumbs=array(
	'Giftcards'=>array('index'),
	$model->title,
);
?>

<h1>Подарочный сертификат "<?php echo $model->title; ?>"</h1>

<?php $columns = array(
	'id',
	'title',
	'desc',
	'nominals',
); ?>


<table class="form-table">
	<tbody>
	<?php foreach($columns as $column) : ?>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'giftcard.'.$column); ?>:</label>
			</td>
			<td class="input-td">
				<p class="login-name"><?php echo CHtml::encode($model->$column); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">
			</td>
		</tr>
	<?php endforeach; ?>

	<tr>
		<td class="labels-td">
			<img src="/img/white-circle.png" alt="">
			<label><?php echo Yii::t('app', 'giftcard.img'); ?>:</label>
		</td>
		<td class="input-td">
			<p class="login-name"><?php echo CHtml::image($model->img); ?></p>
		</td>
		<td class="example-td">
		</td>
		<td class="last-field-td">
		</td>
	</tr>
	</tbody>
</table>