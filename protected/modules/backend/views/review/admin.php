<?php
/* @var $this ReviewController */
/* @var $model Review */

$this->breadcrumbs=array(
	'Reviews'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Review', 'url'=>array('index')),
	array('label'=>'Create Review', 'url'=>array('create')),
);

?>
<h1>Отзывы</h1>

<?php echo CHtml::link('Создать', $this->createUrl('create')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'review-grid',
	'dataProvider'=>$model->search(),
	'htmlOptions'=>array(
		'class'=>'admin-table',
	),
	'columns'=>array(
		'id',
		[
			'header' => 'Пользователь',
			'name' => 'user_id',
			'value' => '$data->user->login'
		],
		'author',
		'text',
		'created',
		'show',
		/*
		'avatar',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
