<?php
/* @var $this ReviewController */
/* @var $model Review */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'review-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => [
        'enctype'=>'multipart/form-data'
    ],
)); ?>

	<?php echo $form->errorSummary($model); ?>

	
<table class="form-table">
	<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'author'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'author',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'author'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'profession'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'profession',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'profession'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'text'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'text',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'text'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'show'); ?>
			</td>
			<td class="input-td">
                <span class="switch">
                    <?php echo $form->checkBox($model, 'show', ['id' => 'autowk']); ?>
                    <label for="autowk">
                        <span></span>
                    </label>
                </span>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'show'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model, 'avatar'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->fileField($image, 'image'); ?>
			</td>
			<td class="example-td">
				<?php echo CHtml::image($model->avatar); ?>
				<?php echo $form->error($image,'image'); ?>
			</td>
		</tr>

		
	</tbody>
</table>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->