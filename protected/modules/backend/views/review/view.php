<?php
/* @var $this ReviewController */
/* @var $model Review */

$this->breadcrumbs=array(
	'Reviews'=>array('index'),
	$model->id,
);
?>

<h1><?php echo Yii::t('app', 'Просмотр отзыва'); ?> #<?php echo $model->id; ?></h1>

<?php $columns = array(
	'id',
	'user_id',
	'author',
	'text',
	'created',
	'show',
); ?>


<table class="form-table">
	<tbody>
		<?php foreach($columns as $column) : ?>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'review.'.$column); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->$column); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		<?php endforeach; ?>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'review.avatar'); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::image($model->avatar); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
	</tbody>
</table>