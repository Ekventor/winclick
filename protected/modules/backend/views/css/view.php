<?php
/* @var $this CssController */
/* @var $model Css */

$this->breadcrumbs=array(
	'Csses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Css', 'url'=>array('index')),
	array('label'=>'Create Css', 'url'=>array('create')),
	array('label'=>'Update Css', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Css', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Css', 'url'=>array('admin')),
);
?>

<h1>View Css #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'css',
		'modified',
	),
)); ?>
