<?php
/* @var $this CssController */
/* @var $model Css */
/* @var $form CActiveForm */
?>

<?php
/* @var $this CityController */
/* @var $model City */
/* @var $form CActiveForm */
?>
<style>
    .CodeMirror {
        height: 600px;4
    }
</style>
<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'css-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>

                <?php $this->widget('ext.widgets.codemirror.Codemirror', [
                    'model' => $model,
                    'attribute' => 'css',
                    'options' => [
                        'mode' => 'css',
                        'theme' => 'cobalt',
                        'lineNumbers' => true,
                    ],
                    'htmlOptions' => [
                        'class' => 'code-mirror-place'
                    ]
                ]); ?>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Сохранить', ['class' => 'wk_button']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

