<?php
/* @var $this CssController */
/* @var $model Css */

$this->breadcrumbs=array(
	'Csses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Css', 'url'=>array('index')),
	array('label'=>'Manage Css', 'url'=>array('admin')),
);
?>

<h1>Create Css</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>