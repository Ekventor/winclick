<?php
/* @var $this CssController */
/* @var $model Css */

$this->breadcrumbs=array(
	'Csses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<h1>Редактирование CSS-стилей</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>