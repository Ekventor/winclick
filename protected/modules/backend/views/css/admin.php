<?php
/* @var $this CssController */
/* @var $model Css */
?>

<h1>Управление классами</h1>

<?php echo CHtml::link('Редактировать',$this->createUrl('update')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'css-grid',
	'dataProvider'=>$model->search(),
    'htmlOptions'=>array(
        'class'=>'admin-table',
    ),
	'columns'=>array(
		'id',
		'modified',
	),
)); ?>
