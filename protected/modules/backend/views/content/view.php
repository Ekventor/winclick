<?php
/* @var $this ContentController */
/* @var $model Content */

$this->breadcrumbs=array(
	'Contents'=>array('index'),
	$model->title,
);
?>

<h1>Просмотр контента #<?php echo $model->id; ?></h1>

<?php $columns = array(
    'id',
    'title',
    'short_text',
    'text',
    'created',
    'modified',
    'alias',
); ?>

<table class="form-table">
    <tbody>
    <?php foreach($columns as $column) : ?>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <label><?php echo Yii::t('app', 'content.'.$column); ?>:</label>
            </td>
            <td class="input-td">
                <p class="login-name"><?= $model->$column; ?></p>
            </td>
            <td class="example-td">
            </td>
            <td class="last-field-td">
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
