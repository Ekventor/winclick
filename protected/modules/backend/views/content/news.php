<?php
/* @var $this ContentController */
/* @var $model Content */

?>

<h1>Управление новостями</h1>

<?php echo CHtml::link('Создать', $this->createUrl('create', ['position' => $position])); ?><br>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'content-grid',
    'dataProvider'=>$dataProvider,
    'htmlOptions'=>array(
        'class'=>'admin-table',
    ),
    'columns'=>array(
        'title',
        'short_text',
        'modified',
        'alias',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
