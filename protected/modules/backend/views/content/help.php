<?php
/* @var $this ContentController */
/* @var $model Content */

?>

<h1>Управление помощью</h1>


<?php echo CHtml::link('Создать', $this->createUrl('create', ['position' => $position])); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'content-grid',
    'dataProvider'=>$dataProvider,
    'htmlOptions'=>array(
        'class'=>'admin-table',
    ),
    'columns'=>array(
        'title',
//        'short_text',
        'created',
        'modified',
        'alias',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
