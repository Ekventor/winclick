<?php
/* @var $this ContentController */
/* @var $model Content */

?>

<h1>Обновить контент #<?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'urls' => $urls)); ?>