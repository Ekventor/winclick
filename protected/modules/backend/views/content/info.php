<?php
/* @var $this ContentController */
/* @var $model Content */

?>

<h1>Как это работает</h1>

<?php echo CHtml::link('Добавить слайд', $this->createUrl('createInfo')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'content-grid',
    'dataProvider'=>$dataProvider,
    'htmlOptions'=>array(
        'class'=>'admin-table',
    ),
    'columns'=>array(
        'title',
        'text',
        'created',
        array(
            'class'=>'CButtonColumn',
            'updateButtonUrl' => '"updateInfo/id/" . $data->id'
        ),
    ),
)); ?>
