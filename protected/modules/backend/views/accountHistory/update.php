<?php
/* @var $this AccountHistoryController */
/* @var $model AccountHistory */

$this->breadcrumbs=array(
	'Account Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<h1><?php echo Yii::t('app', 'Редактирование зачисления'); ?> <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>