<?php
/* @var $this AccountHistoryController */
/* @var $model AccountHistory */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	
	
<table class="form-table">
	<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->label($model,'id'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'id'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->label($model,'account_id'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'account_id'); ?>
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->label($model,'open_balance'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'open_balance'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->label($model,'close_balance'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'close_balance'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->label($model,'amount'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'amount'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->label($model,'date'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'date'); ?>
			</td>
		</tr>

		
	</tbody>
</table>
	
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Поиск'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->