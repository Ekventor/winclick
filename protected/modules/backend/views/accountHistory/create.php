<?php
/* @var $this AccountHistoryController */
/* @var $model AccountHistory */

$this->breadcrumbs=array(
	'Account Histories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AccountHistory', 'url'=>array('index')),
	array('label'=>'Manage AccountHistory', 'url'=>array('admin')),
);
?>

<h1>Create AccountHistory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>