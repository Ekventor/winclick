<?php
/* @var $this AccountHistoryController */
/* @var $model AccountHistory */

$this->breadcrumbs=array(
	'Account Histories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AccountHistory', 'url'=>array('index')),
	array('label'=>'Create AccountHistory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#account-history-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Управление историей аккаунтов'); ?></h1>


<?php echo CHtml::link('Поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'account-history-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions'=>array(
		'class'=>'admin-table',
	),
	'columns'=>array(
		'id',
		'date',
		'account_id',
		'open_balance',
		'amount',
		'close_balance',
		array(
			'class'=>'CButtonColumn',
			'buttons'=>[
				'update'=>[
					'visible'=>'false',
				],
				'delete'=>[
					'visible'=>'false',
				]
			],
		),
	),
)); ?>
