<?php
/* @var $this AccountHistoryController */
/* @var $model AccountHistory */

$this->breadcrumbs=array(
	'Account Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AccountHistory', 'url'=>array('index')),
	array('label'=>'Create AccountHistory', 'url'=>array('create')),
	array('label'=>'Update AccountHistory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AccountHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AccountHistory', 'url'=>array('admin')),
);
?>

<h1>Просмотр изменения счета #<?php echo $model->id; ?></h1>

<?php $columns = array(
	'id',
		'account_id',
		'open_balance',
		'close_balance',
		'amount',
		'date',
		'comment'
); ?>


<table class="form-table">
	<tbody>
		<?php foreach($columns as $column) : ?>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'accounthistory.'.$column); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->$column); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>