<?php
/* @var $this AccountHistoryController */
/* @var $model AccountHistory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'account-history-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

<table class="form-table">
	<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'account_id'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'account_id'); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'account_id'); ?>
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'open_balance'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'open_balance'); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'open_balance'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'close_balance'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'close_balance'); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'close_balance'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'amount'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'amount'); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'amount'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'date'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'date'); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'date'); ?>
			</td>
		</tr>
	</tbody>
</table>
	
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->