<?php
/* @var $this AccountHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Account Histories',
);

$this->menu=array(
	array('label'=>'Create AccountHistory', 'url'=>array('create')),
	array('label'=>'Manage AccountHistory', 'url'=>array('admin')),
);
?>

<h1>Account Histories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
