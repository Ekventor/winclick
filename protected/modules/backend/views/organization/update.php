<?php
/* @var $this OrganizationController */
/* @var $model Organization */

$this->breadcrumbs=array(
	'Organizations'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Organization', 'url'=>array('index')),
	array('label'=>'Create Organization', 'url'=>array('create')),
	array('label'=>'View Organization', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Organization', 'url'=>array('admin')),
);
?>

<h1>Редактирование организации #<?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', [
    'model'=>$model,
    'cities' => $cities,
    'categories' => $categories,
    'image' => $image,
    'activeCity' => $activeCity
]); ?>