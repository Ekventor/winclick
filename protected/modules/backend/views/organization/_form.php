<?php
/* @var $this OrganizationController */
/* @var $model Organization */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'organization-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => [
            'enctype' => 'multipart/form-data'
        ],

    )); ?>

    <table class="form-table">
        <tbody>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'name'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model, 'name'); ?>
            </td>
        </tr>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'category_id'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->dropDownList($model, 'category_id', $categories); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model, 'category_id'); ?>
            </td>
        </tr>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'city_id'); ?>
            </td>
            <td class="input-td">
                <?= Select2::multiSelect('cities_id', $activeCity, $cities, [
                        'select2Options' => [
                            'width' => '250px',
                        ],
                        'placeholder' => 'Выбери город',
                    ]
                ); ?>
                <!--                --><?php //echo $form->dropDownList($model,'city_id', $cities); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model, 'city_id'); ?>
            </td>
        </tr>

        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'specialization'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <?php $this->widget('Redactor', [
                    'model' => $model,
                    'attribute' => 'specialization',
                    'lang' => 'ru',
                    'editorOptions' => [
                        'imageUpload' => $this->createUrl('images'),
                        'fileUpload' => $this->createUrl('files')
                    ]
                ]); ?>
                <?php echo $form->error($model, 'specialization'); ?>
            </td>
        </tr>

        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'contacts'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <?php $this->widget('Redactor', [
                    'model' => $model,
                    'attribute' => 'contacts',
                    'lang' => 'ru',
                    'editorOptions' => [
                        'imageUpload' => $this->createUrl('images'),
                        'fileUpload' => $this->createUrl('files')
                    ]
                ]); ?>
                <?php echo $form->error($model, 'contacts'); ?>
            </td>
        </tr>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'desc'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <?php $this->widget('Redactor', [
                    'model' => $model,
                    'attribute' => 'desc',
                    'lang' => 'ru',
                    'editorOptions' => [
                        'imageUpload' => $this->createUrl('images'),
                        'fileUpload' => $this->createUrl('files')
                    ]
                ]); ?>
                <?php echo $form->error($model, 'desc'); ?>
            </td>
        </tr>

        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'img'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->fileField($image, 'image'); ?>
            </td>
            <td class="example-td">
                <?php echo CHtml::image($model->img); ?>
                <?php echo $form->error($image, 'image'); ?>
            </td>
        </tr>

        </tbody>
    </table>


    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'wk_button']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->