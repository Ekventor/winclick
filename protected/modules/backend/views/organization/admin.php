<?php
/* @var $this OrganizationController */
/* @var $model Organization */

$this->breadcrumbs=array(
	'Organizations'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Organization', 'url'=>array('index')),
	array('label'=>'Create Organization', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#organization-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление организациями</h1>

<?php echo CHtml::link('Создать', $this->createUrl('create')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'organization-grid',
	'dataProvider'=>$model->search(),
    'htmlOptions'=>array(
        'class'=>'admin-table',
    ),
	'columns'=>array(
		'id',
		'name',
		[
            'name' => 'category_id',
            'value' => '$data->category->name',
        ],
		/*
		'desc',
		'img',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
