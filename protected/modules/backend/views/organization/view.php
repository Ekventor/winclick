<?php
/* @var $this OrganizationController */
/* @var $model Organization */

$this->breadcrumbs=array(
	'Organizations'=>array('index'),
	$model->name,
);
?>

<h1>Просмотр организации #<?php echo $model->id; ?></h1>

<?php $columns = array(
    'id',
    'name',
    'category_id',
    'city_id',
    'specialization',
    'contacts',
    'desc'
); ?>


<table class="form-table">
    <tbody>
    <?php foreach($columns as $column) : ?>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <label><?php echo Yii::t('app', 'organization.'.$column); ?>:</label>
            </td>
            <td class="input-td">
                <p class="login-name"><?php if ($column != 'city_id') echo CHtml::encode($model->$column); else echo $cities; ?></p>
            </td>
            <td class="example-td">
            </td>
            <td class="last-field-td">
            </td>
        </tr>
    <?php endforeach; ?>

    <tr>
        <td class="labels-td">
            <img src="/img/white-circle.png" alt="">
            <label><?php echo Yii::t('app', 'organization.img'); ?>:</label>
        </td>
        <td class="input-td">
            <p class="login-name"><?php echo CHtml::image($model->img); ?></p>
        </td>
        <td class="example-td">
        </td>
        <td class="last-field-td">
        </td>
    </tr>
    </tbody>
</table>