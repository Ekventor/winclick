<?php
/* @var $this MenuController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Menus',
);

echo CHtml::ajaxButton('Добавить', $this->createUrl('menu/form'), [
    'success' => 'function (data) {
        $("#item-dialog").empty().append(data).dialog("open");
    }',
	],
	[
		'class' => 'wk_button',
	]);
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', [
    'id' => 'item-dialog',
    'options' => [
        'title' => 'Создание/редактирование ссылки',
        'autoOpen' => false,
        'width' => '570',
        'modal' => true,
        'resizable' => false,
        'dialogClass' => 'dialog-page'
    ]
]);
?>

<?php
    $this->endWidget();
?>


<?php foreach($menues as $key => $menu): ?>
<h1>Меню в позиции "<?php echo $key; ?>"</h1>
<?php
$this->widget('zii.widgets.jui.CJuiSortable', array(
    'id' => $key . '-menu',
    'items' => $menu['menu'],
    'itemTemplate' => '<li id="{id}"><a href="#" class="update-item">{content}</a><a href="#" class="icon-del"></a></li>',
    'options' => array(
        'delay' => '300',
        'stop' => 'js:function(event, ui) {
            elements = [];
            $("#' . $key . '-menu li").each(function() {
                elements.push(this.id);
            });
            data = {
                elements: elements,
            };
            $.post("' . $this->createUrl("menu/updateOrder") . '", data, function(){});
        }',
        'axis' => $menu['sort']
    ),
    'htmlOptions' => [
        'class' => 'elements-menu-' . $menu['sort']
    ]
));
?>
<?php endforeach; ?>
<script>
    $('.icon-del').on('click', function () {
        id = $(this).closest('li').attr('id');
        data = {
            id: id
        };
        if (confirm("Вы уверены, что хотите удалить данный элемент меню?")) {
            $(this).closest('li').remove();
            $.post('<?php echo $this->createUrl('menu/delete')?>', data, function () {
            });
        } else {
            return;
        }
    });

    $('.update-item').on('click', function () {
        id = $(this).closest('li').attr('id');
        data = {
            id: id
        };
        $.post('<?php echo $this->createUrl('menu/form')?>', data, function (data) {
            $('#item-dialog').empty().append(data).dialog('open');
        });
    });
</script>
<style>
    .dialog-page {
        background: #0097C0;
    }
</style>