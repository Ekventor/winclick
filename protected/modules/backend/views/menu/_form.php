<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'pages-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <table class="form-table">
        <tbody>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'url'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->textField($model, 'url', array('size' => 60, 'maxlength' => 255)); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model, 'url'); ?>
            </td>
        </tr>

        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'title'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model, 'title'); ?>
            </td>
        </tr>

        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'unauthorized'); ?>
            </td>
            <td class="input-td">
                <span class="switch">
                    <?php echo $form->checkBox($model, 'unauthorized', ['id' => 'autowk']); ?>
                    <label for="autowk">
                        <span></span>
                    </label>
                </span>
            </td>
            <td class="example-td">
                <?php echo $form->error($model, 'unauthorized'); ?>
            </td>
        </tr>

        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'position'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->dropDownList($model, 'position', $position); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model, 'position'); ?>
            </td>
        </tr>

        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model, 'parent'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->dropDownList($model, 'parent', $parents); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model, 'parent'); ?>
            </td>
        </tr>

        </tbody>
    </table>


    <?php echo CHtml::hiddenField('id', $model->id); ?>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(function(){
        $('#Menu_position').change(function(){
            $.post('<?= $this->createUrl("getParent"); ?>', {position: $(this).val()}, function(result){
                var resultStr = '';
                for (var index in result) {
                    str = '<option value="' + index + '">' + result[index] + '</option>';
                    resultStr = resultStr + str;
                }
                $('#Menu_parent').empty().append(resultStr).val(<?= $model->parent; ?>);
            }, 'json');
        });
        $('#Menu_position').change();
    });
</script>