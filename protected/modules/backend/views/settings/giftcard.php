<?php if(count($data)>0):?>
    <?php foreach($data as $row):?>
        <div class="view" style="height: 25px; margin-bottom: 3px;">
            <?php echo CHtml::link($row->title, array('update', 'id'=>$row->id), ['style' => 'color: #1a97ff; text-decoration: none;']); ?>
            <br />
        </div>
    <?php endforeach;?>
<?php else: ?>
    <div class="view" style="height: 25px; margin-bottom: 3px;">Настроек не определено.</div>
<?php endif;?>

<?= CHtml::form('', 'post', ['enctype'=>'multipart/form-data', 'id'=>'images']);?>
<label>Карта :<br /> <?= CHtml::activeFileField($model, 'image[present]');?> <?= CHtml::image(Y::param('certPath').'/present.jpg?'.time(), '@', ['style'=>'width: 243px; height: 143px;']);?></label><br>
<?= CHtml::submitButton('Загрузить');?>
<?= CHtml::endForm();?>