<?= CHtml::form('', 'post', ['enctype'=>'multipart/form-data', 'id'=>'images']);?>
<div class="settings images">
    <label>Изображение по умолчанию</label>
    <label><?= CHtml::activeFileField($model, 'image[default]');?> <?= CHtml::image(Y::param('certPath').'/default.jpg?'.time(), '@', ['style'=>'width: 432px; height: 189px;']);?></label>
</div>
<?php foreach(Y::param('nominals') as $nominal):?>
<div class="settings images">
    <label>Для номинала <?=$nominal;?></label>
    <label>По умолчанию:<br /> <?= CHtml::activeFileField($model, 'image['.$nominal.'][default]');?> <?= CHtml::image(Y::param('certPath').'/'.$nominal.'default.jpg?'.time(), '@', ['style'=>'width: 432px; height: 189px;']);?></label>
    <?php foreach($letters as $letter):?>
    <label><?= $letter;?>: <?= CHtml::activeFileField($model, 'image['.$nominal.']['.$letter.']');?> <?= CHtml::image(Y::param('certPath').'/'.$nominal.$letter.'.jpg?'.time(), '@', ['style'=>'width: 432px; height: 189px;']);?></label>
    <?php endforeach;?>
    <label>Выигрышный:<br /> <?= CHtml::activeFileField($model, 'image['.$nominal.'][!]');?> <?= CHtml::image(Y::param('certPath').'/'.$nominal.'!.jpg?'.time(), '@', ['style'=>'width: 432px; height: 189px;']);?></label>
</div>
<?php endforeach;?>
<div>
<?= CHtml::submitButton('Загрузить');?>
</div>
<?= CHtml::endForm();?>