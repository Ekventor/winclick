<?php
/* @var $this SettingsController */
/* @var $data Settings */
?>

<div class="view" style="height: auto; margin-bottom: 3px;">
	<?php echo CHtml::encode($data->title); ?>
    :
	<?php echo CHtml::link($data->value ? CHtml::encode($data->value) : '-', array('update', 'id'=>$data->id), ['style' => 'color: #1a97ff; text-decoration: none;']); ?>
	<br />
</div>