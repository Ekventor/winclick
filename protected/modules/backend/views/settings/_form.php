<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'settings-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	
	<div class="row">
		<?php if ($model->type == 'bonus'
			|| $model->type == 'coupon'
			|| $model->type == 'giftcard'):?>
			<div class="row">
			<?php $this->widget('Redactor', [
			    'model' => $model,
			    'attribute' => 'value',
			    'toolbar' => 'mini',
                'lang' => 'ru'
			]); ?>
				<?php echo $form->error($model,'content'); ?>
			</div>
		<?php else: ?>
			<?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>255)); ?>
		<?php endif;?>
		
		<?php echo $form->error($model,'value'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<style>
	.errorMessage {
		width: auto;
		color: #f00;
	}
</style>