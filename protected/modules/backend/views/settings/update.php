<?php
/* @var $this SettingsController */
/* @var $model Settings */

$this->breadcrumbs=array(
	'Settings'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Settings', 'url'=>array('index')),
	array('label'=>'View Settings', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Settings', 'url'=>array('admin')),
);
?>

<h3>Редактирование "<?php echo $model->title; ?>"</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>