<?php
/* @var $this SettingsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Settings',
);

?>

<h1>Настройки</h1>

<?php //$this->widget('zii.widgets.CListView', array(
//	'dataProvider'=>$dataProvider,
//	'itemView'=>'_view',
//)); ?>

<?php
    $this->widget('zii.widgets.jui.CJuiTabs', [
        'tabs' => [
//            'Сертификаты' => ['ajax' => '/backend/settings/cert'],
            'Сайт' => ['ajax' => '/backend/settings/site'],
            'Игра' => ['ajax' => '/backend/settings/game'],
            'Демо-аукцион' => ['ajax' => '/backend/settings/demo'],
//            'Изображения' => ['ajax' => '/backend/settings/images'],
            'Последовательности' => ['ajax' => '/backend/settings/seq'],
            'Подарочные купоны' => ['ajax' => '/backend/settings/bonus'],
//            'Сообщения' => ['ajax' => '/backend/settings/message'],
            'Подарочные сертификаты' => ['ajax' => '/backend/settings/giftcard']
        ]
    ]);
?>