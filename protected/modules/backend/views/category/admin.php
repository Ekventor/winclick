<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Manage',
);
?>

<h1>Управление категориями</h1>

<?= CHtml::link('Создать', $this->createUrl('create')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
    'htmlOptions'=>array(
        'class'=>'admin-table',
    ),
    'columns'=>array(
		'id',
		'name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
