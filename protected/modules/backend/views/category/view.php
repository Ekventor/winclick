<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name,
);

?>

<h1>Категория #<?php echo $model->id; ?></h1>

<?php $columns = array(
    'id',
    'name'
); ?>
<table class="form-table">
    <tbody>
    <?php foreach($columns as $column) : ?>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <label><?php echo Yii::t('app', 'category.'.$column); ?>:</label>
            </td>
            <td class="input-td">
                <p class="login-name"><?php echo CHtml::encode($model->$column); ?></p>
            </td>
            <td class="example-td">
            </td>
            <td class="last-field-td">
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>