<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Users', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Users', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'Просмотр пользователя'); ?>  #<?php echo $model->id; ?></h1>

<?php $columns = array(
	'phone',
	'login',
	'email',
	'created',
	'modified',
); ?>


<table class="form-table">
	<tbody>
		<?php foreach($columns as $column) : ?>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<label><?php echo Yii::t('app', 'users.'.$column); ?>:</label>
			</td>
				<td class="input-td">
			<p class="login-name"><?php echo CHtml::encode($model->$column); ?></p>
			</td>
			<td class="example-td">
			</td>
			<td class="last-field-td">				
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
