<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = [
    'Users' => ['index'],
    'Manage',
];

$this->menu = [
    ['label' => 'List Users', 'url' => ['index']],
    ['label' => 'Create Users', 'url' => ['create']],
];

Yii::app()->clientScript->registerScript(
    'search',
    "$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
    var role = {
        change: function(object) {
            data = {
                role : object.val(),
                userId : object.attr('element')
            };
            $.post('/backend/users/updateRole', data);
        }
    };
</script>

    <h1>Пользователи</h1>

<?php echo CHtml::link('Поиск', '#', ['class' => 'search-button']); ?><br>
<?php echo CHtml::link('Создать', $this->createUrl('create')); ?>

    <div class="search-form" style="display:none">
        <?php
        $this->renderPartial(
            '_search',
            ['model' => $model]
        );
        ?>
    </div><!-- search-form -->

<?php
$this->widget(
    'zii.widgets.grid.CGridView',
    [
        'id' => 'users-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'htmlOptions' => [
            'class' => 'admin-table',
        ],
        'columns' => [
            'id',
            'phone',
            'login',
            'email',
            'created',
            [
                'sortable' => false,
                'name' => 'role',
                'header' => 'Роль',
                'type' => 'raw',
                'value' => 'CHtml::dropDownList(
                "role",
                $data->role,
                $data->getRoles(),
                [
                    "class" => "select-role",
                    "element" => $data->id,
                    "onchange" => "js:role.change($(this));"
                ])'
            ],
            [
                'class' => 'CButtonColumn',
                'template' => '{delete}'
            ],
        ],

    ]
);
?>