<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
    'clientOptions' => [
        'validateOnSubmit' => true,
        'validateOnChange' => false
    ],
	'htmlOptions'=>array(
		'class'=>'my-form'
	),
)); ?>

<table class="form-table">
	<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'phone'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'phone'); ?>
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'login'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'login',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'login'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'password'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'password'); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'email'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'email'); ?>
			</td>
		</tr>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model,'role'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->dropDownList($model,'role', $model->getRoles()); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model,'role'); ?>
            </td>
        </tr>

	</tbody>
</table>
<div class="row buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
</div>
		
<?php $this->endWidget(); ?>