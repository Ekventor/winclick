<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


<table class="form-table">
	<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'id'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>255)); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'phone'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255)); ?>
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'login'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'login',array('size'=>60,'maxlength'=>255)); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'email'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
			</td>
		</tr>

		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'created'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'created'); ?>
			</td>
		</tr>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'modified'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'modified'); ?>
			</td>
		</tr>

		
	</tbody>
</table>
<div class="row buttons">
	<?php echo CHtml::submitButton('Поиск'); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->