<?php
/* @var $this CityController */
/* @var $model City */
/* @var $form CActiveForm */
?>
<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'city-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>

    <table class="form-table">
        <tbody>
        <tr>
            <td class="labels-td">
                <img src="/img/white-circle.png" alt="">
                <?php echo $form->labelEx($model,'name'); ?>
            </td>
            <td class="input-td">
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
            </td>
            <td class="example-td">
                <?php echo $form->error($model,'name'); ?>
            </td>
        </tr>
        </tbody>
    </table>


    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'wk_button']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->