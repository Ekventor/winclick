<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	'Manage',
);

?>

<h1>Управление городами</h1>

<?= CHtml::link('Создать', $this->createUrl('create')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'city-grid',
	'dataProvider'=>$model->search(),
    'htmlOptions'=>array(
        'class'=>'admin-table',
    ),
    'columns'=>array(
		'id',
		'name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
