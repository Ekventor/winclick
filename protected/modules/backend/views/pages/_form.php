<?php
/* @var $this PagesController */
/* @var $model Pages */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pages-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	
<table class="form-table">
	<tbody>
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'title'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'title'); ?>
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'content'); ?>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<?php $this->widget('Redactor', [
					'model' => $model,
					'attribute' => 'content',
                    'lang' => 'ru',
					'editorOptions' => [
						'imageUpload' => $this->createUrl('images'),
						'fileUpload' => $this->createUrl('files')
					]
				]); ?>
				<?php echo $form->error($model,'content'); ?>
			</td>
		</tr>
		
		<tr>
			<td class="labels-td">
				<img src="/img/white-circle.png" alt="">
				<?php echo $form->labelEx($model,'alias'); ?>
			</td>
			<td class="input-td">
				<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?>
			</td>
			<td class="example-td">
				<?php echo $form->error($model,'alias'); ?>
			</td>
		</tr>

	</tbody>
</table>
	
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->