<?php
/* @var $this PagesController */
/* @var $model Pages */

$this->breadcrumbs=array(
	'Pages'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Pages', 'url'=>array('index')),
	array('label'=>'Create Pages', 'url'=>array('create')),
);

?>

<h1>Статические страницы</h1>

<?php
    echo CHtml::link('Создать', $this->createUrl('pages/create'));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions'=>array(
		'class'=>'admin-table',
	),
	'columns'=>array(
		'id',
		'title',
		'author',
		'created',
		'modified',
		'alias',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
