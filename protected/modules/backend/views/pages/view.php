<?php
/* @var $this PagesController */
/* @var $model Pages */

$this->breadcrumbs=array(
	'Pages'=>array('index'),
	$model->title,
);
?>

<h1>Просмотр статической страницы "<?php echo $model->title; ?>"</h1>

<?php echo strip_tags($model->content, Y::param('allowed_tags')); ?>
