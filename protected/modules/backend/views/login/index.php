<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */

?>

<div class="registration-popup admin-login">
    <div class="head-block">
        <p>АВТОРИЗАЦИЯ</p>
    </div>
    <div class="cont-block">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-admin-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        )); ?>

        <?php echo $form->error($model, 'login', ['class' => 'errorMessage admin']); ?>
        <?php echo $form->textField($model, 'login', ['class' => 'fill-field login', 'placeholder' => 'Введите ваш логин']); ?>

        <?php echo $form->error($model, 'password', ['class' => 'errorMessage admin']); ?>
        <?php echo $form->passwordField($model, 'password', ['class' => 'fill-field password', 'placeholder' => 'Введите ваш пароль']); ?>

        <div class="button-wrap">
            <?php echo CHtml::submitButton('Login', ['class' => 'yellow-button']); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <!-- form -->
</div>