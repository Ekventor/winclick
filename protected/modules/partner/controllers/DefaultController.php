<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		if (!Y::isLogged()) {
            $this->redirect($this->createUrl('/partner/partner/login'));
        } else {
            $this->redirect($this->createUrl('/partner/partner/checking'));
        }
        $this->render('index');
	}
}