<?php

class PartnerController extends PController
{
	public $layout = '/layouts/column2';

    public function actionIndex()
	{
		$this->render('index');
	}

    public function actionLogin()
    {
        $this->layout = '/layouts/column1';
        $model=new LoginForm;

        // collect user input data
        if(isset($_POST['PartnerLoginForm']))
        {
            $model->attributes=$_POST['PartnerLoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login()) {
                $this->redirect($this->createUrl('checking'));
            }
        }
//        Y::endJson(['error'=>1, 'errors'=>['common'=>Yii::t('app', 'Ошибка запроса: нет данных')]]);
        $this->render('login');
    }

    public function actionChecking()
    {
        $this->layout = '/layouts/column2';

        if (!Yii::app()->user->isShop()
            && !Yii::app()->user->isAdmin()
        ) {
            $this->redirect($this->createUrl('/partner/partner/login'));
        }

        $number = Yii::app()->request->getParam('number', '');
        $status = '';
        if ($number) {
            $status = Yii::app()->coupon->checkCoupon($number);
        }

        $this->render('checking', [
            'status' => isset($status['status']) ? $status['status'] : '',
            'endTime' => isset($status['endTime']) ? $status['endTime'] : '',
            'number' => $number
        ]);
    }

}