<?php $this->beginContent('/layouts/main_new'); ?>
    <div class="partnersWrapper">
        <div class="partner-page">
            <div class="partner-block">
                <div class="partner-header">
                    <span class="header-text">Проверка номера купона служащими предприятия партнера агентства</span>
                </div>
                <div class="partner-menu">
<!--                    <div class="partner-menu-button">-->
<!--                        <span class="button-text">-->
<!--                            --><?//= Y::isLogged() ? CHtml::link('О проекте', '/partnerAbout') : CHtml::link('О проекте', '#'); ?>
<!--                        </span>-->
<!--                    </div>-->
<!--                    <div class="partner-menu-button">-->
<!--                        <span class="button-text">-->
<!--                            --><?//= Y::isLogged() ? CHtml::link('Оферта', '/oferta') : CHtml::link('Оферта', '#'); ?>
<!--                        </span>-->
<!--                    </div>-->
<!--                    <div class="partner-menu-button">-->
<!--                        <span class="button-text">-->
<!--                    --><?//= Y::isLogged() ? CHtml::link('Помощь', '/partnerHelp') : CHtml::link('Помощь', '#'); ?>
<!--                </span>-->
<!--                    </div>-->
<!--                    <div class="partner-menu-button">-->
<!--                <span class="button-text">-->
<!--                    --><?//= Y::isLogged() ? CHtml::link('Новости', '/partnerNews') : CHtml::link('Новости', '#'); ?>
<!--                </span>-->
<!--                    </div>-->
                </div>
                <div class="partner-content">
                    <div class="left-block">
                        <?php echo $content; ?>
                    </div>
                    <!--                <div class="right-block">-->
                    <!--                    <div class="partner-info">-->
                    <!--                    <span class="info-header">-->
                    <!--                        Хотите увеличить объемы продаж?-->
                    <!--                    </span><br>-->
                    <!--                    <span>-->
                    <!--                        Мы предоставим Вам сервис бесплатно и навсегда!<br>-->
                    <!--                        Свяжитесь с нами по телефону: +7 (906) 033-24-46<br>-->
                    <!--                        или e-mail: <a href="mailto:winklik@bk.ru">winklik2014@bk.ru</a>-->
                    <!--                    </span>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                </div>
                <div class="partner-footer">
                    <span class="footer-text">"Винклик" - сверх эффективный инструмент увеличения объемов продаж!</span>
                </div>
            </div>
        </div>

    </div>
<?php $this->endContent(); ?>