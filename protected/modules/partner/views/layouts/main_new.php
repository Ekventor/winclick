<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Comfortaa&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/main_new.css">
    <link rel="stylesheet" href="/css/main_page.css">
    <script type="text/javascript" src="/js/vendor/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery.color.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery-ui.js"></script>
    <script src="/js/vendor/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script type="text/javascript" src="/js/game.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/smartoverlay.js"></script>
    <script type="text/javascript" src="/js/vendor/cwcle.js" ></script>
    <script type="text/javascript" src="/js/vendor/cycle2.js" ></script>
    <script type="text/javascript" src="/js/vendor/jquery.form.js" ></script>
    <script type="text/javascript" src="/js/vendor/jquery.cycle2.carousel.min.js" ></script>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/vendor/jquery.slot.min.js');?>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/smartdropdown.js');?>
    <!--[if lt IE 9]>
    <script src="/dest/js/html5shiv.js"></script>
    <![endif]-->

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="w1-verification" content="196595389618" />

</head>
<body>
<div class="globalWrapper">

    <?php echo $content; ?>

    <div class="footerBgWrapper">
        <div class="footerWrapper cf" id="f4">
            <div class="footer resize cf">
                <div class="fll left">
                    © ООО “МПА ”ВИНКЛИК” <?= date('Y') ?> год <br>
                    Все права защищены
                </div>
                <div class></div>
                <div class="bottom-menu">
                    <div class="bottom-list">
                        <?php $this->widget('ext.widgets.NavigationMenu', [
                            'items' => [
                                [
                                    'class' => 'ext.widgets.HorizontalMenu',
                                    'options' => [
                                        'position' => 'bottom',
                                        'htmlOptions' => [
                                            'class' => ''
                                        ],
                                        'otherLinks' => [
                                            [
                                                'title' => 'Главная',
                                                'url' => '/',
                                                'prev' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-59109945-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
