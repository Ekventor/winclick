<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Comfortaa&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/main_new.css">
    <script type="text/javascript" src="/js/vendor/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery.tmpl.min.js"></script>
    <script src="/js/vendor/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script type="text/javascript" src="/js/game.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/smartoverlay.js"></script>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/vendor/jquery.slot.min.js');?>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/smartdropdown.js');?>
    <!--[if lt IE 9]>
    <script src="/dest/js/html5shiv.js"></script>
    <![endif]-->

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

</head>
<body>
<!--[if lt IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class="header-wrap">

</div>


<!-- BEGIN CONTENT -->
<?php echo $content; ?>
<!-- END CONTENT -->

<!-- BEGIN FOOTER -->
<div class="footer-wrap">
    <?php if (!Yii::app()->user->isShop()): ?>
    <div class="footer-nav-wrap">
        <div class="c-align">
            <?php $this->widget('ext.widgets.NavigationMenu', [
                'items' => [
                    [
                        'class' => 'ext.widgets.HorizontalMenu',
                        'options' => [
                            'position' => 'bottom',
                            'htmlOptions' => [
                                'class' => 'footer-nav clearfix'
                            ],
                            'otherLinks' => [
                                [
                                    'title' => 'Выход',
                                    'url' => '/',
                                    'prev' => false
                                ]
                            ]
                        ]
                    ]
                ]
            ]);?>
        </div>
    </div>
    <?php endif; ?>
    <div class="c-align">
        <footer class="site-footer clearfix">
            <span class="copyrights">© ЗАО “МПА ”ВИНКЛИК” 2013 год</span>
            <a href="#" class="ed-kashel"><!----></a>
            <span class="social">
				<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
				<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir" data-yashareTheme="button"></div>
		    </span>
            <a href="#" class="years-label sprite"><!----></a>
            <span class="copyrights-2">Все права защищены</span>
        </footer>
    </div>
</div>
<!-- END FOOTER -->

</body>
</html>
