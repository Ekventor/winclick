<?php $this->beginContent('/layouts/main_new'); ?>
    <div class="partnersWrapper">
        <div class="partner-page">
            <div class="partner-block">
                <div class="partner-header">
                    <span class="header-text">Информация для предприятия партнера агентства</span>
                </div>
                <div class="partner-content" style="margin-top: 106px;">
                    <div class="left-block">
                        <?php echo $content; ?>
                    </div>
                </div>
                <div class="partner-footer">
                    <span class="footer-text">"Винклик" - сверх эффективный инструмент увеличения объемов продаж!</span>
                </div>
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>