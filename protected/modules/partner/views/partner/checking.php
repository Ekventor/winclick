<div class="checking-form">
    <div class="form-header">
        Проверка купона
    </div>
    <div class="form">
        <form enctype="multipart/form-data" method="POST">
            <input id="number-input" type="text" name="number" value="<?= CHtml::encode($number); ?>"><br>

            <div class="checking-result">
                <table>
                    <tr>
                        <td>
                            <span>Статус:</span>
                        </td>
                        <td class="result-td">
                            <span class="result-block"><?= $status; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Годен до:</span>
                        </td>
                        <td class="result-td">
                            <span class="result-block"><?= $endTime; ?></span>
                        </td>
                    </tr>
                </table>

            </div>
            <div class="form-submit-block">
                <input id="partner-form-submit" type="submit" value="Проверить">
            </div>
        </form>
        <div class="exit-link">
            <span>
                <?= CHtml::link('Выход', $this->createUrl('/user/logout')); ?>
            </span>
        </div>
    </div>
</div>