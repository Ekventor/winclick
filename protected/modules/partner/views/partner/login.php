<div class="partner-login-form">
    <div class="form-header">
        Вход для партнера
    </div>
    <div class="form">
        <form enctype="multipart/form-data" method="POST">
            <div class="form-label-block">
                <label for="email-input">Логин: </label><br>
            </div>
            <input id="email-input" type="text" name="PartnerLoginForm[login]"><br>

            <div class="form-label-block">
                <label for="pass-input">Пароль: </label><br>
            </div>
            <input id="pass-input" type="password" name="PartnerLoginForm[password]"><br>

            <div class="form-submit-block" style="margin-left: 57px; margin-top: 30px;">
                <input id="partner-form-submit" type="submit" value="Войти">
            </div>
        </form>
    </div>
</div>
