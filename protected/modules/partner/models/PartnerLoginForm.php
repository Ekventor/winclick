<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class PartnerLoginForm extends CFormModel
{
    public $email;
    public $password;


    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that login and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // login and password are required
            array('email, password', 'required'),
            // password needs to be authenticated
            array('password', 'authenticate'),
            ['email', 'email']
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array();
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_identity = new PartnerUserIdentity($this->email, $this->password);
            if (!$this->_identity->authenticate())
                $this->addError('password', 'Incorrect login or password.');
        }
    }

    /**
     * Logs in the user using the given login and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new PartnerUserIdentity($this->email, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === PartnerUserIdentity::ERROR_NONE) {
            $duration = 0; // TODO: possible must be saved for longer
            Yii::app()->user->login($this->_identity, $duration);
            //log action login
            Y::logAction('login', false);

            return true;
        } else
            return false;
    }
}
