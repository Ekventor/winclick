<?php

return [
    'RECOVER_MESSAGE' => 'На вашу почту {email} выслано письмо с дальнейшими инструкциями', //{email}
    'RECOVER_EMAIL' => 'Чтобы восстановить доступ к своему аккаунту, пройдите по ссылке {link}',
    'REG_EMAIL' => 'Чтобы активировать свой аккаунт и установить пароль, вам необходимо пройти по ссылке {link}.', //{token}, {link}
    'COUPON_EMAIL' => "Купонов приобретено: {count}.\n Номера купонов:\n <h2><b>{numbers}</b></h2>. \n Спасибо, что воспользовались нашем сервисом!",
    'AccountHistory.ID' => '#',
    'AccountHistory.Date' => 'Дата',
    'AccountHistory.Open Balance' => 'Начальный баланс',
    'AccountHistory.Account' => 'Аккаунт',
    'AccountHistory.Amount' => 'Зачисление',
    'AccountHistory.Close Balance' => 'Итого',
    'AccountHistory.Comment' => 'Информация',

    'accounthistory.id' => '#',
    'accounthistory.date' => 'Дата',
    'accounthistory.open_balance' => 'Начальный баланс',
    'accounthistory.account' => 'Аккаунт',
    'accounthistory.amount' => 'Зачисление',
    'accounthistory.close_balance' => 'Итого',
    'accounthistory.comment' => 'Информация',
    'accounthistory.account_id' => 'Аккаунт',

    'Time' => 'Время',


    'users.id' => '#',
    'users.phone' => 'Телефон',
    'users.login' => 'Логин',
    'users.email' => 'E-mail',
    'users.created' => 'Дата создания',
    'users.modified' => 'Дата последнего изменения',
    'users.password' => 'Пароль',


    'pages.id' => '#',
    'pages.title' => 'Заголовок',
    'pages.author' => 'Автор',
    'pages.created' => 'Дата создания',
    'pages.modified' => 'Дата изменения',
    'pages.alias' => 'Алиас',
    'pages.content' => 'Текст',


    'review.id' => '#',
    'review.user_id' => 'Пользователь',
    'review.author' => 'Автор',
    'review.profession' => 'Профессия',
    'review.text' => 'Текст',
    'review.created' => 'Дата создания',
    'review.show' => 'Активность',
    'review.avatar' => 'Аватар',


    'mailings.id' => 'ID',
    'mailings.title' => 'Заголовок',
    'mailings.message' => 'Сообщение',
    'mailings.date' => 'Дата',
    'mailings.creator' => 'Создал',
    'mailings.user' => 'Пользователь',
    'mailings.readed' => 'Прочитано',

    'organization.id' => '#',
    'organization.name' => 'Название организации',
    'organization.category_id' => 'Категория организации',
    'organization.city_id' => 'Город',
    'organization.specialization' => 'Специализация',
    'organization.contacts' => 'Контакты',
    'organization.desc' => 'Описание',
    'organization.img' => 'Изображение',

    'city.id' => '#',
    'city.name' => 'Название города',

    'category.id' => '#',
    'category.name' => 'Название категории',

    'content.id' => '#',
    'content.title' => 'Заголовок',
    'content.short_text' => 'Краткое описание',
    'content.text' => 'Текст',
    'content.created' => 'Дата создания',
    'content.modified' => 'Дата изменения',
    'content.alias' => 'Алиас',
    'content.order' => 'Позиция',



    'mailing_table.id' => '#',
    'mailing_table.phone' => 'Номер телефона',
    'mailing_table.login' => 'Логин',

    'giftcard.id' => '#',
    'giftcard.title' => 'Заголовок',
    'giftcard.short_desc' => 'Краткое описание',
    'giftcard.desc' => 'Полное описание',
    'giftcard.small_img' => 'Малое изображение',
    'giftcard.img' => 'Оригинальное изображение',
    'giftcard.nominals' => 'Номиналы',
    'giftcard.addit_info' => 'Дополнительная информация',

    'user_giftcards.id' => '#',
    'user_giftcards.user_id' => 'Пользователь',
    'user_giftcards.giftcard_id' => 'Подарочная карта',
    'user_giftcards.nominal' => 'Номинал',
    'user_giftcards.count' => 'Количество',
    'user_giftcards.date' => 'Дата приобретения',

    
    'Incorrect login or password' => 'Неправильный логин или пароль',

    'coupon.success_take' => 'Операция успешно завершна. На ваш Email отправлены номера ваших подарочных купонов'
];
?>