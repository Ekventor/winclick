<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 04.09.14
 * Time: 17:37
 */
class CardComponent extends ServiceLayerComponent
{
    /**
     * Записывает в AccountHistory все выйгрыши в % за исключением купонов
     * @param $winList
     */
    public function addCardHistory($winList)
    {
        echo 'collecting wins history...';
//        $to_insert[] = '(' .
//            $row['user_id'] . ", '".
//            $row['prize']. "', ".
//            $row['nominal'] .", '" .
//            $row['game_id']."', '" .
//            date('Y-m-d H:i:s', $row['timestamp'])."')";

        echo date('Y-m-d H:i:s') . ": \n";
        try {
            $coupon = Yii::app()->params['coupon'];
            $insertValues = [];
//            echo "Total in: \n";
//            foreach ($winList as $win) {
//                echo $win['user_id'] . ', ' . $win['prize'] . ', ' . $win['nominal'] . "\n";
//            }
//            echo "End total in; \n";

            $wins = [];
            foreach ($winList as $win) {
                if ($win['prize'] != $coupon) {
                    $wins[$win['user_id']][$win['nominal']][] = ['prize' => $win['prize'], 'timestamp' => $win['timestamp']];
                }
            }
//            echo "Result IN: \n";
//            print_r ($wins);
//            echo "Result IN end \n";

            $criteria = new CDbCriteria();
            $to_insert = [];
            foreach ($wins as $userId => $win) {
                foreach($win as $nominal => $prizes) {
                    $criteria->condition = 'user_id = '. $userId . " and type = 'CARD" . $nominal . "'";
                    $criteria->limit = 1;
                    $criteria->order = 'id DESC';
                    $accountHistory = AccountHistory::model()->find($criteria);
                    $balance = $accountHistory !== null ? $accountHistory->close_balance : 0;
                    foreach ($prizes as $prize) {
                        $settings = Settings::model()->findByAttributes(['key' => 'result_percent;' . $prize['prize']]);
                        $amount = ($settings->value * $nominal) / 100;
                        $string = "(0, " . $balance
                            . ", " . ($balance + $amount)
                            . ", " . $amount
                            . ", '" . date('Y-m-d H:i:s', $prize['timestamp'])
                            . "', " . $userId
                            . ", 'CARD" . $nominal
                            . "' , 'Выйгрыш')";
                        $to_insert[] = $string;
                        $balance = $balance + $amount;
                    }
                }
            }
//            echo "Insert result: \n";
//            print_r ($to_insert);
//            echo "Insert result end \n";

            if (count($to_insert)) {
                $insertLine = implode(', ', $to_insert);
                $sql = "INSERT INTO " . AccountHistory::model()->tableName()
                    . " (account_id, open_balance, close_balance, amount, date, user_id, type, comment) VALUES "
                    . $insertLine;
                Yii::app()->db->createCommand($sql)->execute();
            }



//            for ($i = 0; $i <= count($winList); $i++) {
//                if (!isset($winList[$i]) || $winList[$i]['prize'] == $coupon) {
//                    continue;
//                }
//                $userId = $winList[$i]['user_id'];
//                $nominal = $winList[$i]['nominal'];
//                $userWins[] = $winList[$i];
//                for ($j = $i+1; $j < count($winList); $j++) {
//                    if (!isset($winList[$j]) || $winList[$j]['prize'] == $coupon) {
//                        continue;
//                    }
//                    if ($userId == $winList[$j]['user_id'] && $nominal == $winList[$j]['nominal'] && $winList[$j]['prize'] != $coupon) {
//                        $userWins[] = $winList[$j];
//                        unset ($winList[$j]);
//                    }
//                }
//                $winList = array_values($winList);
//                $insertValues = array_merge($this->generateInsertValues($userWins), $insertValues);
//            }
//            if (count($insertValues)) {
//                $insertLine = implode(', ', $insertValues);
//                $sql = "INSERT INTO " . AccountHistory::model()->tableName()
//                    . " (account_id, open_balance, close_balance, amount, date, user_id, type, comment) VALUES "
//                    . $insertLine;
//                Yii::app()->db->createCommand($sql)->execute();
//            }
            echo "Success \n";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Гинерирует VALUES
     * @param $array
     */
    public function generateInsertValues($array)
    {
        $nominal = $array[0]['nominal'];
        $userId = $array[0]['user_id'];
        $criteria = new CDbCriteria();
        $criteria->compare('user_id', $userId);
        $criteria->compare('type', 'CARD' . $nominal);
        $criteria->limit = 1;
        $criteria->order = 'id DESC';
        $accountHistory = AccountHistory::model()->find($criteria);
        if ($accountHistory !== null) {
            $closeBalance = $accountHistory->close_balance;
        } else {
            $closeBalance = 0;
        }
        $insertValue = [];
        foreach ($array as $item) {
            $settings = Settings::model()->findByAttributes(['key' => 'result_percent;' . $item['prize']]);
            $amount = ($settings->value * $nominal) / 100;
            $string = "(0, " . $closeBalance
                . ", " . ($closeBalance + $amount)
                . ", " . $amount
                . ", '" . date('Y-m-d H:i:s', $item['timestamp'])
                . "', " . $item['user_id']
                . ", 'CARD" . $item['nominal']
                . "' , 'Выйгрыш')";
            $insertValue[] = $string;
//            echo "$string \n";
            $closeBalance = $closeBalance + $amount;
        }
        return $insertValue;
    }

    /**
     * Получает историю покупки кард
     * @param $userId
     * @return array
     */
    public function getCardHistory($userId) {
        $nominals = Y::app()->params['nominals'];
        foreach ($nominals as &$nominal) {
            $nominal = 'CARD' . $nominal;
        }
        $acc_id = Yii::app()->account->getAccIdByCurrency(Y::userId(), 'RUB');
            
        $criteria = new CDbCriteria();
        $criteria->addInCondition('type', $nominals);
        $criteria->addCondition('amount < 0');
        $criteria->compare('account_id', $acc_id);
        $criteria->compare('user_id', $userId);
        $criteria->order = 'id DESC';
        $accountHistory = AccountHistory::model()->findAll($criteria);
        $history = [];
        if (count($accountHistory)) {
            foreach ($accountHistory as $item) {
                $type = $item->type;
                $nominal = (int)str_replace('CARD', '', $type);
                $count = round((abs($item->amount) * 100) / ($nominal * 95));
                $history[] = [
                    'date' => date("d.m.Y", strtotime($item->date)),
                    'time' => date("H:i:s", strtotime($item->date)),
                    'sum' => round((((abs($item->amount) / $count) * 100) / 95) * $count),
                    'count' => $count,
                    'info' => $item->comment
                ];
            }
        }
        return $history;
    }

    /**
     * Получить баланс подарочных скидок для конкретного пользователя
     * @param $userId
     * @return int
     */
    public function getCardBalance($userId, $nominal)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('user_id', $userId);
        $criteria->compare('type', 'CARD' . $nominal);
        $criteria->limit = 1;
        $criteria->order = 'id DESC';
        $accountHistory = AccountHistory::model()->find($criteria);
        return $accountHistory !== null ? $accountHistory->close_balance : 0;
    }


    /**
     * Получает количество выйграных карт
     * @param $userId
     * @param $nominal
     * @return int
     */
    public function getCardCount($userId, $nominal)
    {
        $balance = $this->getCardBalance($userId, $nominal);
        return floor($balance / $nominal);
    }

    /**
     * Получить список всех подарочных сертификатов, доступных для данного номинала
     * @param $nominal
     * @return array
     */
    public function getCardSertificates($nominal)
    {
        if (!$nominal) {
            return [];
        }
        $sql = "select * from giftcard a1 where '" . $nominal . "' in (select json_array_elements(a2.nominals)::text from giftcard a2 where a1.id = a2.id)";
        $sertificates = Yii::app()->db->createCommand($sql)->queryAll();
        return $sertificates;
    }
}