<?php

/*
 * Компонент обработки статистики
 *
 * @property array $serviceConfig
 * @property array $_accountInfo 
 */
class StatisticComponent extends ServiceLayerComponent {
    
    public function aggregate() {
        
        $hour_start = mktime(date('H'), 0, 0, date('m'), date('d'), date('Y'));
        $hour_end = mktime(date('H'), 59, 59, date('m'), date('d'), date('Y'));
        
        // users registred in this hour
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('created', date('Y-m-d H:i:s',$hour_start), date('Y-m-d H:i:s',$hour_end));
        
        if ($cnt = Users::model()->count($criteria)) {
            $statistic = new Statistic;
            $statistic->attributes = ['datetime' => date('Y-m-d H:i:s', $hour_start),
                                      'type' => Statistic::TYPE_REGISTER,
                                      'value' => $cnt,
                                      ];
            if ($statistic->save())
                echo 'Register users count saved';
        }
        
        // users who deal any actions in this hour
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('datetime', date('Y-m-d H:i:s',$hour_start), date('Y-m-d H:i:s',$hour_end));
        if ($cnt = UserActionLog::model()->count($criteria)) {
            $statistic = new Statistic;
            $statistic->attributes = ['datetime' => date('Y-m-d H:i:s', $hour_start),
                                      'type' => Statistic::TYPE_ACTIVE_USERS,
                                      'value' => $cnt,
                                      ];
            if ($statistic->save())
                echo 'Active users count saved';
        }
        
        //unique visiters
        
        //$criteria = new CDbCriteria;
        //$criteria->select = 'user_id';
        //$criteria->addCondition('datetime BETWEEN :date_start AND :date_end');
        //$criteria->params = [':date_start' => date('Y-m-d H:i:s',$hour_start),':date_end' => date('Y-m-d H:i:s',$hour_end)];
        //$criteria->group = 'user_id';
        //if ($cnt = UserActionLog::model()->count($criteria)) {
        //    $statistic = new Statistic;
        //    $statistic->attributes = ['datetime' => date('Y-m-d H:i:s', $hour_start),
        //                              'type' => Statistic::TYPE_ACTIVE_USERS,
        //                              'value' => $cnt - 1 , //убираем 
        //                              ];
        //    if ($statistic->save())
        //        echo 'Active users count saved';
        //}
        
        
        // money incoming
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('datetime', date('Y-m-d H:i:s',$hour_start), date('Y-m-d H:i:s',$hour_end));
        $criteria->compare('status', Payments::STATUS_SUCCESS);
        $criteria->select = 'SUM(amount) as amount';
        
        if ($result = Payments::model()->find($criteria)) {
            $statistic = new Statistic;
            $statistic->attributes = ['datetime' => date('Y-m-d H:i:s', $hour_start),
                                      'type' => Statistic::TYPE_INCOMING,
                                      'value' => $result->getAttribute('amount'),
                                      ];
            if ($statistic->save())
                echo 'Incoming saved';
        }
        
        //покупка вк
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('date', date('Y-m-d H:i:s',$hour_start), date('Y-m-d H:i:s',$hour_end));
        $criteria->compare('type', 'WK');
        $criteria->addCondition('amount>0');
        $criteria->select = 'SUM(amount) as amount';
        if ($result = AccountHistory::model()->find($criteria)) {
            $statistic = new Statistic;
            $statistic->attributes = ['datetime' => date('Y-m-d H:i:s', $hour_start),
                                      'type' => Statistic::TYPE_BOUTH_WK,
                                      'value' => $result->getAttribute('amount'),
                                      ];
            if ($statistic->save())
                echo 'Incoming saved';
        }
        
        
        //кол-во игр
        if ($r = Yii::app()->game->getCounters()) {
            $r = $r['data'];
            foreach($r as $nominal => $val) {
                $criteria = new CDbCriteria;
                $criteria->compare('type', Statistic::TYPE_GAMES.';'.$nominal);
                $criteria->select = 'SUM(value) as value';
                if ($t = Statistic::model()->find($criteria)) {
                    $old = $t->getAttribute('value');
                } else {
                    $old = 0;
                }
                $statistic = new Statistic;
                $statistic->attributes = ['datetime' => date('Y-m-d H:i:s', $hour_start),
                                          'type' => Statistic::TYPE_GAMES .';' .$nominal,
                                          'value' => $val - $old,
                                          ];
                if ($statistic->save())
                    echo 'Incoming saved';
            }
            
        }
    }
}