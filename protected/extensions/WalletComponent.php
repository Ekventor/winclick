<?php

/*
 * Компонент взаимодействия приложения с аккаунтами
 *
 * @property integer $merchant_id Идентификатор продавца
 * @property string $success_url Страница куда будет произведен редирект платежным аггрегатором после успешной оплаты
 * @property string $fail_url Страница куда будет произведен редирект платежным аггрегатором в случае провала
 * @property string $secret Ключ для генерации подписи 
 * @property string $pay_url Адрес куда уйдет запрос на оплату
 * 
 */
class WalletComponent extends ServiceHandlerComponent {
    public $secret = null;
    public $merchant_id = null;
    public $success_url = null;
    public $fail_url = null;
    public $pay_url = null;
    
    
    
    /*
     * @return string URL for redirect
     */
    public function pay($acc_id, $amount) {
        $payment = new Payments;
        $payment->attributes = [
                                'amount' => (float)$amount,
                                'datetime' => date('Y-m-d H:i:s', time()),
                                'status' => Payments::STATUS_NEW,
                                'account_id' => (int)$acc_id,
                                'user_id' => Y::userId(),
                                'wmi_order_id' => null,
                                'wmi_currency_id' => null,
                                ];
        if ($payment->save()) {
            $params = [
                        'WMI_MERCHANT_ID' => $this->merchant_id,
                        'WMI_PAYMENT_AMOUNT' =>	sprintf('%.2f', $amount),   //Сумма заказа — число округленное до 2-х знаков после «запятой», в качестве разделителя используется «точка». Наличие 2-х знаков после «запятой» обязательно.
                        'WMI_CURRENCY_ID' => '643',                         //	Идентификатор валюты (ISO 4217): 643 — Российские рубли
                        'WMI_PAYMENT_NO' => $payment->id,                   //	Идентификатор заказа в системе учета интернет-магазина. Значение данного параметра должно быть уникальным для каждого заказа.
                        'WMI_DESCRIPTION' => "BASE64:".base64_encode(Yii::t('app', 'Пополнение счета через платежную систему')),  //Описание заказа (список товаров и т.п.) — отображается на странице оплаты заказа, а также в истории платежей покупателя. Максимальная длина 255 символов.
                        'WMI_SUCCESS_URL' => $this->success_url, 
                        'WMI_FAIL_URL' => $this->fail_url,                  //	Адреса (URL) страниц интернет-магазина, на которые будет отправлен покупатель после успешной или неуспешной оплаты.
                        'WMI_RECIPIENT_LOGIN' => Y::user()->login,          //	Логин плательщика по умолчанию. Значение данного параметра будет автоматически подставляться в поле логина при авторизации. Возможные форматы: электронная почта, номер телефона в международном формате.
                        'WMI_CUSTOMER_EMAIL' => Y::user()->email,           //	Имя, фамилия и email плательщика. Значения данных параметров будут автоматически подставляться в формы некоторых способов оплаты.
                        'WMI_CULTURE_ID' => 'ru-RU',                        //	Язык интерфейса определяется автоматически, но можно задать его: ru-RU — русский; en-US — английский.
                        'user_id' => Y::userId(),
                       ];
            //WMI_SIGNATURE Подпись платежной формы, сформированная с использованием «секретного ключа» интернет-магазина. Необходимость проверки этого параметра устанавливается в настройках интернет-магазина. Подробнее в разделе «Защита платежной формы».
            
            
            // Формирование сообщения, путем объединения значений формы, 
            // отсортированных по именам ключей в порядке возрастания.
            uksort($params, 'strcasecmp');
            $fieldValues = '';
            $string = [];
           
            foreach($params as $key => $value) 
            {
                //Конвертация из текущей кодировки (UTF-8)
                //необходима только если кодировка магазина отлична от Windows-1251
                $value = iconv("utf-8", "windows-1251", $value);
                $fieldValues .= $value;
                $string[] = $key.'='.$value;
            }
           
            // Формирование значения параметра WMI_SIGNATURE, путем 
            // вычисления отпечатка, сформированного выше сообщения, 
            // по алгоритму MD5 и представление его в Base64
           
            $string[] = 'WMI_SIGNATURE=' . base64_encode(pack("H*", md5($fieldValues . $this->secret)));
            $params['WMI_SIGNATURE'] = base64_encode(pack("H*", md5($fieldValues . $this->secret)));
            
            return $this->pay_url . '?' . http_build_query($params);
        }
        
    }
    
    public function result($data, Payments $payment) {
        $params = [];
        // Извлечение всех параметров POST-запроса, кроме WMI_SIGNATURE 
        foreach($data as $name => $value)
        {
          if ($name !== "WMI_SIGNATURE") $params[$name] = $value;
        }
         
        // Сортировка массива по именам ключей в порядке возрастания
        // и формирование сообщения, путем объединения значений формы
         
        uksort($params, "strcasecmp");
        $values = "";
         
        foreach($params as $name => $value)
        {
          $values .= $value;
        }
         
        // Формирование подписи для сравнения ее с параметром WMI_SIGNATURE
         
        $signature = base64_encode(pack("H*", md5($values . $this->secret)));
         
        //Сравнение полученной подписи с подписью W1
         
        if ($signature == $data["WMI_SIGNATURE"]) {
            if (isset($data['WMI_ORDER_STATE']) && strtoupper($data["WMI_ORDER_STATE"]) == "ACCEPTED") {
                  $payment->status = Payments::STATUS_SUCCESS;
                  //$payment->wmi_currency_id = $data['WMI_CURRENCY_ID'];
                  //$payment->wmi_order_id = $data['WMI_ORDER_ID'];
                  if ($payment->update(['status', 'wmi_currency_id', 'wmi_order_id'])) {
                      try {
                          $amount = $payment->amount / Yii::app()->settings->getValue('EXCHANGE_RATE');
                          Yii::app()->account->incBalance($payment->account_id, (int) $amount , Yii::t('app', 'Покупка'));
                      } catch (ZMQSocketException $e) {
                          $payment->status = Payments::STATUS_RETRY;
                          $payment->update(['status']);
                          Y::end('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . urlencode(Yii::t('app', 'Ошибка сервиса счетов, ' . $e->getMessage())));
                          
                      }
                      Y::end('WMI_RESULT=OK');
                  }
              
            } else { // Случилось что-то странное, пришло неизвестное состояние заказа
                Y::end('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . urlencode(Yii::t('app', 'Неверное состояние, ' . $data['WMI_ORDER_STATE'])));
            }
        } else {
          // Подпись не совпадает, возможно вы поменяли настройки интернет-магазина
          Y::end('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . urlencode(Yii::t('app', 'Неверное состояние, ' . $data['WMI_SIGNATURE'])));
        }
    }
    
    private function _generateSignature() {
        
    }
}    
?>