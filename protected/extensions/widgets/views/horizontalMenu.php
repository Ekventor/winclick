<ul class="<?php if ($class) echo $class; ?>" style="display: inline-block;">
    <?php foreach ($menu as $item): ?>
        <li <?php if (isset($item['liClass'])) echo 'class="' . $item['liClass'] . '"'; ?>><?php echo CHtml::link($item['title'], $item['url'], ['class' => isset($item['aClass']) ? $item['aClass'] : '']); ?></li>
    <?php endforeach ?>
</ul>
<?php if(!Yii::app()->user->isGuest && $position == 'top'): ?>
    <div class="user-email-info-head">
        <?= $user->email; ?>
    </div>
<?php endif; ?>
<?php if($position == 'bottom'): ?>
    <div class="wallet-logo">
        <img src="/img/main_new/w1.png">
    </div>
    <div class="eighteen"></div>
<?php endif; ?>
