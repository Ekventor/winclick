<?php
$index = 0;
?>
    <ul>
        <?php foreach ($items as $item): ?>
            <li <?php if(isset($item['active']) && $item['active']) echo 'class="active"'; ?>>
                <?php echo CHtml::link($item['title'], $item['url']); ?>
                <?php if (isset($item['childs'])): ?>
                    <ul style="display: block;">
                        <?php foreach ($item['childs'] as $key => $childs): ?>
                            <li index-data="<?= $index++; ?>" id="<?= $childs['id']; ?>" <?php if(isset($childs['active']) && $childs['active']) echo 'class="current-article"'; ?>>
                                <?php echo CHtml::link($childs['title'], $childs['url']); ?>
                            </li>
                        <?php endforeach; ?>
                        <?php $index = 0; ?>
                    </ul>
                    <!--                <a class="left-panel-menu-item" href="--><!--">Впервые здесь</a>-->
                <?php else: ?>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
<script>
    $(document).ready(function() {
        hash = window.location.hash;
        $(hash).addClass('current-article');
    });
    $('.left-section-ul li li').on('click', function() {
        $('.left-section-ul').find('.current-article').removeClass('current-article');
        $(this).addClass('current-article');
    });
</script>