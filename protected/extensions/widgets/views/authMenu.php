<ul class="<?php if ($class) echo $class; ?>">
    <?php if (Yii::app()->user->isGuest): ?>
        <li> <?php echo CHtml::link('Войти', '#', ['id' => 'login-link', 'class' => 'auth-menu auth']); ?> </li>
        <li> <?php echo CHtml::link('Регистрация', $registrationUrl, $style); ?> </li>
    <?php else: ?>
        <li> <?php echo CHtml::link('Кабинет', '/user/winkliks', ['class' => 'auth-menu auth']); ?> </li>
        <li> <?php echo CHtml::link('Выйти', '/user/logout', ['class' => 'auth-menu reg']); ?> </li>
    <?php endif ?>
</ul>
