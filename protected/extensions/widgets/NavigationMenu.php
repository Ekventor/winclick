<?php

/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 03.07.14
 * Time: 17:14
 */
class NavigationMenu extends CWidget
{
    public $items = [];

    public function run()
    {
        $this->render('navigationMenu', [
            'items' => $this->items
        ]);
    }
}