<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 03.07.14
 * Time: 17:37
 */
class AuthMenu extends CWidget
{
    public $htmlOptions = [];

    public function run() {
        if (count($this->htmlOptions)) {
            $class = $this->htmlOptions['class'];
        }
        $controllerId = Yii::app()->controller->id;
        $actionId = isset(Yii::app()->controller->action) ? Yii::app()->controller->action->id : '';

        $style = [];
        $style['class'] = 'auth-menu reg';
        if ($controllerId == 'site' && $actionId == 'index') {
            $registrationUrl = '#';
            $style['id'] = 'register-link';
        } else {
            $registrationUrl = '/index.php#register-form';
            $style['id'] = '';
        }

        $style['onclick'] = "yaCounter27707655.reachGoal('reg_header_link_click'); return true;";
        $this->render('authMenu', [
            'class' => $class,
            'registrationUrl' => $registrationUrl,
            'style' => $style
        ]);

    }
}