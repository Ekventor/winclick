<?php

/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 03.07.14
 * Time: 11:59
 *
 * Виджет меню в шапке и в футере
 */

/**
 * Class MainMenu
 */
class HorizontalMenu extends CWidget
{
    /**
     * @var string
     */
    public $position = 'top';

    /**
     * @var array
     */
    public $otherLinks = [];

    /**
     * @var array
     */
    public $htmlOptions = [];

    /**
     * @throws CException
     */
    public function run()
    {
        $position = Y::param('menuPosition')[$this->position];
        $criteria = new CDbCriteria();
        $criteria->compare('position', $position['id']);

        if (Y::isGuest()) {
            $criteria->compare('unauthorized', Y::isGuest());
        }
        $criteria->order = '"order"';
        $items = Menu::model()->findAll($criteria);

        $menu = [];
        foreach ($items as $item) {
            $menu[] = [
                'url' => '/' . $item->url,
                'title' => $item->title
            ];
        }

        if (count($this->otherLinks)) {
            $prevMenu = [];
            $postMenu = [];
            foreach ($this->otherLinks as $item) {
                $classes = null;
                if (isset($item['htmlOptions']) && count($item['htmlOptions'])) {
                    $classes = explode('>', $item['htmlOptions']['class']);
                }
                if ((isset($item['prev']) && $item['prev']) || !isset($item['prev'])) {
                    $prevMenu[] = [
                        'title' => $item['title'],
                        'url' => $item['url'],
                        'liClass' => isset($classes) && isset($classes[0]) ? trim($classes[0]) : '',
                        'aClass' => isset($classes) && isset($classes[1]) ? trim($classes[1]) : ''
                    ];
                } else {
                    $postMenu[] = [
                        'title' => $item['title'],
                        'url' => $item['url'],
                        'liClass' => isset($classes) && isset($classes[0]) ? trim($classes[0]) : '',
                        'aClass' => isset($classes) && isset($classes[1]) ? trim($classes[1]) : ''
                    ];
                }
            }
            $menu = array_merge($prevMenu, $menu);
            if (isset($postMenu[0])) {
                $menu[] = $postMenu[0];
            }
        }

        $class = '';
        if (count($this->htmlOptions)) {
            $class = $this->htmlOptions['class'];
        }
        $userId = Yii::app()->user->getId();
        $user = Users::model()->findByPk($userId);
        $this->render('horizontalMenu', [
            'menu' => $menu,
            'class' => $class,
            'position' => $this->position,
            'user' => $user
        ]);
    }

}