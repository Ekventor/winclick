<?php

/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 03.07.14
 * Time: 11:59
 *
 * Виджет меню в шапке и в футере
 */

/**
 * Class MainMenu
 */
class LeftMenu extends CWidget
{
    /**
     * @var string
     */
    public $position = 'left_help';

    /**
     * @throws CException
     */
    public function run()
    {
        $uri = Yii::app()->request->getUrl();
        $position = Y::param('menuPosition')[$this->position]['id'];

        $criteria = new CDbCriteria();
        $criteria->compare('position', $position);
        $criteria->compare('parent', 0);
        $criteria->order = '"order"';

        if (Y::isGuest()) {
            $criteria->compare('unauthorized', Y::isGuest());
        }

        $itemsParent = Menu::model()->findAll($criteria);

        $items = [];
        foreach ($itemsParent as $itemParent) {
            $items[$itemParent->id] = [
                'url' => '/content/' . $itemParent->url,
                'title' => $itemParent->title
            ];

            $criteria = new CDbCriteria();
            $criteria->compare('parent', $itemParent->id);
            $criteria->order = '"order"';
            $itemsChild = Menu::model()->findAll($criteria);

            $urlActive = false;
            if (strpos($uri, '/' . $itemParent->url) === false) {
                foreach ($itemsChild as $itemChild) {
                    if (strpos($uri, '/' . $itemChild->url) !== false) {
                        $urlActive = true;
                    }
                }
            } else {
                $urlActive = true;
            }

            if ($urlActive) {
                $items[$itemParent->id]['active'] = true;

                foreach ($itemsChild as $itemChild) {
                    $items[$itemParent->id]['childs'][$itemChild->id] = [
                        'url' => '/content/' . $itemParent->url . '#' . $itemChild->url,
                        'id' => $itemChild->url,
                        'title' => $itemChild->title
                    ];
//                    if (strpos($uri, '/' . $itemParent->url) !== false) {
//                        $items[$itemParent->id]['childs'][$itemChild->id]['active'] = true;
//                    }
                }
            }
        }

        $this->render('leftMenu', [
            'items' => $items
        ]);
    }

}