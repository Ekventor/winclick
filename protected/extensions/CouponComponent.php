<?php

/**
 * Class ContentComponent
 */
class CouponComponent extends ServiceLayerComponent
{
    private $usedStatus = [
        0 => 'Не получен',
        1 => 'Получено',
        2 => 'Погашено',
        3 => 'Просрочено',
        4 => 'Активен'
    ];

    /**
     * Получает купоны, которые пользователь может приобрести
     * @param $position
     * @param null $url
     * @param bool $pagination
     * @return array
     */
    public function getActiveCoupons($userId, $count = null)
    {
        $year = date('Y');
        $criteria = new CDbCriteria();
        $criteria->addCondition("date::text like '" . $year . "%'");
        $criteria->compare('canceled', 0);
        $criteria->compare('used', 0);
        $criteria->compare('type', 'A');
        $criteria->compare('user_id', $userId);
        if ($count) {
            $criteria->limit = $count;
        }
        return Wins::model()->findAll($criteria);
    }

    /**
     * Получает количество купонов, которые пользователь может приобрести
     * @param $userId
     */
    public function getActiveCouponsCount($userId)
    {
        return count($this->getActiveCoupons($userId));
    }

    /**
     * Получает количество купонов, которые пользователь использовал
     * @param $userId
     * @return CActiveRecord[]
     */
    public function getUsedCoupons($userId)
    {
        return Wins::model()->findAllByAttributes(['canceled' => 0, 'used' => 2, 'type' => 'A', 'user_id' => $userId]);
    }

    /**
     * Получает количество использованных купонов
     * @param $userId
     * @return int
     */
    public function getUsedCouponsCount($userId)
    {
        return count($this->getUsedCoupons($userId));
    }

    /**
     * Получает количество купонов, которые пользователь уже приобрел
     * @param $userId
     * @return array|null
     */
    public function getActivatedCoupons($userId)
    {
        return Wins::model()->findAllByAttributes(['canceled' => 0, 'used' => 1, 'type' => 'A', 'user_id' => $userId]);
    }

    /**
     * Получет количество приобретенных купонов
     * @param $userId
     * @return int
     */
    public function getActivatedCouponsCount($userId)
    {
        return count($this->getActivatedCoupons($userId));
    }

    /**
     * Гененрирует номер уникальный купона
     * @param $winId
     * @return string
     */
    public function generateCouponNumber($winId)
    {
        $win = Wins::model()->findByPk($winId);
        $mask = 'qwertyuiopasdfghjklzxcvbnm1234567890';
        $rand = '';
        for ($i = 0; $i < 3; $i++) {
            $rand .= substr($mask, rand(0, 36), 1);
        }
        return date('Ymd') . date('YmdHis', strtotime($win->date)) . $win->game_id . $rand;
    }

    /**
     * Получает транзакции по купонам
     * @param $userId
     * @return array
     */
    public function getTransactions($userId)
    {
//        $sql = "select count(distinct(a2.id)) as count, a1.used_datetime as used_datetime, a1.used as used, a1.nominal as nominal
//                from wins a1, wins a2
//                where a1.used_datetime = a2.used_datetime and a2.used = 1
//                and a1.nominal = a2.nominal and a1.user_id = ". $userId . "
//                and a1.type = 'A'
//                group by a1.used_datetime, a1.used, a1.nominal";
//        $coupons = Yii::app()->db->createCommand($sql)->queryAll();

        $accountHistory = AccountHistory::model()->findAllByAttributes(['user_id' => $userId, 'type' => 'COUPON']);
        $transactions = [];
        if (count($accountHistory)) {
            foreach ($accountHistory as $item) {
                $transactions[] = [
                    'date' => date('d.m.Y', strtotime($item['date'])),
                    'time' => date('H:i:s', strtotime($item['date'])),
                    'nominal' => 0,
                    'count' => abs($item['amount']),
                    'info' => $item['comment']
                ];
            }
        }
        return $transactions;
    }

    /**
     * Проверка купона магазином по номеру
     * @param $number
     * @return string
     */
    public function checkCoupon($number)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('number', $number);
        $win = Wins::model()->find($criteria);

        $date = $this->formatDate(date('Y-m-d H:i:s'));
        if ($win !== null) {
            $userId = $win->user_id;
            $criteria = new CDbCriteria();
            $criteria->compare('user_id', $userId);
            $criteria->compare('type', 'COUPON');
            $criteria->order = 'id DESC';
            $criteria->limit = 1;
            $accountHistory = AccountHistory::model()->find($criteria);
            $couponCount = $accountHistory->close_balance;
            if ($win->used == 1) {
                if (strtotime($win->used_datetime) < strtotime($date)) {
                    $status = 3;
                    $win->used = 3;
                    $comment = 'Просрочен';
                } else {
                    $status = 4;
                    $win->used = 2;
                    $comment = 'Погашен';
                }
                if ($win->save()) {
                    $this->addChange($couponCount, -1, $userId, $comment);
                }
                return ['status' => $this->usedStatus[$status], 'endTime' => (new DateTime($this->formatDate($win->used_datetime)))->modify('+3 month')->format('d.m.Y')];
            }
            return ['status' => $this->usedStatus[$win->used], 'endTime' => ''];
        } else {
            return ['status' => 'Не найден'];
        }
    }


    private function formatDate($date)
    {
        $couponQuad = Y::param('coupon_quart');
        $month = date('n', strtotime($date));
        $count = 0;
        foreach ($couponQuad as $key => $quad) {
            if ((int) $month < $quad) {
                if ($key == 0) {
                    $quadMonth = $couponQuad[count($couponQuad)-1];
                    $quadYear = (int) date('Y', strtotime($date)) - 1;
                    break;
                } else {
                    $quadMonth = $couponQuad[$key-1];
                    $quadYear = (int) date('Y', strtotime($date));
                    break;
                }
            } else {
                $count++;
            }
        }

        if ($count == count($couponQuad)) {
            $resultDate = date('d-m-Y H:i:s', strtotime('01-' . $couponQuad[count($couponQuad)-1] . '-' . (int) date('Y', strtotime($date))));
        } else {
            $resultDate = date('d-m-Y H:i:s', strtotime('01-' . $quadMonth . '-' . $quadYear));
        }
        return $resultDate;

    }

    /**
     * Посылает пользователю номера приобретенных купонов
     * @param $userId
     * @param $couponNumbers
     */
    public function sendCoupon($userId, $couponNumbers)
    {
        $user = Users::model()->findByPk($userId);
        $numbers = implode("<br>", $couponNumbers);
        return Yii::app()->notifier->notify(
            $user->email,
            Yii::t('app', 'Получение подарочных купонов'),
            Yii::t(
                'app',
                'COUPON_EMAIL',
                ['{count}'=>count($couponNumbers), '{numbers}'=> $numbers]
            ),
            'email'
        );
    }

    /**
     * Добавляет запись в account_history касательно купонов
     * @param $couponCount
     * @param $amount
     * @param $userId
     * @param string $comment
     */
    public function addChange($couponCount, $amount, $userId, $comment = '')
    {
        $accountHistoryAttr = [
            'account_id' => 0,
            'open_balance' => $couponCount,
            'close_balance' => $couponCount + $amount,
            'amount' => $amount,
            'date' => date('Y-m-d H:i:s'),
            'user_id' => $userId,
            'type' => 'COUPON',
            'comment' => $comment
        ];
        $accountHistory = new AccountHistory;

        $accountHistory->attributes = $accountHistoryAttr;
        $accountHistory->save();
    }

    public function addCoupons($userId, $count = 3)
    {
        $sql = "INSERT INTO wins (user_id, type, nominal, game_id, date, canceled, used) VALUES ";
        $inserts = [];
        for ($i = 1; $i <= $count; $i++) {
            $inserts[] = "(" . $userId . ", 'A', 1, 100000, '" . date('Y-m-d H:i:s') . "', 0, 0)";
        }
        $insert = implode(', ', $inserts);
        Yii::app()->db->createCommand($sql . $insert)->execute();
    }

}