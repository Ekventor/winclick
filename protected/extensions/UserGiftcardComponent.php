<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 12/9/14
 * Time: 3:55 PM
 */
class UserGiftcardComponent extends ServiceLayerComponent
{

    public function takingWinAmount($userId)
    {
        $user = Users::model()->findByPk($userId);
        $amount = Y::getSetting('win_amount');
        if ($this->sendWinAmount($userId, $amount)) {
            if ($acc_id = Yii::app()->account->getAccIdByCurrency($userId, 'RUB')) {
                $data = Yii::app()->account->decBalance($acc_id, (float)$amount, Yii::t('app', 'Получение универсальной карты'));
                if ($data['error'] == '') {
                    $model = new UserGiftcards;
                    $model->user_id = $userId;
                    $model->count = $amount ;
                    $model->date = date('Y-m-d H:i:s');
                    $model->status = 1;
                    $model->save();
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }
        return false;
    }

    public function sendWinAmount($userId, $amount)
    {
        $email = Yii::app()->settings->getValue('card_email');
        if (!$email || $email == '') {
            return false;
        }
        $user = Users::model()->findByPk($userId);
        $userEmail = $user->email;
        $message = Yii::app()->controller->renderPartial(
            'template/send_win_amount',
            ['userEmail' => $userEmail, 'amount' => $amount],
            true
        );
        return Yii::app()->notifier->notify($email, 'Получение выйгрыша', $message, 'email');

    }

    /**
     * Приобретение подарочных карт
     * @param $data
     * @param $nominal
     * @param $userId
     */
    public function takingGiftcard($data, $nominal, $userId)
    {
        $count = 0;
        foreach ($data as $key => $giftcard) {
            $model = new UserGiftcards;
            $model->user_id = $userId;
            $model->giftcard_id = $key;
            $model->nominal = $nominal;
            $model->count = $giftcard;
            $model->date = date('Y-m-d H:i:s');
            $model->status = 0;
            $count += $giftcard;
            $model->save();
            $userGiftcards[] = $model;
        }
        if ($this->sendNotify($userGiftcards)) {
            foreach ($userGiftcards as $giftcard) {
                $giftcard->status = 1;
                $giftcard->save();
            }
            $this->updateCardBalance($count, $nominal, $userId);
        }
    }

    /**
     * Отправка информации по приобретенным картам
     * @param $userGiftcards
     * @return bool
     */
    public function sendNotify($userGiftcards)
    {
        $email = Yii::app()->settings->getValue('card_email');
        $data = [];
        foreach ($userGiftcards as $giftcard) {
            $giftcardData['email'] = $giftcard->user->email;
            $giftcardData['name'] = $giftcard->giftcard->title;
            $giftcardData['nominal'] = $giftcard->nominal;
            $giftcardData['count'] = $giftcard->count;
            $data[] = $giftcardData;
        }
        $message = Yii::app()->controller->renderPartial('template/mail_template', ['data' => $data], true);
        return Yii::app()->notifier->notify($email, 'Приобретение карт', $message, 'email');
    }

    /**
     * Обновление информации по имеющимся картам для приобретения подарочных карт
     * @param $count
     * @param $nominal
     * @param $userId
     * @return bool
     */
    public function updateCardBalance($count, $nominal, $userId)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('user_id', $userId);
        $criteria->compare('type', 'CARD' . $nominal);
        $criteria->order = 'id desc';
        $criteria->limit;
        $accountHistory = AccountHistory::model()->find($criteria);
        $openBalance = $accountHistory->close_balance;
        $closeBalance = $openBalance - $count*$nominal;
        $model = new AccountHistory();
        $model->account_id = 0;
        $model->open_balance = $openBalance;
        $model->close_balance = $closeBalance;
        $model->amount = '-' . $count*$nominal;
        $model->date = date('Y-m-d H:i:s');
        $model->user_id = $userId;
        $model->type = 'CARD' . $nominal;
        $model->comment = 'Получение карт';
        return $model->save();
    }

    /**
     * Получение истории приобретения подарочных карт
     * @param $userId
     * @return CActiveDataProvider
     */
    public function getUserGiftcardHistory($userId)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('user_id', $userId);
        $userGiftcards = UserGiftcards::model()->findAll($criteria);
        return new CActiveDataProvider('UserGiftcards', ['criteria' => $criteria]);
    }
}