<?php

/**
 * Class Placeholder
 */
class PlaceholderComponent extends ServiceLayerComponent
{

    /**
     * Заменяет в тексте все плейсхолдеры
     *
     * @param $text
     * @param string $placeholder
     *
     * Плейсхолдер может передаваться массивом. Например: ['seq_100' => 'WIN_SEQUENCE;100'], где ключ массива имеет вид
     * {seq_100} в тексте, а значение - поле key в таблице settings.
     *
     * Плейсхолдер может передаваться именем параметра в конфиге.
     */
    public function replace($text, $placeholder = 'placeholders')
    {
        if (is_array($placeholder)) {
            if (count($placeholder)) {
                $data = $placeholder;
            } else {
                $data = Y::param('placeholders');
            }
        }

        if (is_string($placeholder)) {
            $data = Y::param($placeholder);
        }

        if (isset($data)) {
            foreach ($data as $key => $value) {
                $setting = Settings::model()->findByAttributes(['key' => $value]);
                if ($setting !== null) {
                    $text = str_replace('{' . $key . '}', $setting->value, $text);
                }
            }
        }

        return $text;
    }
}