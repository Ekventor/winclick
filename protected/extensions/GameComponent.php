<?php

/*
 * Компонент взаимодействия игрового сервиса и приложения
 */
class GameComponent extends ServiceHandlerComponent {
    //TODO: default values for account service
    public $serviceConfig = [];
    
    /*
     * Отправка всех настроек в игровой сервис
     */
    public function sendSettings(){
        $criteria = new CDbCriteria;
        
        $criteria->compare('send' , 1);
        $criteria->addInCondition('type', ['seq', 'demo']);
        $criteria->order = 'id ASC';
        $data =['NOMINALS'=> Y::param('nominals')];

        foreach (Y::param('nominals') as $nominal) {
            $criteria = new CDbCriteria;
            $criteria->compare('send' , 1);
            $criteria->compare('type', 'seq');
            $criteria->addCondition("key like '%" . $nominal . "'");
            if ($settings = Settings::model()->findAll($criteria)) {
                foreach($settings as $row) {
                    if (strpos($row->key, 'WIN_SEQUENCE')!== false) {
                        $data['WIN_SEQUENCES'][] = $row->value;
                        continue;
                    }
                    if (strpos($row->key, 'MAX_COUNT')!== false) {
                        $data['MAX_COUNTS'][] = (int)$row->value;
                        continue;
                    }

                    if (strpos($row->key, 'COST')!== false) {
                        $data['COSTS'][] = (int)$row->value;
                        continue;
                    }
                }
            }
        }
        $data['MESSAGES']['BET_WRITE_OFF'] = Yii::app()->settings->getValue('MESSAGES;BET_WRITE_OFF');
        $data['DEMO_WIN_SEQUENCE'] = Yii::app()->settings->getValue('DEMO_WIN_SEQUENCE');
        $data['DEMO_MAX_COUNT'] = (int) Yii::app()->settings->getValue('DEMO_MAX_COUNT');
        $this->getData(['set_settings'=>['settings'=>$data]]);
    }
    
    public function getPrizes() {
        return $this->getData(['get_prizes' => ['delete' => true]]);
    }
    
    public function getCost($nominal = 100) {
        return Y::getSetting('COST;'.(int)$nominal);
    }
    
    public function getCounters() {
        return $this->getData(['get_counters' => new stdClass()]);
    }
}

?>