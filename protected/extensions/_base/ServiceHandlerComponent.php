<?php
/*
 * Компонент взаимодействия приложения с сервисами (игры, счетов, статистики)
 */
abstract class ServiceHandlerComponent extends ServiceLayerComponent {
    
    /*
     * Конфигурационный массив с ключами:
     * dsn - адрес, куда будет произведено соединение
     * auth_key - аутенфикационный ключ, для подтверждениея взаимодействия
     *
     */
    public $serviceConfig = null; 
    private $socket = null;
    private $answer = null;
    
    
    public function connect() {
        if (!is_null($this->socket)) return $this->socket;
        /* Create a socket */
        $this->socket = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_REQ, $this->serviceConfig['id']);
        //Y::dump($this->socket->getEndpoints());
        /* Get list of connected endpoints */
        $endpoints = $this->socket->getEndpoints();
        
        /* Check if the socket is connected */
        if (!in_array($this->serviceConfig['dsn'], $endpoints['connect'])) {
            try {
                $this->socket->connect($this->serviceConfig['dsn']);
            } catch(ZMQSocketException $e) {
                throw new Exception($e->getMessage(), 500);
                return false;
            }
        }
        return $this->socket;
    }
    /*
     * @return array декодированный json ответ от сервиса
     */
    private function _getData() {
        if (!$this->_checkConnection()) {
            return false;
        }
        $this->answer = CJSON::decode($this->socket->recv());
        return $this->answer;
    }
    
    /*
     * @var $data array Данные в формате массива
     * @return array Ответ в формате массива
     */
    private function _sendData($data) {
        if (!$this->_checkConnection()) {
            return false;
        }
        /* Send and receive */
        $data = CJSON::encode(CMap::mergeArray(
                                               ['auth_key'=>$this->serviceConfig['auth_key']],
                                               $data
                                               )
                              );
        try {
            $answer = $this->socket->send($data);
        } catch (ZMQSocketException $e) {
            return false;   
        }
        return true;
    }
    
    private function _checkConnection() {
        if (is_null($this->socket)) {
            if ($this->connect()) {
                return true;
            }
            return false;
        }
        return true;
    }
    
    /*
     * just send, dont recieve answer
     */
    public function sendData($data) {
        $this->_sendData($data);
    }
    /*
     * send and get answer
     */
    public function getData($data) {
        $this->_sendData($data);
        return $this->_getData();
    }
    
    /*
     * debug function, check service work state
     */
    public function ping() {
        $answer = $this->getData(['ping'=> new stdClass()]);
        if ($answer['data'] === 'pong') { 
            return true;
        }
        else {
            return false;
        }
    }
    
    
}