<?php

/**
 * Компонент управления настройками
 *
 * @property array $settings
 * @property array $keys
 * @property array $values
 * 
 */
class SettingsComponent extends ServiceLayerComponent {
    
    public $settings = null;
    public $values = null;
    public $keys = null;
    
    
    public function __construct() {
        $this->loadSettings();
    }
    /**
     * Получение значения по ключу
     *
     * @param string $key
     * 
     * @return null|string
     */
    public function getValue($key) {
        if ($this->has($key)) {
            return $this->values[$key]['value'];
        }
        return null;
    }
    /**
     * Получение модели настройки
     * @param string $key
     *
     * @return null|Settings model
     */
    public function get($key) {
        if ($this->has($key)) {
            return $this->values[$key]['object'];
        }
        return null;
    }
    /**
     * Установка значения для настройки по ключу
     *
     * @param string $key
     * @param mixed $value
     * 
     * @return boolean
     */
    public function setValue($key, $value = null) {
        if ($this->has($key)) {
            $setting = $this->get($key);
            if (is_array($value)) {
                $setting->value = CJSON::encode($value);
            } else {
                $setting->value = $value;
            }
            
            return $setting->update(['value']);
        }
        return false;
    }
    
    /**
     * Получение списка возможных ключей настроек
     *
     * @return array
     */    
    public function getKeys() {
        if (!is_null($this->keys)) {
            return $this->keys;
        }
        
        $return = [];
        
        if (is_null($this->settings)) {
            $this->loadSettings();
        }
        
        
        foreach($this->settings as $setting) {
            $return[] = $setting->key;
        }
        $this->keys = $return;
        return $this->keys;
    }
    
    /**
     * Проверка существования настройки по ключу
     *
     * @param string $key
     * 
     * @return boolean
     */
    public function has($key) {
        return in_array($key, self::getKeys());
    }
    
    
    public function loadSettings() {
        if ($list = Settings::model()->findAll()) {
            $this->settings = $list;
            $this->values = [];
            foreach($list as $setting){
                $this->values[$setting->key]['value'] = $setting->value;
                $this->values[$setting->key]['object'] = $setting;
            }
        }
        return;
    }
    
    public function handleLetters($new_letters) {
        
        $letters = str_split(Y::getSetting('possible_letters'));
        $new_letters = str_split($new_letters);
        $to_remove = array_diff($letters, $new_letters);
        
        $tmp = [];
        foreach ($to_remove as $letter) {
            $tmp[0][] = 'result_state;'.$letter;
            $tmp[1][] = 'result_percent;'.$letter;
        }
        
        if (!empty($tmp)) {
            $criteria = new CDbCriteria;
            $criteria->addInCondition('key', $tmp[0]);
            $criteria->compare('type','message');
            Settings::model()->deleteAll($criteria);
            
            $criteria = new CDbCriteria;
            $criteria->addInCondition('key', $tmp[1]);
            $criteria->compare('type','cert');
            Settings::model()->deleteAll($criteria);
        }
        //создаем записи для новых букв, скипая те, что остались
        foreach($new_letters as $letter) {
            if (in_array($letter, $letters)) continue;
            
            $setting = new Settings;
            $setting->attributes = [
                                'key' => 'result_state;'.$letter,
                                'value' => '',
                                'type' => 'message',
                                'title' => 'Сообщение в окне результата о сертификате '.$letter,
                                'send' => 0,
                                ];
            $setting->save();
            
            $setting = new Settings;
            $setting->attributes = [
                                'key' => 'result_percent;'.$letter,
                                'value' => '',
                                'type' => 'cert',
                                'title' => 'Процент скидки для сертификата '.$letter,
                                'send' => 0,
                                ];
            $setting->save();
            
        } 
        Yii::app()->settings->setValue('possible_letters', implode('', $new_letters));
    }
}