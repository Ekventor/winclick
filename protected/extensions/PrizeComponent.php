<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 04.03.15
 * Time: 16:59
 */
class PrizeComponent extends ServiceHandlerComponent {

    public $prizePerc = 0;

    private function getPrizeSum()
    {
        $query = 'SELECT SUM(amount) FROM payments WHERE status = 10';
        $paymentsSum = Yii::app()->db->createCommand($query)->queryScalar();
        return floor($paymentsSum * $this->prizePerc / 100);
    }

    public function formatPrizeSum()
    {
        $amount = $this->getPrizeSum();
        $digits = preg_split('//', $amount, -1, PREG_SPLIT_NO_EMPTY);
        return $digits;
    }

}
