<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 06.02.15
 * Time: 12:52
 */
class WinComponent extends ServiceLayerComponent
{
    public $couponText = 'Купон';

    public $doubleText = 'Удвоение';

    public $cardType = 'Подарочная карта';

    public function getWins()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date DESC';
        $criteria->limit = 10;
        $wins = Wins::model()->findAll($criteria);

        $giftcards = UserGiftcards::model()->findAll($criteria);

        $array = [];
        foreach ($wins as $win) {
            $array[] = [
                'login' => isset($win->user) ? $win->user->login : 'Игрок' . $win->id,
                'type' => $win->type == 'A' ? $this->couponText : $this->doubleText,
                'count' => $win->type == 'A' ? 1 . ' ШТ.' : ($win->nominal * 2) . ' WK',
                'date' => $win->date
            ];
        }
        foreach ($giftcards as $giftcard) {
            $array[] = [
                'login' => isset($giftcard->user) ? $giftcard->user->login : 'Игрок' . $giftcard->id,
                'type' => $this->cardType,
                'count' => $giftcard->count . ' руб.',
                'date' => $giftcard->date
            ];
        }

        function winSort($a, $b)
        {
            $date1 = strtotime($a['date']);
            $date2 = strtotime($b['date']);
            if ($date2 == $date1) {
                return 0;
            }
            return $date1 > $date2 ? -1 : 1;
        }

        usort($array, 'winSort');


        return $array;
    }
}