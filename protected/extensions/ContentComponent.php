<?php

/**
 * Class ContentComponent
 */
class ContentComponent extends ServiceLayerComponent
{
    /**
     * Получает контент по позиции, либо по урлу
     * @param $position
     * @param null $url
     * @param bool $pagination
     * @return array
     */
    public function getContent($position, $url = null, $pagination = false, $order = null)
    {
        if ($url) {
            $item = Menu::model()->findByAttributes(['url' => $url]);
            $urls = Yii::app()->db->createCommand()
                ->select('url')
                ->from('menu')
                ->where('parent = ' . $item->id)
                ->queryColumn();

            $urls[] = $item->url;
        } else {
            $urls = Yii::app()->db->createCommand()
                ->select('url')
                ->from('menu')
                ->where('position = ' . $position)
                ->queryColumn();
        }

        $criteria = new CDbCriteria();
        $criteria->addInCondition('alias', $urls);
        $criteria->order = $order ? 'id DESC' : 'id';

        $pages = null;
        if ($pagination) {
            $count = Content::model()->count($criteria);
            $pages = new CPagination($count);

            $newsPerPage = Settings::model()->findByAttributes(['key' => 'news_per_page']);
            $pages->pageSize = $newsPerPage->value;
            if ($url) {
                $pages->route = 'content/' . $url;
            }
            $pages->applyLimit($criteria);
        }

        $content = Content::model()->findAll($criteria);

        $dateFormattier = new CDateFormatter('ru_RU');

        $contentArray = [];
        foreach ($content as $item) {
            $contentArray[] = [
                'title' => $item->title,
                'modified' => $dateFormattier->format('dd LLL yyyy', strtotime($item->modified)),
                'short_text' => $item->short_text,
                'text' => $item->text,
                'alias' => $item->alias
            ];
        }

        return ['content' => $contentArray, 'paginator' => $pages];
    }
}