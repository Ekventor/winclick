<?php

/*
 * Компонент рассылки уведомлений
 */
class NotifyComponent extends ServiceLayerComponent {
    public $from = null;
    
    
    public function notify($to, $subject, $message, $type = 'email') {
        $notify = new Notify;
        $notify->attributes = [
                                'to' => $to,
                                'subject' => $subject,
                                'type'=> $type,
                                'date'=> Y::date(),
                                'from' => $this->from,
                                'message' => $message
                               ];
        
        if ($notify->save()) {
            return $this->send($notify);
        }
        return false;
    }
    
    private function _send() {
        
    }
    
    public function send(Notify $notify) {
        $r = false;
        $client = new GearmanClient();
        $client->addServer();
        switch($notify->type) {
            case 'email':
                $client->doBackground("send_email", $notify->id);
            break;
            case 'sms':
                $client->doBackground("send_sms", $notify->id);
            break;
            default:
                $r = false;
        }
       return true;
    }
}