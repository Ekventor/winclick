<?php

/*
 * Компонент взаимодействия приложения с аккаунтами
 *
 * @property array $serviceConfig
 * @property array $_accountInfo 
 */
class AccountComponent extends ServiceHandlerComponent {
    //TODO: default values for account service
    public $serviceConfig = [];
    private $_accountInfo = [];
    
    /*
     * Функция изменения баланса на счете в сторону увеличения
     * 
     * @param integer $acc_id
     * @param float $amount
     * @param string $comment
     *
     * @return array Ответ от сервиса в виде массива
     */
    public function incBalance($acc_id, $amount, $comment = '') {
        return $this->getData(['increase_balance'=>['acc_id' => $acc_id, 'amount'=>$amount, 'comment'=>$comment]]);
    }
    /*
     * Функция изменения баланса на счете в сторону уменьшения
     *
     * @param integer $acc_id
     * @param float $amount
     * @param string $comment
     *
     * @return array Ответ от сервиса в виде массива
     */
    public function decBalance($acc_id, $amount, $comment = '') {
        return $this->getData(['decrease_balance'=>['acc_id' => $acc_id, 'amount'=>$amount, 'comment'=>$comment]]);
    }
    
    /*
     * Функция получения состояния конкретного счета
     *
     * @param integer $acc_id
     * 
     * @return float Current balance
     */
    public function getBalance($acc_id) {
       return $this->getData(['get_account_balance'=>['acc_id' => $acc_id]]);
    }
    
    /*
     * Получение инфорации о счете
     *
     * @param integer $acc_id
     * 
     * @return array
     */
    public function getInfo($acc_id) {
        return $this->getData(['get_account_info'=>['acc_id' => $acc_id]]);
    }
    
    /*
     * Получение списка счетов и информации о них для пользователя
     *
     * @param integer $user_id 
     * 
     * @return array Ответ от сервиса в виде массива
     */
    public function getList($user_id) {
        if(empty($this->_accountInfo)) {
            $this->loadList($user_id);
        }
        return $this->_accountInfo;
    }
    public function loadList($user_id) {
        $this->_accountInfo = $this->getData(['get_user_accounts'=>['user_id' => $user_id]]);
        return $this;
    }
    
    /*
     * Создание счетов для пользователя. 
     *
     * @param integer $user_id
     *
     * @return array Ответ от сервиса в виде массива
     */
    public function create($user_id) {
        return $this->getData(['create_accounts'=>['user_id' => $user_id]]);
    }


    public function delete($user_id) {
        return $this->getData(['delete_accounts' => ['user_id' => $user_id]]);
    }
    
    /*
     * Получение баланса для счета по его типу
     *
     * @param Users $user
     * @param string $currency Возможные значения: WK|RUB|BWK
     *
     * @return float
     */
    public function getBalanceByCurrency(Users $user, $currency = 'WK') {
        $this->_checkCurrency($currency);
        if ($list = self::getList($user->id)) {
            if ($list['data'][$currency]) {
                return $list['data'][$currency]['balance'];
            }
        }
    }
    
    /*
     * Получение идентификатора счета по типу
     * 
     * @param integer $user_id Идентификатор пользователя
     * @param string $currency Возможные значения: WK|RUB|BWK
     *
     * @return integer
     */
    public function getAccIdByCurrency($user_id, $currency = 'WK') {
        $this->_checkCurrency($currency);
        if ($list = self::getList($user_id)) {
            if ($list['data'][$currency]) {
                return $list['data'][$currency]['acc_id'];
            }
        }
    }
    
    private function _checkCurrency($currency) {
        if (!in_array($currency, Yii::app()->params['possible_currencies'])) {
            throw new Exception('Wrong currency requested', 500);
        }
    }
    
    /*
     * Конвертация рублей в винклики
     * 
     * @param integer $user_id
     * @param float $amount
     * 
     */
    public function buy_wk($user_id, $amount, $inc_comment = '', $dec_comment = '') {
        return $this->getData(['buy_wk'=>['user_id' => $user_id, 'amount'=> (float)$amount, 'increase_comment' => $inc_comment, 'decrease_comment' => $dec_comment]]);
    }
    
    
    public function sendSettings() {
        $criteria = new CDbCriteria;
        $criteria->compare('send', 1);
        $criteria->compare('type', 'game');
        $criteria->order = 'id ASC';
        if ($settings = Settings::model()->findAll($criteria)) {
            foreach($settings as $row) {
                $data[$row->key] = $row->value;
            }
        }
        $this->getData(['set_settings'=>['settings'=>$data]]);
    }
    
    
    public function getHistory() {
        return $this->getData(['get_last_transactions' => ['delete' => true]]);
    }
    
    /*
     * Начисление бонусных винкликов
     * 
     * @param integer $user_id Юзер, которому начисляем
     * @param float $amount
     * @param string $comment
     *
     * @return boolean
     */
    
    public function enrollBonus($user_id = null, $amount = 0, $comment = 'Автоматическое начисление бонусов') {
        if ($acc_id = self::getAccIdByCurrency($user_id, 'BWK')) {
            $data = self::incBalance($acc_id, (float)$amount, $comment);
            if ($data['error'] == '') {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}
?>