<?php
/*
 * Компонент взаимодействия приложения с аккаунтами
 */
class AuthServiceComponent extends ServiceHandlerComponent {
    //TODO: default values for account service
    public $serviceConfig = [];
    
    
    public function getGameToken($user_id = null) {
        if (is_null($user_id)) {
            return 'error';
        }
        
        return $this->getData(['generate_token'=>['user_id'=>$user_id]]);
    }
}