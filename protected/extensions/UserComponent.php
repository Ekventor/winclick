<?php

/*
 * Компонент взаимодействия приложения с пользователями
 */
class UserComponent extends ServiceLayerComponent {
    public $sessionKey = 'lastAction'; // session key name
    public $onlineExpirePeriod = 10; // in seconds
    
    public function getOnline() {
	return UserOnline::model()->current()->count(new CDbCriteria(['params'=>[':datetime'=>date('Y-m-d H:i:s', (time() - $this->onlineExpirePeriod))]]));
    }
    
    public function checkIsExpired() {
        if (!Yii::app()->user->hasState($this->sessionKey)) {
            return false;
        }
        return Yii::app()->user->getState($this->sessionKey) > (time() - $this->onlineExpirePeriod);
    }
    
    public function setOnline() {
        
        if ($this->checkIsExpired() === true) {
	    return;
        }
		$user_id = Y::userId();
        if (is_null($user_id) || ((int)$user_id < 1)) {
            $user_id = -rand(1, 1000000);
        }

        if ($model = UserOnline::model()->user()->find(new CDbCriteria(['params'=>[':user_id'=>$user_id]]))) {
            // do nothing
        } else {
            $model = new UserOnline;
            $model->user_id = $user_id;
        }
        
        $model->datetime = date('Y-m-d H:i:s', time());
        
        if($model->save()) {
            Yii::app()->user->setState($this->sessionKey, time());
        }

        return true;
    }
	
    public function autocomplete($term) {
	$dataProvider=Users::model()->autocomplete($term);
	$data=array();
	foreach($dataProvider->data as $user){
	    $data[] = array(
		'id'=>$user->id,
		'login'=>$user->login,
		'phone'=>$user->phone,
	    );
	}
	return $data;
    }
	
    /*
     * Подсчет кол-ва непрочитанных сообщений от администрации
     */
    public function lettersCount() {
	$criteria = new CDbCriteria();
	$criteria->compare('user_id', Y::userId());
	$criteria->compare('readed', 0);
	//$criteria->addInCondition('"user"', array(Y::userId()));
	//$criteria->addInCondition('readed', array(0));
	
	return count(Mailings::model()->findAll($criteria));
    }
    
    public function logAction($user_id, $controller = '', $action = '', $type = 'log', $log_params = true) {
		$log = new UserActionLog;
		$log->attributes = [
			'user_id' => (int) $user_id,
			'controller' => $controller,
			'action' => $action,
			'type' => $type,
			'datetime' => date('Y-m-d H:i:s'),
			'params' => ($log_params ? CJSON::encode(['post'=>$_POST, 'get'=>$_GET, 'coockie'=>$_COOKIE, 'server' => $_SERVER, 'request'=>$_REQUEST]) : null)
		];
		return $log->save(false);
    }
	
	public function getWins($user_id = null) {
		if (is_null($user_id)) {
			return [];
		}

        $nominals = Y::app()->params['nominals'];
		$criteria = new CDbCriteria;
		//TODO: UNCOMMENT
        $criteria->compare('user_id', (int)$user_id);
        $criteria->order = 'id DESC';
        $criteria->limit = 1;

//		$criteria->addCondition("type!='".Y::param('coupon')."'");
        $list = [];
		foreach ($nominals as $nominal) {
            $criteria->condition = "type = 'CARD" . $nominal . "'";
            $accountHistory = AccountHistory::model()->find($criteria);
            $list[$nominal]['sum'] = $accountHistory !== null ? $accountHistory->close_balance : 0;
        }
        return $list;
//        if ($r = Wins::model()->findAll($criteria)) {
//			$list = [];
//			foreach($r as $row) {
//				if (empty($list[$row->nominal]['percents'])) {
//					$list[$row->nominal]['percents'] = 0;
//					$list[$row->nominal]['count'] = 0;
//				}
//
//				$list[$row->nominal]['percents'] += Yii::app()->settings->getValue('result_percent;'.$row->type) - $row->canceled;
//				$list[$row->nominal]['count']++;
//
//			}
//            foreach ($list as $key => &$item) {
//                $persents = $item['percents'];
//                $sum = ($key * $persents) / 100;
//                $item['sum'] = $sum;
//            }
//			return $list;
//		}
//		return [];
		
	}
}
?>