<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.\
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Winklik',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.extensions._base.*',
        'ext.YiiMailer.YiiMailer',
        'ext.widgets.redactorjs.Redactor',
        'ext.widgets.yii-select2-master.Select2'
	),

	'modules'=>array(
                'backend',
                'partner',
                'statistic'
	),

	// application components
	'components'=>array(
	    'user'=>array(
		// enable cookie-based authentication
		'allowAutoLogin'=>true,
                'class' => 'WebUser',
        'loginUrl' => '/site/login'
        ),
            // uncomment the following to enable URLs in path-format
            'urlManager'=>array(
                    'urlFormat'=>'path',
                    'rules'=>array(
                            '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                            '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                            'css/main_new.css' => 'css/css',
                            'backend' => 'backend',
                            'statistic' => 'statistic',
                            'catalog' => 'site/catalog',
                            'partner' => 'partner',
                            'news' => 'content/news',
                            'help' => 'content/help',
                            'info' => 'content/info',
                            'partnerNews' => 'content/partnerNews',
                            'partnerHelp' => 'content/partnerHelp',
                            '\w+' => 'pages/index'
                    ),
            ),
            'authManager' => array(
                'class' => 'PhpAuthManager',
                'defaultRoles' => array('guest'),
            ),
	    // uncomment the following to use a MySQL database
            'db'=>array(
                'connectionString' => 'pgsql:host=localhost;dbname=winclick',
                'emulatePrepare' => true,
                'username' => 'winclick',
                'password' => 'rT1@tot',
                'charset' => 'utf8',
            ),
            'errorHandler'=>array(
                    // use 'site/error' action to display errors
                    'errorAction'=>'site/error',
            ),
            'log'=>array(
                    'class'=>'CLogRouter',
                    'routes'=>array(
                            array(
                                    'class'=>'CFileLogRoute',
                                    'levels'=>'error, warning',
                            ),
                            // uncomment the following to show log messages on web pages
                            /*
                            array(
                                    'class'=>'CWebLogRoute',
                            ),
                            */
                    ),
            ),
            'notifier' => [
                'class' => 'ext.NotifyComponent',
                'from' => 'subscribe@winklik.ru', // must be same in mail.php config
            ],
            'settings' => [
                'class' => 'ext.SettingsComponent',
            ],
            //service-layer component
            'account' => [
                'class' => 'ext.AccountComponent',
                'serviceConfig' => [
                                    'auth_key' => 'd7db66d8b9af121cfb12512cde636af32c2cf77a',
                                    'dsn' => 'tcp://127.0.0.1:5002',
                                    'id'=>'account',
                                    ],
            ],
            'authService' => [
                'class' => 'ext.AuthServiceComponent',
                'serviceConfig' => [
                                    'auth_key' => 'd7db66d8b9af121cfb12512cde636af32c2cf77a',
                                    'dsn' => 'tcp://127.0.0.1:5001',
                                    'id'=>'auth',
                                    ],
            ],
            'game' => [
                'class' => 'ext.GameComponent',
                'serviceConfig' => [
                                    'auth_key' => 'd7db66d8b9af121cfb12512cde636af32c2cf77a',
                                    'dsn' => 'tcp://127.0.0.1:5000',
                                    'id'=>'game',
                                    ],
            ],
            'myuser' => [
                'class' => 'ext.UserComponent',
                'sessionKey' => 'lastAction',
                'onlineExpirePeriod' => 900, //15 minets 60*15
            ],
            'wallet' => [
                'class' => 'ext.WalletComponent',
                'secret' => '7543606d4a4a4771455453724d56735078494b6d6762555a714737',
                'success_url' => 'http://winklik.ru/user/pay/status/success',
                'fail_url' => 'http://winklik.ru/user/pay/status/fail',
                'merchant_id' => '196595389618',
                'pay_url' => 'https://www.walletone.com/checkout/default.aspx',
            ],
            'placeholder' => [
                'class' => 'ext.PlaceholderComponent'
            ],

            'content' => [
                'class' => 'ext.ContentComponent'
            ],
            'coupon' => [
                'class' => 'ext.CouponComponent'
            ],
            'card' => [
                'class' => 'ext.CardComponent'
            ],

            'userGiftcard' => [
                'class' => 'ext.UserGiftcardComponent'
            ],

            'wins' => [
                'class' => 'ext.WinComponent'
            ],

            'prize' => [
                'class' => 'ext.PrizeComponent',
                'prizePerc' => 10
            ]

    ),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
            // this is used in contact page
            'adminEmail'=>'webmaster@example.com',
            'possible_currencies' => ['WK', 'RUB', 'BWK', 'COUPON', ['CARD1', 'CARD10', 'CARD25', 'CARD50', 'CARD100']],
            'gameServiceUrl' => 'http://185.20.225.20:1338',
            'nominals' => [1, 10, 25, 50, 100],
            'certPath' => '/images/certificates',
            'infoPath' => '/images/info',
            'avatarPath' => '/images/avatars',
            'pagesImagePath' => '/images/pages',
            'pagesFilePath' => '/files/pages',
            'contentImagePath' => '/images/content',
            'contentFilePath' => '/files/content',
            'orgImagePath' => '/images/org',
            'orgFilePath' => '/files/org',
            'giftImagePath' => '/images/giftcards',
            'certImage' => ['width' => 432,
                            'height' => 189],
            'wins_show' => 9,
            'menuPosition' => [
                'top' => ['id' => 1, 'sort' => 'x'],
                'bottom' => ['id' => 2, 'sort' => 'x'] ,
                'left_help' => ['id' => 3, 'sort' => 'y'],
                'left_news' => ['id' => 4, 'sort' => 'y'],
                'left_partner_news' => ['id' => 5, 'sort' => 'y'],
                'left_partner_help' => ['id' => 6, 'sort' => 'y']
            ],
            'placeholders' => [
                'seq_1' => 'WIN_SEQUENCE;1',
                'seq_10' => 'WIN_SEQUENCE;10',
                'seq_25' => 'WIN_SEQUENCE;25',
                'seq_50' => 'WIN_SEQUENCE;50',
                'seq_100' => 'WIN_SEQUENCE;100',
                'seq_demo' => 'DEMO_WIN_SEQUENCE',
            ],
            'coupon_live' => 1, //время жизни купона, в сутках
            'coupon_quart' => [2, 5, 8, 11], //в какой месяц купоны сгорают
            'allowed_tags' => '<h1><h2><h3><h4><a><img><div><p><span><ul><li><ol><strike><strong><table><tr><td>',
            'coupon' => 'A', //купон тип сертификата
	),
        'language' => 'ru',
);
