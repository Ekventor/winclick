<?php
return [
    'guest' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Гость',
        'bizRule' => null,
        'data' => null
    ],
    'user' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Пользователь',
        'children' => [
            'guest', // унаследуемся от гостя
        ],
        'bizRule' => null,
        'data' => null
    ],
    'shop' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Магазин',
        'children' => [
            'guest',
        ],
        'bizRule' => null,
        'data' => null
    ],
    'admin' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Администратор',
//        'children' => array(
//            'moderator', // позволим админу всё, что позволено модератору
//        ),
        'bizRule' => null,
        'data' => null
    ],
    'content' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Контент-менеджер',
        'children' => [
            'user', // позволим админу всё, что позволено модератору
        ],
        'bizRule' => null,
        'data' => null
    ],
    'partner' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Партнер',
        'children' => [
            'user', // позволим админу всё, что позволено модератору
        ],
        'bizRule' => null,
        'data' => null
    ]

];