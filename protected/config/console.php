<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Аукцион Винклик',
        // autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.extensions._base.*',
        'ext.YiiMailer.YiiMailer',
	),
	'preload'=>array('log'),

	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'pgsql:host=localhost;dbname=winclick',
			'emulatePrepare' => true,
			'username' => 'winclick',
			'password' => 'rT1@tot',
			'charset' => 'utf8',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        
        'stat' => [
            'class' => 'ext.StatisticComponent',
        ],
                'game' => [
                    'class' => 'ext.GameComponent',
                    'serviceConfig' => [
                                    'auth_key' => 'd7db66d8b9af121cfb12512cde636af32c2cf77a',
                                    'dsn' => 'tcp://127.0.0.1:5000',
                                    'id'=>'game',
                                    ],
                ],
                'account' => [
                    'class' => 'ext.AccountComponent',
                    'serviceConfig' => [
                                        'auth_key' => 'd7db66d8b9af121cfb12512cde636af32c2cf77a',
                                        'dsn' => 'tcp://127.0.0.1:5002',
                                        'id'=>'account',
                                        ],
                ],
                'card' => [
                    'class' => 'ext.CardComponent'
                ]
                
	),
        'params' => [
            'nominals' => ['300', '1000', '2000', '3000', '10000'],
            'coupon' => 'A'
        ]
);