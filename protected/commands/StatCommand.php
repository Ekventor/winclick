<?php

class StatCommand extends CConsoleCommand {
    
    public function actionTest() {
        Yii::app()->stat->getCounters();
    }
    
    public function actionAggregate() {
        Yii::app()->stat->aggregate();
    }
}
    