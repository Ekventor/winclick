<?php

class AccountCommand extends CConsoleCommand {
    
    
    public function actionGetHistory() {
        echo "[".date("d.m.Y H:i:s")."] getHistory \n";
        Y::startTimer('get_data');
        $list = /*CJSON::decode(*/Yii::app()->account->getHistory()['data']/*)*/;
        echo "\t data get in : " . Y::stopTimer('get_data') . "\n";
        $sql_head = 'INSERT INTO '.AccountHistory::model()->tableName() .' (account_id, open_balance, close_balance, amount, date, user_id, type, comment) VALUES';
        
        $to_insert = []; $c = 0; $per_insert = 1000;
        Y::startTimer('handle_data');
        foreach($list as $row) {
            $to_insert[] = '(' . $row['acc_id'] . ", ".$row['opening_balance'].", ".$row['closing_balance'].", " . $row['amount'].", '" . date('Y-m-d H:i:s', $row['timestamp'])."', ".$row['user_id'].", '".$row['acc_type']."', '".$row['comment']."')";
            if (++$c >= $per_insert) {
                $sql = $sql_head . implode(', ', $to_insert);
                $r = Yii::app()->db->createCommand($sql)->execute();
                $to_insert = []; $c = 0;
                echo "\t > executed (".$r." rows)\n";
            }
        }
        if (count($to_insert)>0) {
            $sql = $sql_head . implode(', ', $to_insert);
            $r = Yii::app()->db->createCommand($sql)->execute();
            echo "\t > executed (".$r." rows)\n";
        }
        echo "\t data are handled in : " . Y::stopTimer('handle_data') . "\n";
        
    }
}
