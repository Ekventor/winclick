<?php

class GameCommand extends CConsoleCommand {
    
    
    public function actionGetPrizes() {
        echo "[".date("d.m.Y H:i:s")."] getPrizes \n";
        Y::startTimer('get_data');
        $list = CJSON::decode(Yii::app()->game->getPrizes()['data']);
        echo "\t data get in : " . Y::stopTimer('get_data') . "\n";
        
        //print_r($list['data']);
        $sql_head = 'INSERT INTO '.Wins::model()->tableName() .' (user_id, type, nominal, game_id, date) VALUES';
        $to_insert = []; $c = 0; $per_insert = 1000; $i = 0; $taskList = [];
        Y::startTimer('handle_data');
        $per_task = 500;
        $client = new GearmanClient();
        $client->addServer();
        foreach($list as $row) {
            $taskList[] = $row;
            $to_insert[] = '(' . $row['user_id'] . ", '". $row['prize']. "', ". $row['nominal'] .", '" . $row['game_id']."', '" . date('Y-m-d H:i:s', $row['timestamp'])."')";
            if (++$c >= $per_insert) {
                $sql = $sql_head . implode(', ', $to_insert);
                $r = Yii::app()->db->createCommand($sql)->execute();
                $to_insert = []; $c = 0;
                echo "\t > executed (".$r." rows)\n";
            }
            if (++$i >= $per_task) {
                echo "\t add background task \n";
                $client->addTaskBackground("wins_account_history", CJSON::encode($taskList));
                $taskList = []; $i = 0;
            }
        }
        if (count($to_insert)>0) {
            $sql = $sql_head . implode(', ', $to_insert);
            $r = Yii::app()->db->createCommand($sql)->execute();
            echo "\t > executed (".$r." rows)\n";
        }
        if (count($taskList)) {
            echo "\t add background task \n";
            $client->addTaskBackground("wins_account_history", CJSON::encode($taskList));
        }
        echo "\t run tusks \n";
        $client->runTasks();

        echo "\t data are handled in : " . Y::stopTimer('handle_data') . "\n";
        
    }
}
