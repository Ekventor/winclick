<?php

class WorkerCommand extends CConsoleCommand {
    
    public function actionDo() {
        
        $worker = new GearmanWorker();
        $worker->addServer();
        
        $worker->addFunction("send_email", function(GearmanJob $job) {
            $notify = Notify::model()->findByPk($job->workload());
            $mail = new YiiMailer();
            $mail->setView('simple');
            $mail->setData(array('message' => $notify->message, 'description' => $notify->subject));
            $mail->setFrom($notify->from, Y::app()->name);
            $mail->setTo($notify->to);
            $mail->setSubject($notify->subject);
            $r = $mail->send();
            
            if ($r) {
                $notify->status = Notify::STATUS_SENT;
            } else {
                $notify->status = Notify::STATUS_FAILED;
            }
            
            $notify->update(['status']);
            echo "mail sent successful.\n";
        });
        
        $worker->addFunction("send_sms", function(GearmanJob $job) {
            //$notify = Notify::model()->findByPk($job->workload());
            echo "not implemented yet.\n";
        });

        $worker->addFunction("wins_account_history", function(GearmanJob $job) {
            Yii::app()->card->addCardHistory(CJSON::decode($job->workload()));
        });
        
        
        while ($worker->work());
        
    }
}

