<?php

class m140723_080349_account_history_fields extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('account_history', 'comment', 'string');
	}

	public function safeDown()
	{
		$this->dropColumn('account_history', 'comment');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}