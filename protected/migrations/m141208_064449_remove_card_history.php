<?php

class m141208_064449_remove_card_history extends CDbMigration
{
	public function up()
	{
		$this->delete('account_history', "type like 'CARD%'");
	}

	public function down()
	{
		echo "m141208_064449_remove_card_history does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}