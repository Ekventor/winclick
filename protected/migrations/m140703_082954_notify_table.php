<?php

class m140703_082954_notify_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('notify', [
					      'id'=>'pk',
					      'to' => 'string',
					      'from' => 'string',
					      'type' => 'string',
					      'message' => 'text',
					      'date' => 'datetime',
					      'status' => 'int'
					      ]);
	}

	public function safeDown()
	{
		$this->dropTable('notify');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}