<?php

class m141216_103742_add_setting_email_card extends CDbMigration
{
	public function up()
	{
		$this->insert('settings', ['key' => 'card_email', 'value' => 'cards@winklik.ru', 'type' => 'site', 'title' => 'Email для отправки подарочных карт', 'send' => 0]);
	}

	public function down()
	{
		echo "m141216_103742_add_setting_email_card does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}