<?php

class m140703_075715_user_salt_field extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('users', 'salt', 'string');
		$this->addColumn('users', 'token', 'string DEFAULT NULL');
	}

	public function safeDown()
	{
		$this->dropColumn('users', 'salt');
		$this->dropColumn('users', 'token');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}