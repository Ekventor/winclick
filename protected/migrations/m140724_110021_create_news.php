<?php

class m140724_110021_create_news extends CDbMigration
{
	public function up()
	{
        $this->createTable('content', ['id'=>'pk',
            'title'=>'string',
            'short_text' => 'string',
            'text'=>'text',
            'created'=>'datetime',
            'modified'=>'datetime',
            'alias'=>'string',
        ]);
	}

	public function down()
	{
		echo "m140724_110021_create_news does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}