<?php

class m140808_062311_add_category extends CDbMigration
{
	public function up()
	{
        $this->createTable('category', ['id'=>'pk',
            'name'=>'string'
        ]);

        $data = [
            ['name'=>'Бытовая техника'],
            ['name'=>'Автомобили'],
            ['name'=>'Продовольственные товары'],
            ['name'=>'Косметика']
        ];
        foreach($data as $row) {
            $this->insert('category', $row);
        }


    }

	public function down()
	{
		echo "m140808_062311_add_category does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}