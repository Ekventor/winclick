<?php

class m150212_055342_user_account extends CDbMigration
{
	public function up()
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("role != 'guest'");
		$users = Users::model()->findAll($criteria);

		foreach ($users as $user) {
			Yii::app()->account->create($user->id);
		}
	}

	public function down()
	{
		echo "m150212_055342_user_account does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}