<?php

class m140722_052031_add_convert extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users', 'auto_convert', 'boolean DEFAULT FALSE');
	}

	public function down()
	{
		echo "m140722_052029_add_convert does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}