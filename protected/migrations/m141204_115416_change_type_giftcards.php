<?php

class m141204_115416_change_type_giftcards extends CDbMigration
{
	public function up()
	{
		$sql1 = 'ALTER TABLE giftcard ALTER COLUMN "desc" TYPE text;';
		$sql2 = "ALTER TABLE giftcard ALTER COLUMN \"short_desc\" TYPE text;";
		Yii::app()->db->createCommand($sql1)->execute();
		Yii::app()->db->createCommand($sql2)->execute();

	}

	public function down()
	{
		echo "m141204_115416_change_type_giftcards does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}