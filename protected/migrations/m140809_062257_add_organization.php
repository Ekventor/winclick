<?php

class m140809_062257_add_organization extends CDbMigration
{
	public function up()
	{
        $this->createTable('organization', ['id'=>'pk',
            'name'=>'string',
            'category_id' => 'int',
            'city_id' => 'int',
            'specialization'=>'text',
            'contacts'=>'text',
            'desc'=>'text',
            'img'=>'string',
        ]);

        $this->addForeignKey('fk_organization_category_id', 'organization', 'category_id', 'category', 'id');
        $this->addForeignKey('fk_organization_city_id', 'organization', 'city_id', 'city', 'id');

    }

	public function down()
	{
		echo "m140808_062257_add_organization does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}