<?php

class m150204_113721_add_settings_gifcard_coupon extends CDbMigration
{
	public function up()
	{
		$this->insert('settings', [
			'key' => 'giftcard_info_long',
			'value' => '',
			'type' => 'giftcard',
			'title' => 'Текст внизу',
			'send' => 0
		]);

	}

	public function down()
	{
		echo "m150204_113721_add_settings_gifcard_coupon does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}