<?php

class m140709_111936_notify_subjevt extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('notify', 'subject', 'string');
	}

	public function safeDown()
	{
		$this->dropColumn('notify', 'subject');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}