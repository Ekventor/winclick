<?php

class m141205_073856_add_field_info extends CDbMigration
{
	public function up()
	{
		$this->addColumn('giftcard', 'addit_info', 'TEXT');
	}

	public function down()
	{
		echo "m141205_073856_add_field_info does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}