<?php

class m140715_080548_wins_add_fields extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('wins', 'canceled', 'int DEFAULT 0');
		$this->addColumn('wins', 'canceled_datetime', 'datetime');
		$this->addColumn('wins', 'used', 'int DEFAULT 0');
		$this->addColumn('wins', 'used_datetime', 'datetime');
		
	}

	public function safeDown()
	{
		$this->dropColumn('wins', 'canceled');
		$this->dropColumn('wins', 'canceled_datetime');
		$this->dropColumn('wins', 'used');
		$this->dropColumn('wins', 'used_datetime');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}