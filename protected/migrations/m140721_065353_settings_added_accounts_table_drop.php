<?php

class m140721_065353_settings_added_accounts_table_drop extends CDbMigration
{
	public function safeUp()
	{
		$this->dropForeignKey('fk_account_user_id', 'accounts');
		$this->dropTable('accounts');
		
		
		$data = [
			 ['key'=>'COST;100', 'value'=>'1', 'title'=>'Стоимость игры для номинала 100', 'type'=>'seq', 'send'=>1],
			 ['key'=>'COST;500', 'value'=>'5', 'title'=>'Стоимость игры для номинала 500', 'type'=>'seq', 'send'=>1],
			 ['key'=>'COST;1000', 'value'=>'5', 'title'=>'Стоимость игры для номинала 1000', 'type'=>'seq', 'send'=>1],
			 ['key'=>'COST;5000', 'value'=>'50', 'title'=>'Стоимость игры для номинала 5000', 'type'=>'seq', 'send'=>1],
			 ['key'=>'COST;15000', 'value'=>'50', 'title'=>'Стоимость игры для номинала 15000', 'type'=>'seq', 'send'=>1],
			];
		foreach($data as $row) {
			$this->insert('settings', $row);
		}
		
	}

	public function safeDown()
	{
		$this->createTable('accounts', ['id'=>'pk',
						'balance' => 'float',
						'user_id' => 'int',
						'currency'=>'string']);
		
		$this->addForeignKey('fk_account_user_id', 'accounts', 'user_id', 'users', 'id');
		
		$this->delete('settings', "key IN ('COST;100', 'COST;500', 'COST;1000', 'COST;5000', 'COST;15000')");
		
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}