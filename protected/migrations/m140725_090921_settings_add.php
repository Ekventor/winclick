<?php

class m140725_090921_settings_add extends CDbMigration
{
	public function safeUp()
	{
		$data = [
			 ['key'=>'repeat', 'value'=>'ДЛЯ ПОВТОРНОГО<br /> УЧАСТИЯ НАЖМИТЕ<br /> КНОПКУ СТАРТ', 'title'=>'Сообщение под барабаном после прокрутки барабана', 'type'=>'message', 'send'=>0],
			 ['key'=>'default', 'value'=>'УЧАСТВУЙ И ПОБЕЖДАЙ!', 'title'=>'Сообщение под барабаном по умолчанию', 'type'=>'message', 'send'=>0],
			 ['key'=>'win', 'value'=>'ПОЗДРАВЛЯЕМ!!!<br /> ВЫ ПОБЕДИТЕЛЬ', 'title'=>'Сообщение под барабаном при выигрыше', 'type'=>'message', 'send'=>0],
			 
			 ['key'=>'state_default', 'value'=>'Для участия в аукционе нажмите кнопку старт', 'title'=>'Сообщение в окне результата по умолчанию', 'type'=>'message', 'send'=>0],
			 ['key'=>'state_not_enough_money', 'value'=>'Недостаточно средств для ставки', 'title'=>'Сообщение в окне результата при нехватке средств', 'type'=>'message', 'send'=>0],
			 ['key'=>'state_play', 'value'=>'Вы участник аукциона! С вашего счета списано', 'title'=>'Сообщение в окне результата при нажатии СТАРТ', 'type'=>'message', 'send'=>0],
			 ['key'=>'demo_play', 'value'=>'Вы участник демо-аукциона!', 'title'=>'Сообщение в окне результата при нажатии СТАРТ (демо)', 'type'=>'message', 'send'=>0],
			 
			 ['key'=>'result_state;A', 'value'=>'', 'title'=>'Сообщение в окне результата о сертификате A', 'type'=>'message', 'send'=>0],
			 ['key'=>'result_percent;A', 'value'=>'10', 'title'=>'Процент скидки для сертификата A', 'type'=>'cert', 'send'=>0],
			 ['key'=>'result_state;B', 'value'=>'', 'title'=>'Сообщение в окне результата о сертификате B', 'type'=>'message', 'send'=>0],
			 ['key'=>'result_percent;B', 'value'=>'20', 'title'=>'Процент скидки для сертификата B', 'type'=>'cert', 'send'=>0],
			 //['key'=>'result_state;C', 'value'=>'', 'title'=>'Сообщение в окне результата о сертификате C', 'type'=>'message', 'send'=>0],
			 //['key'=>'result_percent;C', 'value'=>'40', 'title'=>'Процент скидки для сертификата C', 'type'=>'cert', 'send'=>0],
			 //['key'=>'result_state;D', 'value'=>'', 'title'=>'Сообщение в окне результата о сертификате D', 'type'=>'message', 'send'=>0],
			 //['key'=>'result_percent;D', 'value'=>'60', 'title'=>'Процент скидки для сертификата D', 'type'=>'cert', 'send'=>0],
			 //['key'=>'result_state;E', 'value'=>'', 'title'=>'Сообщение в окне результата о сертификате E', 'type'=>'message', 'send'=>0],
			 //['key'=>'result_percent;E', 'value'=>'80', 'title'=>'Процент скидки для сертификата E', 'type'=>'cert', 'send'=>0],
			 ['key'=>'result_state;!', 'value'=>'', 'title'=>'Сообщение в окне результата о выигрышном сертификате', 'type'=>'message', 'send'=>0],
			 ['key'=>'result_percent;!', 'value'=>'95', 'title'=>'Процент скидки для выигрышного сертификата', 'type'=>'cert', 'send'=>0],
			 
			 ['key'=>'possible_letters', 'value'=>'AB', 'title'=>'Возможные символы для последовательности', 'type'=>'seq', 'send'=>0],
			
			];
		foreach($data as $row) {
			$this->insert('settings', $row);
		}
	}

	public function safeDown() {
		$this->delete('settings', 'type=:type', [':type'=>'message']);
		$this->delete('settings', 'type=:type AND key LIKE :key', [':type'=>'message', ':key'=> 'result_state%']);
		$this->delete('settings', 'type=:type AND key LIKE :key', [':type'=>'cert', ':key'=> 'result_percent%']);
		$this->delete('settings', 'type=:type AND key=:key', [':type'=>'seq', ':key'=> 'possible_letters']);
	}
}