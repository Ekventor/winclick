<?php

class m150205_073049_add_win_sum extends CDbMigration
{
	public function up()
	{
		$this->insert('settings', [
			'key' => 'win_amount',
			'value' => 500,
			'type' => 'site',
			'title' => 'Минимальная сумма для вывода денег',
			'send' => 0
		]);
	}

	public function down()
	{
		echo "m150205_073049_add_win_sum does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}