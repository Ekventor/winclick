<?php

class m140717_053217_settings_key extends CDbMigration
{
	public function safeUp()
	{
		if ($setting = Settings::model()->findByAttributes(['key'=>'currency'])) {
			$setting->key = 'EXCHANGE_RATE';
			$setting->send = 1;
			$setting->update(['key', 'send']);
		}
		
		$data = [
			 ['key'=>'DEMO_WIN_SEQUENCE', 'value'=>'AAB', 'title'=>'Выигрышная последовательность', 'send'=>1, 'type'=>'demo'],
			 ['key'=>'DEMO_MAX_COUNT', 'value'=>'10', 'title'=>'Длина последовательности', 'send'=>1, 'type'=>'demo'],
			];
		foreach($data as $row) {
			$this->insert('settings', $row);
		}
	}

	public function down()
	{
		if ($setting = Settings::model()->findByAttributes(['key'=>'EXCHANGE_RATE'])) {
			$setting->key = 'currency';
			$setting->send = 0;
			$setting->update(['key', 'send']);
		}
		
		Settings::model()->deleteAll(new CDbCriteria(['condition'=>'type=:type', 'params'=>[':type'=>'demo']]));
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}