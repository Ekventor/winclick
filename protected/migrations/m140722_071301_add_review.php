<?php

class m140722_071301_add_review extends CDbMigration
{
	public function up()
	{
        $this->createTable('review', ['id'=>'pk',
            'user_id'=>'int',
            'author' => 'string',
            'text'=>'string',
            'created'=>'datetime',
            'show'=> 'boolean',
            'avatar'=>'string']);
        $this->addForeignKey('fk_review_user_id', 'review', 'user_id', 'users', 'id');

	}

	public function down()
	{
		echo "m140722_071300_add_review does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}