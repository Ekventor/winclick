<?php

class m140730_035622_add_news_per_page_setting extends CDbMigration
{
    public function up()
    {
        $this->insert('settings', [
            'key' => 'news_per_page',
            'value' => 10,
            'title' => 'Количество новостей на странице',
            'type' => 'site',
            'send' => 0
        ]);
    }

    public function down()
    {
        echo "m140730_035622_add_news_per_page_setting does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}