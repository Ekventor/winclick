<?php

class m140708_100303_add_settings_send extends CDbMigration
{
	public function safeUp()
	{
        $this->addColumn('settings', 'send', "INT DEFAULT 0");

	}

	public function safeDown()
	{
		$this->dropColumn('settings', 'send');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}