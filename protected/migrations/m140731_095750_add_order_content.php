<?php

class m140731_095750_add_order_content extends CDbMigration
{
	public function up()
	{
        $this->addColumn('content', 'order', 'int');
	}

	public function down()
	{
		echo "m140731_095750_add_order_content does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}