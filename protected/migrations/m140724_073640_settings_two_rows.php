<?php

class m140724_073640_settings_two_rows extends CDbMigration
{
	public function safeUp()
	{
		$data = [
			 ['key'=>'bonus_instruction', 'value'=>'', 'title'=>'Краткое описание бонусной программы', 'type'=>'bonus', 'send'=>0],
			 ['key'=>'bonus_terms', 'value'=>'', 'title'=>'Страница "Условия бонусной программы"', 'type'=>'bonus', 'send'=>0],
			
			];
		foreach($data as $row) {
			$this->insert('settings', $row);
		}
	}

	public function safeDown()
	{
		$this->delete('settings', 'key IN (:key1, :key2)', [':key1'=>'bonus_instruction', ':key2' => 'bonus_terms']);
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}