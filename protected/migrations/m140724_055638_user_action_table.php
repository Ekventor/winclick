<?php

class m140724_055638_user_action_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('user_action_log', [ 'id'=> 'pk',
						        'user_id' => 'int',
						        'datetime' => 'datetime',
							'action' => 'string',
							'controller' => 'string',
							'params' => 'text DEFAULT NULL',
							'type' => 'string',
						       ]);
	}

	public function safeDown()
	{
		$this->dropTable('user_action_log');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}