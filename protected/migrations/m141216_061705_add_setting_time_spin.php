<?php

class m141216_061705_add_setting_time_spin extends CDbMigration
{
	public function up()
	{
		$this->insert('settings', ['key' => 'image_round_time', 'value' => 4, 'type' => 'game', 'title' => 'Время вращения барабана', 'send' => 0]);
	}

	public function down()
	{
		echo "m141216_061705_add_setting_time_spin does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}