<?php

class m140709_044440_settings_add extends CDbMigration
{
	public function safeUp()
	{
		$data = [
			 ['key'=>'sequence', 'value'=>'AABAABAABAABAABAABAABAABAABAABAABAABAABAABAABAAC', 'title'=>'Выигрышная последовательность', 'type'=>'game'],
			 ['key'=>'demo_sequence', 'value'=>'ABABABABABABABABABABABABABCBABABABABABABABABABAC', 'title'=>'Выигрышная последовательность', 'type'=>'demo'],
			];
		foreach($data as $row) {
			$this->insert('settings', $row);
		}
	}

	public function down()
	{
		echo "m140709_044440_settings_add does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}