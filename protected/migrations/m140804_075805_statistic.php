<?php

class m140804_075805_statistic extends CDbMigration
{
	public function safeUp() {
		
		$this->createTable('statistic', [
										 'id' => 'pk',
										 'datetime' => 'datetime',
										 'value' => 'float DEFAULT 0',
										 'type' => 'string',
										 ]);
	}

	public function safeDown()
	{
		$this->dropTable('statistic');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
