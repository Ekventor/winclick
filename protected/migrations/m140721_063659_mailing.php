<?php

class m140721_063659_mailing extends CDbMigration
{
	public function up()
	{
		$this->createTable('mailings', [
			'id'=>'pk',
			'title'=>'string not null',
			'message'=>'text',
			'date'=>'datetime',
			'creator'=>'int not null',
			'user'=>'int not null',
			'readed'=>'int default 0',
		]);
		
		$this->addForeignKey('fk_mailings_creator_user_id', 'mailings', 'creator', 'users', 'id');
		$this->addForeignKey('fk_mailings_user_user_id', 'mailings', 'user', 'users', 'id');
		
	}

	public function down()
	{
		echo "m140721_063659_mailing does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}