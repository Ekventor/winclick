<?php

class m140724_070819_alter extends CDbMigration
{
	public function safeUp()
	{
		$this->dropForeignKey('fk_mailings_user_user_id', 'mailings');
		$this->renameColumn('mailings', 'user', 'user_id');
		$this->addForeignKey('fk_mailings_user_user_id', 'mailings', 'user_id', 'users', 'id');
	}

	public function safeDown()
	{
		$this->dropForeignKey('fk_mailings_user_user_id', 'mailings');
		$this->renameColumn('mailings', 'user_id', 'user');		
		$this->addForeignKey('fk_mailings_user_user_id', 'mailings', 'user', 'users', 'id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}