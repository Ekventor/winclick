<?php

class m140730_081536_payment_orders extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('payments', [
						'id' => 'pk',
						'amount' => 'float',
						'datetime' => 'datetime',
						'status' => 'int DEFAULT 0',
						'account_id' => 'int',
						'user_id' => 'int',
						'wmi_order_id' => 'int DEFAULT NULL',
						'wmi_currency_id' => 'int DEFAULT NULL',
						]);
	}

	public function safeDown()
	{
		$this->dropTable('payments');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}