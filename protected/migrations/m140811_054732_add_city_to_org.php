<?php

/**
 * Class m140811_054732_add_city_to_org
 */
class m140811_054732_add_city_to_org extends CDbMigration
{
    /**
     * @return bool|void
     */
    public function up()
    {
        $this->createTable(
            'city_organization_link',
            [
                'id' => 'pk',
                'city_id' => 'int',
                'organization_id' => 'int'
            ]
        );
        $this->dropForeignKey('fk_organization_city_id', 'organization');
        $this->addForeignKey(
            'fk_city_organization_city_id',
            'city_organization_link',
            'city_id',
            'city',
            'id'
        );
        $this->addForeignKey(
            'fk_city_organization_org_id',
            'city_organization_link',
            'organization_id',
            'organization',
            'id'
        );
    }

    public function down()
    {
        echo "m140811_054732_add_city_to_org does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}