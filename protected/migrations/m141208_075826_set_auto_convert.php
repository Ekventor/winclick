<?php

class m141208_075826_set_auto_convert extends CDbMigration
{
	public function up()
	{
		$this->update('users', ['auto_convert' => 0]);
	}

	public function down()
	{
		echo "m141208_075826_set_auto_convert does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}