<?php

class m140714_071819_add_menu_order extends CDbMigration
{
	public function up()
	{
        $this->addColumn('menu', 'order', "INT DEFAULT 0");
	}

	public function down()
	{
		echo "m140714_071819_add_menu_order does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}