<?php

class m140922_075046_update_bonus extends CDbMigration
{
	public function up()
	{
        $this->update(
            'settings',
            [
                'key' => 'coupon_instruction',
                'value' => ' - ',
                'type' => 'coupon',
                'title' => 'Инструкция для приобретения купонов',
                'send' => 0
            ],
            "key = 'bonus_instruction' AND type = 'bonus'"
        );
	}

	public function down()
	{
		echo "m140922_075046_update_bonus does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}