<?php

class m141222_120022_clear_balance extends CDbMigration
{
	public function up()
	{
		$users = Users::model()->findAllByAttributes(['activated' => true]);
		foreach ($users as $user) {
			Yii::app()->account->delete($user->id);
			Yii::app()->account->create($user->id);
		}
	}

	public function down()
	{
		echo "m141222_120021_clear_balance does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}