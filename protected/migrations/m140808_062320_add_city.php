<?php

class m140808_062320_add_city extends CDbMigration
{
	public function up()
	{
        $this->createTable('city', ['id'=>'pk',
            'name'=>'string'
        ]);

        $data = [
            ['name'=>'Новосибирск'],
            ['name'=>'Москва'],
            ['name'=>'Санкт-Петербург'],
            ['name'=>'Омск'],
            ['name'=>'Краснодар'],
            ['name'=>'Калининград'],
            ['name'=>'Новокузнецк'],
            ['name'=>'Томск'],
            ['name'=>'Владивосток'],
            ['name'=>'Оймякон'],
            ['name'=>'Нижний Новгород'],
            ['name'=>'Екатеринбург'],
            ['name'=>'Красноярск'],
            ['name'=>'Кемерово']
        ];
        foreach($data as $row) {
            $this->insert('city', $row);
        }
	}

	public function down()
	{
		echo "m140808_062320_add_city does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}