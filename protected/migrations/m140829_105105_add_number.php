<?php

class m140829_105105_add_number extends CDbMigration
{
	public function up()
	{
        $this->addColumn('wins', 'number', "VARCHAR(55)");
    }

	public function down()
	{
		echo "m140829_105105_add_number does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}