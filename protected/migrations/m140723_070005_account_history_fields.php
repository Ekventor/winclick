<?php

class m140723_070005_account_history_fields extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('account_history', 'user_id', 'int');
		$this->addColumn('account_history', 'type', 'string');
	}

	public function safeDown()
	{
		$this->dropColumn('account_history', 'user_id');
		$this->dropColumn('account_history', 'type');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}