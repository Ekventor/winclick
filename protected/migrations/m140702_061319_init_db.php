<?php

class m140702_061319_init_db extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('settings', [
						'id'=>'pk',
						'key'=> 'string',
						'value'=> 'string',
						'type'=>'string',
						'title'=>'string'
						]);
		$this->createTable('menu', ['id'=>'pk',
					    'url'=>'string',
					    'title'=>'string',
					    'unauthorized' => 'boolean',
					    'position'=>'int',
					    'parent'=>'bigint']);
		$this->createTable('pages', ['id'=>'pk',
					     'title'=>'string',
					     'content'=>'text',
					     'author'=>'string',
					     'created'=>'datetime',
					     'modified'=>'datetime',
					     'alias'=>'string',
					     ]);
		
		$this->createTable('users', ['id'=>'pk',
					     'phone'=>'string',
					     'login'=>'string',
					     'password'=>'string',
					     'email'=> 'string',
					     'created'=>'datetime',
					     'modified'=>'datetime']);
		
		$this->createTable('accounts', ['id'=>'pk',
						'balance' => 'float',
						'user_id' => 'int',
						'currency'=>'string']);
		
		$this->addForeignKey('fk_account_user_id', 'accounts', 'user_id', 'users', 'id');
		
		$this->createTable('sertificate', ['id'=>'pk',
						   'type'=>'string',
						   'number'=>'string',
						   'nominal'=>'int',
						   'canceled'=>'boolean',
						   'activated'=>'boolean',
						   'user_id'=>'int',
						   'date_add' => 'datetime',
						   'date_take' => 'datetime',
						   ]);
		
		$this->createTable('wins', ['id'=>'pk',
					    'user_id'=>'int',
					    'type'=>'string',
					    'nominal'=>'int',
					    'game_id'=>'string',
					    'date'=>'datetime']);
		
		$this->createTable('currencies', ['id'=>'pk',
						  'value'=>'float',
						  'date'=>'datetime']);
		
		$this->createTable('account_history', ['id'=>'pk',
						       'account_id'=>'int',
						       'open_balance' => 'float',
						       'close_balance'=>'float',
						       'amount'=>'float',
						       'date'=>'datetime',
					]);
		$this->createIndex('i_account_history_account', 'account_history', 'account_id');
	
	}

	public function down()
	{
		echo "m140702_061319_init_db does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}