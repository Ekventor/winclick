<?php

class m140822_053801_create_css_styles extends CDbMigration
{
	public function up()
	{
        $this->createTable('css', ['id'=>'pk',
            'css'=>'text',
            'modified' => 'datetime',
        ]);

    }

	public function down()
	{
		echo "m140822_053801_create_css_styles does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}