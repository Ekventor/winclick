<?php

class m141014_101245_create_giftcard extends CDbMigration
{
	public function up()
	{
		$this->createTable('giftcard', [
			'id'=>'pk',
			'title'=> 'string',
			'desc'=> 'string',
			'img'=>'string',
			'nominals'=>'json'
		]);
	}

	public function down()
	{
		echo "m141014_101243_create_giftcard does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}