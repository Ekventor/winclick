<?php

class m141114_112608_alter_column_settings extends CDbMigration
{
	public function up()
	{
		$sql = "ALTER TABLE settings ALTER COLUMN value TYPE text";
		Yii::app()->db->createCommand($sql)->execute();
	}

	public function down()
	{
		$sql = "ALTER TABLE settings ALTER COLUMN value TYPE varchar(255)";
		Yii::app()->db->createCommand($sql)->execute();
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}