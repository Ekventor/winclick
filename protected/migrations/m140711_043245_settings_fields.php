<?php

class m140711_043245_settings_fields extends CDbMigration
{
	public function safeUp()
	{
		Yii::app()->db->createCommand('UPDATE settings SET send=0;')->execute();
		$data = [];
		foreach(Y::param('nominals') as $nominal) {
			$data[] = ['key'=>'WIN_SEQUENCE;' . $nominal, 'value'=>'AAB', 'title'=>'Выигрышная последовательность номинала '.$nominal, 'type'=>'seq', 'send'=>1];
			$data[] = ['key'=>'MAX_COUNT;' . $nominal, 'value'=>'10', 'title'=>'Длина последовательности номинала '.$nominal, 'type'=>'seq', 'send'=>1];
		}
		
		foreach($data as $row) {
			$this->insert('settings', $row);
		}
		
		if ($s = Settings::model()->find(new CDbCriteria(['condition'=>'key=:key', 'params'=>[':key'=>'sequence']])))
			$s->delete();
		if ($s = Settings::model()->find(new CDbCriteria(['condition'=>'key=:key', 'params'=>[':key'=>'demo_sequence']])))
			$s->delete();
	}

	public function safeDown()
	{
		Settings::model()->deleteAll(new CDbCriteria(['condition'=>'type=:type', 'params'=>[':type'=>'seq']]));
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}