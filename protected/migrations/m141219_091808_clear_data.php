<?php

class m141219_091808_clear_data extends CDbMigration
{
	public function up()
	{
		$this->delete('account_history');
		$this->delete('payments');
		$this->delete('mailings');
		$this->delete('notify');
		$this->delete('user_giftcards');
//		$this->delete('users', "login LIKE 'test%'");
		$this->delete('wins');
	}

	public function down()
	{
		echo "m141219_091807_clear_data does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}