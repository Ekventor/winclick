<?php

class m150206_063715_delete_from_user_giftcards extends CDbMigration
{
	public function up()
	{
		$this->dropForeignKey('fk_user_giftcards_giftcard_id', 'user_giftcards');
		$this->dropColumn('user_giftcards', 'giftcard_id');
		$this->dropColumn('user_giftcards', 'nominal');
	}

	public function down()
	{
		echo "m150206_063715_delete_from_user_giftcards does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}