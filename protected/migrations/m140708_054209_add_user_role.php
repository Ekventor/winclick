<?php

class m140708_054209_add_user_role extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('users', 'role', "VARCHAR(55) DEFAULT 'guest'");
	}

	public function safeDown()
	{
		$this->dropColumn('users', 'role');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}