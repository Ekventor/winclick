<?php

class m140922_084918_add_coupon_live extends CDbMigration
{
	public function up()
	{
        $this->insert('settings', [
            'key' => 'coupon_live',
            'value' => '1',
            'type' => 'coupon',
            'title' => 'Время жизни купона, в сутках',
            'send' => 0
        ]);
	}

	public function down()
	{
		echo "m140922_084918_add_coupon_live does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}