<?php

class m141223_071947_add_wins extends CDbMigration
{
	public function up()
	{
		$userIds = [1, 2, 3, 4, 17];
		$nominals = Y::param('nominals');
		$days = [21, 22, 23];
		for ($i = 0; $i < 10; $i++) {
			$this->insert('wins', [
				'user_id' => $userIds[rand(0, 4)],
				'type' => '!',
				'nominal' => $nominals[rand(0, 4)],
				'game_id' => rand(1, 5) . '0000' . $i,
				'date' => '2014-12-' . rand(21, 23) . ' ' . rand(0, 23) . ':' . rand(0, 59) . ':' . rand(0, 59),
				'canceled' => 0,
				'used' => 0
			]);
		}
	}

	public function down()
	{
		echo "m141223_071947_add_wins does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}