<?php

class m141203_094110_add_fields_to_giftcards extends CDbMigration
{
	public function up()
	{
		$this->addColumn('giftcard', 'short_desc', 'string');
		$this->addColumn('giftcard', 'small_img', 'string');
	}

	public function down()
	{
		echo "m141203_094109_add_fields_to_giftcards does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}