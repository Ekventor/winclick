<?php

class m141017_034437_add_giftcard_settings extends CDbMigration
{
	public function up()
	{
		$this->insert('settings', ['key' => 'giftcard_info', 'value' => '', 'type' => 'giftcard', 'title' => 'Информация для приобретения элетронных сертификатов', 'send' => 0]);
	}

	public function down()
	{
		echo "m141017_034437_add_giftcard_settings does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}