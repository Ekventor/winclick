<?php

class m140919_053606_add_messages extends CDbMigration
{
	public function up()
	{
        $this->insert(
            'settings',
            [
                'key' => 'MESSAGES;BET_WRITE_OFF',
                'value' => ' ',
                'type' => 'seq',
                'title' => 'Коментарий к игре',
                'send' => 1
            ]);
	}

	public function down()
	{
		echo "m140919_053606_add_messages does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}