<?php

class m140714_063136_online_user_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('user_online', [ 'id' => 'pk',
						   'user_id' => 'int',
						   'datetime' => 'datetime',
						   ]);
		
	}

	public function safeDown()
	{
		$this->dropTable('user_online');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}