<?php

class m150130_105539_update_nominals extends CDbMigration
{
	public function up()
	{
		/** Изменения номинала 300*/
		$this->update(
			'settings',
			[
				'key'=>'COST;1',
				'title'=>'Стоимость игры для ставки 1'
			],
			"key = 'COST;300'"
		);
		$this->update(
			'settings',
			[
				'key'=>'WIN_SEQUENCE;1',
				'title'=>'Выигрышная последовательность ставки 1'
			],
			"key = 'WIN_SEQUENCE;300'"
		);
		$this->update(
			'settings',
			[
				'key'=>'MAX_COUNT;1',
				'title'=>'Длина последовательности ставки 1'
			],
			"key = 'MAX_COUNT;300'"
		);
		/** конец*/

		/** Изменение номинала 1000 */
		$this->update(
			'settings',
			[
				'key'=>'COST;10',
				'title'=>'Стоимость игры для ставки 10'
			],
			"key = 'COST;1000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'WIN_SEQUENCE;10',
				'title'=>'Выигрышная последовательность ставки 10'
			],
			"key = 'WIN_SEQUENCE;1000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'MAX_COUNT;10',
				'title'=>'Длина последовательности ставки 10'
			],
			"key = 'MAX_COUNT;1000'"
		);
		/** конец */

		/** Изменение номинала 2000 */
		$this->update(
			'settings',
			[
				'key'=>'COST;25',
				'title'=>'Стоимость игры для ставки 25'
			],
			"key = 'COST;2000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'WIN_SEQUENCE;25',
				'title'=>'Выигрышная последовательность ставки 25'
			],
			"key = 'WIN_SEQUENCE;2000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'MAX_COUNT;25',
				'title'=>'Длина последовательности ставки 25'
			],
			"key = 'MAX_COUNT;2000'"
		);
		/** конец */

		/** Изменение номинала 3000 */
		$this->update(
			'settings',
			[
				'key'=>'COST;50',
				'title'=>'Стоимость игры для ставки 50'
			],
			"key = 'COST;3000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'WIN_SEQUENCE;50',
				'title'=>'Выигрышная последовательность ставки 50'
			],
			"key = 'WIN_SEQUENCE;3000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'MAX_COUNT;50',
				'title'=>'Длина последовательности ставки 50'
			],
			"key = 'MAX_COUNT;3000'"
		);
		/** конец */

		/** Изменение номинала 10000 */
		$this->update(
			'settings',
			[
				'key'=>'COST;100',
				'title'=>'Стоимость игры для ставки 100'
			],
			"key = 'COST;10000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'WIN_SEQUENCE;100',
				'title'=>'Выигрышная последовательность ставки 100'
			],
			"key = 'WIN_SEQUENCE;10000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'MAX_COUNT;100',
				'title'=>'Длина последовательности ставки 100'
			],
			"key = 'MAX_COUNT;10000'"
		);
		/** конец */

	}

	public function down()
	{
		echo "m150130_105539_update_nominals does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}