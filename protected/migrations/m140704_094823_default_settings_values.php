<?php

class m140704_094823_default_settings_values extends CDbMigration
{
	public function safeUp()
	{
		$data = [
			 ['key'=>'not_payed_cert_ttl', 'value'=>'10', 'title'=>'Срок хранения неоплаченных сертификатов', 'type'=>'cert'],
			 ['key'=>'site_status', 'value'=>'1', 'title'=>'Вкл./выкл. сайт', 'type'=>'site'],
			 ['key'=>'image_round_speed', 'value'=>'1000', 'title'=>'Скорость вращения изображений', 'type'=>'game'],
			 ['key'=>'win_notice_time', 'value'=>'10', 'title'=>'Время отображения надписи "Вы победили"', 'type'=>'game'],
			 ['key'=>'currency', 'value'=>'5', 'title'=>'Курс рублей к винкликам', 'type'=>'game'],
			 ['key'=>'transaction_ttl', 'value'=>'14', 'title'=>'Срок хранения данных о транзакциях', 'type'=>'site'],
			 
			// ['key'=>'', 'value'=>'', 'title'=>'', 'type'=>''],
			 ];
		foreach($data as $row) {
			$this->insert('settings', $row);
		}
	}

	public function safeDown()
	{
		$this->truncateTable('settings');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}