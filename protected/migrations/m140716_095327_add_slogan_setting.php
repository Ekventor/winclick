<?php

class m140716_095327_add_slogan_setting extends CDbMigration
{
    public function up()
    {
        $this->insert('settings', [
            'key' => 'slogan',
            'value' => 'Вы любите много покупать, но не любите много платить?! Тогда этот сервис создан для Вас!',
            'title' => 'Слоган внизу страницы',
            'type' => 'site'
        ]);
    }

    public function down()
    {
        echo "m140716_095327_add_slogan_setting does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}