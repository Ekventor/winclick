<?php

class m141017_074526_update_settings extends CDbMigration
{
	public function up()
	{
		$this->update(
			'settings',
			[
				'key'=>'COST;300',
				'title'=>'Стоимость игры для номинала 300'
			],
			"key = 'COST;300'"
		);
		$this->update(
			'settings',
			[
				'key'=>'COST;2000',
				'title'=>'Стоимость игры для номинала 2000'
			],
			"key = 'COST;2000'"
		);
		$this->update(
			'settings',
			[
				'key'=>'COST;3000',
				'title'=>'Стоимость игры для номинала 3000'
			],
			"key = 'COST;3000'"
		);

	}

	public function down()
	{
		echo "m141017_074523_update_settings does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}