<?php

class m141209_082427_create_user_giftcard_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('user_giftcards', [
			'id'=>'pk',
			'user_id'=> 'int',
			'giftcard_id'=> 'int',
			'nominal'=>'int',
			'count' => 'int',
			'date'=>'datetime',
			'status' => 'int'
		]);
		$this->addForeignKey('fk_user_giftcards_user_id', 'user_giftcards', 'user_id', 'users', 'id');
		$this->addForeignKey('fk_user_giftcards_giftcard_id', 'user_giftcards', 'giftcard_id', 'giftcard', 'id');
	}

	public function down()
	{
		echo "m141209_082426_create_user_giftcard_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}