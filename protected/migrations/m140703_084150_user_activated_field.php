<?php

class m140703_084150_user_activated_field extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('users', 'activated', 'boolean DEFAULT FALSE');
	}

	public function down()
	{
		$this->dropColumn('users', 'activated');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}