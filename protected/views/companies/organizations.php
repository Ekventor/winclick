<div>
<div class="search-more">
    <?php if (!empty($in_categories)):?>
    <span style="float: left;">Уточните категорию: </span>
    <div style="margin: 0 0 0 189px;">
        <?php foreach($in_categories as $cat):?>
        <span class="category-span">
          <a href="#" onclick="javascript: game.selectCat(<?=$cat->id;?>); return false;"><?= $cat->name;?></a>
        </span>
      <?php endforeach;?>
    </div>
    <?php endif;?>
</div>


<div class="search-result">
<?php if (count($list)>0):?>
    <?php foreach($list as $item):?>
<div class="result-block">
    <div class="left-block">
        <img src="<?= $item->img;?>" alt="@" title="<?= $item->name;?>">
    </div>
    <div class="info-block">
        <ul>
          <li><?= $item->name;?></li>
          <li>Специализация:
            <span><?= $item->specialization;?></span>
          </li>
          <li>Контакты:
            <span><?=$item->contacts;?></span>
          </li>
        </ul>
      </div>
    </div>
    <?php endforeach;?>
<?php else: ?>
<?php endif;?>
</div>
</div>
