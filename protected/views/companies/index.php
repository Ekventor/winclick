<div class="search-block">
    <div class="search-input">
        <span class="ocular"></span>
        <input type="text" name="text" id="search_text" placeholder="Название предприятия">

        <div data-id="category" class="dropdown">
            <div class="selected" id="qwe">
                <input type="hidden" value="" name="city_id" data-id="selected-value">
                <input type="text" placeholder="Название предприятия" data-id="find-value" class="find"
                       style="margin:1px;"/>
                <span>Категория</span>
                   <div class="arrows"></div>
            </div>
            <div class="options scrollable">
                <span data-value="-1" data-descr="Категория">Любая</span>
                <?php if (count($categories) > 0): ?>
                    <?php foreach ($categories as $category): ?>
                        <span data-value="<?= $category->id; ?>"
                              data-descr="<?= $category->name; ?>"><?= $category->name; ?></span>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div data-id="city" class="dropdown">
            <div class="selected" id="qwe">
                <input type="hidden" value="" name="city_id" data-id="selected-value">
                <input type="text" placeholder="Начните вводить название города" data-id="find-value" class="find"
                       style="margin:1px;"/>
                <span>Город</span>
                <div class="arrows"></div>
            </div>
            <div class="options scrollable">
                <span data-value="-1" data-descr="Город">Любой</span>
                <?php if (count($cities) > 0): ?>
                    <?php foreach ($cities as $city): ?>
                        <span data-value="<?= $city->id; ?>" data-descr="<?= $city->name; ?>"><?= $city->name; ?></span>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>


    <div class="search-link float-right"><span id="search_companies" style="    bottom: 10px;
    left: 22px;
    position: relative;">Поиск</span></div>

</div>
<div class="search-more"></div>
<div class="search-result">
    <?php $this->renderPartial('organizations', ['list' => Organization::model()->findAll(new CDbCriteria(['limit' => 100])), 'in_categories' => []]); ?>
</div>


<script>
    $(document).ready(function() {
        $obj = $('.search-block .dropdown .selected');
        $obj.on('click', function() {
            $('.arrows').removeClass('active');
            if ($(this).parent().find('.options.scrollable').hasClass('opened')) {
                $(this).find('.arrows').addClass('active');
            } else {
                $(this).find('.arrows').removeClass('active');
            }
        });
        $(':not(.selected)').on('click', function() {
            $('.arrows').removeClass('active');
        });
    });
</script>