<?php
/* @var $this StaticPageController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = $model->title;
?>
<div class="<?= $model->alias; ?>">

<?php echo strip_tags($text, Y::param('allowed_tags')); ?>
</div>