<div class="content-wrap">
    <div class="content-head">
        <div class="content-head-left">
            <p>Содержание раздела</p>
        </div>
        <div class="content-head-right smaller">
                            <p>Новости</p>
        </div>
    </div>
    <div class="content"> <!-- content start -->

        <div class="left-section">
            <div id="sidebar">
                <?php
                $this->widget('ext.widgets.LeftMenu', array(
                    'position'=>$position,
                ));
                ?>
            </div><!-- sidebar -->
        </div>
        <div class="news-section">
            <?php foreach ($news as $new): ?>
            <div class="news-block">
                <div class="news-block-date">
                    <p>
                        <?php echo $new['modified']; ?>
                    </p>
                </div>
                <p class="news-head-p"> <?php echo $new['title']; ?> </p>
                <?php echo Yii::app()->placeholder->replace($new['short_text']); ?>
                <div class="show-hide-block-wrap">
                    <?php echo Yii::app()->placeholder->replace($new['text']); ?>
                </div>
                <div class="news-block-control">
                    <img src="/img/triangle.png" alt="" />
                    <p>Подробнее</p>
                </div>
            </div>
            <?php endforeach; ?>
            <?php $this->widget('CLinkPager', [
                'pages' => $pages,
            ]); ?>
        </div>
        <div style="clear: both;"></div>
    </div> <!-- content end -->
</div>
