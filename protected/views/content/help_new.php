<div class="helpWrapper">
    <div class="help-wrap">
        <div class="help-menu-content">
            <div class="help-menu-title">
                Содержание раздела
            </div>
            <div class="help-menu-list">
                <?php
                $this->widget('ext.widgets.LeftMenu', array(
                    'position'=>$position,
                ));
                ?>
            </div>
        </div>
        <div class="help-content">
            <div class="help-content-title">
                Помощь
            </div>
            <div class="help-content-list">
                <?php foreach($helps as $help): ?>
                <div class="help-block">
                    <a name="<?= $help['alias']; ?>"></a>
                    <div class="help-block-title">
                        <?php echo $help['title']; ?>
                    </div>
                    <div class="help-block-text">
                        <?php echo Yii::app()->placeholder->replace($help['short_text']); ?>
                        <div class="show-hide-block-wrap">
                            <?php echo Yii::app()->placeholder->replace($help['text']); ?>
                        </div>
                    </div>
                    <div class="help-block-control">
                        <img src="/img/triangle.png" alt="" />
                        <p>Подробнее</p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
    <div class="plant-coins"></div>
</div>
<div style="clear: both"></div>
<script>
    $(document).ready(function() {
        hash = window.location.hash;
        element = hash.replace('#', '');
        $('.help-block').find('a[name="' + element + '"]').parent().find('.help-block-title').addClass('active');
    });


    $(document).on('scroll', function() {
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        if (scrollTop > 300) {
            $('.up-slider').removeClass('hidden');
        } else {
            $('.up-slider').addClass('hidden');
        }
    });

    $('.help-menu-list li li a').on('click', function(e) {
        if (parseInt($(this).closest('li').attr('index-data')) < 5) {
            e.preventDefault();
        }
        $('.help-menu-list li li').removeClass('current-article');
        $(this).closest('li').addClass('current-article');
        href = $(this).attr('href');
        url = href.split('#');
        help = url[1];
        $('.help-block .help-block-title').removeClass('active');
        $('.help-block').find('a[name="' + help + '"]').parent().find('.help-block-title').addClass('active');
    });
</script>