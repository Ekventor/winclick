<div class="sliderWrapper">
    <div class="content-wrap">
        <ul id="info-slider">
            <?php foreach ($slides as $slide): ?>
                <li><?php echo $slide->text; ?></li>
            <?php endforeach; ?>
        </ul>
        <!--        <div class="content-head">-->
        <!--            <div class="content-head-right">-->
        <!--                <p>Как это работает?</p>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--        <div class="content"> <!-- content start -->
        <!---->
        <!---->
        <!--            <div class="typical-page slider">-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div> <!-- content end -->
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#info-slider').anythingSlider({
            expand: true,
            autoPlay: false,
            buildNavigation: false,
            buildStartStop: false
        });

//        $('.info-slider-wrap .slider').carouFredSel({
//            scroll: {
//                duration: 1000
//            },
//            auto: {
//                play: false
//            },
//            height: 'auto',
//            prev: '.info-slider-wrap .left-arrow',
//            next: '.info-slider-wrap .right-arrow',
////        pagination: '.info-slider-wrap .bottom-pag'
//        });
    });

</script>
