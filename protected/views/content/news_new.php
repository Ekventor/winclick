<?php foreach ($news as $new): ?>
<div class="news-block">
    <div class="news-block-title">
        <div class="new-date">
            <?= $new['modified']; ?>
        </div>
        <div class="new-title">
            <?= $new['title']; ?>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="news-block-content">
        <?= Yii::app()->placeholder->replace($new['short_text']); ?>
        <div class="show-hide-block-wrap">
            <?= Yii::app()->placeholder->replace($new['text']); ?>
        </div>

    </div>
    <div class="news-block-control">
        <img src="/img/triangle.png" alt="" />
        <p>Подробнее</p>
    </div>
</div>
<?php endforeach; ?>
<?php $this->widget('CLinkPager', [
    'pages' => $pages,
]); ?>
