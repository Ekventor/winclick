<div class="content-wrap">
    <div class="up-slider hidden">
        <a href="#"></a>
    </div>
    <div class="content-head">
        <div class="content-head-left">
            <p>Содержание раздела</p>
        </div>
        <div class="content-head-right smaller">
                            <p>Помощь</p>
        </div>
    </div>
    <div class="content"> <!-- content start -->

        <div class="left-section">
            <div id="sidebar">
                <?php
                $this->widget('ext.widgets.LeftMenu', array(
                    'position'=>$position,
                ));
                ?>
            </div><!-- sidebar -->
        </div>
        <div class="news-section">

            <?php foreach($helps as $help): ?>
            <div class="news-block">

                <p class="news-head-p"> <?php echo $help['title']; ?></p>
                <a name="<?= $help['alias']; ?>"></a>
                <?php echo Yii::app()->placeholder->replace($help['short_text']); ?>
                <div class="show-hide-block-wrap">
                    <?php echo Yii::app()->placeholder->replace($help['text']); ?>
                </div>
                <div class="news-block-control">
                    <img src="/img/triangle.png" alt="" />
                    <p>Подробнее</p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div style="clear: both;"></div>
    </div> <!-- content end -->
</div>

<script>
    $(document).ready(function() {
        hash = window.location.hash;
        element = hash.replace('#', '');
        $('.news-section').find('a[name="' + element + '"]').closest('div').find('.news-head-p').addClass('active');
    });


    $(document).on('scroll', function() {
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        if (scrollTop > 300) {
            $('.up-slider').removeClass('hidden');
        } else {
            $('.up-slider').addClass('hidden');
        }
    });

    $('.left-section-ul li li').on('click', function() {
        href = $(this).find('a').attr('href');
        url = href.split('#');
        help = url[1];
        $('.news-head-p').removeClass('active');
        $('.news-section').find('a[name="' + help + '"]').closest('div').find('.news-head-p').addClass('active');
    });
</script>