<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="content-wrap">
        <div class="content-head">
            <div class="content-head-right">
<!--                <p>Название Раздела</p>-->
            </div>
        </div>
        <div class="content"> <!-- content start -->

            <div class="left-section">
                <div id="sidebar">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items'=>$this->menu,
                        'htmlOptions'=>array('class'=>'left-section-ul'),
                    ));
                    ?>
                </div><!-- sidebar -->
            </div>
            <div class="news-section">
                <?php echo $content; ?>
            </div>
            <div style="clear: both;"></div>
        </div> <!-- content end -->
</div>
<?php $this->endContent(); ?>