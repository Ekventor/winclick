<?php $this->beginContent('//layouts/main_new'); ?>
    <style>
    </style>
    <div class="orange-line borders" style="margin-top: 50px"></div>
    <div class="newsWrapper">
        <div class="news-wrap">
            <div class="news-title">
                Новости
            </div>
            <div class="news-content">
                <?= $content; ?>
            </div>
        </div>
        <div class="present-box"></div>
    </div>
    <div class="orange-line borders"></div>
    <div class="footerBgWrapper">
        <div class="footerWrapper cf" id="f4">
            <div class="footer resize cf">
                <div class="fll left">
                    © ООО “МПА ”ВИНКЛИК” <?= date('Y') ?> год <br>
                    Все права защищены
                </div>
                <div class></div>
                <div class="bottom-menu">
                    <div class="bottom-list">
                        <?php $this->widget('ext.widgets.NavigationMenu', [
                            'items' => [
                                [
                                    'class' => 'ext.widgets.HorizontalMenu',
                                    'options' => [
                                        'position' => 'bottom',
                                        'htmlOptions' => [
                                            'class' => ''
                                        ]
                                    ]
                                ]
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>