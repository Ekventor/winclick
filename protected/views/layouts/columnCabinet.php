<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main_new'); ?>
<?php $this->menu = [
    ['label' => '<div>Винклики</div>', 'url' => ['/user/winkliks']],
    ['label' => '<div>Предоплаченные карты</div>', 'url' => ['/user/giftcards']],
    ['label' => '<div>Подарочные купоны</div>', 'url' => ['/user/bonus']],
    ['label' => '<div>Транзакции</div>', 'url' => ['/user/transactions']],
    ['label' => '<div>Персональные данные</div>', 'url' => ['/user/profile']],
] ?>
    <div class="orange-line borders" style="margin-top: 50px;"></div>
    <div class="cabinetWrapper">
        <div class="cabinet-wrap">
            <div class="content">
                <div class="left_panel float-left">
                    <div class="cabinet-menu-title">
                        Содержание раздела
                    </div>
                    <div class="cabinet-menu-list">
                        <?php
                        $this->widget('zii.widgets.CMenu', array(
                            'activateParents' => true,
                            'activeCssClass' => 'active',
                            'items' => $this->menu,
                            'encodeLabel' => false
                        ));
                        ?>
                    </div>
                </div>

                <div class="personal-data float-left">
                    <div class="cabinet-content-title"><?= $this->pageTitle; ?></div>
                    <div class="cabinet-content">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- content-wrap end -->
    <script>
        $('#mailing_button').append(' <?php echo Yii::app()->myuser->lettersCount() > 0 ? '<div class="my-letter">'. Yii::app()->myuser->lettersCount() .'</div>' : ''; ?> ');
    </script>
    <div class="orange-line borders"></div>
    <div class="footerBgWrapper">
        <div class="footerWrapper cf" id="f4">
            <div class="footer resize cf">
                <div class="fll left">
                    © ЗАО “МПА ”ВИНКЛИК” <?= date('Y') ?> год <br>
                    Все права защищены
                </div>
                <div class></div>
                <div class="bottom-menu">
                    <div class="bottom-list">
                        <?php $this->widget('ext.widgets.NavigationMenu', [
                            'items' => [
                                [
                                    'class' => 'ext.widgets.HorizontalMenu',
                                    'options' => [
                                        'position' => 'bottom',
                                        'htmlOptions' => [
                                            'class' => ''
                                        ]
                                    ]
                                ]
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gray-popup-bg"><!-- --></div>
    <div class="result-popup">
        <div class="cont-block">
            <div class="message">
                Сообщение
            </div>
            <div class="button-wrap">
                <button type="button" onclick="javascript: smartOverlay.hide();">
                    OK
                </button>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>