<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html> <!--<![endif]-->
<head profile="http://www.w3.org/2005/10/profile">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Comfortaa&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/main_new.css">
    <link rel="stylesheet" href="/css/main_page.css">
    <link rel="stylesheet" href="/css/vendor/anythingslider.css">
    <link rel="stylesheet" href="/css/vendor/theme-metallic.css">
    <script type="text/javascript" src="/js/vendor/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery.color.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery-ui.js"></script>
    <script src="/js/vendor/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script type="text/javascript" src="/js/game.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/smartoverlay.js"></script>
    <script type="text/javascript" src="/js/vendor/cwcle.js" ></script>
    <script type="text/javascript" src="/js/vendor/cycle2.js" ></script>
    <script type="text/javascript" src="/js/vendor/jquery.form.js" ></script>
    <script type="text/javascript" src="/js/vendor/jquery.anythingslider.js"></script>
    <script src="/js/vendor/swfobject_modified.js" type="text/javascript"></script>
    <link rel="shortcut icon" type="image/ico" href="favicon.ico"/>

    <script type="text/javascript" src="/js/vendor/jquery.cycle2.carousel.min.js" ></script>
<!--    --><?php //Yii::app()->getClientScript()->registerScriptFile('/js/vendor/jquery.jSlots.js');?>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/vendor/jquery.slot.min.js');?>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/smartdropdown.js');?>
    <!--[if lt IE 9]>
    <script src="/dest/js/html5shiv.js"></script>
    <![endif]-->

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="w1-verification" content="196595389618" />

</head>
<body>
<div class="globalWrapper">
    <div class="menuWrapper cf">
        <div class="menuBlock resize cf">
            <?php $this->widget('ext.widgets.NavigationMenu', [
                'items' => [
                    [
                        'class' => 'ext.widgets.HorizontalMenu',
                        'options' => [
                            'position' => 'top',
                            'htmlOptions' => [
                                'class' => 'fll'
                            ],
                            'otherLinks' => [
                                [
                                    'title' => '',
                                    'url' => '/',
                                    'htmlOptions' => [
                                        'class' => 'home > sprite'
                                    ]
                                ]
                            ]

                        ],
                    ]
                ],
            ]);?>
            <?php $this->widget('ext.widgets.NavigationMenu', [
                'items' => [
                    [
                        'class' => 'ext.widgets.AuthMenu',
                        'options' => [
                            'htmlOptions' => [
                                'class' => 'auth-menu-list'
                            ]
                        ],
                    ]
                ]
            ]);?>
        </div>
    </div>
    <div class="fireworks" style="position: absolute; top: 50px; left: 90px; display: none;">
        <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="1440" height="400">
            <param name="movie" value="/swf/firework-test.swf">
            <param name="quality" value="high">
            <param name="wmode" value="transparent">
            <param name="swfversion" value="6.0.65.0">
            <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
            <param name="expressinstall" value="Scripts/expressInstall.swf">
            <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
            <!--[if !IE]>-->
            <object type="application/x-shockwave-flash" data="/swf/firework-test.swf" width="1240" height="200">
                <!--<![endif]-->
                <param name="quality" value="high">
                <param name="wmode" value="transparent">
                <param name="swfversion" value="6.0.65.0">
                <param name="expressinstall" value="Scripts/expressInstall.swf">
                <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                <div>
                    <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                    <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                </div>
                <!--[if !IE]>-->
            </object>
            <!--<![endif]-->
        </object>
        <script type="text/javascript">
            swfobject.registerObject("FlashID");
        </script>
    </div>
    <?php echo $content; ?>
    <?php $this->renderPartial('//site/partials/login_form');?>
    <?php $this->renderPartial('//site/partials/game_js_settings');?>
    <div class="gray-popup-bg"><!-- --></div>
    <div class="result-popup sertificates">
        <div class="head-block">
            <p>Winklik</p>
                  <span class="close-button" onclick="javascript: smartOverlay.hide();">
                  </span>
        </div>
        <div class="cont-block">
            <div class="message">
                Сообщение
            </div>
            <div class="button-wrap">
                <!--            <button type="button" onclick="javascript: smartOverlay.hide();">-->
                <!--                OK-->
                <!--            </button>-->
            </div>
        </div>
    </div>

</div>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-59109945-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
