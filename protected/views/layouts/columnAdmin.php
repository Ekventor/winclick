<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

    <div class="content-wrap admin">
        <div class="content-head">
            <div class="content-head-right">
                                <p>Администрирование</p>
            </div>
        </div>
        <div class="content" style="width: 100%;"> <!-- content start -->

            <div class="left-section">
                <div id="sidebar">
                    <?php
                    if (Yii::app()->user->isAdmin()) {
                        $this->widget('zii.widgets.CMenu', [
                            'activateParents' => true,
                            'activeCssClass' => 'active',
                            'items'=>[
                                ['label' => 'Настройки', 'url' => ['/backend/settings/index']],
//                                ['label' => 'Css-стили', 'url' => ['/backend/css/update']],
                                ['label' => 'Пользователи', 'url' => ['/backend/users/admin']],
                                ['label' => 'Страницы', 'url' => ['/backend/pages/admin']],
                                ['label' => 'Меню', 'url' => ['/backend/menu/index']],
//                            ['label' => 'Аккаунты', 'url' => ['/backend/accounts/admin']],
//                                ['label' => 'История аккаунтов', 'url' => ['/backend/accountHistory/admin']],
                                ['label' => 'Отзывы', 'url' => ['/backend/review/admin']],
//                                ['label' => 'Рассылки', 'url' => ['/backend/mailings/admin']],
                                ['label' => 'Новости', 'url' => ['/backend/content/news']],
                                ['label' => 'Новости для партнеров', 'url' => ['/backend/content/partnerNews']],
                                ['label' => '"Помощь" для партнеров', 'url' => ['/backend/content/partnerHelp']],
                                ['label' => 'Раздел "Помощь"', 'url' => ['/backend/content/help']],
                                ['label' => 'Раздел "Как это работает"', 'url' => ['/backend/content/info']],
                                ['label' => 'Слайдер на главной', 'url' => ['/backend/content/mainInfo']],
                                ['label' => 'Организации', 'url' => ['/backend/organization/admin']],
                                ['label' => 'Города', 'url' => ['/backend/city/admin']],
                                ['label' => 'Категории организаций', 'url' => ['/backend/category/admin']],
//                                ['label' => 'Подарочные сертификаты', 'url' => ['/backend/giftcard/admin']],
                            ],
                            'htmlOptions'=>array('class'=>'left-section-ul'),
                        ]);
                    } else {
                        $this->widget('zii.widgets.CMenu', [
                            'activateParents' => true,
                            'activeCssClass' => 'active',
                            'items'=>[
//                                ['label' => 'Настройки', 'url' => ['/backend/settings/index']],
//                                ['label' => 'Css-стили', 'url' => ['/backend/css/update']],
//                                ['label' => 'Пользователи', 'url' => ['/backend/users/admin']],
                                ['label' => 'Страницы', 'url' => ['/backend/pages/admin']],
                                ['label' => 'Меню', 'url' => ['/backend/menu/index']],
//                            ['label' => 'Аккаунты', 'url' => ['/backend/accounts/admin']],
//                                ['label' => 'История аккаунтов', 'url' => ['/backend/accountHistory/admin']],
                                ['label' => 'Отзывы', 'url' => ['/backend/review/admin']],
//                                ['label' => 'Рассылки', 'url' => ['/backend/mailings/admin']],
                                ['label' => 'Новости', 'url' => ['/backend/content/news']],
                                ['label' => 'Новости для партнеров', 'url' => ['/backend/content/partnerNews']],
                                ['label' => '"Помощь" для партнеров', 'url' => ['/backend/content/partnerHelp']],
                                ['label' => 'Раздел "Помощь"', 'url' => ['/backend/content/help']],
                                ['label' => 'Раздел "Как это работает"', 'url' => ['/backend/content/info']],
                                ['label' => 'Организации', 'url' => ['/backend/organization/admin']],
                                ['label' => 'Города', 'url' => ['/backend/city/admin']],
                                ['label' => 'Категории организаций', 'url' => ['/backend/category/admin']],
                            ],
                            'htmlOptions'=>array('class'=>'left-section-ul'),
                        ]);

                    }
                    ?>
                </div><!-- sidebar -->
            </div>
            <div class="right-section admin">
                <?php echo $content; ?>
            </div>
            <div style="clear: both;"></div>
        </div> <!-- content end -->
    </div>
<?php $this->endContent(); ?>