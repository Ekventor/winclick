<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Comfortaa&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/main_new.css">
    <script type="text/javascript" src="/js/vendor/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery.tmpl.min.js"></script>
    <script src="/js/vendor/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script type="text/javascript" src="/js/game.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/smartoverlay.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery-ui.js"></script>
    <script src="/js/vendor/jquery.carouFredSel-6.2.1-packed.js"></script>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/vendor/jquery.slot.min.js');?>
    <?php Yii::app()->getClientScript()->registerScriptFile('/js/smartdropdown.js');?>
    <!--[if lt IE 9]>
    <script src="/dest/js/html5shiv.js"></script>
    <![endif]-->

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

</head>
<body>
<!--[if lt IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<!-- BEGIN HEADER -->
<div class="actions-panel-wrap">
    <div class="c-align clearfix">
        <?php $this->widget('ext.widgets.NavigationMenu', [
            'items' => [
                [
                    'class' => 'ext.widgets.HorizontalMenu',
                    'options' => [
                        'position' => 'top',
                        'htmlOptions' => [
                            'class' => 'navigation-menu float-left clearfix'
                        ],
                        'otherLinks' => [
                            [
                                'title' => '',
                                'url' => '/',
                                'htmlOptions' => [
                                    'class' => 'home > sprite'
                                ]
                            ]
                        ]
                    ],
                ], [
                    'class' => 'ext.widgets.AuthMenu',
                    'options' => [
                        'htmlOptions' => [
                            'class' => 'user-panel float-right clearfix'
                        ]
                    ]
                ]
            ],
        ]);?>

    </div>
</div>

<div class="header-wrap">
    <div class="c-align clearfix autorization">
        <div class="logo float-left">
            <a href="<?= $this->createAbsoluteUrl('/');?>"><!-----></a>
        </div>
        <h1 class="site-name float-left center">
            АУКЦИОН &laquo;ВИНКЛИК&raquo;
            <span class="subtitle">первый 100% беспроигрышный</span>
        </h1>

        <div class="site-guests-panel float-right">
            <div class="text-panel float-left">
                <span class="title">НА САЙТЕ</span>
                <span class="current-number sprite"><?= Yii::app()->myuser->getOnline();?></span>
            </div>
        </div>
    </div>
</div>
<!-- END HEADER -->

<!-- BEGIN CONTENT -->
<?php echo $content; ?>
<!-- END CONTENT -->

<!-- BEGIN FOOTER -->
<div class="footer-wrap">
    <div class="footer-nav-wrap">
        <div class="c-align">
            <?php $this->widget('ext.widgets.NavigationMenu', [
                'items' => [
                    [
                        'class' => 'ext.widgets.HorizontalMenu',
                        'options' => [
                            'position' => 'bottom',
                            'htmlOptions' => [
                                'class' => 'footer-nav clearfix'
                            ]
                        ]
                    ]
                ]
            ]);?>
        </div>
    </div>
    <div class="c-align">
        <footer class="site-footer clearfix">
            <span class="copyrights">© ЗАО “МПА ”ВИНКЛИК” 2013 год</span>
            <a href="#" class="ed-kashel"><!----></a>
            <span class="social">
				<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
				<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir" data-yashareTheme="button"></div> 
		    </span>
            <a href="#" class="years-label sprite"><!----></a>
            <span class="copyrights-2">Все права защищены</span>
        </footer>
    </div>
</div>
<!-- END FOOTER -->


<!--<script src="dest/js/scripts.min.js"></script>-->

<!-- HIDDEN REG FORM стили в main.css-->
<!--<div class="hidden_reg" id="reg_hidden">-->
<!--    <form>-->
<!--        <p>Авторизация</p>-->
<!--        <input type="text" name="login" placeholder="&nbsp;&nbsp;&nbsp;Введите ваш логин">-->
<!--        <input type="text" name="password" placeholder="&nbsp;&nbsp;&nbsp;Введите ваш пароль">-->
<!--        <a href="#">Забыли пароль?</a>-->
<!--        <input type="submit" name="submit" value="Войти">-->
<!--    </form>-->
<!--</div>-->

<!-- HIDDEN REG FORM2 стили в main.css-->
<!--<div class="hidden_reg2" id="reg_hidden2">-->
<!--    <form>-->
<!--        <p class="form-h-p">Регистрация</p>-->
<!--        <label>Введите ваш логин:</label>-->
<!--        <input type="text" name="login">-->
<!--        <label>Введите ваш email:</label>-->
<!--        <input type="text" name="email">-->
<!--        <div class="form-text-wrap">-->
<!--            <p>Я в полном объеме ознакомлен и безоговорочно признаю все положения <a href="#">пользовательского соглашения</a><input type="checkbox" checked></p>-->
<!--        </div>-->
<!--        <input type="submit" class="blue-button" name="submit" value="Зарегистрировать">-->
<!--    </form>-->
<!--</div>-->


<div class="gray-popup-bg"><!-- --></div>
<div class="result-popup">
<!--    <div class="head-block">-->
<!--        <p>Winklik</p>-->
<!--              <span class="close-button" onclick="javascript: smartOverlay.hide();">-->
<!--                <!-- -->
<!--              </span>-->
<!--    </div>-->
    <div class="cont-block">
        <div class="message">
            Сообщение
        </div>
        <div class="button-wrap">
            <button type="button" onclick="javascript: smartOverlay.hide();">
                OK
            </button>
        </div>
    </div>
</div>

<?php $this->renderPartial('//site/partials/login_form');?>
<?php $this->renderPartial('//site/partials/game_js_settings');?>
<!--<script src="/dest/js/scripts.min.js"></script>-->
</body>
</html>
