<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/main_new'); ?>
<!--    <div class="content-wrap">-->
<!--        <div class="content-head">-->
<!--            <div class="content-head-right">-->
<!--                <p>--><?php //echo $this->pageTitle; ?><!--</p>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="content"> <!-- content start -->
<!--                --><?php //echo $content; ?>
<!--        </div>-->
        <!-- content end -->
<!--    </div> <!-- content-wrap end -->

    <div class="orange-line borders" style="margin-top: 50px;"></div>
    <div class="typicalWrapper">
        <div class="typical-page-wrap">
            <div class="typical-page-header">
                <?= $this->pageTitle; ?>
            </div>
            <div class="deviver right-full"></div>
            <div class="typical-page-content">
                <?= $content; ?>
            </div>
        </div>
<!--        <div class="dice"></div>-->
    </div>
    <div class="orange-line borders"></div>
    <div class="footerBgWrapper">
        <div class="footerWrapper cf" id="f4">
            <div class="footer resize cf">
                <div class="fll left">
                    © ООО “МПА ”ВИНКЛИК” <?= date('Y') ?> год <br>
                    Все права защищены
                </div>
                <div class></div>
                <div class="bottom-menu">
                    <div class="bottom-list">
                        <?php $this->widget('ext.widgets.NavigationMenu', [
                            'items' => [
                                [
                                    'class' => 'ext.widgets.HorizontalMenu',
                                    'options' => [
                                        'position' => 'bottom',
                                        'htmlOptions' => [
                                            'class' => ''
                                        ]
                                    ]
                                ]
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>