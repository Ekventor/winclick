<div class="coupons-wrap">

    <?= CHtml::form("/user/buyCoupons", 'POST'); ?>
    <table class="winclicks-form-table">

        <tr>
            <td class="labels-td">
                <p>Купоны в наличии:</p>
            </td>
            <td class="input-td">
                <div class="how-much-winclicks-now" id="active-coupons">
                    <?= $activeCouponsCount; ?>
                </div>
            </td>
            <td class="nominal-td" colspan="2">
                <p></p>
            </td>

        </tr>
<!--        <tr>-->
<!--            <td class="labels-td">-->
<!--                <p>Купоны использованные<br>при приобретении товаров и услуг</p>-->
<!--            </td>-->
<!--            <td class="input-td">-->
<!--                <div class="how-much-winclicks-now" id="used-coupons">-->
<!--                    --><?php //echo $usedCouponsCount; ?>
<!--                </div>-->
<!--            </td>-->
<!--            <td class="nominal-td" colspan="2">-->
<!--                <p></p>-->
<!--            </td>-->
<!---->
<!--        </tr>-->

        <tr>
            <td class="labels-td">
                <p>Укажите количество купонов для получения:</p>
            </td>
            <td class="input-td">
                <?= CHtml::textField('coupons', 0, ['id' => 'get-coupons']); ?>
            </td>
            <td class="nominal-td">
                <p></p>
            </td>
            <td style="width: 120px;">
                <div class="errorMessage cabinet" style="display: none;"></div>
            </td>
        </tr>

    </table>
    <?=
    CHtml::ajaxSubmitButton('Получить', '/user/buyCoupons', [
        'success' => 'function(data){
                                            result = JSON.parse(data);
                                            $(".result-popup .message").html(result.message);
                                            smartOverlay.setPopup(".result-popup").show();
                                            if (!result.error) {
                                                $("#active-coupons").html(
                                                    parseInt($("#active-coupons").html()) - parseInt($("#get-coupons").val())
                                                );
                                                $("#get-coupons").val(0);
                                            }
                                        }'
    ],
        [
            'class' => 'main-button'
        ]
    );
    ?>
    <?= CHtml::endForm(); ?>
    <div class="coupon-info">
        <?= Yii::app()->settings->getValue('coupon_instruction'); ?>
    </div>
</div>
<script>
    $(document).ready(function(){
        smartOverlay.init('.result-popup', '.gray-popup-bg');
    });
</script>