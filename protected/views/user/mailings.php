<?php
/* @var $this MailingsController */
/* @var $dataProvider CActiveDataProvider */

	$dataProvider->pagination = false;
?>

<div class="float-left admin-page-wrap">
	<h2 class="page-cont-title upper"><?= $this->pageTitle; ?></h2>
	<ul class="admin-page">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'mailings_view',
			'emptyText'=>'<li>
							<div class="head-block">
								<h4 class="admin-article-title">Нет сообщений</h4>
							</div>
						</li>',
			'summaryText'=>'',
		)); ?>
	</ul>
</div>

