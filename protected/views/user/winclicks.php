<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 17.07.14
 * Time: 18:38
 */
?>
<?php $form = $this->beginWidget('CActiveForm', [
    'id' => 'my-form',
    'action' => '/wallet/pay',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ]
]); ?>
<table class="winclicks-form-table">

    <tr>
        <td class="labels-td">
            <p>Счет (куплено WK):</p>
        </td>
        <td class="input-td">
            <div class="how-much-winclicks-now">
                <?= Yii::app()->account->getBalanceByCurrency(Y::user(), 'WK');
                ?>
            </div>
        </td>
        <td class="nominal-td" colspan="2">
            <p>wk</p>
        </td>

    </tr>

    <tr>
        <td class="labels-td">
            <p>Купить винклики (укажите кол-во):</p>
        </td>
        <td class="input-td">
            <?php echo $form->textField($number, 'amount'); ?>
        </td>
        <td class="nominal-td">
            <p>wk</p>
        </td>
        <td style="width: 120px;">
            <div class="errorMessage cabinet" style="display: none;">
                <!--                --><?php //echo $form->error($number, 'amount', ['class' => 'errorMessage cabinet']); ?>
        </td>
    </tr>

    <!--        <tr>-->
    <!--            <td class="labels-td">-->
    <!--                <p>Автоматическая покупка wk</p>-->
    <!--            </td>-->
    <!--            <td class="input-td">-->
    <!--                <span class="switch"> -->
    <!--                    --><?php //echo CHtml::checkBox('Users[auto_convert]', $user->auto_convert, ['id' => 'autowk', 'onclick'=>'javascript: game.updateWk($(this));']); ?>
    <!--                    <label for="autowk"><span></span></label>-->
    <!--                </span>-->
    <!--            </td>-->
    <!--            <td class="nominal-td" colspan="2">-->
    <!---->
    <!--            </td>-->
    <!---->
    <!--        </tr>-->

</table>

<?php echo CHtml::button('Купить', ['class' => 'main-button', 'id' => 'winklik-submit']); ?>

<script>
    $(document).ready(function() {
        $('#winklik-submit').on('click', function() {
            if (!parseInt($('#Number_amount').val())) {
                $($('#Number_amount')).css('border', '1px solid red');
                $('#Number_amount').parents('tr').find('.errorMessage').html('Укажите кол-во').show();
            } else {
                $('#my-form').submit();
            }
        });
    });
</script>
<span class="default-message-text">
            Рекомендуемая сумма покупки - 100 винкликов (1 винклик = <?= Yii::app()->settings->getValue('EXCHANGE_RATE'); ?>
    рублей)
    </span>
<br>
<br>

<!--    --><?php //if (!$enoughMoney): ?>
<span class="error-message-text" style="display: none;">
            На Вашем счету недостаточно средств. Пополните<br>пожалуйста счет в разделе <?php echo CHtml::link('деньги', $this->createUrl('money')); ?>
    .
        </span>
<!--    --><?php //endif; ?>
<?php $this->endWidget(); ?>

<script>
    $(document).ready(function () {
        smartOverlay.init('.result-popup', '.gray-popup-bg');
    });
</script>