<div class="personal-data ">
<!--  <h2 class="page-cont-title">РЕДАКТИРОВАНИЕ ПЕРСОНАЛЬНЫХ ДАННЫХ</h2>-->
  <form class="my-form" action="" method="post">
    <table class="form-table personal-form-table">
      <tbody>
        <tr>
          <td class="labels-td">
            <img src="/img/white-circle.png" alt="">
            <label>Логин:</label>
          </td>
          <td class="input-td">
            <p class="login-name"><?= $user->login;?></p>
          </td>
          <td class="example-td"></td>
          <td class="last-field-td"></td>
        </tr>
        <tr>
          <td class="labels-td">
            <img src="/img/white-circle.png" alt="">
            <label>e-mail:</label>
          </td>
          <td class="input-td">
              <?php if ($user->getError('email')): ?>
              <?= CHtml::activeTextField($user, 'email', ['class' => 'errorField']);?>
              <?php else: ?>
              <?= CHtml::activeTextField($user, 'email');?>
              <?php endif; ?>
          </td>
          <td class="example-td">
            <p>(пример: test@winklik.ru)</p>
          </td>
          <td class="last-field-td">
              <?php if ($user->getError('email')): ?>
                  <div class="errorMessage"><?php echo $user->getError('email'); ?></div>
              <?php endif; ?>
          </td>
        </tr>
        <tr>
          <td class="labels-td">
            <img src="/img/white-circle.png" alt="">
            <label>Телефон:</label>
          </td>
          <td class="input-td">
              <?php if ($user->getError('phone')): ?>
                <?= CHtml::activeTextField($user, 'phone', ['class' => 'errorField']);?>
              <?php else: ?>
                <?= CHtml::activeTextField($user, 'phone');?>
              <?php endif; ?>
          </td>
          <td class="example-td">
            <p>(пример: 89011234567)</p>
          </td>
          <td class="last-field-td">
              <?php if ($user->getError('phone')): ?>
                <div class="errorMessage"><?php echo $user->getError('phone'); ?></div>
              <?php endif; ?>
          </td>
        </tr>
        <?php if ($user->getError('rePassword') || $user->getError('newPassword')): ?>
        <tr>
            <td colspan="4" align="center">
                <div class="errorMessage" style="width: 100%;"><?php echo $user->getError('newPassword'); ?></div>
            </td>
        </tr>
        <?php endif; ?>
        <tr>
          <td class="labels-td">
            <img src="/img/white-circle.png" alt="">
            <label>Пароль:</label>
          </td>
          <td class="input-td">
              <?php if ($user->getError('newPassword')): ?>
              <?= CHtml::activePasswordField($user, 'newPassword', ['class' => 'errorField']);?>
              <?php else: ?>
                  <?= CHtml::activePasswordField($user, 'newPassword');?>
              <?php endif; ?>
          </td>
          <td class="example-td">
            <label>Повторите пароль:</label>
          </td>
          <td class="last-field-td">
              <?php if ($user->getError('rePassword')): ?>
                  <?= CHtml::activePasswordField($user, 'rePassword', ['class' => 'errorField']);?>
              <?php else: ?>
                  <?= CHtml::activePasswordField($user, 'rePassword');?>
              <?php endif; ?>
          </td>
        </tr>

        <?php if (!Yii::app()->user->hasState('changePassword') || !Yii::app()->user->getState('changePassword')):?>
        <tr>
          <td class="labels-td">
            <img src="/img/white-circle.png" alt="">
            <label>Старый пароль:</label>
          </td>
          <td class="input-td">
            <?php if ($user->getError('password')): ?>
                <?= CHtml::passwordField('oldPassword', '', ['class' => 'errorField']);?>
                <?php else: ?>
                <?= CHtml::passwordField('oldPassword');?>
                <?php endif; ?>
          </td>
          <td class="example-td">
            <p>Для изменения данных введите старый пароль</p>
          </td>
          <td class="last-field-td">
              <?php if ($user->getError('password')): ?>
                  <div class="errorMessage"><?php echo $user->getError('password'); ?></div>
              <?php endif; ?>
          </td>
        </tr>
        <?php endif;?>
      </tbody>
    </table>
    <input type="submit" name="submit" value="Сохранить" class="main-button">
  </form>
</div>