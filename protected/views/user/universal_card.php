<div class="card-block">
    <div class="prize-block">
        <?= CHtml::hiddenField('current-cert', 0); ?>
        <div class="card-image">
            <img src="/images/certificates/present.jpg">
        </div>
        <div class="count">
                        <span id="count1">
                            <?= $winBalance; ?> <div class="rub-symbol"></div>
                        </span>
            <?php if ($winBalance < Y::getSetting('win_amount')): ?>
                <?= CHtml::button('Получить', ['class' => 'get-button disabled-button', 'disabled' => 'disabled']); ?>
            <?php else: ?>
                <?= CHtml::button('Получить', ['class' => 'get-button', 'id' => 'take-cards']); ?>
            <?php endif; ?>
        </div>
        <div class="card-info">
                        <span>
                            <?= Yii::app()->settings->getValue('giftcard_info'); ?>
                        </span>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="giftcards-info">
        <?= Y::getSetting('giftcard_info_long'); ?>
    </div>
</div>
<?php
Yii::app()->getClientScript()->registerScript(
    'giftcards',
    '$(document).ready(function(){
        giftcards.setWinclickCost(' . $winclickCost . ');
        giftcards.init();
    });'
);
?>