<?php
/* @var $this MailingsController */
/* @var $data Mailings */
?>

<div class="view">
	
	<li>
		<div class="head-block">
			<h4 class="admin-article-title"><?php echo $data->title; ?></h4>
		</div>
		<div class="content-block">
			<p class="admin-article">
				<?php echo $data->message; ?>
			</p>
		</div>
	</li>


</div>