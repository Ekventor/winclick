<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/15/14
 * Time: 5:18 PM
 */
?>
<div class="simple-text-block float-left">

    <h2 class="page-cont-title upper">Получение электронных подарочных сертификатов</h2>

    <div class="card-block">
        <div class="card-image">
            <img src="/images/certificates/<?= $cardNominal?>-present.jpg">
        </div>
        <div class="card-info">
                <span>
                    <?= Yii::app()->settings->getValue('giftcard_info'); ?>
                </span>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div class="line" style="clear: both;"></div>
    <div class="sert-list-block">
        <?php foreach (Yii::app()->card->getCardSertificates($cardNominal) as $key => $sertificate): ?>
            <?php if (($key % 3 == 0) && ($key != 0)): ?>
                <div class="line" style="clear: both;"></div>
            <?php endif; ?>
        <div class="sert-block">
            <div class="sert-image">
                <?= CHtml::image($sertificate['img']); ?>
            </div>
            <div class="sert-info">
                <span>
                    <?= CHtml::encode($sertificate['desc']);?>
                </span>
            </div>
            <div class="sert-buy-button">
                <input class="golden-button" type="button" value="Обменять">
            </div>
        </div>
        <?php endforeach; ?>
        <div class="line" style="clear: both;"></div>
    </div>
</div> <!-- personal-data end -->
<script>
    $(document).ready(function() {
        var arr = [];
        $('.sert-list-block .sert-info').each(function(obj) {
            arr[arr.length] = $(this).height();
        });
        var max = Math.max.apply(Math, arr);
        $('.sert-list-block .sert-info').each(function(obj) {
            $(this).height(max);
        });
    });
</script>
