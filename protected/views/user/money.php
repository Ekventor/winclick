<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 17.07.14
 * Time: 16:36
 */
?>
<div class="personal-data float-left">
    <h2 class="page-cont-title upper"><?= $this->pageTitle; ?></h2>


    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'my-form',
        'action' => '/wallet/pay/',
        'htmlOptions' => [
            'action' => '/wallet/pay/'
        ],
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true
        ]
    )); ?>


    <table class="money-form-table">

        <tr>
            <td class="labels-td">
                <p>Счет (деньги в наличии):</p>
            </td>
            <td class="input-td">
                <div class="how-much-money"><?=Yii::app()->account->getBalanceByCurrency(Y::user(), 'RUB');
                    ?></div>
            </td>
            <td class="nominal-td" colspan="2">
                <p>руб.</p>
            </td>

        </tr>
        <tr>
            <td class="labels-td">
                <p>Пополнить счет (введите сумму):</p>
            </td>
            <td class="input-td">
                <?php echo $form->textField($number, 'amount', ['placeholder' => 150]); ?>
            </td>
            <td class="nominal-td">
                <p>руб.</p>
            </td>
            <td style="width: 120px;">
                <?php echo $form->error($number, 'amount', ['class' => 'errorMessage cabinet']); ?>
            </td>
        </tr>
    </table>
    <?php echo CHtml::submitButton('Пополнить'); ?>
    <span class="default-message-text">
        Рекомендованная сумма пополнения - 150 рублей
    </span>

    
    <?php if (Yii::app()->user->hasFlash('message')): ?>
        <span class="error-message-text">
            <?= Yii::app()->user->getFlash('message');?>
        </span>
    <?php endif; ?>


    
    <?php $this->endWidget(); ?>

</div>
