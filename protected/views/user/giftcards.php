<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 17.07.14
 * Time: 18:32
 */
//Y::dump($list, 0);
?>
<style>

</style>
<div class="personal-data float-left">
    <div class="accordion-panel-wrap">


        <?php foreach (Y::param('nominals') as $nominal): ?>
            <div class="tranzaction-panel cards<?= $nominal; ?>">

                <div class="tranzaction-panel-arrow"></div>
                <p class="float-left">Карты номиналом</p>

                <div class="cards-sum-wrap float-left">
                    <p class="cash-sum"><span><?= $nominal; ?></span> руб:</p>
                </div>
                <div class="cards-number-wrap">
                    <p class="number-of-cards"><span></span></p>
                </div>

            </div>


            <div class="card-block" id="card<?= $nominal; ?>">
                <div class="prize-block">
                    <?= CHtml::hiddenField('current-cert', 0); ?>
                    <div class="card-image">
                        <img src="/images/certificates/<?= $nominal; ?>-present.jpg">
                    </div>
                    <div class="count">
                        <span id="count<?= $nominal; ?>">
                            <?= Yii::app()->card->getCardCount($userId, $nominal); ?>
                        </span>
                        <?php if (!Yii::app()->card->getCardCount($userId, $nominal)): ?>
                            <?= CHtml::button('Обменять', ['class' => 'golden-button exch-button disabled', 'disabled' => 'disabled']); ?>
                        <?php else: ?>
                            <?= CHtml::button('Обменять', ['class' => 'golden-button exch-button']); ?>
                        <?php endif; ?>
                    </div>
                    <div class="card-info">
                        <span>
                            <?= Yii::app()->settings->getValue('giftcard_info'); ?>
                            <!--                            Здесь вы можете приобрести электронные подарочные серти- фикаты наших партнеров. Спасибо вам, что пользуетесь нашим сер- висом! Мы вам всегда рады!-->
                        </span>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="line" style="clear: both;"></div>
                <p class="digital-sert">Электронные подарочные сертификаты и карты от компании “Дарить Легко”</p>

                <div class="line" style="clear: both;"></div>
                <div class="sert-list-block">
                    <div class="section-sert">
                        <?php foreach (Yii::app()->card->getCardSertificates($nominal) as $key => $sertificate): ?>
                        <?php if ($key % 4 == 0 && $key != 0): ?>
                    </div>
                    <div class="line" style="clear: both;"></div>
                    <div class="section-sert">
                        <?php endif; ?>
                        <div class="sert-block" sert-id="<?= $sertificate['id']; ?>" nominal="<?= $nominal; ?>">
                            <a href="javascript:void(0);">
                                <div class="sert-image">
                                    <img src="<?= $sertificate['small_img'] ?>" alt="">
                                </div>
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="line" style="clear: both;"></div>
                    <div class="cart" id="sert<?= $nominal; ?>">
                        <div class="title">
                            <span><img src="/img/new/cart.png" class="pull-left"> Карты, которые вы получите по результатам обмена</span>
                            <?= CHtml::button('Получить', ['class' => 'golden-button pull-right disabled take-cards', 'disabled' => 'disabled']); ?>
                        </div>
                        <div class="serts">
                            <div class="sert" id="sert-0" style="display: none;">
                                <img class="image" src="">
                                <a href="javascript:void(0);" class="close" sert-id="0"><img
                                        src="/img/new/close.png"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div> <!-- personal-data end -->

<?php
Yii::app()->getClientScript()->registerScript(
    'giftcards',
    '$(document).ready(function(){
        giftcards.setWinclickCost(' . $winclickCost . ');
        giftcards.init();
    });'
);
?>
