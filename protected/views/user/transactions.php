<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 17.07.14
 * Time: 18:29
 */
?>
<div class="personal-data float-left">
    <div class="accordion-panel-wrap">
        <div class="tranzaction-panel winclicks">
            <div class="tranzaction-panel-arrow"></div>
            <p>Винклики</p>
        </div>

        <div class="tranzaction-table-wrap winclicks_table">
            <?php
            $this->widget('MTable', array(
                'id' => 'wk-grid',
                'htmlOptions' => array('class' => 'tranzaction-table'),
                'enableSorting' => false,
                'dataProvider' => $account_history[1],
                'pager' => [
                    'class' => 'CLinkPager',
                    'maxButtonCount' => 5
                ],
                'summaryText' => '',
                'columns' => array(
                    array('name' => 'date', 'value' => 'date("d.m.Y", strtotime($data->date));'),
                    array('name' => 'time', 'value' => 'date("H:i:s", strtotime($data->date));', 'header' => Yii::t('app', 'Время')),

                    array('name' => 'payin', 'value' => '$data->amount>0 ? $data->amount : "";', 'header' => Yii::t('app', 'Поступило')),
//                    array('name' => 'payout', 'value' => '$data->amount<0 ? abs($data->amount) : "";', 'header' => Yii::t('app', 'Списано')),
//                    array('name' => 'close_balance', 'value' => '$data->close_balance'),
//
//                    array('name' => 'comment', 'value' => '$data->comment'),
                ),
                'headers' => array(
                    'date',
                    'time',
                    'payin'
//                    'summ' => array(
//                        'label' => Yii::t('app', 'Сумма (wk)'),
//                        'childs' => array(
//                            'payin',
//                            'payout',
//                            'close_balance',
//                        ),
//                    ),
//                    'comment',

                )

            ));
            ?>
        </div>

        <div class="tranzaction-panel money">
            <div class="tranzaction-panel-arrow"></div>
            <p>История заказа</p>
        </div>

        <div class="tranzaction-table-wrap money_table">
            <?php
            $this->widget(
                'zii.widgets.grid.CGridView',
                [
                    'id' => 'user-giftcards-grid',
                    'dataProvider' => $orderHistory,
                    'htmlOptions' => [
                        'class' => 'tranzaction-table',
                    ],
                    'enableSorting' => false,
                    'summaryText' => '',
                    'columns' => [
                        [
                            'header' => 'Дата',
                            'value' => 'date("d.m.Y", strtotime($data->date))'
                        ],
//                        'nominal',
                        [
                            'header' => 'Наименование',
                            'value' => '$data->count'
                        ],
//                        'count'
                    ]
                ]
            );
            ?>

        </div>

    </div>

</div>

<script>
    $(document).ready(function() {
        $('.tranzaction-panel.winclicks').click();
    });
</script>

<style>
    table.tranzaction-table, .tranzaction-table table {
        width: 100%;
    }

</style>