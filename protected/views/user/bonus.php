<div class="simple-text-block float-left">
    <div class="bonus clearfix">
        <div class="col-2 float-right">
            <div class="bonus-panel">
                <div class="upper-block"></div>
                <div class="hr"><!----></div>
                <div class="present-action">
                    <div class="coupons-have">
                        <div class="info-text">
                            <span>Купоны в наличии</span>
                        </div>
                        <div class="coupon-count-info">
                            <span class="doted-line-1"></span>
                            <span class="coupons-number"><?= $activeCouponsCount ?></span>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="hr"><!----></div>
                    <div class="coupons-send">
                        <div class="info-text">
                            <span>Купоны использованные</span><br>
                            <span>при приобретении товаров и услуг</span>
                        </div>
                        <div class="coupon-count-info">
                            <span class="doted-line-2"></span>
                            <span class="coupons-number"><?= $usedCouponsCount ?></span>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="hr"><!----></div>
                    <div class="coupons-action">
                        <?= CHtml::form("/user/buyCoupons", 'POST'); ?>
<!--                        <form action="/user/buyCoupons" method="POST">-->
                            <span>Укажите количество купонов:</span>

                            <div class="coupon-form">
                                <div class="number-input">
                                    <?= CHtml::textField('coupons', 0); ?>
<!--                                    <input type="text" value="0" name="coupons">-->
                                </div>
                                <div class="number-submit">
                                    <?= CHtml::ajaxSubmitButton('Получить', '/user/buyCoupons', [
                                        'success' => 'function(data){
                                            result = JSON.parse(data);
                                            $(".result-popup .message").html(result.message);
                                            smartOverlay.setPopup(".result-popup").show();
                                            if (!result.error) {
                                                $(".coupons-have .coupons-number").html(
                                                    parseInt($(".coupons-have .coupons-number").html()) - parseInt($("#coupons").val())
                                                );
                                                $("#coupons").val(0);
                                            }
                                        }'],
                                        [
                                            'class' => 'golden-bonus-button'
                                        ]
                                    )?>
<!--                                    <a href="#" class="golden-bonus-button" id="submit-coupon">Получить</a>-->
                                </div>
                            </div>
                        <?= CHtml::endForm();?>
<!--                        </form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="coupon-info">
<!--        <span class="col-title">Инструкция</span>-->
        <?= Yii::app()->settings->getValue('coupon_instruction'); ?>
<!--        <p>-->
<!--            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum-->
<!--            has been the industry's standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the-->
<!--            printing and typesetting industry. Lorem Ipsum-->
<!--            has been the industry's standard dummy text ever since the 1500s-->
<!--        </p>-->
    </div>


    <!--    <p class="central-align bonus-link-wrap">-->
    <!--        --><?php //echo CHtml::link('Условия бонусной программы', $this->createUrl('user/bonus/view/rule'), ['class' => 'black-link']); ?>
    <!--    </p>-->
</div> <!-- personal-data end -->
<script>
    $(document).ready(function(){
        smartOverlay.init('.result-popup', '.gray-popup-bg');
        $('#submit-coupon').on('click', function(){
//            $(this).preventDefault();
            $(this).closest('form').submit();
        });
    });
</script>