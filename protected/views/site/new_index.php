<script>
    $(document).ready(function() {
        var nominals = JSON.parse('<?= $jsonNominals; ?>');
        Rater.init('#pref-rate', '#next-rate', nominals);
    });

</script>

<style>
</style>

<div class="firstWrapper cf">
    <div class="firstBlock resize cf">
        <div class="h1-title">Первый 100%</div>
        <div class="sub-title">Беспроигрышный аукцион</div>
    </div>
    <div class="auctionWrapper">
        <div class="auction-head">
            <div class="auction-head-title">
                Демо-аукцион подарочных купонов и карт <span id="card-nominal"></span>
            </div>
<!--            <div class="auction-games-left">-->
<!--                До победы - <span id="games-left">03</span>-->
<!--            </div>-->
            <div class="auction-head-right">
                <div class="auction-super-prize">
                    <div class="super-prize-text">Суперприз</div>
                    <div class="thousands">
                        <div class="thousand1"><?= isset($prizeDigits[count($prizeDigits)-6]) ? $prizeDigits[count($prizeDigits)-6] : 0; ?></div>
                        <div class="thousand2"><?= isset($prizeDigits[count($prizeDigits)-5]) ? $prizeDigits[count($prizeDigits)-5] : 0; ?></div>
                        <div class="thousand3"><?= isset($prizeDigits[count($prizeDigits)-4]) ? $prizeDigits[count($prizeDigits)-4] : 0; ?></div>
                    </div>
                    <div class="numbers">
                        <div class="number1"><?= isset($prizeDigits[count($prizeDigits)-3]) ? $prizeDigits[count($prizeDigits)-3] : 0; ?></div>
                        <div class="number2"><?= isset($prizeDigits[count($prizeDigits)-2]) ? $prizeDigits[count($prizeDigits)-2] : 0; ?></div>
                        <div class="number3"><?= isset($prizeDigits[count($prizeDigits)-1]) ? $prizeDigits[count($prizeDigits)-1] : 0; ?></div>
                    </div>
                    <div class="header-rub"></div>
                </div>
            </div>
        </div>
        <div class="auction-block">
            <div class="auction-slot-block">
                <div class="guide guide-right"></div>
                <div class="auction-slots">
                    <?php for ($i = 0; $i < 3; $i++): ?>
                        <div class="slot1">
                            <div class="prize-list">
                                <ul>
                                    <?php foreach (Y::param('nominals') as $nominal): ?>
                                        <li><span class="coupon-title">Суперприз</span></li>
                                        <?php foreach (str_split(Y::getSetting('possible_letters')) as $letter): ?>
                                            <li><?php if ($letter == 'A'): ?>
                                                    <span class="coupon-title">Купон</span>
                                                <?php elseif ($letter == 'B'): ?>
                                                    <span class="x-mult">x</span>&nbsp;<span class="double-2">2</span>
                                                <?php endif; ?>
                                                <?php if ($letter == 'C'): ?>
                                                    <span class="back-title">Возврат WK</span>

                                                <?php endif; ?></li>
                                        <?php endforeach; ?>
                                        <li><span class="coupon-title">Карта</span></li>
                                    <?php endforeach; ?>

                                </ul>
                            </div>
                            <div class="green-bg"></div>
                            <div class="win-border"></div>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="guide guide-left"></div>
            </div>

            <div class="auction-info-block">
                <div class="auction-game-info-block">
                    <div class="game-information" data-type="start-info">
                        <div class="arrow-down"></div>
                        <div class="auction-info-text">
                            <div class="auction-info-text-title">
                                Для участия в аукционе
                            </div>
                            <p>
                                Установите одну из пяти ставок 1, 10, 25, 50 или 100
                                путем нажатия кнопки ПЛЮС или МИНУС, далее нажмите кнопку СТАРТ.
                            </p>
                        </div>
                    </div>
                    <div class="game-information" data-type="coupon-info">
                        <div class="man-persent"></div>
                        <div class="auction-info-text">
                            <div class="auction-info-text-title">
                                Купон на скидку ваш!
                            </div>
                            <p>
                                Для его получения перейдите в личный кабинет.
                                Купон дает право приобрести товары и услуги с ЭКСКЛЮЗИВНОЙ СКИДКОЙ.
                            </p>
                        </div>
                    </div>
                    <div class="game-information" data-type="win-info">
                        <div class="win-cup"></div>
                        <div class="auction-info-text">
                            <div class="auction-info-text-title">
                                Поздравляем вы выиграли !!!
                            </div>
                            <p>
                                У Вас на счете "УДВОЕНО" более <?php echo Y::getSetting('win_amount'); ?> wk,
                                что дает Вам право на получение
                                карты. Для получения карты перейдите в личный кабинет или
                                продолжайте участвовать в аукционе.
                            </p>
                        </div>
                    </div>
                    <div class="game-information" data-type="double-info">
                        <div class="double-rate"></div>
                        <div class="auction-info-text">
                            <div class="auction-info-text-title">
                                Ваша ставка удвоена !!!
                            </div>
                            <p>
                                Вы выиграли ставку в удвоенном размере. wk-ставки зачислены на счет "УДВОЕНО",
                                накопив не менее <?php echo Y::getSetting('win_amount'); ?> wk Вы можете
                                получить карту номиналом соответственно
                                не менее <?php echo Y::getSetting('win_amount'); ?> рублей.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="user-game-info-block">
                    <div class="game-rate-block">
                        <div class="user-game-info-title">
                            Ставка за участие
                        </div>
                        <div class="rate-input">
                            <div class="arrow-left-rate" id="pref-rate"></div>
                            <input type="text" disabled value="<?= Y::param('nominals')[count(Y::param('nominals'))-1]; ?>">
                            <div class="arrow-right-rate" id="next-rate"></div>
                        </div>
                    </div>
                    <div class="game-rate-doubled-block">
                        <div class="user-game-info-title">
                            Удвоено
                        </div>
                        <div class="rate-doubled-info" id="RUB_balance">
                            0
                        </div>
                        <div class="wk-balance-win-bg"></div>
                    </div>
                    <div class="user-wk-count">
                        <div class="user-game-info-title">
                            Куплено Wk
                        </div>
                        <div class="user-wk-info" id="WK_balance">
                            -
                        </div>

                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="button-start-block">
                    <button type="button" id="play">Старт !!!</button>
                </div>

            </div>
        </div>
        <div class="auction-footer">
            Карты с деньгами, купоны со скидками и суперпризы
        </div>
    </div>
</div>

<div class="orange-line"></div>

<div class="infoWrapper cf">
    <div class="info-block">
        <div class="info-header">
            Первый 100% беспроигрышный <br>
            приветсвует вас!
        </div>
        <div class="deviver right-full"></div>
        <div class="info-text">
            <ul>
                <li>
                    <p>
                        Наш аукцион лучшее место отлично провести время,<br>
                        выигрывая карты с деньгами, купоны со скидками<br>
                        и суперпризы.
                    </p>
                </li>
                <li>
                    <p>
                        Функция удвоения WK приумножит частоту ваших побед,<br>
                        делая аукцион азартнее и увлекательнее.<br>
                    </p>
                </li>
                <li>
                    <p>
                        Банковские карты и купоны с аукциона <br>
                        выгодно дарить и приятно получать! <br>
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="info-coins-block">
        <div class="info-coin-block">
            <span class="info-coin cube-coin"></span>

            <p>Азартно</p>
        </div>
        <div class="info-coin-block" style="margin-left: 38px;">
            <span class="info-coin bag-coin"></span>

            <p>Выгодно</p>
        </div>
        <div class="info-coin-block" style="margin-left: 45px; width: 25%;">
            <span class="info-coin time-coin"></span>

            <p>Мгновенно</p>
        </div>
    </div>
    <style>

    </style>
</div>

<div class="orange-line"></div>

<style>
    .slide {
        height: 100px;
    }
</style>
<?php if (count($slides)): ?>
    <?php $this->renderPartial('//site/partials/info', ['slides' => $slides]); ?>
<?php endif; ?>
<!--<div class="feedWrapper cf" id="f3">-->
<!--    <div class="feedBlock resize cf">-->
<!--        <div class="feed-header">-->
<!--            <div class="deviver left-part"></div>-->
<!--            <div class="text">Отзывы пользователей</div>-->
<!--            <div class="deviver right-part"></div>-->
<!--        </div>-->
<!--        <div style="clear: both;"></div>-->
<!--        <style>-->
<!--        </style>-->
<!--        <div class="sider_carousel"-->
<!--        ">-->
<!---->
<!---->
<!--        <div class="cycle-slideshow"-->
<!--             data-cycle-fx="scrollHorz"-->
<!--             data-cycle-timeout="60000"-->
<!--             data-cycle-next="#nextSlide"-->
<!--             data-cycle-prev="#prevSlide"-->
<!--             data-cycle-pager=".example-pager"-->
<!--             data-cycle-slides="div.slide">-->
<!---->
<!--            --><?php //foreach ($reviews as $review): ?>
<!--                <div class="slide">-->
<!--                    <div class="left fll">-->
<!--                        <div class="cf"></div>-->
<!--                        <div class="feedAuthor">-->
<!--                            <div class="feedName">--><?php //echo $review->author ?><!--  </div>-->
<!--                            <div class="feedProf">--><?php //echo $review->profession ?><!--</div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="center fll">-->
<!--                        <img src="--><?php //echo $review->avatar ?><!--" width="170" heigth="170" alt="">-->
<!--                    </div>-->
<!--                    <div class="right flr">-->
<!--                        <p>--><?php //echo $review->text ?><!--</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            --><?php //endforeach; ?>
<!--        </div>-->
<!--        <div class="cf"></div>-->
<!--        <div class="example-pager"></div>-->
<!--        <div class="arrowLeft">-->
<!--            <a class="arrowL" href="#" id="prevSlide"></a>-->
<!--        </div>-->
<!--        <div class="arrowRight">-->
<!--            <a class="arrowR" href="#" id="nextSlide"></a>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--</div>-->
<!--</div>-->
<!---->
<!--<div class="orange-line"></div>-->

<div class="presentWrapper cf">
    <div class="resultWrapper">
        <div class="result-text">
            <div class="result-text-title t-red">
                первый 100%
            </div>
            <div class="result-text-subtitle sbt-red">
                беспроигрышный аукцион
            </div>
            <div class="result-text-list">
                <ul>
                    <li>
                        <span class="coin k-coin"></span>
                        <span class="text-title">Универсальные подарочные купоны</span>

                        <p>
                            на эксклюзивную скидку предоставляемую предприятиями партнерами агентства.
                        </p>
                    </li>
                    <li>
                        <span class="coin ak-coin"></span>
                        <span class="text-title">Банковские предоплаченные карты</span>

                        <p>
                            для оплаты товаров и услуг в сети интернет.
                        </p>
                    </li>
                    <li>
                        <span class="coin sp-coin"></span>
                        <span class="text-title">Суперпризы 25 000 рублей.</span>

                        <p>
                            регулярно предоставляются наиболее активным участникам аукциона.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="result-table">
            <div class="fll">
                <div class="result-text-title t-orange">
                    результаты торгов
                </div>
                <div class="result-text-subtitle sbt-orange">
                    текущие сутки
                </div>
            </div>
            <div class="cup flr">

            </div>
            <div style="clear: both;"></div>
            <?php $this->renderPartial('wins_table', ['data'=>Yii::app()->wins->getWins()]);?>
        </div>
        <div style="clear: both;"></div>
        <div class="bottom-title">
            С нами выгодно всегда !!!
        </div>
    </div>
</div>
<div class="lastWrapper cf">
    <?php if (Yii::app()->user->isGuest): ?>
        <?php $this->renderPartial('//site/partials/register_form');?>
    <?php endif; ?>
    <div class="footerWrapper cf" id="f4">
        <div class="footer resize cf">
            <div class="fll left">
                © ООО “МПА ”ВИНКЛИК” <?= date('Y')?> год <br>
                Все права защищены
            </div>
            <div class></div>
            <div class="bottom-menu">
                <div class="bottom-list">
                    <?php $this->widget('ext.widgets.NavigationMenu', [
                        'items' => [
                            [
                                'class' => 'ext.widgets.HorizontalMenu',
                                'options' => [
                                    'position' => 'bottom',
                                    'htmlOptions' => [
                                        'class' => ''
                                    ]
                                ]
                            ]
                        ]
                    ]);?>
                </div>
            </div>
        </div>
    </div>
</div>

