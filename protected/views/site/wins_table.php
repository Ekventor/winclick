<div class="winners-table">
    <table>
        <?php if (!count($data)): ?>
            Нет данных
        <?php else: ?>
            <?php foreach ($data as $key => $win): ?>
                <tr>
                    <td><?= $win['login']; ?></td>
                    <td><?= $win['type']; ?></td>
                    <td><?= $win['count']; ?></td>
                </tr>
                <?php if($key < 5): ?>
                    <tr>
                        <td colspan="3">
                            <div class="stars"></div>
                        </td>
                    </tr>
                <?php else: ?>
                    <?php break; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
</div>

<script>
//    $(document).ready(function () {
//        game.last_win = <?//= isset($data[0]) ? $data[0]->id : 0;?>//;
//    });
</script>

