<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>
<?php if (Yii::app()->user->isGuest): ?>
    <div class="marketing-info-block">
        <div class="c-align">
            <div class="block-title-wrap">
                <h2 class="block-title">
                    АУКЦИОНЫ ПОДАРОЧНЫХ СЕРТИФИКАТОВ И КАРТ
                </h2>
<!--                <span class="title-label">для тех кто любит много покупать, но не любит много платить</span>-->
            </div>
            <div class="label-block clearfix label-info">
                <?= CHtml::image('/images/info/top.png'); ?>
            </div>
        </div>
    </div>
    <style>
        .label-info {
            width: 945px;
            max-height: 277px;
            padding: 0;
        }
    </style>
<?php endif; ?>
<?php if (Yii::app()->user->isGuest): ?>
    <div class="cards-panel-wrap">
        <div class="c-align">
            <ul class="cards-list clearfix">
                <li class="blue-card float-left">
                    <?= CHtml::image('/images/info/left.png'); ?>
<!--                    <div class="head-block cards">-->
<!--                        <h3 class="card-title">удобно дарить</h3>-->
<!--                    </div>-->
<!--                    <div class="content-block">-->
<!--                        <img src="/img/card-img-1.png"/>-->
<!--                    </div>-->
<!--                    <div class="footer-block clearfix">-->
<!--                        <span class="footer-text float-left">приятно получать</span>-->
<!--                        <span class="sprite like float-right"><!----</span>-->
<!--                    </div>-->
                </li>
                <li class="blue-card float-left">
                    <?= CHtml::image('/images/info/center.png'); ?>
                    <!--                    <div class="head-block cards">-->
<!--                        <h3 class="card-title">участие в аукционе:</h3>-->
<!--                    </div>-->
<!--                    <div class="content-block">-->
<!--                        <ul class="content-list">-->
<!--                            <li>-->
<!--                                <span class="marker"></span>АЗАРТНО - <span>для многих</span>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <span class="marker"></span>ВЫГОДНО - <span>всегда</span>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <span class="marker"></span>МГНОВЕННО - <span>для всех</span>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                    <div class="footer-block clearfix">-->
<!--                        <div class="card-star" style="padding: 0 52px;">-->
<!--                            <div class="star"></div>-->
<!--                            <div class="star"></div>-->
<!--                            <div class="star"></div>-->
<!--                            <div class="star"></div>-->
<!--                            <div class="star"></div>-->
<!--                        </div>-->
<!--                    </div>-->
                </li>
                <li class="blue-card float-right">
                    <?= CHtml::image('/images/info/right.png'); ?>
                    <!--                    <div class="head-block cards">-->
<!--                        <h3 class="card-title">надежно</h3>-->
<!--                    </div>-->
<!--                    <div class="content-block">-->
<!--                        <div class="bank-block">-->
<!--                            <div class="bank-logo">-->
<!--                            </div>-->
<!--                            <div class="bank-info">-->
<!--                                эмитент подарочных карт<br>-->
<!--                                один из самых стабильных<br>-->
<!--                                банков России!-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="footer-block clearfix">-->
<!--                        <span class="footer-text float-left">Банк Новый Символ</span>-->
<!--                        <span class="sprite like float-right"><!----</span>-->
<!--                    </div>-->
                </li>
            </ul>
        </div>
    </div>
<?php endif; ?>
<div class="auction-panel-wrap">
    <div class="c-align">
        <span class="auction-title">УКАЖИТЕ НОМИНАЛ ПОДАРОЧНОЙ КАРТЫ, КОТОРУЮ ВЫ &laquo;РЕШИЛИ ПОЛУЧИТЬ&raquo;</span>
        <ul class="rating-panel clearfix">
            <li class="title">НОМИНАЛ:</li>
            <?php foreach (Y::param('nominals') as $id => $nominal): ?>
                <li class="rating" data-value="<?= $nominal; ?>"><?= $nominal; ?></li>
            <?php endforeach; ?>
        </ul>
        <div class="clearfix">
            <div class="col-1">
                <div class="panel-1 auction-panel">
                    <div class="border-fix">
                        <div class="table-head">
                            <span class="title">РЕЗУЛЬТАТЫ ТОРГОВ</span>
                            <span class="subtitle">( победители и приобретенные ими карты )</span>
                        </div>
                        <?php $this->renderPartial('//site/wins_table', ['data' => Wins::model()->default()->findAll()]); ?>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <?php $this->renderPartial('//site/partials/auction'); ?>
            </div>
        </div>
    </div>
</div>

<?php if (Yii::app()->user->isGuest): ?>
    <div class="registration-panel-wrap">
        <div class="c-align clearfix">
            <div class="col-1 float-left">
                <div class="review-panel-wrap">
                    <span class="block-title">Отзывы пользователей:</span>

                    <div class="review-slider-wrap">
                        <span class="left-arrow"><!----></span>
                        <span class="right-arrow"><!----></span>

                        <div class="slider clearfix" style="left: 0 !important;">
                            <?php foreach ($reviews as $review): ?>
                                <div class="slide" style="magrin: 0;">
                                    <div class="review-panel clearfix" style="height: 110px; width: 365px;">
                                        <div class="ava-block float-left">
                                            <?php echo CHtml::image($review->avatar); ?>
                                        </div>
                                        <div class="text-panel float-left">
                                            <span class="title"><?php echo $review->author; ?></span>

                                            <p>
                                                <?php echo $review->text; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                </div>

                <div class="start-registration clearfix">
                    <p class="block-title">
                        <?php echo $slogan->value; ?>
                    </p>
                </div>
            </div>

            <div class="col-2 float-right">
                <?php $this->renderPartial('partials/register_form'); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
