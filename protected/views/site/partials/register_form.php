<!--<a name="register-form"></a>-->
<!--<div class="registration-form-wrap">-->
<!--    <div class="clearfix">-->
<!--        <span class="block-title"><span class="text">РЕГИСТРАЦИЯ</span></span>-->
<!--    </div>-->
<!--    <form id="register-form">-->
<!--        <dl>-->
<!--            <dt>Введите ваш логин:</dt>-->
<!--            <dd class="login placeholder">-->
<!--                <input class="fill-field login" type="text" name="RegisterForm[login]" id="login">-->
<!--                <span class="error-message">Не корректные данные или данный логин уже зарегистрирован.</span>-->
<!--            </dd>-->
<!--        </dl>-->
<!--        <dl>-->
<!--            <dt>-->
<!--                Введите ваш e-mail:-->
<!--            </dt>-->
<!--            <dd class="email placeholder">-->
<!--                <input class="fill-field mail" type="text" name="RegisterForm[email]" id="email">-->
<!--                <span class="error-message">Не корректные данные или данная почта уже зарегистрирована.</span>-->
<!--            </dd>-->
<!--        </dl>-->
<!--        <div class="clearfix user-license license placeholder">-->
<!--            <span class="white">Я в полном объеме ознакомлен и безоговорочно признаю все положения</span>-->
<!--                <a href="/terms_of_use" target="_blank" class="orange-link">пользовательского соглашения</a><span class="check-wrap"><input type="checkbox" name="RegisterForm[license]" id="license"/>-->
<!--            </span>-->
<!--            <span class="error-message">Необходимо подтвердить прочтение пользовательского соглашения</span>-->
<!--        </div>-->
<!--        <input type="hidden" name="ajax" value="register-form" />-->
<!---->
<!--        <input type="submit" class="blue-button" value="ЗАРЕГИСТРИРОВАТЬ" onclick="yaCounter27707655.reachGoal('reg_click'); return true;"/>-->
<!--    </form>-->
<!--</div>-->

<div class="lastBlock resize cf">
    <div class="registration-block">
        <div class="registration-head">Регистрация</div>
        <div class="registration-info">
            <?= CHtml::image(Y::param('infoPath').'/reg.png', '@', ['style'=>'width: 397px; height: 323px;']) ?>
        </div>
        <a name="register-form"></a>
        <div class="registration-form">
            <form id="register-form">
                <div class="login-input login placeholder">
                    <input type="text" name="RegisterForm[login]" placeholder="Введите ваш логин">
                    <div class="input-icon key-icon"></div>
                    <span class="error-message registration-error">Не корректные данные или данный логин уже зарегистрирован.</span>
                </div>
                <div class="email-input email placeholder">
                    <input type="text" name="RegisterForm[email]" placeholder="Введите ваш Email">
                    <div class="input-icon imp-icon"></div>
                    <span class="error-message registration-error">Не корректные данные или данная почта уже зарегистрирована.</span>
                </div>
                <input type="hidden" value="register-form" name="ajax">

                <div class="registration-text license placeholder">
                    В полном объеме ознакомлен и безоговорочно признаю все положения
                    <a href="/terms_of_use" target="_blank">пользовательского соглашения</a> <input type="checkbox"
                                                                                                    name="RegisterForm[license]">
                    <span class="error-message registration-error" style="top: 78px;">Необходимо подтвердить прочтение пользовательского соглашения</span>
                </div>
                <button type="submit" onclick="yaCounter27707655.reachGoal('reg_click'); return true;">Зарегистрировать</button>
            </form>
        </div>
    </div>
</div>


