<style>
    .auction-panel-wrap .auction-panel.panel-1 {
        height: 464px;
        border: none;
    }
</style>
<div class="auction-result">
    <div class="win-results">
        <div class="result-header">
                                <span>
                                    Результат участия в аукционе
                                </span>
        </div>
        <div class="result-nominal">
            подарочной карты номиналом <span class="nominal" id="game-nominal">#</span> рублей
        </div>
        <div class="money-results">
            <table>
                <tr>
                    <td>
                        <div class="cur-win">
                            <div class="cur-win-summ-block">
                                <span class="sum-text top">кол-во</span>
                                <span class="sum" id="cumulate-card-sum">0</span>
                                <span class="sum-text">карты</span>
                            </div>
                            <div class="cur-win-block">
                                <div class="win-bg"></div>
                                <span class="cur-win-info top">- - - накопительная карта - - -</span>
                                <span class="sum" id="auction-discount">0</span>
                                <span class="white-rubl"></span>
                                <span class="cur-win-info bottom">- - - накопительная карта - - -</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="cur-label">
                            <div class="win-arrow">
                                <img src="/img/main/win-arrow-red.png">
                                <img src="/img/main/win-arrow-blue.png">
                                <div class="win-message">
                                    <span id="win-plus">+</span> <span id="win">0</span>
                                </div>
                            </div>
                            <span class="label" style="opacity: 1;">Ваш выйгрыш <br/>
                            накопительная карта</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="sum-win">
                            <div class="sum-win-summ-block">
                                <span class="sum-text top">кол-во</span>
                                <span class="sum" id="universal-card-sum">0</span>
                                <span class="sum-text">карты</span>
                            </div>
                            <div class="sum-win-block">
                                <div class="win-bg"></div>
                                <span class="sum-win-info top">универсальная подарочная карта</span>
                                <span class="sum" id="sum-discount">0</span>
                                <span class="white-rubl"></span>
                                <span class="sum-win-info bottom">универсальная подарочная карта</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="sum-label">
                            Универсальные <br/>
                            подарочные карты<br/>
                            номиналом <span id="info-nominal">#</span> руб.<br/>

                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="user-account-info">
        <table>
            <tr>
                <td>
                    <div class="money-block">
                        <span>мои деньги</span>

                        <div class="money-sum" id="RUB_balance">
                            <?= round(Yii::app()->account->getBalanceByCurrency(Y::user(), 'RUB'), 2); ?>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="wk-block">
                        <span>мои винклики</span>

                        <div class="wk-sum" id="WK_balance">
                            <?= round(Yii::app()->account->getBalanceByCurrency(Y::user(), 'WK'), 2);?>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="pay-block">
                        <span>за подачу заявки</span>

                        <div class="pay-sum" id="cost-game">
                            #
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="express-buy">
            <span class="info-label">
                экспресс приобретение винкликов
            </span>
        <div class="info-buy-block">
                <span class="coins-img"></span>
        </div>
        <div class="winkliks-count-block">
            <span class="winkliks-count" id="winklicks-count">#</span>
<!--            <span class="winkliks-name"> wk</span>-->
        </div>
        <div class="action-buy">
            <input type="button" id="buy-wk-express" class="golden-button" value="Купить">
        </div>
        <div style="clear: both;"></div>
        <div class="bottom-line"></div>
    </div>
</div>
