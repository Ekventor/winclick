<script>
    $(document).ready(function(){
        game.setUrls('<?= $this->createAbsoluteUrl('/');?>', '<?= Y::param('gameServiceUrl');?>');
        <?php if (Y::isGuest()):?>
            game.setDemo();
        <?php endif;?>
        <?php if (Y::isLogged() && Y::user()->auto_convert):?>
            game.setAutoMoney();
        <?php endif;?>

        game.setWins(
            {   <?php foreach(Y::param('nominals') as $nominal):?>
                    <?php $i=4; ?>
                    <?php foreach(str_split(Y::getSetting('possible_letters')) as $letter):?>
                        <?php $i++; ?>
                        '<?=$nominal.$letter;?>': <?= $i;?>,
                    <?php endforeach;?>
                '<?= $nominal;?>!': <?=$i;?>,
                <?php endforeach;?>
            }).setTimings({
                win: <?= Y::getSetting('win_notice_time') * 1000;?>,
                spin: <?= Y::getSetting('image_round_speed');?>,
            });


//        game.setWins(
//        { '<?//=max(Y::param('nominals')); $i=2;?>//!': 1,
//                                    <?php //foreach(Y::param('nominals') as $nominal):?>
<!--                                            --><?php //if ($nominal != max(Y::param('nominals'))):?>
//                                                '<?//= $nominal;?>//!': <?//=$i++;?>//,
//                                            <?php //endif;?>
<!--                                            --><?php //foreach(str_split(Y::getSetting('possible_letters')) as $letter):?>
//                                                    '<?//=$nominal.$letter;?>//': <?//= $i++;?>//,
//                                            <?php //endforeach;?>
<!--                                    --><?php //endforeach;?>
//        }).setTimings({
//            win: <?//= Y::getSetting('win_notice_time') * 1000;?>//,
//            spin: <?//= Y::getSetting('image_round_speed');?>//,
//        });
        
        var i = 0;
        var time = parseInt('<?= Y::getSetting("image_round_speed"); ?>');
        var timeFirst = Math.floor(2 * time / 3);
        var timeSecond = Math.floor(time / 3);
        var timeThird = Math.floor(time);
        var arr = [timeFirst, timeSecond, timeThird];
        $('.slot1').each(function() {
            time = arr[i];
            i++;
            if (i == 3) {
                $(this).jSlots({ 'number': 1,
                    'onEnd': game.spinEnd,
                    'easing' : 'easeOutQuad',
                    'time' : time,         // Number: total time of spin animation
                    'loops' : <?= Y::getSetting('image_round_time');?>,
                });

                return;
            }
            $(this).jSlots({ 'number': 1,
                'easing' : 'easeOutQuad',
                'time' : time,         // Number: total time of spin animation
                'loops' : <?= Y::getSetting('image_round_time');?>,
            });
        });
        $('.slot').prepend('<li class="default"><img src="/images/certificates/default.jpg" alt="default game"/></li>').css('position', 'relative').css('top', '0px');
    });

    <?php

        $result = [];
        $posibleLetters = str_split(Y::getSetting('possible_letters'));
        $posibleLetters[] = '!';
        $resultString = '';
        foreach($posibleLetters as $letter) {
            $percent = Y::getSetting('result_percent;'.$letter);
            $string = "'" . $letter . "': {";
            foreach(Y::app()->params['nominals'] as $nominal) {
                if ($percent) {
                    $discount = round($nominal * $percent / 100);
                    $result[$letter][$nominal] = Y::getSetting('result_state;'.$letter) . ' ' . $discount . ' рублей.';
                } else {
                    $result[$letter][$nominal] = Y::getSetting('result_state;'.$letter);
                }
                $word = "'" . $nominal . "' : '" . $result[$letter][$nominal] . "',";
                $string .= $word;
            }
            $string .= "},";
            $resultString .= $string;
        }
    ?>

    var messages = {
            '!': '<?= Y::getSetting('result_state;!');?> <?= Y::getSetting('result_percent;!');?>%.',
            'default': '<?= Y::getSetting('state_default');?>',
            'not_enough_money': '<?= Y::getSetting('state_not_enough_money');?>',
            'slogan': '<?= Y::getSetting('default');?>',
            'repeat': '<?= Y::getSetting('repeat');?>',
            'win': '<?= Y::getSetting('win');?>',
            <?= $resultString;?>
//            '!': '<?//= Y::getSetting('result_state;!');?>// <?//= Y::getSetting('result_percent;!');?>//%.',
            'select_nominal': 'Напоминаем, что перед участием в аукционе вам необходимо указать номинал подарочной карты',
    };

    var winAmount = parseInt('<?= Y::getSetting('win_amount'); ?>');

    var costs = {
        <?php foreach(Y::param('nominals') as $nominal):?>
        <?=$nominal;?>: '<?= Y::getSetting('state_play');?> <?= Yii::app()->game->getCost($nominal);?>wk',
        <?php endforeach;?>
        'demo': '<?= Y::getSetting('demo_play');?>',
    }

    var game_cost = {
        <?php foreach(Y::param('nominals') as $nominal):?>
            <?=$nominal;?>: '<?= Yii::app()->game->getCost($nominal);?>wk',
        <?php endforeach;?>
    }

    <?php
        $posibleLetters = str_split(Y::getSetting('possible_letters'));
        $posibleLetters[] = '!';
        $resultString = '';
        foreach($posibleLetters as $letter) {
            $percent = Y::getSetting('result_percent;'.$letter);
            if ($percent) {
                $string = "'" . $letter . "': {";
                foreach(Y::app()->params['nominals'] as $nominal) {
                    $discount = round($nominal * $percent / 100);
                    $result[$letter][$nominal] = $discount;
                    $word = "'" . $nominal . "' : '" . $result[$letter][$nominal] . "',";
                    $string .= $word;
                }
                $string .= "},";
                $resultString .= $string;
            }
        }
    ?>

    var discounts = {
        <?= $resultString; ?>
    };

    var User = {
        <?php if (Y::isLogged()):?>
        'game_token': '<?=Yii::app()->authService->getGameToken(Y::userId())['data'];?>',
        <?php else: ?>
        'game_token': 'demo',
        <?php endif;?>
    }
    
    
</script>