<div class="feedWrapper cf" id="f3">
    <div class="feedBlock resize cf">
        <div class="feed-header">
            <div class="deviver left-part"></div>
            <div class="text">Зарегистрируйся &nbsp; и &nbsp; получай</div>
            <div class="deviver right-part"></div>
        </div>
        <div style="clear: both;"></div>

        <div class="content-wrap" style="width: 930px; height: 294px;">
            <ul id="info-slider">
                <?php foreach ($slides as $slide): ?>
                    <li><?php echo $slide->text; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="orange-line"></div>

<script>
    $(document).ready(function () {
        $('#info-slider').anythingSlider({
            expand: true,
            autoPlay: false,
            buildNavigation: false,
            buildStartStop: false,
            buildArrows: false
        });
    });
</script>
