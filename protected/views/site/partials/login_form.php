<?php //is this needed really?
    if (Y::isGuest()):?>
    <div class="registration-popup">
            <div class="head-block">
              <p>АВТОРИЗАЦИЯ</p>
              <span class="close-button" onclick="javascript: smartOverlay.hide();">
                <!-- -->
              </span>
            </div>
            <div class="cont-block">
                
              <form id="login-form">
                <div class="placeholder">
                    <span class="error-message">Логин и пароль не совпадают</span>
                    <input class="fill-field login" type="text" name="LoginForm[login]" placeholder="Введите ваш логин">
                    <div class="input-icon key-icon"></div>
                  <input class="fill-field password" type="password" name="LoginForm[password]" placeholder="Введите ваш пароль">
                </div>
                <div class="clearfix">
                  <a class="float-right" href="#" id="password-recover-link">Забыли пароль?</a>
                </div>
                <div class="button-wrap">
                    <input type="hidden" name="ajax" value="login-form" />
<!--                    <input class="yellow-button" type="submit" name="submit" value="Войти">-->
                    <button type="submit" name="submit" style="margin: 0 84px 20px;">Войти</button>
                </div>
              </form>
              
              <form id="recover-form" style="display: none">
                <div class="padding-fix placeholder" >
                    <span class="error-message posit-fix">Такой почты не существует</span>
                    <input class="fill-field" type="text" name="RecoverForm[email]" placeholder="Введите Ваш e-mail">
                </div>
                <div class="clearfix">
                    <a class="float-right"  href="#" id="cancel-recover-link">Назад</a>
                </div>
                <div class="button-wrap">
                    <input type="hidden" name="ajax" value="recover-form" />
                    <button type="submit" name="submit">Отправить</button>
<!--                    <input class="yellow-button" type="submit" name="submit" value="Отправить">-->
                </div>
              </form>
            </div>
    </div>
<?php endif;?>