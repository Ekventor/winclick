<div class="panel-3 auction-panel">
        <div class="border-fix">
                <div class="head-block clearfix">
                        <?php if (Y::isLogged()):?>
                        <span class="title float-left">АУКЦИОН №<span id="game_id"> </span></span>
                        <?php else:?>
                        <span class="title float-left">ДЕМО-АУКЦИОН</span>
                        <?php endif;?>
                        <input type="text" class="float-right number" value="-" readonly id="game_remains">
                        <span class="subtitle float-right">ДО ФИНИША:</span>
                </div>
                <div class="content-block clearfix">
                        <div class="status-panel-wrap">
                            <div class="status-panel" id="game_result">
                                <span class="info-border"></span>
                                <span class="text"><?= Y::getSetting('state_default');?></span>
                                <span class="info-border right-border"></span>
                            </div>
                        </div>
                        <div class="lot-panel-wrap solo">
                                <ul class="slot">
                                    <li><img src="/images/certificates/<?=max(Y::param('nominals'));?>!.jpg" alt="<?=max(Y::param('nominals'));?>_!"/></li>
                                        <?php foreach(Y::param('nominals') as $nominal):?>
                                                <?php foreach(str_split(Y::getSetting('possible_letters')) as $letter):?>
                                                        <li><?= CHtml::image('/images/certificates/'.$nominal.$letter.'.jpg', $nominal.'_'.$letter);?></li>
                                                <?php endforeach;?>
                                                <?php if ($nominal != max(Y::param('nominals'))):?>
                                                        <li><?= CHtml::image('/images/certificates/'.$nominal.'!.jpg', $nominal.'_!');?></li>
                                                <?php endif;?>
                                                
                                        <?php endforeach;?>
                                        
                                </ul>
                        </div>
                        <div class="over-auction">
                                <img src="/img/main/card-border.png" />
                        </div>
                        <div class="button-panel-wrap clearfix">
                                <span class="float-left slogan">УЧАСТВУЙ И ПОБЕЖДАЙ!</span>
                                <button class="red-grad-button red-grad-button-golden float-right" id="play">СТАРТ</button>
                        </div>
                </div>
                <div class="bottom-line">
                    <?php if (!Y::isLogged()): ?>
                        ЗАРЕГИСТРИРУЙТЕСЬ И ПОЛУЧАЙТЕ РЕАЛЬНУЮ ВЫГОДУ!
                    <?php endif; ?>
                    <!-- -->
                </div>
                <div class="preload">
                    <?php foreach(Y::param('nominals') as $nominal): ?>
                        <li class="default" style="display: none;">
                            <?= CHtml::image('/images/certificates/'.$nominal.'default.jpg', 'default '.$nominal);?>
                        </li>
                    <?php endforeach; ?>
                </div>
        </div>
</div>
