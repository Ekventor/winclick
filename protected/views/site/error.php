<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Error';
$this->breadcrumbs = array(
    'Error',
);
?>

<div class="error-content">
    <div class="c-align">
        <div class="error-title">
            <h2>Error <?php echo $code; ?></h2>
        </div>

        <div class="clearfix">
                <div class="error-text"><?php echo CHtml::encode($message); ?></div>
        </div>
    </div>
</div>
