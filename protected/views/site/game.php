<div class="auction-panel-wrap no-border">
  <div class="c-align">
    <span class="auction-title">УКАЖИТЕ  НОМИНАЛ  ПОДАРОЧНОЙ КАРТЫ,  КОТОРУЮ  ВЫ  РЕШИЛИ  ПОЛУЧИТЬ</span>
    <ul class="rating-panel clearfix">
      <li class="title">НОМИНАЛ:</li>
      <?php foreach(Y::param('nominals') as $id => $nominal):?>
        <li class="rating" data-value="<?= $nominal;?>"><?= $nominal;?></li>
      <?php endforeach;?>
    </ul>
    <div class="clearfix">
      <div class="col-1">
        <div class="panel-1 auction-panel">
            <?php $this->renderPartial('//site/partials/auction_result');?>
        </div>
      </div>
      <div class="col-2">
        <?php $this->renderPartial('//site/partials/auction');?>
      </div>
    </div>
  </div>
</div>