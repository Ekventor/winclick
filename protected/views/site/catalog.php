<style>
    .typical-page {
        padding-bottom: 0px;
    }

    .catalog-page .sert-block {
        float: left;
        width: 233px;
        text-align: center;
        height: 330px;
        padding-top: 20px;
        position: relative;
    }

    .catalog-page .sert-block .sert-info {
        text-align: justify;
        width: 185px;
        margin: 0 auto;
        font-size: 12px;
    }

    .catalog-page .sert-block .sert-more-info-block {
        display: none;
    }
    .catalog-page .sert-block .sert-additional-info-block {
        display: none;
    }

    .catalog-page .sert-block .sert-info b {
        font-size: 14px;
    }
    .catalog-page .catalog-serts-line:first-child .sert-block {
        padding-top: 0;
    }

    .catalog-page .sert-block .sert-image img {
        height: 115px;
        width: 185px;
        border: 0px solid;
        border-radius: 10px;
        box-shadow: 1px 1px 1px black;
    }

    .catalog-page .sert-block .sert-image {
        margin-bottom: 10px;
    }
    .catalog-page .catalog-serts-line {
        height: 330px;
    }
    .catalog-page .catalog-serts-line:first-child .sert-block:nth-child(n) {
        border-right: 1px solid #CECCCD;
        border-bottom: 1px solid #CECCCD;
    }

    .catalog-page .catalog-serts-line:first-child .sert-block:first-child {
        border-right: 1px solid #CECCCD;
        border-bottom: 1px solid #CECCCD;
    }

    .catalog-page .catalog-serts-line:first-child .sert-block:last-child {
        border-bottom: 1px solid #CECCCD;
        border-right: none;
    }

    /***/
    .catalog-page .catalog-serts-line .sert-block:nth-child(n) {
        border-right: 1px solid #CECCCD;
        border-bottom: 1px solid #CECCCD;
    }

    .catalog-page .catalog-serts-line .sert-block:first-child {
        border-right: 1px solid #CECCCD;
        border-bottom: 1px solid #CECCCD;
    }

    .catalog-page .catalog-serts-line .sert-block:last-child {
        border-bottom: 1px solid #CECCCD;
        border-right: none;
    }

    /***/
    .catalog-page .catalog-serts-line:last-child .sert-block:nth-child(n) {
        border-right: 1px solid #CECCCD;
        border-bottom: none;
    }

    .catalog-page .catalog-serts-line:last-child .sert-block:first-child {
        border-right: 1px solid #CECCCD;
        border-bottom: none;
    }

    .catalog-page .catalog-serts-line:last-child .sert-block:last-child {
        border-bottom: none;
        border-right: none;
    }

    /***/

    .catalog-page .catalog-serts-line:last-child {
        height: 380px;
    }
    .catalog-page .sert-block .sert-more-info-button button {
        margin: 0 auto;
    }

    .catalog-page .sert-block .sert-more-info-button {
        position: absolute;
        width: 100%;
        margin: 0 auto;
        bottom: 25px;
    }

    .red-button-info {
        background-color: #c61c05;
        border: medium none;
        border-radius: 5px;
        color: #ffffff;
        font-size: 15px;
        height: 25px;
        text-transform: uppercase;
        width: 120px;
        cursor: pointer;
    }

    .result-popup.sertificates {
        left: 24%;
        width: 500px;
        z-index: 1000;
    }

    .result-popup.sertificates .cont-block {
        border-radius: 0px 0px 5px 5px ;
    }

    .result-popup.sertificates .head-block p{
        font-size: 20px;
        font-weight: bold;
    }

    .result-popup.sertificates .button-wrap {
        text-align: left;
        color: #ffffff;
    }

    .sert-more-info-block img {
        /*height: 230px;*/
        /*width: 460px;*/
    }
    .sert-more-info-block .sert-orig-image {
        margin-bottom: 20px;
    }

    .sert-more-info-block .sert-more-info {
        font-weight: 400;
        font-size: 14px;
        width: 460px;
        text-align: justify;
        margin: 0 auto;
    }
    .sert-more-info-block .sert-more-info b {
        font-size: 15px;
    }
</style>
<div class="catalog-page">
    <div class="catalog-block">
            <?php foreach ($sertificates as $key => $sertificate): ?>
                <?php if(!($key % 4)): ?>
                    <div class="catalog-serts-line">
                <?php endif; ?>
                <div class="sert-block">
                    <div class="sert-image">
                        <?= CHtml::image($sertificate->small_img); ?>
                    </div>
                    <div class="sert-info">
                        <b>
                            <?= $sertificate->title; ?>
                        </b>

                        <p>
                            <?= $sertificate->short_desc; ?>
                        </p>
                    </div>
                    <div class="sert-more-info-button">
                        <button class="red-button-info" type="button">Подробнее</button>
                    </div>
                    <div class="sert-more-info-block">
                        <div class="sert-orig-image">
                            <?= CHtml::image($sertificate->img); ?>
                        </div>
                        <div class="sert-more-info">
                            <b><?= $sertificate->title; ?></b><br>
                            <?= $sertificate->desc; ?>
                        </div>
                    </div>
                    <div class="sert-additional-info-block">
                        <div class="sert-title">
                            <?= $sertificate->title; ?>
                        </div>
                        <div class="sert-additional-info">
                            <?= $sertificate->addit_info; ?>
                        </div>
                    </div>
                </div>
                <?php if(!(($key+1) % 4) || count($sertificates) == $key+1): ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
    </div>

</div>


<script>
    $(document).ready(function() {
        smartOverlay.init('.result-popup.sertificates', '.gray-popup-bg');
        $('.sert-more-info-button button').on('click', function() {
            $moreInfo = $(this).parents('.sert-block').find('.sert-more-info-block').clone();
            $title = $(this).parents('.sert-block').find('.sert-additional-info-block .sert-title').clone();
            $additInfo = $(this).parents('.sert-block').find('.sert-additional-info-block .sert-additional-info').clone();
            $popup = $('.result-popup.sertificates');
            $popup.find('.message').html($moreInfo.show());
            $popup.find('.button-wrap').html($additInfo);
            $popup.find('.head-block p').html($title);
            smartOverlay.setPopup('.result-popup.sertificates').show();
        });
    });
</script>