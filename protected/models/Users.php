<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $phone
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $created
 * @property string $modified
 * @property string $salt
 * @property string $token
 * @property boolean $activated
 *
 * The followings are the available model relations:
 * @property Accounts[] $accounts
 */
class Users extends CActiveRecord
{
    const ROLE_ADMIN = 'admin';
    const ROLE_MODER = 'shop';
    const ROLE_USER = 'user';

	public $newPassword = null;
	public $rePassword = null;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phone, salt', 'length', 'max'=>255),
			array('login, email', 'length',  'min'=> 3, 'max'=>50, 'allowEmpty'=>false),
			array('newPassword, rePassword', 'length',  'min'=> 3, 'max'=>50, 'allowEmpty'=>false, 'on'=>'update'),
            array('email', 'email', 'on' => 'update'),
			array('created, modified, activated, token, password, auto_convert, role', 'safe'),
			array('email', 'unique', 'message'=> 'Выбранный E-mail занят кем-то другим'),
            array(
                'phone',
                'ext.validators.PcSimplePhoneValidator',
                'on'=>'update',
                'message' => 'Неправильный номер телефона',
                'minNumDigits' => 4,
                'maxNumDigits' => 12,
                'emptyMessage' => 'Введите номер телефона'
            ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, phone, login, password, email, created, modified, role', 'safe', 'on'=>'search'),
			array('newPassword', 'compare', 'compareAttribute'=>'rePassword', 'strict'=>true),
            array('login, email', 'length',  'min'=> 3, 'max'=>50, 'allowEmpty'=>false, 'on' => 'adminAdd'),
            array('email', 'email', 'on' => 'adminAdd'),
            array(
                'phone',
                'ext.validators.PcSimplePhoneValidator',
                'on'=>'adminAdd',
                'message' => 'Неправильный номер телефона',
                'minNumDigits' => 4,
                'maxNumDigits' => 12,
                'emptyMessage' => 'Введите номер телефона'
            ),

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accounts' => array(self::HAS_MANY, 'Accounts', 'user_id'),
			'mails' => array(self::HAS_MANY, 'Mailings', 'user_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'users.id'),
			'phone' => Yii::t('app', 'users.phone'),
			'login' => Yii::t('app', 'users.login'),
			'password' => Yii::t('app', 'users.password'),
			'email' => Yii::t('app', 'users.email'),
			'created' => Yii::t('app', 'users.created'),
			'modified' => Yii::t('app', 'users.modified'),
			'salt' => Yii::t('app', 'users.salt'),
			'token' => Yii::t('app', 'users.token'),
			'activated' => yii::t('app', 'users.activated'),
            'newPassword' => Yii::t('app', 'Новый пароль'),
            'role' => 'Роль'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('activated',$this->activated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function autocomplete($term)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->addSearchCondition('login', $term, true, 'OR', 'ILIKE');
		$criteria->addSearchCondition('phone', $term, true, 'OR', 'ILIKE');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public function beforeSave() {
		parent::beforeSave();
		if ($this->isNewRecord && !$this->password) { //generate token and send to user
			$this->token = $this->generateToken();
			$this->created = Y::date();
			$this->generateSalt(6);
		}
		$this->modified = Y::date();
		return true;
	}

	public function beforeDelete()
	{
		parent::beforeDelete();
		Mailings::model()->deleteAll("user_id = {$this->id}");
		return true;
	}
	
	
	public function generateToken($length = 50) {
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
	    
		for ($i = 0; $i < $length; $i++) {
		    $key .= $keys[array_rand($keys)];
		}
	    
		return $key;
	}
	
	public function generateSalt($length = 4) {
		$salt = '';
		$keys = ['/','!','@','#','$','%','^','&','*','(',')','\\', '/', ':', ';', '{', '}', ']', '[','?', '>', '<', '.',','];
	    
		for ($i = 0; $i < $length; $i++) {
		    $salt .= $keys[array_rand($keys)];
		}
	    
		$this->salt = $salt;
		return $this->salt;
	}
	
	public function createPassword($str, $email = null) {
        $userEmail = $email !== null ? $email : $this->email;
        return md5($str . md5($this->salt . $userEmail));
	}

    public function getRoles()
    {
        $authManager = new PhpAuthManager();
        $authManager->init();
        $roles = $authManager->roles;
        $rolesArray = [];
        foreach ($roles as $role) {
            $rolesArray[$role->name] = $role->description;
        }
        return $rolesArray;
    }
}
