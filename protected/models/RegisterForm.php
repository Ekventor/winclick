<?php

/**
 * RegisterForm class.
 * RegisterForm is the data structure for keeping
 * user login form data. It is used by the 'register' action of 'SiteController'.
 */
class RegisterForm extends CFormModel
{
	public $login;
	public $email;
        public $license;
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// login and email are required
			array('login, email, license', 'required'),
                        array('login', 'unique','attributeName'=>'login', 'className'=>'Users'),
                        array('email', 'unique','attributeName'=>'email', 'className'=>'Users'),
			// email must be valid email
			array('email', 'email'),
                        //max login length 
                        array('login, email', 'length', 'max'=>50, 'min'=> 3, 'allowEmpty'=>false),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'login' => Yii::t('app', 'Login'),
                        'email' => Yii::t('app', 'Email'),
                        'license' => Yii::t('app', 'License'),
		);
	}
}
