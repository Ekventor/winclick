<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class Number extends CFormModel
{
    public $amount;

    /**
     * Declares the validation rules.
     * The rules state that login and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // login and password are required
            array('amount', 'required', 'message' => 'Некорректные данные'),
            array('amount', 'numerical',
                'min' => 1,
                'integerOnly' => true,
                'message' => 'Некорректные данные',
                'tooSmall' => 'Некорректные данные'
            )
            // rememberMe needs to be a boolean
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'amount'=>'Amount',
        );
    }

}
