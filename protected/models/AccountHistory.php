<?php

/**
 * This is the model class for table "account_history".
 *
 * The followings are the available columns in table 'account_history':
 * @property integer $id
 * @property integer $account_id
 * @property double $open_balance
 * @property double $close_balance
 * @property double $amount
 * @property string $date
 * @property integer $user_id
 * @property string $type
 * @property string $comment
 */
class AccountHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'account_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, user_id', 'numerical', 'integerOnly'=>true),
			array('open_balance, close_balance, amount', 'numerical'),
			array('date, type, comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, account_id, open_balance, close_balance, amount, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'AccountHistory.ID'),
			'account_id' => Yii::t('app', 'AccountHistory.Account'),
			'open_balance' => Yii::t('app', 'AccountHistory.Open Balance'),
			'close_balance' => Yii::t('app', 'AccountHistory.Close Balance'),
			'amount' => Yii::t('app', 'AccountHistory.Amount'),
			'date' => Yii::t('app', 'AccountHistory.Date'),
			'user_id' => Yii::t('app', 'AccountHistory.User id'),
			'type' => Yii::t('app','AccountHistory.Type'),
			'comment' => Yii::t('app', 'AccountHistory.Comment'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('open_balance',$this->open_balance);
		$criteria->compare('close_balance',$this->close_balance);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccountHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
