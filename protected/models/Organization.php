<?php

/**
 * This is the model class for table "organization".
 *
 * The followings are the available columns in table 'organization':
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property integer $city_id
 * @property string $specialization
 * @property string $contacts
 * @property string $desc
 * @property string $img
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property City $city
 */
class Organization extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'organization';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, city_id', 'numerical', 'integerOnly'=>true),
			array('name, img', 'length', 'max'=>255),
			array('specialization, contacts, desc', 'safe'),
            array('name, specialization, contacts, desc', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, category_id, city_id, specialization, contacts, desc, img', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Y::t('app', 'organization.id'),
			'name' => Y::t('app', 'organization.name'),
			'category_id' => Y::t('app', 'organization.category_id'),
			'city_id' => Y::t('app', 'organization.city_id'),
			'specialization' => Y::t('app', 'organization.specialization'),
			'contacts' => Y::t('app', 'organization.contacts'),
			'desc' => Y::t('app', 'organization.desc'),
			'img' => Y::t('app', 'organization.img'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('specialization',$this->specialization,true);
		$criteria->compare('contacts',$this->contacts,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('img',$this->img,true);

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Organization the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
