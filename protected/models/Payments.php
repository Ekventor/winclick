<?php

/**
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property integer $id
 * @property double $amount
 * @property string $datetime
 * @property integer $status
 * @property integer $account_id
 * @property integer $user_id
 * @property integer $wmi_order_id
 * @property integer $wmi_currency_id
 */
class Payments extends CActiveRecord
{
	const STATUS_NEW = 0;
	const STATUS_SUCCESS = 10;
	const STATUS_RETRY = 20;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, account_id, user_id, wmi_order_id, wmi_currency_id', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('status', 'default', 'value'=> self::STATUS_NEW),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, amount, datetime, status, account_id, user_id, wmi_order_id, wmi_currency_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'amount' => 'Amount',
			'datetime' => 'Datetime',
			'status' => 'Status',
			'account_id' => 'Account',
			'user_id' => 'User',
			'wmi_order_id' => 'Wmi Order',
			'wmi_currency_id' => 'Wmi Currenct',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('wmi_order_id',$this->wmi_order_id);
		$criteria->compare('wmi_currency_id',$this->wmi_currency_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
