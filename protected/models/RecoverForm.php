<?php

/**
 * RecoverForm class.
 * RecoverForm is the data structure for keeping
 * user login form data. It is used by the 'recover' action of 'UserController'.
 */
class RecoverForm extends CFormModel
{
	
	public $email;
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email must be valid email
			array('email', 'email'),
			array('email', 'exist', 'attributeName'=>'email', 'className'=>'Users', 'message'=>'Email not created'),
                        //max login length 
                        array('email', 'length', 'max'=>50, 'min'=> 3),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
                        'email' => Yii::t('app', 'Email'),
		);
	}
	
	public function recover() {
		if ($user = Users::model()->findByAttributes(['email'=>$this->email])) {
			$user->token = $user->generateToken();
			//$user->activated = 0; //no way!~
			if ($user->update(['token'])) {
				Yii::app()->notifier->notify($user->email, Yii::t('app', 'Восстановление доступа к профилю'), Yii::t('app', 'RECOVER_EMAIL', ['{token}'=>$user->token, '{link}'=>Yii::app()->getController()->createAbsoluteUrl('/user/activate/token/'.$user->token)]), 'email');
				return true;
			}
		}
		return false;
	}
}
