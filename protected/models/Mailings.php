<?php

/**
 * This is the model class for table "mailings".
 *
 * The followings are the available columns in table 'mailings':
 * @property integer $id
 * @property string $title
 * @property string $message
 * @property string $date
 * @property integer $creator
 * @property integer $user_id
 * @property integer $readed
 *
 * The followings are the available model relations:
 * @property Users $_creator
 * @property Users $_user
 */
class Mailings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mailings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, creator, user_id', 'required'),
			array('creator, user_id, readed', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('message, date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, message, date, creator, user_id, readed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'_creator' => array(self::BELONGS_TO, 'Users', 'creator'),
			'user' => array(self::BELONGS_TO, 'Users', ['user_id'=>'id']),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'mailings.id'),
			'title' => Yii::t('app', 'mailings.title'),
			'message' => Yii::t('app', 'mailings.message'),
			'date' => Yii::t('app', 'mailings.date'),
			'creator' => Yii::t('app', 'mailings.creator'),
			'user' => Yii::t('app', 'mailings.user'),
			'readed' => Yii::t('app', 'mailings.readed'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('creator',$this->creator);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('readed',$this->readed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mailings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
