<?php

/**
 * This is the model class for table "sertificate".
 *
 * The followings are the available columns in table 'sertificate':
 * @property integer $id
 * @property string $type
 * @property string $number
 * @property integer $nominal
 * @property boolean $canceled
 * @property boolean $activated
 * @property integer $user_id
 * @property string $date_add
 * @property string $date_take
 */
class Sertificate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sertificate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nominal, user_id', 'numerical', 'integerOnly'=>true),
			array('type, number', 'length', 'max'=>255),
			array('canceled, activated, date_add, date_take', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, number, nominal, canceled, activated, user_id, date_add, date_take', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'number' => 'Number',
			'nominal' => 'Nominal',
			'canceled' => 'Canceled',
			'activated' => 'Activated',
			'user_id' => 'User',
			'date_add' => 'Date Add',
			'date_take' => 'Date Take',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('nominal',$this->nominal);
		$criteria->compare('canceled',$this->canceled);
		$criteria->compare('activated',$this->activated);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('date_add',$this->date_add,true);
		$criteria->compare('date_take',$this->date_take,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sertificate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
