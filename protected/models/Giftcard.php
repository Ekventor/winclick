<?php

/**
 * This is the model class for table "giftcard".
 *
 * The followings are the available columns in table 'giftcard':
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property string $img
 * @property string $nominals
 */
class Giftcard extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'giftcard';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, short_desc, img, small_img', 'length', 'max'=>255),
			array('nominals', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, desc, img, nominals, short_desc, small_img, addit_info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Y::t('app', 'giftcard.id'),
			'title' => Y::t('app', 'giftcard.title'),
			'short_desc' => Y::t('app', 'giftcard.short_desc'),
			'desc' => Y::t('app', 'giftcard.desc'),
			'small_img' => Y::t('app', 'giftcard.small_img'),
			'img' => Y::t('app', 'giftcard.img'),
			'nominals' => Y::t('app', 'giftcard.nominals'),
			'addit_info' => Y::t('app', 'giftcard.addit_info')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('nominals',$this->nominals,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Giftcard the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
