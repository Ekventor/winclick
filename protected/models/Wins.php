<?php

/**
 * This is the model class for table "wins".
 *
 * The followings are the available columns in table 'wins':
 * @property integer $id
 * @property integer $user_id
 * @property string $type
 * @property integer $nominal
 * @property string $game_id
 * @property string $date
 * @property integer $canceled
 * @property string $canceled_datetime
 * @property integer $used
 * @property string $used_datetime
 */
class Wins extends CActiveRecord
{
		
		const USED_EXCHANGED = 1;
		const USED_BOUGHT = 2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'wins';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, nominal, canceled, used', 'numerical', 'integerOnly'=>true),
			array('type, game_id', 'length', 'max'=>255),
			array('date, canceled_datetime, used_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, type, nominal, game_id, date, canceled, canceled_datetime, used, used_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => [self::BELONGS_TO, 'Users', 'user_id'],
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'type' => 'Type',
			'nominal' => 'Nominal',
			'game_id' => 'Game',
			'date' => 'Date',
			'canceled' => 'Canceled',
			'canceled_datetime' => 'Canceled Datetime',
			'used' => 'Used',
			'used_datetime' => 'Used Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('nominal',$this->nominal);
		$criteria->compare('game_id',$this->game_id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('canceled',$this->canceled);
		$criteria->compare('canceled_datetime',$this->canceled_datetime,true);
		$criteria->compare('used',$this->used);
		$criteria->compare('used_datetime',$this->used_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public function scopes() {
		return [
			'default' => [ 'condition' => "type='!'",
					'limit' => Y::param('wins_show'),
					'with'=>'user',
					'order' => 'date DESC',
			],
			'last' => [ 'condition' => "type='!' AND t.id>:lid",
				    'order'=> 'date DESC',
				    'with' => 'user',
				   ],
		];
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Wins the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
