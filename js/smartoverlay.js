var smartOverlay = {
    //author: Serduk Alex, 2013
    inited: false,
    showed: false,
    container: '.overlay',
    popup: '.popup',
    callback: '',
    confirmTemplate:   '<div>{{html text}}</div>' +
        '<div class="right"><button class="button" onclick="javascript: smartOverlay.confirmReturn(); return false;">Да</button> <button class="button" onclick="javascript: smartOverlay.hide(); return false;">Нет</button></div>',
 
    init: function(popup, container) {
        this.container = container || this.container;
        this.setPopup(popup);
        if (!this.inited) { 
            $(this.container).click(function(){
                smartOverlay.hide();
            });
            $(document).keyup(function(e) {
                if ((e.keyCode == 27)&&(smartOverlay.showed)) { smartOverlay.hide(); }   // esc
            });
            $(window).scroll(function(e){ e.preventDefault(); });
            this.inited = true;
        }
        return this;
    },
    show: function() {
        $(document).bind('mousewheel DOMMouseScroll',function(){
            stopWheel();
        });
        $(this.popup).trigger('show.smartOverlay');
        $(this.container).width($(window).width()).height($(window).height()).show();

        var pp_h = $(this.popup).outerHeight();
        var pp_w = $(this.popup).outerWidth(true);
        var win_h = $(window).height();
        var win_w = Math.floor($(window).width()/2);
        if (win_h>pp_h) {
            $(this.popup).css({top: $(document).scrollTop()+(win_h-pp_h)/2, left: (win_w - Math.floor(pp_w/2))});
        } else {
            $(this.popup).css({top: $(document).scrollTop(), left: (win_w - Math.floor(pp_w/2))})
        }
        $(this.popup).show();
        this.showed = true;
        return this;
    },
    hide: function() {
        $(document).unbind('mousewheel DOMMouseScroll');
        $(this.container).hide();
        $(this.popup).hide();
        this.showed = false;
        $(this.popup).trigger('hide.smartOverlay');
        return this;
    },
    setPopup: function(popup) {
        this.popup = popup || this.popup;
        return this;
    },
    confirm: function(text, callback) {
        this.setPopup('#confirm');
        text = text || 'Are you sure?';
        $(this.popup).html('');
        this.callback = callback;
        $.tmpl(this.confirmTemplate, {'text': text}).appendTo(this.popup);
        this.show();
        return this;
    },
    confirmReturn: function() {
        this.callback();
	this.hide();
        return false;
    },
}


function stopWheel(e){
    if(!e){ /* IE7, IE8, Chrome, Safari */
        e = window.event;
    }
    if(e.preventDefault) { /* Chrome, Safari, Firefox */
        e.preventDefault();
    }
    e.returnValue = false; /* IE7, IE8 */
}