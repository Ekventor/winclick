var game = {
    url: '',
    game_url: 'http://winclick.linode:1338',
    nominal: false,
    wins: null,
    timings: {
        win: 5000,
        spin: 5,
    },
    current_game: 0,
    intervals: {
        'wins': null,
    },
    timeouts: {
        'winner': null,
        'repeat': null,
    },
    win: 1,
    is_active: false,
    last_answer: null,
    last_win: 0,
    is_demo: false,
    auto_convert: false,
    init: function() {
        this.bindActions();
        smartOverlay.init('.result-popup', '.gray-popup-bg');
        WinEvent.init('.game-information');
    },
    setUrls: function(url, game_url) {
        this.url = url;
        this.game_url = game_url;
    },
    setDemo: function() {
        this.is_demo = true;
    },
    setAutoMoney: function() {
        this.auto_convert = true;
        return this;
    },
    setWins: function(wins) {
        this.wins = wins;
        return this;
    },
    setTimings: function(timings) {
        this.timings = timings;
        return this;
    },
    refreshWins: function() {
        var self = this;
        //TODO: remove 
        
        $.get(this.url + '/site/getlastwins/last/' + this.last_win, function(data){
            if (data.error == 0) {
                self.last_win = data.last;
                for (i in data.data) {
                    var row = data.data[i];
                    self.addWinner(row);
                }
            }
        }, 'json');
    },
    addWinner: function(data) {
        var  row = $('<tr><td style="color: #cbdde8;">' + data.date + '</td><td style="color: #cbdde8;">' + data.game_id + '</td><td class="white-text login-winner">' + data.login + '</td><td class="white-text" align="right">' + data.nominal + ' руб.</td></tr>');;
        row.find('td').hide();
        $('.auction-results').prepend(row);
        row.find('td').fadeIn(2000);
    },
    register: function(form_data) {
        $('#register-form dl').removeClass('error-wrap');
        
        $.post(this.url + '/user/register', form_data, function(data){
            $('#register-form .placeholder').removeClass('error-wrap').addClass('alert-wrap');
            if (data.error) {
                //show errors on form
                for (i in data.errors) {
                    var err = data.errors[i];
                    $('#register-form .'+i+'.placeholder').removeClass('alert-wrap').addClass('error-wrap');
                    if (i != 'license') {
                        $('#register-form .'+i+'.placeholder .error-message').html(err[0])
                    }
                    
                }
            } else {
              //TODO: show success register message
              $('#register-form').html('<div class="register-success-message">' + data.message + '</div>');
            }
        }, 'json');
    },
    login: function(form_data) {
        $('#login-form .placeholder').removeClass('alert-wrap').removeClass('error-wrap');
        $.post(this.url + '/user/login', form_data, function(data){
            if (data.error) {
                $('#login-form .placeholder').removeClass('alert-wrap').addClass('error-wrap');
            } else {
                $('#login-form .placeholder').removeClass('error-wrap').addClass('alert-wrap');
                window.location.reload();
            }
        }, 'json');
        
    },
    recover: function(form_data) {
      
        $('#recover-form .placeholder').removeClass('alert-wrap').removeClass('error-wrap');
        $.post(this.url + '/user/recover', form_data, function(data){
            if (data.error) {
                $('#recover-form .placeholder').removeClass('alert-wrap').addClass('error-wrap');
            } else {
                $('#recover-form .placeholder').addClass('ready').html(data.message);
                $('#recover-form .button-wrap').remove();
            }
        }, 'json');
    },
    selectNominal: function(nominal) {
        var self = this;
        if (parseInt(nominal) == this.nominal) {
            return;
        }
        if (this.is_active) {
            return;
        }
        this.nominal = parseInt(nominal);

        //$('#game_result .text').text('Для участия в аукционе нажмите кнопку "СТАРТ"');
        if (!self.is_demo) {
            //$('#game-nominal').html(this.nominal);
            //$('#cost-game, #winklicks-count').html(game_cost[this.nominal]);
            //$('#info-nominal').html(this.nominal);
            //$('#auction-discount').html(0);
            //$.post('/user/getCardBalance', {'nominal': this.nominal}, function(r) {
            //    $('#cumulate-card-sum').html(JSON.parse(r).cumulativeCardCount);
            //    $('#universal-card-sum').html(JSON.parse(r).universeCardCount);
            //    $('#sum-discount').html(JSON.parse(r).universeCardBalance);
            //    $('#auction-discount').html(JSON.parse(r).cumulativeCardBalance);
            //});
        }
        //$('.slot .default').hide();
        //$('.slot .default img[alt="default ' + this.nominal + '"]').parent().show();
        //$('.slot .default').remove();
        //$('.slot').prepend('<li class="default"><img src="/images/certificates/' + this.nominal + 'default.jpg" alt="default '+this.nominal+'"/></li>').css('position', 'relative').animate({'top': '0px'});
        //
        //$('.rating-panel li.rating').removeClass('active');
        //$('.rating-panel li.rating[data-value="'+nominal+'"]').addClass('active');
        //$('#play').removeAttr('disabled');
    },
    getWinner: function() {
        return this.win;
    },
    spinEnd: function() {
        var self = game;
        $('#play').removeAttr('disabled');
        $('#buy-wk-express').removeAttr('disabled');

        self.is_active = false;
        Rater.bindActions();
        console.log('spin ended');
        //last_answer
        //{"connection": 6, "game": {"current": 6, "max": 10, "id": "game1", "remains": 4}, "prize": "B"}
        var g = self.last_answer.game;
        var oldRubBalance = parseInt($('#RUB_balance').html());
        var rubBalance = parseInt($('#RUB_balance').html()) + (self.nominal * 2);
        if (!self.is_demo) {
            //if (g.id != self.current_game) {
            //    self.current_game = g.id;
            //    $('#sum-discount').html(parseInt($('#sum-discount').html()) + parseInt($('#auction-discount').html()));
            //    $('#auction-discount').html('0');
            //}
            $('#WK_balance').html(Math.round(self.last_answer.accounts.WK.balance*100)/100);
            if (self.last_answer.prize == 'B' || self.last_answer.prize == '!') {
                $('.wk-balance-win-bg').animate({opacity: 1}, 1000, function() {
                    $('#RUB_balance').html(Math.round(self.last_answer.accounts.RUB.balance*100)/100);
                    $('.wk-balance-win-bg').animate({opacity: 0}, 1000);
                });
            }
            if (self.last_answer.prize == 'C') {
                $('.wk-balance-win-bg').animate({opacity: 1}, 1000, function() {
                    $('#RUB_balance').html(Math.round(self.last_answer.accounts.RUB.balance*100)/100);
                    $('.wk-balance-win-bg').animate({opacity: 0}, 1000);
                });
            }
            if (self.last_answer.prize in discounts) {
                var win = parseInt(discounts[self.last_answer.prize][self.nominal]),
                    currentCumulative = parseInt($('#auction-discount').html());
                if (self.last_answer.prize == '!') {
                    win_effect.animateMainPrize(win);
                } else if(currentCumulative + win >= self.nominal) {
                    win_effect.animateCumulativeWin(win, self.nominal)
                } else if(currentCumulative + win < self.nominal) {
                    win_effect.animateWin(win)
                }
                //win_effect.animate(parseInt(discounts[self.last_answer.prize][self.nominal]));
                //$('.win-bg').animate({opacity: 1}, 1000, function() {
                //    console.log('all good');
                //    $('#auction-discount').html(parseInt($('#auction-discount').html()) + );
                //}).animate({opacity: 0}, 1000);
                //$('#auction-discount').html(parseInt($('#auction-discount').html()) + parseInt(discounts[self.last_answer.prize][self.nominal]));
            }
        } else {
            if (self.last_answer.prize == 'B' || self.last_answer.prize == '!') {
                $('.wk-balance-win-bg').animate({opacity: 1}, 1000, function() {
                    $('#RUB_balance').html(parseInt($('#RUB_balance').html()) + (self.nominal * 2));
                    $('.wk-balance-win-bg').animate({opacity: 0}, 1000);
                });
            }
            if (self.last_answer.prize == 'C') {
                $('.wk-balance-win-bg').animate({opacity: 1}, 1000, function() {
                    $('#RUB_balance').html(parseInt($('#RUB_balance').html()) + (self.nominal));
                    $('.wk-balance-win-bg').animate({opacity: 0}, 1000);
                });
            }
        }

        if (self.last_answer.prize == 'A') {
            WinEvent.onCoupon();
            //self.timeouts.winner = setTimeout(function() {
            //    WinEvent.onStart();
            //}, self.timings.win);
        } else if ((self.last_answer.prize == 'B' || self.last_answer.prize == '!')
            && rubBalance > winAmount && oldRubBalance < winAmount) {
            WinEvent.onWin();
            //self.timeouts.winner = setTimeout(function() {
            //    WinEvent.onStart();
            //}, self.timings.win);

        } else if ((self.last_answer.prize == 'B' || self.last_answer.prize == '!')
            && (rubBalance <= winAmount || oldRubBalance > winAmount)) {
            WinEvent.onDouble();
            //self.timeouts.winner = setTimeout(function() {
            //    WinEvent.onStart();
            //}, self.timings.win);
        }

        $('#game_remains').val(g.remains); //THIS IS INPUT THIS IS MADNESS ARRR
        $('#game_id').html(g.id);
        $('#game_result .text').html(messages[self.last_answer.prize][self.nominal]);
        
        //if (self.last_answer.prize == '!') {
        //    $('.auction-panel .slogan').addClass('winner').html(messages.win);
        //    self.timeouts.repeat = setTimeout(function(){$('.auction-panel .slogan').removeClass('winner').addClass('again').html(messages.repeat);},self.timings.win);
        //    self.timeouts.winner = setTimeout(function(){$('.auction-panel .slogan').removeClass('winner').removeClass('again').html(messages.slogan);},self.timings.win * 2);
        //} else {
        //    $('.auction-panel .slogan').removeClass('winner').addClass('again').html(messages.repeat);
        //    self.timeouts.winner = setTimeout(function(){$('.auction-panel .slogan').removeClass('winner').removeClass('again').html(messages.slogan);},self.timings.win);
        //}


    },
    play: function() {
        if (this.nominal === false) {
            $('#game_result .text').html(messages.select_nominal);
            return;
        } 
        var self = this;

        $('.fireworks').hide();
        if (self.timeouts.winner != null) {
            clearTimeout(self.timeouts.winner);
            clearTimeout(self.timeouts.repeat);
            //WinEvent.onStart();
            //$('.auction-panel .slogan').removeClass('winner').removeClass('again').html(messages.slogan);
        }
        //block play button
        $('#play').attr('disabled', 'disabled');
        $('#buy-wk-express').attr('disabled', 'disabled');
        self.is_active = true;
        Rater.unbindActions();
        var params = {'nominal': self.nominal, 'token': User.game_token};
        if (self.is_demo) {
            params.demo = 1;
        }
        if (self.auto_convert) {
            params.auto_convert = 1;
        }
        $.getJSON(self.game_url + '?callback=?', params, function(data){
            if (typeof(data.error) != 'undefined') { //error then
                $('.result-popup .message').html(messages.not_enough_money);
                smartOverlay.setPopup('.result-popup').show();

                $('#game_result .text').html(messages.not_enough_money);
                $('#game_remains').val('-'); //THIS IS INPUT THIS IS MADNESS ARRR
                $('#game_id').html(' -');
                self.is_active = false;
                $('#buy-wk-express').removeAttr('disabled');
                Rater.bindActions();
                $('#play').removeAttr('disabled');
                return;
            }

            if (self.is_demo) {
                $('#game_result .text').html(costs['demo']);
            } else {
                $('#game_result .text').html(costs[self.nominal]);
            }

            //TODO: remove debug echo
            console.log('game answer');
            //console.log(data);

            self.last_answer = data;
            if (!self.is_demo) {
                //данные баланса обновляем сразу, как получили ответ
                var rubBalance = parseInt($('#RUB_balance').html());
                var wkBalance = parseInt($('#WK_balance').html());
                newWkBalance = wkBalance - self.nominal;
                newRubBalance = rubBalance - self.nominal;
                if (newWkBalance < 0) {
                    $('#RUB_balance').html(newRubBalance);
                }
                $('#WK_balance').html(Math.round(self.last_answer.accounts.WK.balance*100)/100);
            }

            self.win = self.wins[self.nominal + data.prize];
            //console.log(self.nominal + data.prize);
            $('.slot .default').remove();

            $('.jSlots-wrapper').trigger('jSlots.play'); //spin to win!
        });
    },
    bindActions: function() {
        console.log('init actions');
        var self = this;

        //$('.cur-win').on('click', function() {
        //    win_effect.animate(500);
        //});
        $('#buy-wk-express').on('click', function() {
            if (!game_cost[self.nominal]) {
                return false;
            }
            $.post('/user/expressBuy', {count: game_cost[self.nominal]}, function(r) {
                if (!JSON.parse(r).hasResult) {
                    return false;
                }
                if (JSON.parse(r).enoughMoney) {
                    result = JSON.parse(r).result.data;
                    for (var index in result) {
                        $('#' + result[index]['acc_type'] + '_balance').html(Math.round(result[index]['balance']*100)/100);
                    }
                    $('#play').removeAttr('disabled');
                } else {
                    alert('Недостаточно стредств для покупки винкликов!');
                }
            });
        });


        $('#register-form').submit(function(event){
            event.preventDefault();
            self.register($(this).serialize());
        });
        
        $('#login-form').submit(function(event){
            event.preventDefault();
            self.login($(this).serialize());
        });
        
        $('#recover-form').submit(function(event){
            event.preventDefault();
            self.recover($(this).serialize());
        });
        
        $('body').on('click', '#login-link', function(event){
            event.preventDefault();
            smartOverlay.setPopup('.registration-popup').show();
        });
        $('#password-recover-link').click(function(event){
            event.preventDefault();
            $('#login-form').fadeOut(1000, function(){$('#recover-form').fadeIn();});
        });
        $('#cancel-recover-link').click(function(event){
            event.preventDefault();
            $('#recover-form').fadeOut(1000, function(){$('#login-form').fadeIn();});
        });
        $('#register-link').click(function(event){
            event.preventDefault(); 
            $('html, body').animate({scrollTop : $('#register-form').offset().top + $('html').scrollTop() - 50}, 500, 'swing', function(){});
        });
        
        $('.rating-panel li.rating').click(function(event){
            event.preventDefault();
            self.selectNominal($(this).data('value'));
        });
        $('#play').click(function(e){
            e.preventDefault();
            self.play();
        });
        
        self.intervals.wins = setInterval(function(){game.refreshWins();}, 15000);
        
        $('#search_companies').click(function(e){
                e.preventDefault();
                var params = {};
                params.string = $('#search_text').val();
                params.city = $('.dropdown[data-id="city"]').smartDropdown('getValue');
                params.category= $('.dropdown[data-id="category"]').smartDropdown('getValue');
                
                $.post(self.url + '/companies/search', params, function(data){
                    if (data.error == 0) {
                        $('.search-result').hide().html($(data.html).find('.search-result').html()).fadeIn();
                        $('.search-more').hide().html($(data.html).find('.search-more').html()).fadeIn();
                    } else {
                        $('.search-result').hide().html(data.message).fadeIn();
                        $('.search-more').html('');
                    }
                }, 'json');
        });
    },
    
    updateWk: function($checkbox) {
        
        if ($checkbox.attr('checked')) {
            $.post(this.url + '/user/autowk', {'auto_convert': 1}, function(r){console.log(r);});
        } else {
            $.post(this.url + '/user/autowk', {'auto_convert': 0}, function(r){console.log(r);});
        }
    },
    selectCat: function(catId) {
        $('.dropdown[data-id="category"]').smartDropdown('select', catId);
        $('.search-more').html('');
    }
}

giftcards = {
    url: '',
    data: {100: 0, 500: 0, 1000: 0, 5000: 0, 10000: 0},
    recount: '',
    step: 95,
    winclickCost: 5,
    errorMessage: {
        notEnoughCard: 'Недостаточно выйграных карт на счете!',
        emptyCurrentCard: 'Первоначально необходимо выбрать карту, которую вы желаете получить в обмен на универсальную карту'
    },
    successMessage: {
        exchangeSuccess: 'Реквизиты карт будут отправлены на указанную вами электронную почту'
    },

    currentSert: {
        300: {img: '', id: 0},
        1000: {img: '', id: 0},
        2000: {img: '', id: 0},
        3000: {img: '', id: 0},
        10000: {img: '', id: 0}
    },
    currentSerts: {
        300: [],
        1000: [],
        2000: [],
        3000: [],
        10000: []
    },

    setData: function(data) {
        this.data = data;
    },
    setWinclickCost: function(cost) {
        this.winclickCost = cost;
    },
    up: function(nominal) {
        
    },
    down: function(nominal) {
        
    },

    certInCurrent: function(id, nominal) {
        var self = this;
        for (var index in self.currentSerts[nominal]) {
            if (self.currentSerts[nominal][index]['id'] == id) {
                return true;
            }
        }
        return false;
    },

    deleteFromCurrent: function(id, nominal) {
        var self = this;
        for (var index in self.currentSerts[nominal]) {
            if (self.currentSerts[nominal][index]['id'] == id) {
                self.currentSerts[nominal].splice(index, 1);
                return true;
            }
        }
        return false;
    },

    init: function () {
        var self = this;

        smartOverlay.init('.result-popup', '.gray-popup-bg');

        $('#take-cards').on('click', function() {
            $.post('/user/giftcardTake', {}, function(result) {
                if (!result) {
                    $('.result-popup .message').html('Непредвиденная ошибка!');
                    smartOverlay.setPopup('.result-popup').show();
                    return;
                }
                if (result.error) {
                    $('.result-popup .message').html(result.message);
                    smartOverlay.setPopup('.result-popup').show();
                    return;
                }

                $('.result-popup .message').html(result.message);
                smartOverlay.setPopup('.result-popup').show();
                setTimeout(location.reload(), 4000);
                //self.currentSerts[nominal] = [];
                //$defaultSert = $('#sert' + nominal).find('#sert-0').clone();
                //$('#sert' + nominal).find('.serts').empty().append($defaultSert);
                //$('#sert' + nominal).find('input').addClass('disabled').prop('disabled', true);
            }, 'json');

        });

        $('.sert-block').on('click', function() {
            var imgSrc = $(this).find('img').attr('src'),
                sertId = $(this).attr('sert-id'),
                nominal = $(this).attr('nominal'),
                $parent = $(this).parents('.card-block');
            $parent.find('.card-image img.current-sert-image').remove();
            $img = $parent.find('.card-image img').hide().clone();
            $img.attr('src', imgSrc).addClass('current-sert-image').show().appendTo($parent.find('.card-image'));
            $parent.find('input[name="current-cert"]').val(sertId);
            giftcards.currentSert[nominal]['img'] = imgSrc;
            giftcards.currentSert[nominal]['id'] = sertId;
        });

        $('.exch-button').on('click', function() {
            var $parent = $(this).parents('.card-block'),
                nominal = $parent.attr('id').replace('card', ''),
                $count = $('#count' + nominal),
                count = $('#count' + nominal).text();

            $parent.find('.card-image img.current-sert-image').remove();
            $parent.find('.card-image img').show();

            if (self.currentSert[nominal]['img'] == '' || self.currentSert[nominal]['id'] == 0) {
                self.currentSert[nominal] = {img: '', id: 0};
                $('.result-popup .message').html(self.errorMessage.emptyCurrentCard);
                smartOverlay.setPopup('.result-popup').show();
                return false;
            }

            if (count == 0) {
                self.currentSert[nominal] = {img: '', id: 0};
                $('.result-popup .message').html(self.errorMessage.notEnoughCard);
                smartOverlay.setPopup('.result-popup').show();
                return false;
            }

            $count.html(count - 1);
            if (parseInt($count.html()) == 0) {
                $count.closest('div').find('input').addClass('disabled').prop('disabled', true);
            }

            self.currentSerts[nominal].push({img: self.currentSert[nominal]['img'], id: self.currentSert[nominal]['id']});
            var $sert = $parent.find('#sert-0').clone();
            $sert.attr('id', 'sert-' + self.currentSert[nominal]['id'])
                .css('display', 'block')
                .find('.image')
                .attr('src', self.currentSert[nominal]['img']);
            $sert.find('a.close').attr('sert-id', self.currentSert[nominal]['id']);
            if (self.currentSerts[nominal].length) {
                $('#sert' + nominal).find('input').removeClass('disabled').prop('disabled', false);
            }

            $sert.find('a.close').on('click', function() {
                var sertId = $(this).attr('sert-id');
                self.deleteFromCurrent(sertId, nominal);
                $parent.find('#sert-' + sertId).remove();
                $count.html(parseInt($count.html()) + 1);
                $count.closest('div').find('input').removeClass('disabled').prop('disabled', false);
                if (!self.currentSerts[nominal].length) {
                    $('#sert' + nominal).find('input').addClass('disabled').prop('disabled', true);
                }
            });
            $parent.find('.cart .serts').append($sert);
            self.currentSert[nominal] = {img: '', id: 0};
        });

        $('.take-cards1').on('click', function() {
            var $parent = $(this).parents('.card-block'),
                nominal = $parent.attr('id').replace('card', ''),
                currentSerts = self.currentSerts[nominal];
            var data = {
                serts: currentSerts,
                nominal: nominal
            };
            $.post('/user/giftcardTake', {data: data}, function(result) {
                if (result.error) {
                    alert(result.message);
                    return;
                }

                $('.result-popup .message').html(self.successMessage.exchangeSuccess);
                smartOverlay.setPopup('.result-popup').show();
                self.currentSerts[nominal] = [];
                $defaultSert = $('#sert' + nominal).find('#sert-0').clone();
                $('#sert' + nominal).find('.serts').empty().append($defaultSert);
                $('#sert' + nominal).find('input').addClass('disabled').prop('disabled', true);
            });
        });


    },
    init_old: function(){
        var self = this;

        smartOverlay.init('.result-popup', '.gray-popup-bg');

        $('[data-nominal]').each(function(){
            self.data[$(this).data('nominal')] = parseInt($(this).find('.total_percent').html());
        });
        
        $('[data-action="reset"]').click(function() {
            var nominal = $(this).parents('.giftcards-table').data('nominal');
            $parent = $(this).parents('.giftcards-table');
            $parent.find('.total_percent').html(self.data[nominal] + '<span class="blue-rubl"></span>');
            $parent.find('.summ-number').html(0);
            $parent.find('[data-id]').val(0);
        });

        $('[data-action="exchange"],[data-action="buy"]').click(function(){
            var action = $(this).data('action');
            var nominal = $(this).parents('.giftcards-table').data('nominal');
            var max = 0;
            if (action == 'exchange') {
                max = self.data[nominal];
            } else {
                max = Math.floor(self.data[nominal] / self.step);
            }
            
            var amount = parseInt($('[data-nominal="'+nominal+'"] [data-id="'+action+'"]').val());
            
            if (amount > max) {
                amount = max;
                $('[data-nominal="'+nominal+'"][data-id="'+action+'"]').val(max)
            }
            
            if (amount <=0) {
                return;
            }   
            self[action](nominal, amount);
        });
       
        $('.giftcards-table .val-change').on('click', function () {
            $input = $(this).closest('td').find('input[type="text"]');
            var nominal = $input.parents('.giftcards-table').data('nominal');

            rubl = '<span class="blue-rubl"></span>';
            if (!$(this).hasClass('exchange')) {
                var max = Math.floor(self.data[nominal] / self.step);
                val = parseInt($input.val());
                
                sum = (self.step * nominal) / 100;
                if ($(this).hasClass('plus')) {
                    if ((val+1)>max) {
                        $input.val(max);
                        return;
                    }
                    if ((parseInt($('[data-nominal="'+nominal+'"] .total_percent').html()) - sum) >= 0) {
                        $input.val(val + 1);
                        $('[data-nominal="'+nominal+'"] .total_percent')
                            .html((parseInt($('[data-nominal="'+nominal+'"] .total_percent').html()) - sum) + rubl)
                    }
                } else if ($(this).hasClass('minus') && val != 0) {
                    $input.val(val - 1);
                    $('[data-nominal="'+nominal+'"] .total_percent')
                        .html((parseInt($('[data-nominal="'+nominal+'"] .total_percent').html()) + sum) + rubl)
                }
            
                $('[data-nominal="'+nominal+'"] .summ-number').html($input.val() * nominal*0.05);
            } else {
                var max = self.data[nominal];
                val = parseInt($input.val());
                if ($(this).hasClass('plus')) {
                    if ((val+1)>max) {
                        $input.val(max);
                        return;
                    }
                    if ((parseFloat($('[data-nominal="'+nominal+'"] .total_percent').html()) - self.winclickCost) >= 0) {
                        $input.val(val + 1);
                        $('[data-nominal="'+nominal+'"] .total_percent').html((parseFloat($('[data-nominal="'+nominal+'"] .total_percent').html()) - self.winclickCost) + rubl)
                    }
                } else if ($(this).hasClass('minus') && val != 0) {
                    $input.val(val - 1);
                    $('[data-nominal="'+nominal+'"] .total_percent').html((parseFloat($('[data-nominal="'+nominal+'"] .total_percent').html()) + self.winclickCost) + rubl)
                }
            }
        });
    },
    buy: function(nominal, amount) {
        if (amount) {
            $('#cards-buy-' + nominal).submit();
        }
        //$.post(this.url + '/user/buy', {'nominal': nominal, 'amount': amount}, function(data){
        //    if (data.error == 0) {
        //        $('[data-nominal="'+nominal+'"] .summ-number').html(0);
        //        $('[data-nominal="'+nominal+'"] [data-id="buy"]').val(0);
        //        console.log('success: ' + data.message);
        //    } else {
        //        console.log('error: ' + data.message);
        //    }
        //    $('.result-popup .message').html(data.message);
        //    smartOverlay.setPopup('.result-popup').show();
        //    //$('#result-message').empty().html(data.message).dialog('open');
        //}, 'json');
    },
    exchange: function(nominal, amount) {
        $.post(this.url + '/user/exchange', {'nominal': nominal, 'amount': amount}, function(data){
            if (data.error == 0) {
                $('[data-nominal="'+nominal+'"] [data-id="exchange"]').val(0);
                console.log('success: ' + data.message);
            } else {
                console.log('error: ' + data.message);
            }
            $('.result-popup .message').empty().html(data.message);
            smartOverlay.setPopup('.result-popup').show();
        }, 'json');
        //$('.result-popup .message').empty().html('data.message');
        //smartOverlay.setPopup('.result-popup').show();
    }
    
}

win_effect = {

    resetStyle: function() {
        $('.win-arrow img').first().css('opacity', 0);
        $('.win-arrow img').last().css('opacity', 1);
        $('#win, #win-plus').css('color', '#a9d5ee');
    },

    animateMainPrize: function(win) {
        this.resetStyle();
        $('.sum-win .win-bg').animate({opacity: 1}, 500, function() {
            $('#sum-discount').html(parseInt($('#sum-discount').html()) + win);
            $('#universal-card-sum').html(parseInt($('#universal-card-sum').html()) + 1);
        }).animate({opacity: 0}, 500);
    },

    animateCumulativeWin: function(win, nominal) {
        this.resetStyle();
        $('#win').html(win);
        $('.label').animate({opacity: 0}, 300, function() {
            $('.win-arrow').animate({opacity: 1}, 300, function() {
                $('#win-plus').animate({
                    color: '#e89995'
                }, 300, function () {
                    $('#win').animate({color: '#72e27c'}, 300, function () {
                        setTimeout(function(){
                            $('.win-arrow img').first().animate({opacity: 1}, 300);
                            $('.win-arrow img').last().animate({opacity: 0}, 300);
                        }, 0);
                        $('.cur-win .win-bg').animate({opacity: 1}, 500, function() {
                            var cumulative = parseInt($('#auction-discount').html()) + win;
                            $('#auction-discount').html(cumulative - nominal);
                            $('#cumulate-card-sum').html(0);
                        }).animate({opacity: 0}, 500, function() {
                            $('.win-arrow').animate({opacity: 0}, 300, function() {
                                $('.label').animate({opacity: 1}, 300);
                            });
                            $('.sum-win .win-bg').animate({opacity: 1}, 500, function() {
                                $('#sum-discount').html(parseInt($('#sum-discount').html()) + nominal);
                                $('#universal-card-sum').html(parseInt($('#universal-card-sum').html()) + 1);
                            }).animate({opacity: 0}, 500);
                        });
                    });
                });
            });
        });
    },

    animateWin: function(win) {
        this.resetStyle();
        $('#win').html(win);
        $('.label').animate({opacity: 0}, 300, function() {
            $('.win-arrow').animate({opacity: 1}, 300, function() {
                $('#win-plus').animate({
                    color: '#e89995'
                }, 300, function () {
                    $('#win').animate({color: '#72e27c'}, 300, function () {
                        setTimeout(function(){
                            $('.win-arrow img').first().animate({opacity: 1}, 300);
                            $('.win-arrow img').last().animate({opacity: 0}, 300);
                        }, 0);
                        $('.cur-win .win-bg').animate({opacity: 1}, 500, function() {
                            $('#auction-discount').html(parseInt($('#auction-discount').html()) + win);
                            $('#cumulate-card-sum').html(parseInt($('#cumulate-card-sum').html()) + 1);
                        }).animate({opacity: 0}, 500, function() {
                            $('.win-arrow').animate({opacity: 0}, 300, function() {
                                $('.label').animate({opacity: 1}, 300);
                            });
                        });
                    });
                });
            });
        });
    },

    salut: function() {
        $('.salut').animate({opacity: 1}, 500, function(){
            $('.salut').animate({opacity: 0}, 2000, function(){
            });
        });
    }

};

var Rater = {

    prefSelector: '#pref-rate',

    nextSelector: '#next-rate',

    values: [],

    init: function(prefSelector, nextSelector, values) {
        var self = this,
            nominal = $(prefSelector).parent().find('input').val();

        self.prefSelector = prefSelector;
        self.nextSelector = nextSelector;

        self.values = values;

        game.selectNominal(parseInt(nominal));

        self.bindActions()
    },

    bindActions: function() {
        var self = this;
        $(self.prefSelector).on('click', function() {
            self.changeRate('pref');
        });
        $(self.nextSelector).on('click', function() {
            self.changeRate('next')
        });

    },

    unbindActions: function() {
        var self = this;
        $(self.prefSelector).off('click');
        $(self.nextSelector).off('click');
    },

    changeRate: function(action) {
        var self = this,
            currentRatePos = self.currentRatePos(),
            $input = $(self.prefSelector).parent().find('input');

        if (action == 'pref') {
            if (currentRatePos == 0) {
                return;
            }
            newPos = parseInt(currentRatePos) - 1;
        }
        if (action == 'next') {
            if (currentRatePos == (self.values.length - 1)) {
                return;
            }
            newPos = parseInt(currentRatePos) + 1;
        }
        rate = self.values[newPos];
        $input.val(rate);
        game.selectNominal(rate);
    },

    currentRatePos: function() {
        var self = this,
            currentRate = $(self.prefSelector).parent().find('input').val();
        for (var index in self.values) {
            if (self.values[index] == currentRate) {
                return index;
            }
        }


    }
};


var WinEvent = {
    startBlock: '',

    couponPrizeBlock: '',

    doublePrizeBlock: '',

    winPrizeBlock: '',

    winSelector: '',

    init: function(winSelector) {
        var self = this;

        self.winSelector = winSelector;
        self.startBlock = $(winSelector + '[data-type="start-info"]');
        self.couponPrizeBlock = $(winSelector + '[data-type="coupon-info"]');
        self.doublePrizeBlock = $(winSelector + '[data-type="double-info"]');
        self.winPrizeBlock = $(winSelector + '[data-type="win-info"]');

    },

    onStart: function() {
        var self = this;
        $(self.winSelector).hide();
        self.startBlock.show();
        $('.fireworks').hide();
    },

    onCoupon: function() {
        var self = this;
        $(self.winSelector).hide();
        self.couponPrizeBlock.show();
        $('.fireworks').hide();
    },

    onDouble: function() {
        var self = this;
        $(self.winSelector).hide();
        self.doublePrizeBlock.show();
        $('.fireworks').hide();
    },

    onWin: function() {
        var self = this;
        $(self.winSelector).hide();
        self.winPrizeBlock.show();
        $('.fireworks').show();
    }
};

