(function( $ ){
	var methods = {
		init: function(options) {
			var settings = $.extend({
				search: 'standart',
				find: true,
				optionsPosition: 'bottom'
			}, options)
			var self = this;
			return this.each(function() {
				var $this = $(this);
					var data = $this.data('smartDropdown');	//если не инициализированн, то делаем это
				if (!data) {
					var optionBlock = $this.find('.options');
					var fakeOptionBLock = $('<div data-type="fake" class="options" style="display: none; max-height: 350px; height: auto;"></div>');
					//fakeOptionBLock.smartExpandDropDown();
					$this.append(fakeOptionBLock);
					var marginLeft = -optionBlock.width()/2;
					//optionBlock.css('margin-left', marginLeft);
					optionBlock.css('position', 'absolute');
					//fakeOptionBLock.css('margin-left', marginLeft);
					var selectBlock = $this.find('.selected');
					var optionsWidth = optionBlock.width();
					// optionBlock.css('width', optionsWidth);
					fakeOptionBLock.css('width', optionsWidth);
					var selectedSpan = selectBlock.find('span');
					var selectInput = selectBlock.find('input[data-id="selected-value"]');
					var findInput = selectBlock.find('input[data-id="find-value"]');
					if (settings.optionsPosition == 'top') {
						optionBlock.css('top', '-130%');
					}
					$this.data('smartDropdown', {
						init: true,
						optionBlock: optionBlock,
						selectBlock: selectBlock,
						selectInput: selectInput,
						findInput: findInput,
						selectedSpan: selectedSpan,
						fakeOptionBLock: fakeOptionBLock,
						find: settings.find
					});
					selectBlock.click(function(e) {
						e.stopPropagation();
						methods.toggle.apply($this);
					});
					optionBlock.find('span[data-value]').on('click.smartDropdown.optionSelect', function(e) {
						selectInput.val($(this).data('value'));
						selectedSpan.text($(this).data('descr'));
						selectedSpan.attr('data-sporttype', $(this).data('sporttype'));
						$this.trigger('change', 'change.smartDropdown');
					});
					fakeOptionBLock.on('click.smartDropdown.optionSelect', 'span[data-value]', function(e) {
						selectInput.val($(this).data('value'));
						selectedSpan.text($(this).data('descr'));
						selectedSpan.attr('data-sporttype', $(this).data('sporttype'));
						$this.trigger('change', 'change.smartDropdown');
					});
					findInput.click(function(e) {
						e.stopPropagation();
					});

					if (settings.search == 'standart') {
						findInput.keyup(function(e) {
							var self = this;
							if ($(this).val().length) {
								fakeOptionBLock.empty();
								// fakeOptionBLock.append('<ul data-id="null"></ul>');
								var findedOptions = [];

								optionBlock.find('span[data-value]').each(function(i, val) {
									var regexp = new RegExp($(self).val(), "i");
									if (regexp.test($(this).text())) {
										findedOptions.push($(val));
									}
								});
								findedOptions = $(findedOptions);
								//var findedOptions = optionBlock.find('span:contains("'+$(this).val()+'"), span:contains("'+ucfirst($(this).val())+'")');
								if (findedOptions.length) {
									findedOptions.each(function(i, val) {
										// $(val).parent().find('.opened').removeClass('opened');
										// fakeOptionBLock.find('ul[data-id="null"]').append($(val).parent().clone());
										fakeOptionBLock.append('<span data-descr="' + $(val).text() + '" data-value="'+$(val).data('value')+'">'+$(val).text()+'</span>');
									});
								} else {
									fakeOptionBLock.append('Ничего не найдено');
								}
								optionBlock.removeClass('opened');
								fakeOptionBLock.show();
							} else {
								fakeOptionBLock.hide();
								optionBlock.addClass('opened');
							}
						});
					} else {
						findInput.keyup(function(e) {
						var self = this;
						if ($(this).val().length) {
							fakeOptionBLock.empty();
							fakeOptionBLock.append('<ul data-id="null"></ul>');
							var findedOptions = [];

							optionBlock.find('span[data-value]').each(function(i, val) {
								var regexp = new RegExp($(self).val(), "i");
								if (regexp.test($(this).text())) {
									findedOptions.push($(val));
								}
							});
							findedOptions = $(findedOptions);
							//var findedOptions = optionBlock.find('span:contains("'+$(this).val()+'"), span:contains("'+ucfirst($(this).val())+'")');
							if (findedOptions.length) {
								findedOptions.each(function(i, val) {
									$(val).parent().find('.opened').removeClass('opened');
									fakeOptionBLock.find('ul[data-id="null"]').append($(val).parent().clone());
									// fakeOptionBLock.append('<span data-value="'+$(val).data('value')+'">'+$(val).text()+'</span>');
								});
							} else {
								fakeOptionBLock.append('Ничего не найдено');
							}
							optionBlock.removeClass('opened');
							fakeOptionBLock.show();
						} else {
							fakeOptionBLock.hide();
							optionBlock.addClass('opened');
						}
					});
					}


				}
			});
		},
		toggle: function() {
			var data = $(this).data('smartDropdown');
			if (data.optionBlock.hasClass('opened')) {
				methods.hide.apply(this);
			} else {
				$('html').trigger('click.smartDropdown');
				methods.show.apply(this);
			}
		},
		show: function() {
			var data = $(this).data('smartDropdown');
			data.optionBlock.addClass('opened');
			if (data.find) {
				data.selectedSpan.hide();
				data.findInput.show();
			}
			data.findInput.focus();
			var self = this;
			$('html').on('click.smartDropdown', function() {
				methods.hide.apply(self);
			})
			// TODO: keybind
			$('html').on('keydown.smartDropdown', function(e) {
				// console.log(data.optionBlock.find('ul.opened').css('background', 'red'));
				switch (e.keyCode) {
					case 38:
						e.preventDefault();
						e.stopPropagation(); 
						break;
					case 40:
						e.preventDefault();
						e.stopPropagation(); 
						break;
					case 37:
						e.preventDefault();
						e.stopPropagation(); 
						break;
					case 39:
						e.preventDefault();
						e.stopPropagation(); 
						break;
				}
			})
		},
		hide: function() {
			var data = $(this).data('smartDropdown');
			data.optionBlock.removeClass('opened');
			data.findInput.hide();
			data.findInput.val('');
			data.selectedSpan.show();
			data.fakeOptionBLock.hide();
			$('html').off('click.smartDropdown');
			$('html').off('keydown.smartDropdown');
		},
		select: function(id) {
			return this.each(function(){
				var data = $(this).data('smartDropdown');
				var selectedBlock = data.selectBlock;
				if (typeof(id)=='number') {
					var selectedEL = data.optionBlock.find('span[data-value="'+id+'"]');
				} else {
					var selectedEL = data.optionBlock.find('span[data-descr="'+id+'"]');
				}
				
				selectedBlock.find('input[type="hidden"]').val(selectedEL.data('value'));
				selectedBlock.find('span').text(selectedEL.data('descr')); 
				$(this).trigger('change');
			});
		},
		rebind: function() {
			return this.each(function(){
				var $this = $(this);
				var data = $this.data('smartDropdown');
				//remove old handlers
				data.optionBlock.find('span[data-value]').off('click.smartDropdown.optionSelect');
				//rebind handlers
				data.optionBlock.find('span[data-value]').on('click.smartDropdown.optionSelect', function(e) {
						data.selectInput.val($(this).data('value'));
						data.selectedSpan.text($(this).data('descr'));
						data.selectedSpan.attr('data-sporttype', $(this).data('sporttype'));
						$this.trigger('change', 'change.smartDropdown');
					});
			});
		},
		unset: function() {
			return this.each(function(){
				var data = $(this).data('smartDropdown');
				var selectedBlock = data.selectBlock;
				var selectedEL = data.optionBlock.find('span[data-value="null"]');
				selectedBlock.find('input[type="hidden"]').val(selectedEL.data('value'));
				selectedBlock.find('span').text(selectedEL.data('descr'));
				$(this).trigger('change');
			});
		},
		getValue: function() {
				var data = $(this).data('smartDropdown');
				if (data.selectInput.val())
					return data.selectInput.val();
				else return null;
		}
	}
	$.fn.smartDropdown = function( method ) {

		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.smartDropdown' );
		}

  };
})( jQuery );





(function( $ ){
	var methods = {
		init: function(options) {
			var settings = $.extend({

			}, options)
			var self = this;
			return this.each(function() {
				var $this = $(this);
				var data = $this.data('smartExpandDropDown');	//try get data
				if (!data) {	//if not init => init
					//init data
					//write data to data-object
					$this.data('smartExpandDropDown', {
					})
					//set events
					$this.on('click', 'span.expand', function(e) {
						e.preventDefault();
						e.stopPropagation();
						var showId = $(this).data('link');
						var ul = $(this).parent().find('ul[data-id="'+showId+'"]');
						ul.css('left', '100%');
						ul.addClass('opened');
						ul.animate({left: '0%'}, 250);
						$this.animate({height: ul.innerHeight()}, 250);
						$this.find('ul[data-id="'+ul.data('parent')+'"]').height(ul.innerHeight());
					});
					$this.on('click', 'li[data-action="back"]',function(e) {
						e.preventDefault();
						e.stopPropagation();
						var ul = $this.find('ul[data-id="'+$(this).data('link')+'"]');
						ul.animate({left: '100%'}, 250, function(){ul.removeClass('opened')});
						var parent = $this.find('ul[data-id="'+ul.data('parent')+'"]');
						if (parent.height()) {
							parent.height('auto');
							$this.animate({height: parent.innerHeight()}, 250);
						} else {
							$this.css({height: 'auto'});
						}
					});
				}
			});
		},
		show: function(ulId) {
			return this.each(function() {
				$(this).find('ul.opened').removeClass('opened');
				if (ulId) {
					$(this).find('ul[data-id="'+ulId+'"]').addClass('opened');
				}
			});
		}
	}
	jQuery.fn.smartExpandDropDown = function( method ) {

		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.smartExpandDropDown' );

		}

  };
})( jQuery );