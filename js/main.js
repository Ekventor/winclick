$(document).ready(function(){

	// Custom drop select panel script begin
    $('.drop-panel-button').click(function(){
        $(this).parents('.drop-panel-wrap').find('.drop-panel').show();
    });

    $('html').click(function(){
        $('.drop-panel').hide();
    });

    $('.drop-panel, .drop-panel-button').click(function(){
        event.stopPropagation();
    });

    $('.select-list li').click(function(){
        var value = $(this)[0].innerHTML;
        $(this).parents('.drop-panel-wrap').find('.select').val(value);
        $('.drop-panel').hide();
    });
    // Custom drop select panel script end

    // show/hide panels begin
    $.fn.togglepanels = function(){
        return this.each(function(){
            $(this).addClass("ui-accordion ui-accordion-icons ui-widget ui-helper-reset")
                    .find("div.tranzaction-panel")
                    .addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom")
                    .hover(function() { $(this).toggleClass("ui-state-hover"); })
                    .prepend('<span class="ui-icon ui-icon-triangle-1-e"></span>')
                    .click(function() {
                        $('div.tranzaction-panel').not(this)
                            .addClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
                            .find("> .ui-icon").addClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
                            .next().slideUp();
                        $(this)
                                .toggleClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
                                .find("> .ui-icon").toggleClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
                                .next().slideToggle();
                        return false;
                    })
                    .next()
                    .addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom")
                    .hide();
        });
    };

    $(".accordion-panel-wrap").togglepanels();
    // show/hide panels end

    // slider init

    //$('.review-slider-wrap .slider').carouFredSel({
    //    scroll : {
    //        duration : 1000
    //    },
    //    prev: '.review-slider-wrap .left-arrow',
    //    next: '.review-slider-wrap .right-arrow'
    //});

    // slider init end


    // show more news and help texts block script begin
    $('.news-block-control').click(function(){
        var $button = $(this);
        if($button.find('p').text() == 'Подробнее'){
            $button.find('p').text('Скрыть');
            $button.parents('.news-block').find('.show-hide-block-wrap').show('medium');
        } else {
            $button.find('p').text('Подробнее');
            $button.parents('.news-block').find('.show-hide-block-wrap').hide('medium');
        };


    });
    $('.help-block-control').click(function(){
        var $button = $(this);
        if($button.find('p').text() == 'Подробнее'){
            $button.find('p').text('Скрыть');
            $button.parents('.help-block').find('.show-hide-block-wrap').show('medium');
        } else {
            $button.find('p').text('Подробнее');
            $button.parents('.help-block').find('.show-hide-block-wrap').hide('medium');
        };


    });
    // show more news and help texts block script end
    

    game.init();
    smartOverlay.init('.registration-popup', '.gray-popup-bg');
    $.fn.scrollTo = function( target, options, callback ){
      if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
      var settings = $.extend({
        scrollTarget  : target,
        offsetTop     : 50,
        duration      : 500,
        easing        : 'swing'
      }, options);
      return this.each(function(){
        var scrollPane = $(this);
        var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
        var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
        scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
          if (typeof callback == 'function') { callback.call(this); }
        });
      });
    }
    
    
    $('.dropdown').smartDropdown({find: false});
});