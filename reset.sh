#!/bin/bash

if [ ! -d assets ]
then
    mkdir assets
fi

if [ ! -d protected/runtime ]
then
    mkdir protected/runtime
fi

if [ ! -f constants.php ]
then
    echo -en "<?php \n" > constants.php
    echo -en "/** ENVIRONMENT-SENSITIVE CONSTANTS. IN GIT-REPOSITORY THIS FILE SHOULD CONTAINS ONLY DEBUG AND TRACE-LEVEL CONSTANTS */\n" >> constants.php
    echo -en "defined('YII_DEBUG') or define('YII_DEBUG',false);\n" >> constants.php
    echo -en "defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',0);\n" >> constants.php
fi

if [ ! -f protected/config/main.local.php ]
then
    echo -en "<?php\n" > protected/config/main.local.php
    echo -en "/** ENVIRONMENT-SENSITIVE CONFIG. IN GIT-REPOSITORY THIS FILE SHOULD CONTAINS EMPTY ARRAY */\n" >> protected/config/main.local.php
    echo -en "return [];\n" >> protected/config/main.local.php
fi

if [ ! -f protected/config/console.local.php ]
then
    echo -en "<?php\n" > protected/config/console.local.php
    echo -en "/** ENVIRONMENT-SENSITIVE CONFIG. IN GIT-REPOSITORY THIS FILE SHOULD CONTAINS EMPTY ARRAY */\n" >> protected/config/console.local.php
    echo -en "return [];\n" >> protected/config/console.local.php
fi

chown -R apache:apache .

chmod -R 775 assets
chmod -R 775 protected/runtime

chmod -R 755 themes
chmod -R 755 constants.php
chmod -R 755 protected/config/*.local.php